import React from 'react';
import LoginScreen from './app/screens/login_screen';
import {createAppContainer, createStackNavigator} from 'react-navigation';
import RegisterScreen from "./app/screens/register_screen";
import {Provider} from "react-redux";

import {setJSExceptionHandler, getJSExceptionHandler, setNativeExceptionHandler} from 'react-native-exception-handler';


import MainScreen from "./app/screens/main_screen";
import FactScreen from "./app/screens/extended view screens/fact_screen";
import SpeakerScreen from "./app/screens/extended view screens/speaker_screen";
import PartnerScreen from "./app/screens/extended view screens/partner_screen";
import WebViewScreen from "./app/screens/extended view screens/webview_screen";
import CalendarItemScreen from "./app/screens/extended view screens/calendar_item_screen";
import CalendarAddItemScreen from "./app/screens/extended view screens/calendar_add_item_screen";
import CalendarEditItemScreen from "./app/screens/extended view screens/calendar_edit_item_screen";
import storeExport from "./app/store";
import MessageScreen from "./app/screens/extended view screens/messages_screen";
import RoomCreationScreen from "./app/screens/extended view screens/chat_room_create_screen";
import PersonalChatCreationScreen from "./app/screens/extended view screens/chat_personal_create_screen";
import AddUserScreen from "./app/screens/extended view screens/chat_add_user_screen";
import ChooseEventIdScreen from "./app/screens/extended view screens/choose_event_id_screen";

const Emitter = require("tiny-emitter");
const emitter = new Emitter();
import {initws} from "./app/methods/webSocket";
import {WS_URL} from "./app/constants/backend";
import {Root} from "native-base";
import {Platform, StatusBar, View} from "react-native";
import {InitNavigator} from "./app/routes/InitialRoute";
import { PersistGate } from 'redux-persist/integration/react';


/*const _XHR = GLOBAL.originalXMLHttpRequest ?
    GLOBAL.originalXMLHttpRequest :
    GLOBAL.XMLHttpRequest;

XMLHttpRequest = _XHR;*/

global.console.error = console.log;


/*const exceptionhandler = (error, isFatal) => {
    alert("Encountered a "+(isFatal ? "fatal " : "non-fatal ") + "error:\n"+error);
};
setJSExceptionHandler(exceptionhandler, true);*/

/*const MainNavigator = createStackNavigator(
    {
        Login:                          {screen: LoginScreen},
        Register:                       {screen: RegisterScreen},
        MainScreen:                     {screen: MainScreen},

        //fullscreen views
        FactScreen:                     {screen: FactScreen},
        SpeakerScreen:                  {screen: SpeakerScreen},
        PartnerScreen:                  {screen: PartnerScreen},
        WebViewScreen:                  {screen: WebViewScreen},
        CalendarItemScreen:             {screen: CalendarItemScreen},
        CalendarAddItemScreen:          {screen: CalendarAddItemScreen},
        CalendarEditItemScreen:         {screen: CalendarEditItemScreen},
        MessageScreen:                  {screen: MessageScreen},
        RoomCreationScreen:             {screen: RoomCreationScreen},
        PersonalChatCreationScreen:     {screen: PersonalChatCreationScreen},
        AddUserScreen:                  {screen: AddUserScreen},
        ChooseEventIdScreen:            {screen: ChooseEventIdScreen}
    },
    {
        initialRouteName: "Login",
        headerMode:"none"
    }
);*/
const {store, persistor} = storeExport();

const AppContainer = createAppContainer(InitNavigator);

export default class App extends React.Component {
    constructor (props) {
        super(props);

        initws(WS_URL);
        //initchat();
        //socket.onmessage = (msg) => {
        //    console.log(msg);
        //}
        //persistor.purge();
    }

    render() {
        return (
            <Root>
                <Provider store={store}>
                    <PersistGate loading={null} persistor={persistor}>
                        {/*<StatusBar hidden={true} />*/}
                        <AppContainer />
                    </PersistGate>
                </Provider>
            </Root>
        )
    }
}
