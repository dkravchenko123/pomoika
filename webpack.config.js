const createExpoWebpackConfigAsync = require('@expo/webpack-config');

module.exports = async function(env, argv) {
  try {
    const config = await createExpoWebpackConfigAsync(env, argv);
    config.resolve.alias['react-native-video'] = 'react-native-web-video';
    /*config.performance = {
        hints: false
    };*/
    return config;
  } catch (err) {
    console.log(err);
  }
  // Customize the config before returning it.
  //config.resolve.alias['react-native'] = 'react-native-web'
};

