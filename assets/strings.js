
export const ruStrings = {
    logincard_login_placeholder: 'Логин',
    logincard_password_placeholder: 'Пароль',
    logincard_login_button: 'Вход',
    logincard_registration_button: 'Регистрация',
    logincard_guest: 'Войти как гость',
    event_card_choose_button: 'Выбрать',
    event_card_empty_text: 'Мероприятие не выбрано!',
    calendar_add_button: 'Добавить событие',
    calendar_hide_button: 'Свернуть',
    calendar_open_button: 'Открыть',
    logincard_authorization: 'Авторизация',
    chat: 'Чат',
    event_participants: 'Участники',
    upcoming_sessions: 'Ближайшие сессии:',
    more_details: 'Подробнее...',
    header_program: 'Программа',
    header_speakers: 'Спикеры',
    header_partners: 'Партнеры',
    speaker_card_raiting: 'Рейтинг спикера:',
    speaker_card_session_count: 'Количество сессий:',
    menu_about_the_fund: 'О фонде',
    menu_change_event: 'Смена мероприятия',
    menu_programs: 'Программы',
    menu_program_lab: 'Программа Лаборатории Социальных Инвестиций',
    menu_program_case_zone: 'Программа Кейс-зоны',
    menu_business_program: 'Деловая Программа',
    menu_gala_reception: 'Торжественный Прием',
    menu_sport_program: 'Спортивная Программа',
    menu_cultural_program: 'Культурная Программа',
    menu_exhibition: 'Выставка',
    menu_exhibition_of_the_forum: 'Выставка Российского инвестиционного форума',
    menu_case_zone: 'Кейс-зона 2020',
    menu_map: 'Карта',
    menu_transport: 'Транспорт',
    menu_shuttles: 'Шаттлы',
    menu_order_taxi: 'Заказать такси',
    menu_food: 'Питание',
    menu_services: 'Сервисы',
    menu_excursions: 'Экскурсии',
    menu_car_rental: 'Аренда машины',
    menu_social_networks: 'Социальные сети',
    menu_facebook: 'facebook',
    menu_twitter: 'twitter',
    menu_instagram: 'instagram',
    menu_vk: 'vkontakte',
    menu_youtube: 'youtube',
    menu_partners: 'Партнеры',
    menu_brand_foto: 'Фирменное фото',
    menu_about_app: 'О приложении',
    menu_club_roscongress: 'Клуб Росконгресс',
    fact_card_location: 'Место проведения:',
    fact_card_event_raiting: 'Рейтинг события:'
};

export const enStrings = {
    logincard_login_placeholder: 'Login',
    logincard_password_placeholder: 'Password',
    logincard_login_button: 'Login',
    logincard_registration_button: 'Registration',
    logincard_guest: 'Login as guest',
    event_card_choose_button: 'Choose',
    event_card_empty_text: 'No event selected!',
    calendar_add_button: 'Add',
    calendar_hide_button: 'Collapse',
    calendar_open_button: 'Open',
    logincard_authorization: 'Authorization',
    chat: 'Chat',
    event_participants: 'Participants',
    upcoming_sessions: 'Upcoming sessions:',
    more_details: 'More details...',
    header_program: 'Program',
    header_speakers: 'Speakers',
    header_partners: 'Partners',
    speaker_card_raiting: 'Speaker Rating:',
    speaker_card_session_count: 'Number of sessions:',
    menu_about_the_fund: 'About the Fund',
    menu_change_event: 'Change event',
    menu_programs: 'Programms',
    menu_program_lab: 'Social Investment Lab Program',
    menu_program_case_zone: 'Case-zone Program',
    menu_business_program: 'Business Program',
    menu_gala_reception: 'Gala Reception',
    menu_sport_program: 'Sport Program',
    menu_cultural_program: 'Cultural Program',
    menu_exhibition: 'Exhibition',
    menu_exhibition_of_the_forum: 'Exhibition of the Russian Investment Forum',
    menu_case_zone: 'Case-zone 2020',
    menu_map: 'Map',
    menu_transport: 'Transport',
    menu_shuttles: 'Shuttles',
    menu_order_taxi: 'Order a taxi',
    menu_food: 'Food',
    menu_services: "Services",
    menu_excursions: 'Excursions',
    menu_car_rental: 'Car rental',
    menu_social_networks: 'Social networks',
    menu_facebook: 'facebook',
    menu_twitter: 'twitter',
    menu_instagram: 'instagram',
    menu_vk: 'vkontakte',
    menu_youtube: 'youtube',
    menu_partners: 'Partners',
    menu_brand_foto: 'Brand photo',
    menu_about_app: 'About app',
    menu_club_roscongress: 'Roscongress club',
    fact_card_location: 'Location:',
    fact_card_event_raiting: 'Event raiting:'
};

let strings = {
    logincard_disclaimer: {
        ru: "Ваши логин и пароль были отправлены Вам вместе с приглашением на Форум",
        en: "Your login and password were included in your invitation to the Forum"
    },
    logincard_login_placeholder: {
        ru: "Логин",
        en: "Login"
    },
    logincard_password_placeholder: {
        ru: "Пароль",
        en: "Password"
    },
    clear_news_tag_filter: {
        ru: "Сбросить фильтр",
        en: "Clear flter"
    },
    calendar_create_card_label: {
        ru: "Создать событие",
        en: "Create item"
    },
    calendar_edit_card_label: {
        ru: "Изменить событие",
        en: "Edit item"
    },
    calendar_create_button_label: {
        ru: "Создать",
        en: "Create"
    },
    calendar_edit_button_label: {
        ru: "Сохранить",
        en: "Save"
    },
    calendar_choose_time: {
        ru: "Выберите дату и время",
        en: "Choose time and date"
    },
    calendar_choose_place: {
        ru: "Выберите место",
        en: "Choose location"
    },
    calendar_time_button_label: {
        ru: "Выбрать",
        en: "Set"
    },
    calendar_place_button_label: {
        ru: "Выбрать",
        en: "Set"
    },
    calendar_item_name_placeholder: {
        ru: "Название события",
        en: "Item name"
    },
    calendar_item_theme_placeholder: {
        ru: "Тема события",
        en: "Item theme"
    },
    calendar_item_description_placeholder: {
        ru: "Описание события",
        en: "Item description"
    },
    calendar_item_place_placeholder: {
        ru: "Выбор места",
        en: "Choose location"
    },
    calendar_item_place_name_placeholder: {
        ru: "Название места",
        en: "Choose location name"
    },
    calendar_choose_item_type_label: {
        ru: "Выберите тип события",
        en: "Choose item type"
    },
    calendar_place_on_site: {
        ru: "На площадке",
        en: "On site"
    },
    calendar_place_off_site: {
        ru: "Вне площадки",
        en: "Off site"
    },
    calendar_search_label: {
        ru: "Найти участников",
        en: "Find people"
    },
    calendar_guest_button_label: {
        ru: "Закрыть",
        en: "Close"
    },
    fact_rating_button_label: {
        ru: "Оценить",
        en: "Review"
    },
    fact_question_input_placeholder: {
        ru: "Введите вопрос",
        en: "Write your question"
    },
    fact_question_submit_button: {
        ru: "Отправить",
        en: "Submit"
    },
    cancel:{
        ru: "Отмена",
        en: "Cancel"
    },
    attention:{
        ru:"Внимание!",
        en:"Attention!"
    },
    ok:{
        ru:"ОК",
        en:"OK"
    },
    fact_survey_label: {
        ru: "Голосование",
        en: "Survey"
    },
    fact_answer_input_placeholder: {
        ru: "Ваш ответ",
        en: "Your answer"
    },
    fact_survey_submit_button: {
        ru: "Ответить",
        en: "Submit"
    },
    fact_survey_get_results: {
        ru: "Результаты",
        en: "Results"
    },
    fact_survey_not_started: {
        ru: "Голосование еще не началось",
        en: "Survey has not started yet"
    },
    fact_voiting_dates: {
        ru: "Даты проведения голосования",
        en: "Voting dates"
    },
    user_filter_label: {
        ru: "Фильтровать пользователей",
        en: "Filter users"
    },
    user_filter_project: {
        ru: "По проектам",
        en: "By project"
    },
    user_filter_industry: {
        ru: "По отрасли",
        en: "By industry"
    },
    user_filter_company: {
        ru: "По организации",
        en: "By company"
    },
    user_filter_user_type: {
        ru: "По типу пользователя",
        en: "By user type"
    },
    chat_invite_active_label: {
        ru: "Приглашения",
        en: "Invites"
    },
    chat_block: {
        ru: "Заблокировать",
        en: "Block"
    },
    chat_complain: {
        ru: "Жалоба",
        en: "Report"
    },
    chat_invited: {
        ru: "[Приглашение]",
        en: "[Invite]"
    },
    chat_theme_input:{
        ru:"Введите тему",
        en:"Choose a theme"
    },
    chat_rename_group_placeholder:{
        ru:"Название группы",
        en:"Group name"
    },
    chat_edit_theme:{
        ru:"Изменить тему",
        en:"Edit theme"
    },
    confirm_user_ban:{
        ru:"Вы уверены, что хотите заблокировать данного пользователя?",
        en:"Are you sure you want to block this user?"
    },
    notifications_none: {
        ru: "Нет последних уведомлений",
        en: "No recent notifications"
    },
    alert_success:{
        ru:"Успех",
        en:"Success"
    },
    alert_alert:{
        ru:"Внимание",
        en:"Attention"
    },
    error:{
        ru:"Произошла ошибка",
        en:"Encountered an error"
    },
    fact_successfully_added_to_calendar:{
        ru:"Событие добавлено в календарь",
        en:"Event added to calendar"
    },
};

Object.keys(ruStrings).forEach((str_key) => {
    if (strings.hasOwnProperty(str_key)) {
        strings[str_key].ru = ruStrings[str_key];
    } else {
        strings[str_key] = {
            ru: ruStrings[str_key]
        };
        //console.log("new string", strings[str_key]);
    }
});

Object.keys(enStrings).forEach((str_key) => {
    if (strings.hasOwnProperty(str_key)) {
        strings[str_key].en = enStrings[str_key];
    } else {
        strings[str_key] = {
            en: enStrings[str_key]
        };
        //console.log("new string", strings[str_key]);
    }
});

//console.log(strings);

export default strings;
