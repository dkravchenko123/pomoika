import React from "react";
import {Container, ScrollableTab, Tabs, Tab, TabHeading} from "native-base";
import {Text, ScrollView, View, TouchableOpacity, Dimensions, PixelRatio} from "react-native";
import AboutRKTab from "../tabs/about_rk_tab";
import NewsArticlesTab from "../tabs/news_articles_tab";
import MainHeader from "../headers_footers/main_header.js"
import { LinearGradient } from 'expo-linear-gradient';


//styles
import tab from "../../styles/tabs";
import button from "../../styles/buttons";
import DrawerContent from "../cards/DraweContent";
import Drawer from "react-native-drawer";
import ChooseEventIdScreen from "../../screens/extended view screens/choose_event_id_screen";
import {ButtonsOverGradient} from "../headers_footers/buttons_over_gradient";
import {Notifications} from "expo";
import TagFilter from "../headers_footers/tag_filter";

const window = Dimensions.get("window");

const BUTTONS_RU = [
    "Мероприятия",
    "Новости",
    "Аналитика",
];

const BUTTONS_EN = [
    "Events",
    "News",
    "Analytics",
];

export class HomePage extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            active_tab:0
        };

        this.drawer = new React.createRef();
        this.menu_fun = this.menu_fun.bind(this);
        this.close_drawer = this.close_drawer.bind(this);
        this.change_active = this.change_active.bind(this);
    }

    menu_fun () {
        this.drawer.open();
    }

    close_drawer () {
        this.drawer.close();
    }

    change_active (num) {
        this.setState({active_tab:num})
    }

    _handleNotifications = async (notification) => {
        //alert(JSON.stringify(notification));
        //console.log("notification", notification);
        let notif_data = notification.notification["_data"];
        this.props.navigation.navigate(
            "MessageScreen",
            {
                room_id:notif_data.room_matrix_id,
                full_name:notif_data.room_name,
                avatar:notif_data.room_avatar,
                is_not_group:notif_data.room_type != "group",
                addressee_matrix_id:notif_data.sender_matrix_id
            }
        );
    };

    componentDidMount() {


        this.didBlurSubscription = this.props.navigation.addListener(
            'willBlur',
            payload => {
                console.log("blurring, this.drawer=",this.drawer);
                if (this.drawer) this.close_drawer();
                //console.log('willBlur', payload);
            }
        );
    }

    componentWillUnmount() {
        if (this.didBlurSubscription) this.didBlurSubscription.remove();
    }

    render () {
        console.log("getfontScale "+PixelRatio.getFontScale());
        return (
            <Drawer
                content={<DrawerContent navigation={this.props.navigation} open_facts={this.open_facts} close_drawer={this.close_drawer}/>}
                ref={(r) => this.drawer = r}
                openDrawerOffset={0.0}
                side={"right"}
                acceptPan
                negotiatePan
            >
                <View zIndex={2} style={{flex:1/*height:"100%"*/}}>
                    <MainHeader menu_fun={this.menu_fun} navigation={this.props.navigation}/>

                    <View zIndex={5}>
                        <ButtonsOverGradient zIndex={5} init_active={0} active_call={this.change_active} buttons={BUTTONS_RU}/>
                    </View>

                    {this.state.active_tab == 0 &&
                        <View style={{flex:1, marginTop:-50}} zIndex={3}>
                            <ChooseEventIdScreen navigation={this.props.navigation}/>
                        </View>
                    }

                    {this.state.active_tab == 1 &&
                        <View style={{flex:1, marginTop:-50}} zIndex={3}>
                            <TagFilter zIndex={4} type={"4"}/>
                            <NewsArticlesTab zIndex={3} type={1} navigation={this.props.navigation}/>
                        </View>
                    }

                    {this.state.active_tab == 2 &&
                        <View style={{flex:1, marginTop:-50}} zIndex={3}>
                            <TagFilter zIndex={4} type={"5"}/>
                            <NewsArticlesTab zIndex={3} type={3} navigation={this.props.navigation}/>
                        </View>
                    }
                </View>
            </Drawer>
        );
    }
}
