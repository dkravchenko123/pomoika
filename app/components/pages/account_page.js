import React from "react";
import {Container, Fab, ScrollableTab, Tabs, Tab, TabHeading} from "native-base";
import {ScrollView, Text, TouchableOpacity, View} from "react-native";
import {BusinessCard} from "../cards/BusinessCard";
import {AccreditationTab} from "../tabs/accreditation_tab";
import MyEventsTab from "../tabs/my_events_tab";
import PersonalCard from "../cards/PersonalCard";
import {UpdateButton} from "../update_button";
import {Ionicons, MaterialIcons} from "@expo/vector-icons";
import {receiveData} from "../../actions/data";
import {togglePersonalInfoOverlay} from "../../actions/control";
import {connect} from "react-redux";
import UpdatePersonalInfoCard from "../cards/UpdatePersonalInfoCard";
import DrawerContent from "../cards/DraweContent";
import MainHeader from "../headers_footers/main_header";
import Drawer from "react-native-drawer";
import SubscreenHeader from "../headers_footers/subscreen_header";


export class AccountPage extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            fabActive:false
        };
    }

    render () {
        return (
            <View style={{height:"100%"}}>
                <SubscreenHeader custom_back={this.props.toggle}/>
                <PersonalCard navigation={this.props.navigation} />
            </View>
        );
    }
}

const mapStateToProps = state => {
    return {
        //received_data: state.data.received_data,
        pi_overlay: state.control.pi_overlay
    }
};

const mapDispatchToProps = dispatch => {
    return {
        //receiveData: (data) => dispatch(receiveData(data)),
        //toggle: () => dispatch(togglePersonalInfoOverlay())
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(AccountPage);
