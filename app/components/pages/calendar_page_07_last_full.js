import React from "react";
import {Button, Container, Content, Fab, Grid, Row, ScrollableTab, Segment, Tabs, Tab, TabHeading} from "native-base";
import {ActivityIndicator, Dimensions, FlatList, Image, Platform, RefreshControl, ScrollView, StyleSheet, Text, TouchableOpacity, View, TextInput} from "react-native";
import {BusinessCard} from "../cards/BusinessCard";
import {Ionicons, MaterialIcons} from "@expo/vector-icons";
import moment from 'moment';
import {
    receiveData, removeData, setCalCurDate, setCalendarNeedsUpdate, updCalendar
} from "../../actions/data";
import {cancel_reconnect, getws, init_timeout} from "../../methods/webSocket";
import {connect} from "react-redux";
import {FactCard} from "../cards/FactCard";
import FactsTab from "../tabs/facts_tab";
import {
    backendRequest, backendRequestCustomSocket, backendRequestPromise,
    extractResponse
} from "../../methods/ws_requests";
import {
    Agenda,
    Calendar,
    CalendarList,
    CalendarProvider,
    ExpandableCalendar,
    LocaleConfig
} from "react-native-calendars";
import {findOrNext, isLater, parseCalItems, sameDate} from "../../methods/calendar_methods";
import {CalendarDayCard} from "../cards/CalendarDayCard";
import {disableNetWarn, enableNetWarn} from "../../actions/control";
import Drawer from "react-native-drawer";
import DrawerContent from "../cards/DraweContent";
import MainHeader from "../headers_footers/main_header";
import {WS_URL} from "../../constants/backend";
import button from "../../styles/buttons";
import {localeStr} from "../../methods/translation_provider";

LocaleConfig.locales['ru'] = {
    monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
    monthNamesShort: ['Янв.','Фев.','Март','Апр.','Май','Июнь','Июль','Авг.','Сент.','Окт.','Нояб.','Дек.'],
    dayNames: ['Воскресенье','Понедельник','Вторник','Среда','Четверг','Пятница','Суббота'],
    dayNamesShort: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
    today: 'Сегодня'
};
LocaleConfig.locales['en'] = {
    monthNames: ['January','February','March','April','May','June','July','August','September','October','November','December'],
    monthNamesShort: ['Jan.','Feb.','Mar','Apr.','May','June','July','Aug.','Sep.','Oct.','Nov.','Dec.'],
    dayNames: ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'],
    dayNamesShort: ['Sun','Mon','Tue','Wed','Thur','Fri','Sat'],
    today: 'Today'
};


const window = Dimensions.get('window');


class CalendarPage extends React.Component {
    constructor (props) {
        super (props);
        let date = new Date().toISOString().split("T")[0];
        this.state={
            selected_day:date,
            year_picker_open:false,
            month_picker_open:false,
            year_button_location:{},
            cal_location:{},
            active_mode:"year",
        };

        this.selectedDayLocation = {
            from_top: 0,
            from_bottom: 0
        };

        this.possible_dates=([...Array(8)]).map((el, index) => {return(index+parseInt(date.split("-")[0])-4)});
        console.log(this.possible_dates);

        this._onRefresh = this._onRefresh.bind(this);

        this.drawer = new React.createRef();
        this.year_button = new React.createRef();
        this.flatlist_ref = new React.createRef();
        this.agenda_ref = new React.createRef();
        this.year_scroll = new React.createRef();
        this.cal_offset = {};

        this.monthChangeTimeout = null;

        this.menu_fun = this.menu_fun.bind(this);
        this.close_drawer = this.close_drawer.bind(this);
        this.switch_to = this.switch_to.bind(this);
        this._onRefresh = this._onRefresh.bind(this);
    }

    _onRefresh () {
        backendRequestPromise(
            "calendarGetItems",
            this.props.userToken
        ).then((response) => {
            this.props.setCalendarNeedsUpdate(false);
            let parsed_cal = parseCalItems(response);
            let sorted_keys = Object.keys(parsed_cal).sort((d1, d2) => {return(isLater(d1,d2) ? -1 : 1)});
            this.props.updCalendar(parsed_cal, sorted_keys);

            backendRequestPromise(
                "calendarGetUserInvites",
                this.props.userToken
            ).then((resp) => {
                this.props.setCalendarNeedsUpdate(false);
                let response = resp.map((el) => {return {...el, invite:true, item_id:(el.item_id || Math.floor(Math.random()*1000))};});
                let parsed_cal = {...parseCalItems(response), ...this.props.calendar.items};
                let sorted_keys = Object.keys(parsed_cal).sort((d1, d2) => {return(isLater(d1,d2) ? -1 : 1)});
                this.props.updCalendar(parsed_cal, sorted_keys);
            });
        });

        /*let calendar_socket = new WebSocket(WS_URL);


        let invite_socket = new WebSocket(WS_URL);
        invite_socket.onmessage = (msg) => {
            //this.props.setCalendarNeedsUpdate(false);
            let parsed_msg = JSON.parse(msg.data);
            console.log("received calendar invites", parsed_msg);
            if (parsed_msg.data != null) {
                let response = parsed_msg.data.map((el) => {return {...el, invite:true, item_id:(el.item_id || Math.floor(Math.random()*1000))};});
                let parsed_cal = {...parseCalItems(response), ...this.props.calendar.items};
                let sorted_keys = Object.keys(parsed_cal).sort((d1, d2) => {return(isLater(d1,d2) ? -1 : 1)});
                this.props.updCalendar(parsed_cal, sorted_keys);
            }
            invite_socket.close();
        };


        calendar_socket.onmessage = (msg) => {
            this.props.setCalendarNeedsUpdate(false);
            let parsed_msg = JSON.parse(msg.data);
            console.log("received calendar", parsed_msg);
            if (parsed_msg.data != null) {
                let response = parsed_msg.data;
                let parsed_cal = parseCalItems(response);
                let sorted_keys = Object.keys(parsed_cal).sort((d1, d2) => {return(isLater(d1,d2) ? -1 : 1)});
                this.props.updCalendar(parsed_cal, sorted_keys);
            }
            calendar_socket.close();
            if (invite_socket.readyState == 1) {
                backendRequestCustomSocket(invite_socket, "calendarGetUserInvites", this.props.userToken);
            }
        };
        calendar_socket.onopen = (()=>{
            backendRequestCustomSocket(calendar_socket, "calendarGetItems", this.props.userToken);
        });*/
    }

    componentDidMount() {
        this.year_button.measure((fx, fy, width, height, px, py) => {
            this.setState({year_button_location:{fx, fy, width, height, px, py}});
        });
        let today = new Date();
        this.props.setCalCurDate(today.getDate().toString().split("T")[0]);

        if (this.props.needs_update) {this._onRefresh()}

        this.didBlurSubscription = this.props.navigation.addListener(
            'willBlur',
            payload => {
                if (this.close_drawer) this.close_drawer();
                //console.log('willBlur', payload);
            }
        );
    }

    componentWillUnmount() {
        if (this.didBlurSubscription) this.didBlurSubscription.remove();
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        console.log("guest status is "+this.props.guestStatus);
        if (this.year_picker_open) this.year_scroll.scrollTo(this.state.selected_day.split("-")[0]*30);
        if (this.props.needs_update) {this._onRefresh()}
    }

    menu_fun () {
        this.drawer.open();
    }

    close_drawer () {
        this.drawer.close();
    }

    switch_to(date) {
        console.log("switching to "+date);
        //this.agenda_ref.chooseDay(date);
        // if (this.props.calendar.sorted_keys && this.props.calendar.sorted_keys.length != 0 && !this.props.needs_update) this.flatlist_ref.scrollToIndex({index:findOrNext(this.props.calendar.sorted_keys, date)});
        this.setState({selected_day:date});
    }

    setMode = (mode) => {
        console.log("Mode to " + mode);
        console.log("Mode <== " + this.state.active_mode);
        this.setState({active_mode:mode});
        console.log("Mode is " + this.state.active_mode);
    };

    render () {
        LocaleConfig.defaultLocale = this.props.lang;
        let markedDates = {};
        for (let date in this.props.calendar.items) {
            markedDates = {
                ...markedDates,
                [date]: {marked: true, startingDay: true, endingDay: true, color: 'blue'}
            };
        }
        if (markedDates.hasOwnProperty(this.state.selected_day)) {
            markedDates[this.state.selected_day] = {
                ...markedDates[this.state.selected_day],
                selected: true,
                textColor: '#000'
            };
        } else {
            markedDates[this.state.selected_day] = {selected: true, textColor: '#000'};
        }

        console.log([window.height, window.width]);

        return (
            <Drawer
                key={LocaleConfig.defaultLocale}
                content={<DrawerContent navigation={this.props.navigation} close_drawer={this.close_drawer}/>}
                ref={(r) => this.drawer = r}
                openDrawerOffset={0.0}
                side={"right"}
                acceptPan
                negotiatePan
            >
                <View style={{flex:1/*height:"100%"*/}}>
                    {/* <MainHeader menu_fun={this.menu_fun} navigation={this.props.navigation}/> */}

                    {this.state.year_picker_open &&
                        <View
                            style={{
                                position:"absolute",
                                top:this.state.year_button_location.py+this.state.year_button_location.height,
                                left:this.state.year_button_location.px+5,
                                width:100,
                                height:160,
                                backgroundColor:"white",
                                borderLeftWidth:2,
                                borderRightWidth:2,
                                borderBottomWidth:2,
                                borderColor:"rgb(220,219,216)",
                                zIndex:100
                            }}
                        >
                            <ScrollView ref={(r) => {this.year_scroll=r}}>
                                {this.possible_dates.map((date) => {
                                    let date_arr = this.state.selected_day.split("-");
                                    return (
                                        <TouchableOpacity
                                            style={{width:69, height:30, justifyContent:"center", alignItems:"center"}}
                                            onPress={() => {
                                                let d_arr = this.state.selected_day.split("-");
                                                this.switch_to([date, d_arr[1], d_arr[2]].join("-"));
                                                this.setState({
                                                    year_picker_open:false
                                                });
                                            }}
                                        >
                                            <Text style={{fontSize:18, fontWeight:(date_arr[0]==date ? "bold" : "normal")}}>{date}</Text>
                                        </TouchableOpacity>
                                    )
                                })}
                            </ScrollView>
                        </View>
                    }

                    {/*<View style={{ flex: 1, width: window.width, minHeight: 70, borderBottomWidth:2, borderBottomColor:"rgb(220,219,216)", alignItems:"center", justifyContent:"center", flexDirection:"row" }}>
                        <TextInput
                            style={{ flex: 1,borderRadius: 10, height: 50, backgroundColor: 'rgba(220,219,216, 0.2)', paddingHorizontal: 20, width: '90%', marginVertical: 10, marginHorizontal: 20 }}
                            placeholder={this.props.lang === 'ru' ? 'Поиск + фильтр' : 'Search + filter'}
                            placeholderTextColor={'gray'}
                            value={''}
                        />
                        <Ionicons name='ios-search' size={30} color={'gray'} style={{ position: 'absolute', right: 40 }} />
                    </View>*/}

                    <View style={{alignItems:"center", flexDirection:"row", margin:10, justifyContent: "space-between" }}>
                      <View style={{alignItems:"center", flexDirection:"row", margin:0 }}>
                          <Text>Показать: </Text>

                          <TouchableOpacity
                            onPress={() => { this.setMode("year"); }}
                          >
                            <Text style={[{ color : this.state.active_mode === "year" ? 'red' : 'LightGrey', textDecorationLine: 'underline' }]}>год</Text>
                          </TouchableOpacity>

                          <Text> | </Text>
                          <TouchableOpacity
                            onPress={() => { this.setMode("month"); }}
                          >
                            <Text style={[{ color : this.state.active_mode === "month" ? 'red' : 'LightGrey', textDecorationLine: 'underline' }]}>месяц</Text>
                          </TouchableOpacity>

                          <Text> | </Text>
                          <TouchableOpacity
                            onPress={() => { this.setMode("week"); }}
                          >
                            <Text style={[{ color : this.state.active_mode === "week" ? 'red' : 'LightGrey', textDecorationLine: 'underline' }]}>неделя</Text>
                          </TouchableOpacity>

                          <Text> | </Text>

                          <TouchableOpacity
                            onPress={() => { this.setMode("day"); }}
                          >
                            <Text style={[{ color : this.state.active_mode === "day" ? 'red' : 'LightGrey', textDecorationLine: 'underline' }]}>день</Text>
                          </TouchableOpacity>
                      </View>
                      <TouchableOpacity
                          style={[button.header, button.active, {marginTop:0, marginLeft:20, width:220, alignSelf: "Right"}]}
                          onPress={() => {this.props.navigation.navigate("CalendarEditItemScreen", {is_new:true})}}
                      >
                          <Text style={{color:"white"}}>
                              {localeStr('calendar_add_button', this.props.lang)}
                          </Text>
                      </TouchableOpacity>
                    </View>

                    <View style={{ height:"85%", flexDirection:"row", margin:5, backgroundColor:"rgb(111,222,240)" }}>
                        <View style={{ flex: 1, flexDirection:"row", margin:0 }}>
                            <View style={{ width:"97%", flexDirection:"column", margin:5, backgroundColor:"rgb(240,240,240)" }}>
                                <View style={{ paddingHorizontal: 20, height:50, alignSelf: "center", alignItems:"center", justifyContent:"space-between", flexDirection:"row", margin:0 }}>
                                    <View style={{ paddingHorizontal: 20, borderBottomColor:"rgb(220,219,216)", height:50, alignItems:"center", justifyContent:"space-between", flexDirection:"row"}}>
                                        <TouchableOpacity
                                            ref={(r) => {this.year_button = r}}
                                            style={{height:50, width:80, alignItems:"center", justifyContent:"center"}}
                                            onLayout={() => {
                                                this.year_button.measure((fx, fy, width, height, px, py) => {
                                                    this.setState({year_button_location:{fx, fy, width, height, px, py}});
                                                });
                                            }}
                                            onPress={() => {
                                                this.setState({year_picker_open:!this.state.year_picker_open});
                                            }}
                                        >
                                            <Text style={{fontSize:16}}>{this.state.selected_day.split("-")[0]}</Text>
                                        </TouchableOpacity>

                                        <View style={{height:50, alignItems:"center", justifyContent:"space-between", flexDirection:"row"}}>
                                            <TouchableOpacity
                                                style={{marginRight: 6}}
                                                onPress={() => {
                                                    let d_arr = this.state.selected_day.split("-");
                                                    if (d_arr[1] > 1) {
                                                        this.switch_to([d_arr[0], "0".repeat(2-(parseInt(d_arr[1])-1).toString().length)+(parseInt(d_arr[1])-1).toString(), d_arr[2]].join("-"));
                                                    } else {
                                                        this.switch_to([parseInt(d_arr[0])-1, "12", d_arr[2]].join("-"));
                                                    }
                                                }}
                                            >
                                                <Ionicons
                                                    size={24}
                                                    name={"ios-arrow-back"}
                                                />
                                            </TouchableOpacity>
                                            <View style={{minWidth:100, flexDirection:"row", justifyContent:"center"}}>
                                                <Text
                                                    style={{fontSize:16}}
                                                >
                                                    {LocaleConfig.locales[this.props.lang]["monthNames"][this.state.selected_day.split("-")[1]-1]}
                                                </Text>
                                            </View>
                                            <TouchableOpacity
                                                style={{marginLeft: 6}}
                                                onPress={() => {
                                                    let d_arr = this.state.selected_day.split("-");
                                                    if (d_arr[1] < 12) {
                                                        this.switch_to([d_arr[0], parseInt(d_arr[1])+1, d_arr[2]].join("-"));
                                                    } else {
                                                        this.switch_to([parseInt(d_arr[0])+1, "01", d_arr[2]].join("-"));
                                                    }
                                                }}
                                            >
                                                <Ionicons
                                                    size={24}
                                                    name={"ios-arrow-forward"}
                                                />
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                </View>
                                <ScrollView
                                    scrollEnabled={!false}
                                >
                                    <Container
                                        scrollEnabled={false}
                                        style={{backgroundColor:"rgb(240,240,240)"}}
                                    >

                                        <View
                                            ref={(r) => this.cal_ref = r}
                                            style={{
                                                backgroundColor:"#ffffff00",
                                            }}
                                            zIndex={5}
                                        >
                                            <View
                                                style={{
                                                    backgroundColor:"white",
                                                    //elevation:5,
                                                }}
                                            >
                                                <CalendarList
                                                    zIndex={6}
                                                    style={{
                                                        marginTop:(0),
                                                        marginBottom :(0),
                                                    }}
                                                    headerStyle={{
                                                        marginTop:(0)
                                                    }}

                                                    //theme={{textMonthFontSize: 0}}
                                                    theme={{textDayFontSize: 25, textMonthFontSize: 25, textDayHeaderFontSize: 20}}
                                                    hideArrows={true}
                                                    //current={this.state.selected_day}

                                                    // Initially visible month. Default = Date()
                                                    //current={'2020-01-25'}
                                                    // Minimum date that can be selected, dates before minDate will be grayed out. Default = undefined
                                                    //minDate={'2020-01-01'}
                                                    // Maximum date that can be selected, dates after maxDate will be grayed out. Default = undefined
                                                    //maxDate={'2020-03-31'}

                                                    hideExtraDays={true}
                                                    pagingEnabled={true}

                                                    firstDay={1}

                                                    //horizontal={true}
                                                    hideDayNames={false}
                                                    showWeekNumbers={false}
                                                    
                                                    calendarWidth={'100'}
                                                    pastScrollRange={5}
                                                    futureScrollRange={5}

                                                    //
                                                    //calendarWidth={150}
                                                    //calendarHeight={100}
                                                    //
                                                    scrollEnabled={true}
                                                    showScrollIndicator={false}

                                                    onVisibleMonthsChange={(mon) => {
                                                        if (this.monthChangeTimeout) {
                                                            clearTimeout(this.monthChangeTimeout);
                                                        }
                                                        console.log("visible months changed to ", mon);
                                                        if (mon.length == 1) {
                                                            let new_selected = [...mon[0].dateString.split("-").slice(0,2), this.state.selected_day.split("-")[2]].join("-");
                                                            //if (this.props.calendar.sorted_keys && this.props.calendar.sorted_keys.length != 0 && !this.props.needs_update) this.flatlist_ref.scrollToIndex({index:findOrNext(this.props.calendar.sorted_keys, new_selected)});
                                                            this.monthChangeTimeout = setTimeout(() => {
                                                                this.setState({
                                                                    selected_day:new_selected,
                                                                })
                                                            }, 5);
                                                        }
                                                    }}

                                                    ref={(ref) => {this.agenda_ref=ref}}
                                                    onDayPress={(day) => {
                                                        console.log('selected day', day["dateString"]);
                                                        //this.props.setCalCurDate(day["dateString"]);
                                                        //if (this.props.calendar.sorted_keys && this.props.calendar.sorted_keys.length != 0 && !this.props.needs_update) this.flatlist_ref.scrollToIndex({index:findOrNext(this.props.calendar.sorted_keys, day["dateString"])});
                                                        this.setState({
                                                            selected_day:day["dateString"]
                                                        })
                                                    }}
                                                    selected={this.props.calendar.cur_day}
//                                                    pastScrollRange={48}
//                                                    futureScrollRange={48}
                                                    dayComponent={({date, state, marking}) => {
                                                        return <TouchableOpacity
                                                            ref={(r) => {if (marking["selected"]) this.selectedDay = r}}
                                                            onLayout={() => {
                                                                if (marking["selected"]) {
                                                                    setTimeout(() => {
                                                                        if (this.selectedDay) {
                                                                            if (this.cal_ref) {
                                                                                this.selectedDay.measure((dx, dy, dw, dh, dpx, dpy) => {
                                                                                    //console.log({x, y, w, h, px, py});
                                                                                    this.cal_ref.measure((cx, cy, cw, ch, cpx, cpy) => {
                                                                                        //console.log({x, y, w, h, px, py});
                                                                                        this.selectedDayLocation =
                                                                                            {
                                                                                                from_top: dpy - cpy,
                                                                                                from_bottom: 0
                                                                                            };
                                                                                        console.log({selectedDayLocation: {from_top: dpy - cpy, from_bottom: (cpy + ch) - (dpy + dh)}});
                                                                                    })
                                                                                })
                                                                            }
                                                                        }
                                                                    }, 50);
                                                                }
                                                            }}
                                                            onPress={() => {
                                                                //this.agenda_ref.chooseDay(date.dateString)
                                                                //if (this.props.calendar.sorted_keys && this.props.calendar.sorted_keys.length != 0 && !this.props.needs_update) this.flatlist_ref.scrollToIndex({index:findOrNext(this.props.calendar.sorted_keys, date.dateString)});
                                                                this.setState({selected_day:date.dateString})
                                                            }}
                                                        >
                                                            <View
                                                                style={[
                                                                    {height:20, width:20, borderRadius:10, justifyContent: "center", alignItems:"center"},
                                                                    marking["marked"] && {borderWidth:2, borderColor:"#A50E2D"},
                                                                    marking["selected"] && {borderWidth:2, borderColor:"#A50E2D", backgroundColor:"#A50E2D"}
                                                                ]}
                                                            >
                                                                <Text style={{fontSize:26}}>{date.day}</Text>
                                                            </View>
                                                        </TouchableOpacity>;
                                                    }}
                                                    renderItem={() => {return <View />;}}
                                                    renderEmptyDay={() => {return <View />;}}
                                                    //renderEmptyData={() => {return <View />;}}
                                                    renderKnob={() => {return <View style={{marginTop: 6, alignSelf:"center", width:75, height:6, borderRadius:3, backgroundColor:"#A50E2D"}} />;}}
                                                    // specify your item comparison function for increased performance
                                                    rowHasChanged={(r1, r2) => {return r1.text !== r2.text}}
                                                    hideKnob={false}
                                                    markedDates={markedDates}
                                                    displayLoadingIndicator={false}
                                                />
                                            </View>
                                        </View>
                                    </Container>
                                </ScrollView>
                            </View>
                        </View>

                        <View style={{ width:"60%", flexDirection:"column", margin:5, backgroundColor:"rgb(240,240,240)" }}>
                            {!this.props.needs_update && Object.keys(this.props.calendar.items).length > 0
                                ?
                                <View style={{flex:1, marginTop:0}} zIndex={6}>
                                    {Object.keys(this.props.calendar.items).length > 0 &&
                                        <Container scrollEnabled={true} style={{backgroundColor:"rgb(240,240,240)"}}>
                                            <FlatList
                                                ref={(ref) => {this.flatlist_ref=ref}}
                                                contentContainerStyle={{paddingTop:10, paddingBottom:60}}
                                                ListFooterComponent={<View style={{marginTop:5, height: 160, marginBottom: 90, backgroundColor:"rgb(240,240,240)" }} />}

                                                refreshControl={
                                                    <RefreshControl
                                                        refreshing={this.props.needs_update}
                                                        onRefresh={this._onRefresh}
                                                    />
                                                }
                                                keyExtractor={(el) => {return el.toString()}}
                                                data={this.props.calendar.sorted_keys.filter((elem) => moment(elem).isAfter(this.state.selected_day) || moment(elem).isSame(this.state.selected_day))}//.slice(cur_index, this.props.calendar.sorted_keys.length)}
                                                renderItem={(el) => {
                                                    let el_key = el.item;
                                                    //console.log("calendar date: " + el_key);
                                                    return (
                                                        <CalendarDayCard
                                                            key={el_key}
                                                            items={this.props.calendar.items[el_key]}
                                                            date={el_key}
                                                            navigation={this.props.navigation}
                                                            language={this.props.lang}
                                                            token={this.props.userToken}
                                                            update={this.props.setCalendarNeedsUpdate}
                                                        />
                                                    );
                                                }}
                                            />
                                        </Container>
                                    }
                                </View>
                                :
                                <View style={{flex:1, marginTop:-20}} zIndex={3}>
                                    <ScrollView
                                        style={{flex:1}}
                                        refreshControl={
                                            <RefreshControl
                                                refreshing={this.props.needs_update}
                                                onRefresh={this._onRefresh}
                                            />
                                        }
                                    />
                                </View>
                            }
                        </View>
                    </View>
                </View>
            </Drawer>
        );
    }

    renderItem(item) {
        return (
            <View style={[styles.item, {height: item.height}]}><Text>{item.name}</Text></View>
        );
    }

    renderEmptyDate() {
        return (
            <View style={styles.emptyDate}><Text>This is empty date!</Text></View>
        );
    }
}

const styles = StyleSheet.create({
    item: {
        backgroundColor: 'white',
        flex: 1,
        borderRadius: 5,
        padding: 10,
        marginRight: 10,
        marginTop: 17
    },
    emptyDate: {
        height: 15,
        flex:1,
        paddingTop: 30
    }
});

const mapStateToProps = (state, ownProps) => {
    return {
        received_data:           state.data.received_data,
        lang:                    state.data.settings.lang,
        calendar:                state.data.calendar,
        userToken:               state.data.userToken,
        needs_update:            state.data.calendar.needs_update,
        guestStatus:             state.data.guestStatus
    }
};

const mapDispatchToProps = dispatch => {
    return {
        receiveData:            (data) => dispatch(receiveData(data)),
        updCalendar:            (items, sorted_keys) => dispatch(updCalendar({items, sorted_keys})),
        setCalCurDate:          (date) => dispatch(setCalCurDate({date})),
        removeData:             (key) => dispatch(removeData({key})),
        enableNetWarn:          () => dispatch(enableNetWarn()),
        disableNetWarn:         () => dispatch(disableNetWarn()),
        setCalendarNeedsUpdate: (needs_update) => dispatch(setCalendarNeedsUpdate({needs_update}))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(CalendarPage);
