import React from "react";
import {
    ActionSheet,
    Button,
    Card,
    Container,
    Content,
    Fab,
    Grid,
    Root,
    Row,
    ScrollableTab,
    Segment,
    Tabs,
    Tab,
    TabHeading,
    Picker,
} from "native-base";
import {
    ActivityIndicator,
    Animated,
    Dimensions,
    FlatList,
    Image,
    KeyboardAvoidingView,
    Linking,
    Platform,
    RefreshControl,
    ScrollView,
    Share,
    StyleSheet,
    Text,
    TextInput,
    TouchableOpacity,
    View
} from "react-native";
import {BusinessCard} from "../cards/BusinessCard";
import {AntDesign, SimpleLineIcons, Ionicons, MaterialCommunityIcons, MaterialIcons} from "@expo/vector-icons";
import {
    addAlias,
    addCanAlias,
    addInvite, addJoined, clearRooms,
    receiveData, removeData, setCalCurDate, setChatToken, updCalendar, updMessages
} from "../../actions/data";
import {getws} from "../../methods/webSocket";
import {connect} from "react-redux";
import {FactCard} from "../cards/FactCard";
import FactsTab from "../tabs/facts_tab";
import {
    backendRequest,
    extractResponse
} from "../../methods/ws_requests";
import {findOrNext, isLater, parseCalItems, sameDate} from "../../methods/calendar_methods";
import { GiftedChat } from 'react-native-gifted-chat';
//import "../../methods/poly";
import matrixcs from "matrix-js-sdk";
import * as DocumentPicker from 'expo-document-picker';
import * as FileSystem from 'expo-file-system';
import * as Permissions from 'expo-permissions';
import * as ImagePicker from 'expo-image-picker';
import {ImageMessageOverlay} from "../overlays/image_message_overlay";
import {FileMessageOverlay} from "../overlays/FileMessageOverlay";

import {getchat, refreshRooms} from "../../methods/chat_client";
import RoomList from "../cards/RoomList";
import Drawer from "react-native-drawer";
import DrawerContent from "../cards/DraweContent";
import MainHeader from "../headers_footers/main_header";
import MyWebView from "react-native-webview-autoheight";
import MessageScreen from "../../screens/extended view screens/messages_screen";
import AccountPage from "./account_page";
import PerconalChatCreationScreen from "../../screens/extended view screens/chat_personal_create_screen";
import RoomCreationScreen from "../../screens/extended view screens/chat_room_create_screen";
import button from "../../styles/buttons";

const SideMenu = require('react-native-side-menu');

const window = Dimensions.get('window');
const base64js = require('base64-js');
//const downloadManager = require("react-native-simple-download-manager");


const IMAGE_BUTTONS = [
    {
        text: "Открыть камеру",
        icon: "camera"
    },
    {
        text: "Выбрать фото из галереи",
        icon: "folder"
    },
    {
        text: "Выбрать файл",
        icon: ""
    }
];

const LONG_PRESS_BUTTONS = [
    {
        text: "Share",
        icon: "share"
    },
    {
        text: "Cancel",
        icon: "close"
    }
];


class ChatPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: "",
            password: "",
            render: true,
            access_token: "",
            ready_to_message: false,
            room_alias_name: "",
            room_alias: "",
            room_id: "",
            next_batch: "",

            client_ready: false,

            image_message_overlay: false,
            image_uri: "",
            image_base64: null,

            file_message_overlay: false,
            file_uri: "",
            file_name: "",

            menu_open: false,
            menu_offset: new Animated.Value(-1 * window.width),

            refreshing:false,

            message_screen_props:null,

            //account_view_active:false
            active_screen:"chat"
        };

        this.messages = {};
        this.invites = [];
        this.responses = [];
        this.joined_rooms = {};

        //this.room_id_to_alias = {};

        //this.chat_ref = React.createRef();

        //client = null;

        //this.client = matrixcs.createClient("https://matrix.sistyle.ru:8443");
        this.client = getchat();

        this._onRefresh = this._onRefresh.bind(this);

        this.chooseChat = this.chooseChat.bind(this);
        //this._trigger_image_overlay = this._trigger_image_overlay.bind(this);
        //this._trigger_file_overlay = this._trigger_file_overlay.bind(this);
    }

    chooseChat(chat_info) {
        this.setState({
            message_screen_props:null
        }, () => {
            this.setState({
                message_screen_props:{...chat_info}
            });
        });
    }

    setView = (view) => {
        this.setState({active_screen:view});
    };

    _onRefresh () {
        this.setState({refreshing:true});
        this.props.clearRooms();
        refreshRooms();
        this.setState({refreshing:false}, () => {console.log()});
    }

    componentDidMount() {
        if (Platform.OS != "ios" && Platform.OS != "android") {
            const brokenActionSheetOnWeb = document.querySelector(
                '#root div + div[style*="border-color: rgb(255, 0, 0);"]',
            );
            if (brokenActionSheetOnWeb) {
                console.log("found red box");
                brokenActionSheetOnWeb.style.display = 'none'
            } else {
                console.log("have not found the red box", brokenActionSheetOnWeb);
            }
        }

        this.props.removeData("chatGetRoomInfo");
        this.props.removeData("getRequestedUserInfo");
        if (this.props.chatLogin != "" && this.props.chatReady) refreshRooms();

        this.didBlurSubscription = this.props.navigation.addListener(
            'willBlur',
            payload => {
                if (this.close_drawer) this.close_drawer();
                //console.log('willBlur', payload);
            }
        );
    }

    componentWillUnmount() {
        if (this.didBlurSubscription) this.didBlurSubscription.remove();
    }

    render () {
        return (
            <View style={{flex:1, backgroundColor:"rgb(240,240,240)"}}>
                <View
                    style={{
                        backgroundColor:"rgb(240,240,240)",
                        width:window.width,
                        height:window.height,
                        flexDirection:"row"
                    }}
                >
                    {((screen) => {
                        switch(screen) {
                            case "chat":
                                return (
                                    <View
                                        style={{
                                            width: window.width * 0.3,
                                            borderRightWidth: 2,
                                            borderRightColor: "rgb(220,219,216)",
                                        }}
                                    >
                                        <ScrollView
                                            scrollEnabled
                                            contentContainerStyle={{
                                                flexGrow: 1,
                                                paddingBottom: 120,
                                                marginTop: -5,
                                                backgroundColor: "rgb(240,240,240)"
                                            }}
                                            zIndex={1}
                                        >
                                            {this.props.chatReady && !this.state.refreshing &&
                                            <View>
                                                {!this.state.refreshing &&
                                                <RoomList chooseChat={this.chooseChat} user_id={this.state.user_id}
                                                          navigation={this.props.navigation}
                                                          joined={true}/>}
                                                {!this.state.refreshing &&
                                                <RoomList chooseChat={this.chooseChat} user_id={this.state.user_id}
                                                          navigation={this.props.navigation}/>}
                                                {/*<View style={{height: 120}}/>*/}
                                            </View>
                                            }
                                            <TouchableOpacity
                                                style={{
                                                    alignSelf: "center",
                                                    marginTop: 15
                                                }}
                                                onPress={() => {
                                                    this._onRefresh();
                                                }}
                                            >
                                                <Ionicons size={30} name={"md-refresh"}/>
                                            </TouchableOpacity>
                                        </ScrollView>
                                        <View
                                            zIndex={5}
                                            style={{
                                                backgroundColor: "#fff",
                                            }}
                                        >
                                            {this.props.chatLogin != "" &&
                                            <View
                                                zIndex={3}
                                                style={{
                                                    height: 120,
                                                    borderBottomWidth: 2,
                                                    borderBottomColor: "rgb(220,219,216)",
                                                    flexDirection: "row",
                                                    justifyContent: "space-between",
                                                }}
                                            >
                                                {/*
                                                <View style={{
                                                    alignSelf: "center",
                                                    alignItems: "center",
                                                    textAlign: "center",
                                                    marginLeft: 10
                                                }}>
                                                    <Text style={{fontWeight: "bold", fontSize: 18}}>Чат</Text>
                                                </View>
                                                */}

                                                {/*
                                                <TouchableOpacity
                                                    onPress={() => {
                                                        if (this.props.chatLogin != "" && this.props.chatReady) refreshRooms();
                                                    }}
                                                >
                                                    <Ionicons name={"md-refresh"}/>
                                                </TouchableOpacity>
                                                */}

                                                {/* */}
                                                <View style={{
                                                    height: 120,
                                                    flexDirection: "column",
                                                    alignSelf: "center",
                                                    marginRight: 10
                                                }}>
                                                    <TouchableOpacity
                                                        style={[button.header, button.active, {alignSelf:"left", marginTop:20, marginLeft:20}]}
                                                        onPress={() => {
                                                            //this.props.navigation.navigate("PersonalChatCreationScreen");
                                                            this.setView("create_dm");
                                                        }}
                                                    >
                                                        {/* <AntDesign size={32} name={"adduser"}/> */}
                                                        <Text style={{color:"white"}}>Создать чат</Text>
                                                    </TouchableOpacity>
                                                    <TouchableOpacity
                                                        style={[button.header, button.active, {alignSelf:"left", marginTop:20, marginLeft:20, width:220}]}
                                                        onPress={() => {
                                                            //this.props.navigation.navigate("RoomCreationScreen");
                                                            this.setView("create_group");
                                                        }}
                                                    >
                                                        {/*
                                                        <AntDesign size={32} name={"addusergroup"}/>
                                                        */}
                                                        <Text style={{color:"white"}}>Создать групповой чат</Text>
                                                    </TouchableOpacity>
                                                    {/*
                                                    <TouchableOpacity
                                                        style={{alignSelf: "center"}}
                                                        onPress={() => {
                                                            //this.toggleAccountView();
                                                            this.setView("account");
                                                        }}
                                                    >
                                                        <Ionicons size={32} name={"md-settings"}/>
                                                    </TouchableOpacity>
                                                    */}
                                                </View>
                                                {/* */}
                                                
                                                {/*
                                                <View style={{
                                                    alignSelf: "center",
                                                    alignItems: "center",
                                                    textAlign: "center",
                                                    marginLeft: 10
                                                }}>
                                                    <TouchableOpacity
                                                        style={[button.header, button.active, {alignSelf:"center", marginTop:20}]}
                                                        onPress={() => {
                                                            Alert.alert("Тест!", "Кнонка нажата!");
                                                        }}
                                                    >
                                                        <Text style={{color:"white"}}>Создать 1</Text>
                                                    </TouchableOpacity>
                                                </View>
                                                */}
                                            </View>
                                            }
                                        </View>
                                    </View>
                                );
                                break;
                            case "account":
                                return (
                                    <View
                                        style={{
                                            width: window.width * 0.3,
                                            height: window.height,
                                            borderRightWidth: 2,
                                            borderRightColor: "rgb(220,219,216)",
                                        }}
                                    >
                                        <AccountPage navigation={this.props.navigation} toggle={() => {this.setView("chat")}}/>
                                    </View>
                                );
                                break;
                            case "create_dm":
                                return (
                                    <View
                                        style={{
                                            width: window.width * 0.3,
                                            height: window.height,
                                            borderRightWidth: 2,
                                            borderRightColor: "rgb(220,219,216)",
                                        }}
                                    >
                                        <PerconalChatCreationScreen navigation={this.props.navigation} toggle={() => {this.setView("chat")}}/>
                                    </View>
                                );
                                break;
                            case "create_group":
                                return (
                                    <View
                                        style={{
                                            width: window.width * 0.3,
                                            height: window.height,
                                            borderRightWidth: 2,
                                            borderRightColor: "rgb(220,219,216)",
                                        }}
                                    >
                                        <RoomCreationScreen navigation={this.props.navigation} toggle={() => {this.setView("chat")}}/>
                                    </View>
                                );
                                break;
                            default:
                                return (
                                    <View
                                        style={{
                                            width: window.width * 0.3,
                                            height: window.height,
                                            borderRightWidth: 2,
                                            borderRightColor: "rgb(220,219,216)",
                                        }}
                                    >
                                        <View
                                            zIndex={5}
                                            style={{
                                                backgroundColor: "#fff",
                                            }}
                                        >
                                            {this.props.chatLogin != "" &&
                                            <View
                                                zIndex={3}
                                                style={{
                                                    height: 60,
                                                    borderBottomWidth: 2,
                                                    borderBottomColor: "rgb(220,219,216)",
                                                    flexDirection: "row",
                                                    justifyContent: "space-between",
                                                }}
                                            >
                                                <View style={{
                                                    alignSelf: "center",
                                                    alignItems: "center",
                                                    textAlign: "center",
                                                    marginLeft: 10
                                                }}>
                                                    <Text style={{fontWeight: "bold", fontSize: 18}}>Чат</Text>
                                                </View>
                                                {/*<TouchableOpacity
                                            onPress={() => {
                                                if (this.props.chatLogin != "" && this.props.chatReady) refreshRooms();
                                            }}
                                        >
                                            <Ionicons name={"md-refresh"}/>
                                        </TouchableOpacity>*/}
                                                <View style={{
                                                    height: 38,
                                                    flexDirection: "row",
                                                    alignSelf: "center",
                                                    marginRight: 10
                                                }}>
                                                    <TouchableOpacity
                                                        style={{alignSelf: "center"}}
                                                        onPress={() => {
                                                            this.props.navigation.navigate("RoomCreationScreen");
                                                        }}
                                                    >
                                                        <AntDesign size={32} name={"addusergroup"}/>
                                                    </TouchableOpacity>
                                                    <TouchableOpacity
                                                        style={{alignSelf: "center"}}
                                                        onPress={() => {
                                                            this.props.navigation.navigate("PersonalChatCreationScreen");
                                                        }}
                                                    >
                                                        <AntDesign size={32} name={"adduser"}/>
                                                    </TouchableOpacity>
                                                    <TouchableOpacity
                                                        style={{alignSelf: "center"}}
                                                        onPress={() => {
                                                            this.toggleAccountView();
                                                        }}
                                                    >
                                                        <Ionicons size={32} name={"md-settings"}/>
                                                    </TouchableOpacity>
                                                </View>
                                            </View>
                                            }
                                        </View>



                                        <ScrollView
                                            scrollEnabled
                                            contentContainerStyle={{
                                                flexGrow: 1,
                                                paddingBottom: 120,
                                                marginTop: -5,
                                                backgroundColor: "rgb(240,240,240)"
                                            }}
                                            zIndex={1}
                                        >
                                            {this.props.chatReady && !this.state.refreshing &&
                                            <View>
                                                {!this.state.refreshing &&
                                                <RoomList chooseChat={this.chooseChat} user_id={this.state.user_id}
                                                          navigation={this.props.navigation}
                                                          joined={true}/>}
                                                {!this.state.refreshing &&
                                                <RoomList chooseChat={this.chooseChat} user_id={this.state.user_id}
                                                          navigation={this.props.navigation}/>}
                                                {/*<View style={{height: 120}}/>*/}
                                            </View>
                                            }
                                            <TouchableOpacity
                                                style={{
                                                    alignSelf: "center",
                                                    marginTop: 15
                                                }}
                                                onPress={() => {
                                                    this._onRefresh();
                                                }}
                                            >
                                                <Ionicons size={30} name={"md-refresh"}/>
                                            </TouchableOpacity>
                                        </ScrollView>
                                    </View>
                                );
                                break;

                        }
                    })(this.state.active_screen)}

                    {/*Chat list*/}

                    {/*Messages*/}
                    {this.state.message_screen_props != null &&
                        <MessageScreen {...this.state.message_screen_props}/>
                    }
                </View>
            </View>
        )
    }
}

function cmpAliases(r1, r2) {
    let a1 = r1["roomAlias"];
    let a2 = r2["roomAlias"];
    if (a1 > a2) return 1;
    if (a1 < a2) return -1;
    if (a1 == a2) return 0;
}

const mapStateToProps = (state, ownProps) => {
    return {
        received_data:           state.data.received_data,
        lang:                    state.data.settings.lang,
        userToken:               state.data.userToken,
        chatToken:               state.data.chat.token,
        chatLogin:               state.data.chat.login,
        chatPassword:            state.data.chat.pass,
        chatReady:               state.data.chat.ready,
        joined_rooms:            state.data.chat.joined_rooms,
        invites:                 state.data.chat.invites
    }
};

const mapDispatchToProps = dispatch => {
    return {
        receiveData:            (data) => dispatch(receiveData(data)),
        removeData:             (key) => dispatch(removeData({key})),
        setChatToken:           (token) => dispatch(setChatToken({token})),
        updMessages:            (new_msg, room) => dispatch(updMessages({new_msg, room})),
        addInvite:              (room) => dispatch(addInvite({room})),
        addJoined:              (room) => dispatch(addJoined({room})),
        addCanAlias:            (room, alias) => dispatch(addCanAlias({room, alias})),
        addAlias:               (room, alias, type) => dispatch(addAlias({room, alias, type})),
        clearRooms:             () => dispatch(clearRooms()),
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(ChatPage)
//                                            {/* height: "80%", */}
