import React from "react";
import {Button, Container, Fab, Grid, Row, ScrollableTab, Segment, Tabs, Tab, TabHeading} from "native-base";
import {ActivityIndicator, Image, Modal, PixelRatio, ScrollView, Text, TouchableOpacity, View} from "react-native";
import {BusinessCard} from "../cards/BusinessCard";
import {Ionicons, MaterialIcons} from "@expo/vector-icons";
import {
    initForm,
    receiveData, removeData, setAvailableFilter, setCurDate,
    setCurrentFilter, setDatesArray,
    setFactArray,
    setFilters,
    setIdMap,
    setProgram, setSpeakersArray
} from "../../actions/data";
import {cancel_reconnect, getws, init_timeout} from "../../methods/webSocket";
import {connect} from "react-redux";
import {FactCard} from "../cards/FactCard";
import FactsTab from "../tabs/facts_tab";
import {backendRequest, backend_request_legacy} from "../../methods/ws_requests";
import SpeakersTab from "../tabs/speakers_tab";
import FilterPage from "./filter_page";
import {applyFilter, dateSort, getFilterOptions, parseFilter, parseFilterToArray} from "../../methods/filter_parsing";
import {disableNetWarn, enableNetWarn, toggleFilterView} from "../../actions/control";
import PartnersTab from "../tabs/partners_tab";
import {sortSpeakers} from "../../methods/filter_parsing";
import Drawer from "react-native-drawer";
import DrawerContent from "../cards/DraweContent";
import MainHeader from "../headers_footers/main_header";
import button from "../../styles/buttons";
import ChooseEventIdScreen from "../../screens/extended view screens/choose_event_id_screen";
import NewsArticlesTab from "../tabs/news_articles_tab";


class EventPage extends React.Component {
    constructor (props) {
        super (props);
        this.state = {
            got_facts:false,
            got_filters:false,
            filter_view:false,

            active_tab:0
        };
        this.program = [];
        this.filters = {};
        this.parsedFilter = "";

        this.drawer = new React.createRef();
        this.menu_fun = this.menu_fun.bind(this);
        this.close_drawer = this.close_drawer.bind(this);
        //this.toggleFilterView = this.toggleFilterView.bind(this);
    }
/*
    toggleFilterView() {
        this.setState({filter_view:!this.state.filter_view});
    }*/


    componentDidMount() {
        if (this.props.filter_view) this.props.toggleFilterView();

        this.didBlurSubscription = this.props.navigation.addListener(
            'willBlur',
            payload => {
                if (this.close_drawer) this.close_drawer();
                //console.log('willBlur', payload);
            }
        );
    }

    componentWillUnmount() {
        if (this.didBlurSubscription) this.didBlurSubscription.remove();
    }


    menu_fun () {
        this.drawer.open();
    }

    close_drawer () {
        this.drawer.close();
    }

    render () {
        let accent_color = null;  //this.props.event_json.style ? (this.props.event_json.style.Accent_color ? this.props.event_json.style.Accent_color : "white") : null;
        let text_color = "white";    //this.props.event_json.style ? (this.props.event_json.style.Text_color ? this.props.event_json.style.Text_color : "white") : null;
        return (
            <Drawer
                content={<DrawerContent is_event navigation={this.props.navigation} open_facts={this.open_facts} close_drawer={this.close_drawer}/>}
                ref={(r) => this.drawer = r}
                openDrawerOffset={0.0}
                side={"right"}
                acceptPan
                negotiatePan
            >
                <View style={{flex:1, backgroundColor:"rgb(240,240,240)"/*height:"100%"*/}}>
                    <MainHeader is_event_screen menu_fun={this.menu_fun} navigation={this.props.navigation}/>
                    {/*{this.props.image
                        ?*/}
                    {this.props.program_ready
                        ?
                        <Container>
                            <Modal
                                animationType="fade"
                                transparent={true}
                                visible={this.props.filter_view}
                                onRequestClose={() => {
                                    this.props.toggleFilterView();
                                }}
                            >
                                <FilterPage />
                            </Modal>
                            <View style={{width:window.width, height:50}}>
                                <View
                                    style={{height:50, flexDirection:"row", justifyContent:"space-between", alignItems:"center", marginHorizontal:16}}
                                >
                                    <TouchableOpacity
                                        onPress={() => {
                                            this.setState({active_tab:0})
                                        }}
                                    >
                                        <View
                                            style={[
                                                button.header,
                                                (this.state.active_tab == 0 &&
                                                    (
                                                        accent_color
                                                            ?
                                                                {backgroundColor:accent_color}
                                                            :
                                                                button.active
                                                    )
                                                ),
                                                (accent_color && {borderColor:accent_color})]}
                                        >
                                            <Text style={[this.state.active_tab == 0 && {color:text_color}, {fontSize:14*PixelRatio.getFontScale()}]}>Программа</Text>
                                        </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                        onPress={() => {
                                            this.setState({active_tab:1})
                                        }}
                                    >
                                        <View
                                            style={[
                                                button.header,
                                                (this.state.active_tab == 1 &&
                                                    (
                                                        accent_color
                                                            ?
                                                            {backgroundColor:accent_color}
                                                            :
                                                            button.active
                                                    )
                                                ),
                                                (accent_color && {borderColor:accent_color})]}
                                        >
                                            <Text style={[this.state.active_tab == 1 && {color:text_color}, {fontSize:14*PixelRatio.getFontScale()}]}>Спикеры</Text>
                                        </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                        onPress={() => {
                                            this.setState({active_tab:2})
                                        }}
                                    >
                                        <View
                                            style={[
                                                button.header,
                                                (this.state.active_tab == 2 &&
                                                    (
                                                        accent_color
                                                            ?
                                                            {backgroundColor:accent_color}
                                                            :
                                                            button.active
                                                    )
                                                ),
                                                (accent_color && {borderColor:accent_color})]}
                                        >
                                            <Text style={[this.state.active_tab == 2 && {color:text_color}, {fontSize:14*PixelRatio.getFontScale()}]}>Партнеры</Text>
                                        </View>
                                    </TouchableOpacity>
                                </View>
                            </View>
                            {((tab) => {
                                switch (tab) {
                                    case 0:
                                        return (
                                            <View style={{flex:1}}>
                                                {/*{this.props.filter_view
                                                    ?
                                                    <FilterPage />
                                                    :*/}
                                                    <FactsTab navigation={this.props.navigation}/>

                                            </View>
                                        );
                                    case 1:
                                        return (
                                            <View style={{flex:1}}>
                                                <SpeakersTab  navigation={this.props.navigation}/>
                                            </View>
                                        );
                                    case 2:
                                        return (
                                            <View style={{flex:1}}>
                                                <PartnersTab navigation={this.props.navigation}/>
                                            </View>
                                        );
                                }
                            })(this.state.active_tab)}
                        </Container>
                        :
                        <View style={{marginTop:10}}>
                            <ActivityIndicator size={"small"} color={"#000"}/>
                        </View>
                    }
                </View>
            </Drawer>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        received_data:           state.data.received_data,
        lang:                    state.data.settings.lang,
        filter_view:             state.control.event_filter_view,
        userToken:               state.data.userToken,
        event_id:                state.data.event.event_id,
        program_ready:           state.data.event.program_ready,
        event_json:              state.data.event.event_json,
    }
};

const mapDispatchToProps = dispatch => {
    return {
        removeData:              (key) => dispatch(removeData({key})),
        receiveData:             (data) => dispatch(receiveData(data)),
        setProgram:              (program) => dispatch(setProgram({program})),
        setFilters:              (filters) => dispatch(setFilters({filters})),
        setFactArray:            (fact_array) => dispatch(setFactArray({fact_array})),
        setSpeakersArray:        (speakers_array) => dispatch(setSpeakersArray({speakers_array})),
        setIdMap:                (id_map) => dispatch(setIdMap({id_map})),
        toggleFilterView:        () => dispatch(toggleFilterView()),
        setCurrentFilter:        (current_filter) => dispatch(setCurrentFilter({current_filter})),
        setAvailableFilter:      (available_filter) => dispatch(setAvailableFilter({available_filter})),
        setDatesArray:           (dates_array) => dispatch(setDatesArray({dates_array})),
        setCurDate:              (cur_date) => dispatch(setCurDate({cur_date})),
        enableNetWarn:           () => dispatch(enableNetWarn()),
        disableNetWarn:          () => dispatch(disableNetWarn()),
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(EventPage);
