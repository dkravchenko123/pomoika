import React from "react";
import {Dimensions, ScrollView, Text, TouchableOpacity, View} from "react-native";
import {setAvailableFilter, setCurrentFilter} from "../../actions/data";
import {connect} from "react-redux";
import {styles} from "../../styles/login_screen_styles";
import {FactCard} from "../cards/FactCard";
import {toggleFilterView} from "../../actions/control";
import FilterPickerCard from "../cards/FilterPickerCard";
import {getFilterOptions, updateFilter} from "../../methods/filter_parsing";
import {Card} from "native-base";
import card from "../../styles/cards";
import button from "../../styles/buttons";

const window = Dimensions.get("window");

class FilterPage extends React.Component {
    constructor (props) {
        super (props);
    }

    componentDidMount() {
        //console.log("available filter at the time of mounting: "+JSON.stringify(this.props.event.available_filter));
        if (Object.keys(this.props.event.available_filter).length == 0) {
            console.log("no available filter at the time of mounting!");
            this.props.setAvailableFilter(getFilterOptions(this.props.event.fact_array, this.props.event.id_map, this.props.event.current_filter));
        }
    }

    render() {
        let {filters, fact_array, id_map, current_filter, available_filter} = this.props.event;

        let background_color = "rgb(240,240,240)";//this.props.event_json.style ? (this.props.event_json.style.Primary_color ? this.props.event_json.style.Primary_color : null) : null;
        let accent_color = null;//this.props.event_json.style ? (this.props.event_json.style.Accent_color ? this.props.event_json.style.Accent_color : "white") : null;
        let text_color = null;//this.props.event_json.style ? (this.props.event_json.style.Text_color ? this.props.event_json.style.Text_color : "black") : null;
        /*let filters = this.props.event.filters;
        let current_filter = this.props.event.current_filter;
        let available_filter = this.props.event.available_filter;*/

        if ( this.props.filter_view ) {
            console.log("event.filters: "+JSON.stringify(filters));
            console.log("event.current_filter: "+JSON.stringify(current_filter));
            console.log("event.available_filter: "+JSON.stringify(available_filter));
            return (
                <View style={{flex:1}}>
                    <TouchableOpacity style={{height:window.height*0.35, backgroundColor:"#00000060"}} onPress={() => {this.props.toggleFilterView()}}/>
                    <View style={{height:window.height*0.65+40, marginTop:-20, marginBottom:-20, borderRadius:15, justifyContent:"space-between", padding:15, paddingBottom:65, backgroundColor:(background_color ? background_color : "white")}}>
                        <View style={{flex:1}}>
                            <Text style={{fontWeight:"bold", fontSize:22, marginBottom:10, alignSelf:"center", color:text_color}}>{"Доступные программы:"}</Text>
                            {available_filter && available_filter["Programs"] && available_filter["Programs"].length > 0
                                ?
                                //Programs are found in available_filter, show all of them and highlight the ones that are found in current_filter

                                <Card style={{
                                    justifyContent: "space-between",
                                    backgroundColor: (background_color ? "white" : "rgb(240,240,240)"),
                                    borderRadius: 15,
                                    padding:15,
                                    marginBottom:10
                                }}>
                                    {available_filter["Programs"].sort((el1, el2) => el1 - el2).map((program_ID) => {
                                        console.log("gonna show program " + JSON.stringify(program_ID));
                                        let program_name;
                                        for (let e = 0; e < filters["Programs"].length; e++) {
                                            if (filters["Programs"][e]["ProgramID"] == program_ID) {
                                                program_name = filters["Programs"][e]["NameProgram"];
                                                break;
                                            }
                                        }
                                        if (!program_name) program_name = program_ID.toString();
                                        if (current_filter && current_filter["Programs"] && current_filter["Programs"].includes(program_ID)) {
                                            return <TouchableOpacity key={program_ID} onPress={() => {
                                                let new_cur_filter = updateFilter(current_filter, program_ID, "Programs");
                                                this.props.setAvailableFilter(getFilterOptions(fact_array, id_map, new_cur_filter));
                                                this.props.setCurrentFilter(new_cur_filter);
                                            }}><Text style={{fontWeight: "500"}}>{program_name}</Text></TouchableOpacity>
                                        } else {
                                            return <TouchableOpacity key={program_ID} onPress={() => {
                                                let new_cur_filter = updateFilter(current_filter, program_ID, "Programs");
                                                this.props.setAvailableFilter(getFilterOptions(fact_array, id_map, new_cur_filter));
                                                this.props.setCurrentFilter(new_cur_filter);
                                            }}><Text style={{fontWeight: "normal"}}>{program_name}</Text></TouchableOpacity>

                                        }
                                        //return <TouchableOpacity key={program["ProgramID"]} onPress={() => {this.props.updateFilter(program["ProgramID"])}}><Text style={this.props.filter.tags.includes(program["ProgramID"])? {fontWeight:"500"} : {fontWeight:"normal"}}>{program["NameProgram"]}</Text></TouchableOpacity>
                                    })}
                                </Card>
                                :
                                //Show all Programs

                                filters && filters["Programs"] && filters["Programs"].length > 0
                                    ?
                                    <Card style={{
                                        justifyContent: "space-between",
                                        backgroundColor: (background_color ? "white" : "rgb(220,219,216)"),
                                        borderRadius: 15,
                                        padding:15
                                    }}>
                                        {[...filters["Programs"]].sort((el1, el2) => el1["ProgramID"] - el2["ProgramID"]).map((program) => {
                                            console.log("gonna show " + JSON.stringify(program));
                                            if (program) {
                                                return <TouchableOpacity key={program["ProgramID"]} onPress={() => {
                                                    let new_cur_filter = updateFilter(current_filter, program["ProgramID"], "Programs");
                                                    this.props.setAvailableFilter(getFilterOptions(fact_array, id_map, new_cur_filter));
                                                    this.props.setCurrentFilter(new_cur_filter);
                                                }}><Text>{program["NameProgram"]}</Text></TouchableOpacity>
                                            } else {
                                                return null;
                                            }
                                        })}
                                    </Card>
                                    :
                                    null
                            }
                            <ScrollView style={{flex:1}}>
                                <FilterPickerCard label={"Тип"} filter_type={"FactType"}/>
                                <FilterPickerCard places={true} label={"Местоположение"} filter_type={"Places"}/>
                                <FilterPickerCard label={"Направления"} filter_type={"Courses"}/>
                                <View style={{marginBottom: 80}}/>
                            </ScrollView>
                        </View>

                        <TouchableOpacity
                            onPress={() => {
                                this.props.toggleFilterView()
                            }}
                        >
                            <View style={[button.base, button.active, {
                                width: 130,
                                height: 30,
                                alignSelf:"center",
                                alignItems:"center",
                                backgroundColor:"rgb(169,25,59)",
                                borderColor:"rgb(169,25,59)"
                            }]}>
                                <Text style={{color:'white'}}>Применить</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>
            );
        } else {
            console.log("not showing filter");
            return null;
        }
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        lang:               state.data.settings.lang,
        //filter:             state.control.event_filter,
        event:              state.data.event,
        filter_view:        state.control.event_filter_view,
        event_json:         state.data.event.event_json,
    }
};

const mapDispatchToProps = dispatch => {
    return {
/*      receiveData:        (data) => dispatch(receiveData(data)),*/
        /*updateFilter:       (ID) => dispatch(updateFilter({ID})),
        togglePlaceFilter:       (ID) => dispatch(togglePlaceFilter({ID}))*/
        setCurrentFilter:       (new_filter) => dispatch(setCurrentFilter({current_filter: new_filter})),
        setAvailableFilter:       (new_filter) => dispatch(setAvailableFilter({available_filter: new_filter})),
        toggleFilterView:           () => dispatch(toggleFilterView())
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(FilterPage);
