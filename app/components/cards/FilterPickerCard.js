import React from "react";
import {ActionSheetIOS, ScrollView, Text, TouchableOpacity, View} from "react-native";
import {setAvailableFilter, setCurrentFilter} from "../../actions/data";
import {connect} from "react-redux";
import {Card, Picker} from "native-base";
import {styles} from "../../styles/login_screen_styles";
import {getFilterOptions, updateFilter} from "../../methods/filter_parsing";



class FilterPickerCard extends React.Component {
    selected_values = [];

    componentDidMount() {

    }

    render() {
        let background_color = "rgb(240,240,240)";   //this.props.event_json.style ? (this.props.event_json.style.Primary_color ? this.props.event_json.style.Primary_color : null) : null;
        let accent_color = null;                     //this.props.event_json.style ? (this.props.event_json.style.Accent_color ? this.props.event_json.style.Accent_color : "white") : null;
        let text_color = null;                       //this.props.event_json.style ? (this.props.event_json.style.Text_color ? this.props.event_json.style.Text_color : "black") : null;

        let {filters, fact_array, id_map, current_filter, available_filter} = this.props.event;
        let item_array = [<Picker.Item key={0} label={"-Выбрать-"} value={"-Выбрать-"} />];
        //let item_array = [];
        //item_array = [ ...item_array, ...(this.props.choices.map((item) => {return (<Picker.Item key={item.name} label={item.name} value={{...item}} />)}))];
        if (available_filter && available_filter[this.props.filter_type] && available_filter[this.props.filter_type].length != 0) {
            item_array = [ ...item_array, ...(available_filter[this.props.filter_type].map((item_id) => {
                let item = filters[this.props.filter_type].find((el) => el[this.props.filter_type.slice(0,-1)+"ID"]==item_id);
                if (item) {
                    return (<Picker.Item key={item_id} label={this.props.places ? item["PlaceDescription"] : item["Name"+this.props.filter_type.slice(0,-1)]} value={item_id} />);
                } else {
                    return null;
                }
            }))]
        } else {
            //item_array = [ ...item_array, ...(filters[this.props.filter_type].map((elem) => {}))]
            console.log("can't filter by "+this.props.filter_type);
            return null;
        }

        let need_padding = current_filter && current_filter[this.props.filter_type] && current_filter[this.props.filter_type].length != 0;
        return (
            <View>
                <Text style={{fontWeight:"bold", fontSize:22, alignSelf:"center", color:text_color}}>{this.props.label}</Text>
                <Card style={{
                    justifyContent: "space-between",
                    backgroundColor: (background_color ? "white" : "rgb(220,219,216)"),
                    borderRadius: 15,
                    marginTop:10,
                    paddingHorizontal:15,
                    paddingBottom:need_padding ? 15 : 0
                }}>
                        <View style={{maxHeight:160}}>
                        <Picker
                            style={{justifyContent:"center", flexDirection:"row"}}
                            onValueChange={(value, index) => {
                                if (index!=0) {
                                    let new_cur_filter = updateFilter(current_filter, value, this.props.filter_type);
                                    this.props.setAvailableFilter(getFilterOptions(fact_array, id_map, new_cur_filter));
                                    this.props.setCurrentFilter(new_cur_filter);
                                }
                            }}
                            style={styles.picker}
                            selectedValue={"-Выбрать-"}
                        >
                            {item_array}
                        </Picker>
                        <View style={{maxHeight:100}}>
                            <ScrollView nestedScrollEnabled>
                                {
                                    current_filter &&
                                    current_filter[this.props.filter_type] &&
                                    current_filter[this.props.filter_type].length != 0 &&
                                    current_filter[this.props.filter_type].map(
                                        (value) => {
                                            let filter_id_name = this.props.filter_type.slice(0,-1)+"ID";

                                            if (filters[this.props.filter_type].find((el) => el[filter_id_name]==value)) {
                                                return <TouchableOpacity
                                                    key={value}
                                                    onPress={() => {
                                                        console.log("touched value: "+JSON.stringify(value));
                                                        let new_cur_filter = updateFilter(current_filter, value, this.props.filter_type);
                                                        this.props.setAvailableFilter(getFilterOptions(fact_array, id_map, new_cur_filter));
                                                        this.props.setCurrentFilter(new_cur_filter);
                                                    }}
                                                >
                                                    <View style={{flexDirection: "row"}}>
                                                        {this.props.places
                                                            ?
                                                            <Text>{filters[this.props.filter_type].find((el) => el[filter_id_name]==value)["PlaceDescription"]}</Text>
                                                            :
                                                            <Text>{filters[this.props.filter_type].find((el) => el[filter_id_name]==value)["Name"+this.props.filter_type.slice(0,-1)]}</Text>
                                                        }
                                                        <Text style={{color:"red"}}>{" X"}</Text>
                                                    </View>
                                                </TouchableOpacity>
                                            } else {
                                                return null;
                                            }
                                        }
                                    )
                                }
                            </ScrollView>
                        </View>
                    </View>
                </Card>
            </View>
        );
    }
}

function intersect(a, b) {
    let t;
    if (b.length > a.length) {t = b; b = a; a = t;} // indexOf to loop over shorter
    return a.filter(function (e) {
        return b.indexOf(e) > -1;
    });
}

const mapStateToProps = (state, ownProps) => {
    return {
        lang:               state.data.settings.lang,
        //filter:             state.control.event_filter
        event:            state.data.event,
        event_json:         state.data.event.event_json,
    }
};

const mapDispatchToProps = dispatch => {
    return {
        /*      receiveData:        (data) => dispatch(receiveData(data)),*/
        /*toggleFilter:       (ID) => dispatch(toggleFilter({ID})),
        togglePlaceFilter:       (ID) => dispatch(togglePlaceFilter({ID}))*/
        setCurrentFilter:       (new_filter) => dispatch(setCurrentFilter({current_filter: new_filter})),
        setAvailableFilter:       (new_filter) => dispatch(setAvailableFilter({available_filter: new_filter}))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(FilterPickerCard);
