import React from "react";
import {
    ActivityIndicator, Dimensions,
    Image,
    KeyboardAvoidingView,
    Picker, PixelRatio,
    Platform,
    Text,
    TextInput,
    ToastAndroid,
    TouchableOpacity,
    View
} from "react-native";
import {Button, Card, CardItem, Col, Grid, Row} from "native-base";
import {connect} from "react-redux";
import {SimpleLineIcons, Ionicons, MaterialCommunityIcons} from "@expo/vector-icons";
import {toggleExpandedFact, toggleExpandedSpeaker} from "../../actions/control";
import card from "../../styles/cards";
import button from "../../styles/buttons";

const window = Dimensions.get("window");

class SpeakerCard extends React.Component {
    render () {
        let speaker = this.props.speaker;
        if (this.props.expanded == speaker["SpeakerID"]) {
            return(null
            );
        } else {
            return(
                <Card style={[card.base, {alignSelf:"center", width:window.width-30, marginTop:10, paddingVertical: 10, paddingHorizontal:10}, this.props.base_color && {backgroundColor:this.props.base_color}]}>
                    <TouchableOpacity
                        style={{flex:1, width:"100%", height:"100%"}}
                        onPress={() => {
                            //this.props.toggleExpandedSpeaker(speaker["SpeakerID"]);
                            this.props.navigation.navigate("SpeakerScreen", {speaker});
                        }}>
                        <Grid /*style={{height:100}}*/>
                            <Col size={1} style={{ justifyContent: "center", alignItems: "center", marginRight: 16}}>
                                <View style={{width:60, height:60, borderRadius:30, justifyContent:'center', alignItems:"center"}}>
                                    <Image style={{height:60, width:60, borderRadius:30, resizeMode:"cover"}} source={{uri:speaker["SpeakerUserImgURL"]}}/>
                                </View>
                            </Col>
                            <Col size={4} >
                                <Row style={{marginBottom:3, marginTop:2}}>
                                    <Text style={{fontWeight:'bold'}}>{speaker["SpeakerFirstName"] + " " + speaker["SpeakerLastName"]}</Text>
                                </Row>
                                <Row style={{marginBottom:3}}>
                                    <Text>
                                        {
                                            ((description) => {
                                                let split_desc = description.split(" ");
                                                if (split_desc.length > 12) {
                                                    return (split_desc.slice(0, 12).join(" ")+"...");
                                                } else {
                                                    return (split_desc.slice(0, 12).join(" "));
                                                }
                                            })(speaker["SpeakerDescription"])
                                        }
                                    </Text>
                                </Row>
                            </Col>
                        </Grid>
                        {speaker["SpeakerRating"] != null &&
                            <Grid style={{padding: 3, flexDirection:"row"}}>
                                <Text style={{fontWeight:"bold", marginBottom:2}}>Рейтинг спикера:</Text>
                                <View style={{flexDirection:"row", alignItems:"center", marginLeft:5, marginBottom:2}}>
                                    {((rating) => {
                                        let show = [];
                                        for (let i = 0; i < 5; i++) {
                                            if (i <= rating-1) {
                                                show.push(<Ionicons color={this.props.accent_color} style={{alignSelf:"center", marginRight:2}} size={16} name={"ios-star"}/>);
                                            } else {
                                                show.push(<Ionicons color={this.props.accent_color} style={{alignSelf:"center", marginRight:2}} size={16} name={"ios-star-outline"}/>);
                                            }
                                        }
                                        return (show);
                                    })(speaker["SpeakerRating"])}
                                    <Text style={{marginLeft:3}}>{speaker["SpeakerRating"]}</Text>
                                </View>
                            </Grid>
                        }
                        {speaker["SpeakerSessionCount"] != 0 &&
                            <Grid style={{padding: 3}}>
                                <Text style={{fontWeight:"bold", marginBottom:8}}>{`Количество сессий: ${speaker["SpeakerSessionCount"]}`}</Text>
                            </Grid>
                        }
                    </TouchableOpacity>
                </Card>
            );
        }

    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        lang:               state.data.settings.lang,
        expanded:           state.control.expanded_speaker_id
    }
};

const mapDispatchToProps = dispatch => {
    return {
        toggleExpandedSpeaker: (speaker_id) => dispatch(toggleExpandedSpeaker({speaker_id}))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(SpeakerCard);
