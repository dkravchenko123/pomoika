import {Image, ScrollView, Text, TouchableOpacity, View} from "react-native";
import {SettingsInteractionCard, SettingsInteractionCardItem} from "./SettingsInteractionCard";
import {Content} from "native-base";
import React from "react";
import {WS_URL} from "../../constants/backend";
import {backendRequestCustomSocket} from "../../methods/ws_requests";
import {addContacts} from "../../actions/data";
import {connect} from "react-redux";
import {AntDesign, EvilIcons} from "@expo/vector-icons";

class ChatUserInfoCard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            addressee_matrix_id:this.props.addressee_matrix_id
        };
    }

    componentDidMount() {
        console.log("addresse matrix id", this.state.addressee_matrix_id);
        if (this.state.addressee_matrix_id) {
            let username = this.state.addressee_matrix_id.split("@")[1].split(":")[0];
            if (!this.props.contacts.hasOwnProperty(username)) {
                console.log("don't have a contact");
                let info_socket = new WebSocket(WS_URL);
                info_socket.onmessage = (msg) => {
                    let parsed_msg = JSON.parse(msg.data);
                    console.log(parsed_msg);
                    if (parsed_msg.statusCode == 200) {
                        this.props.addContacts({[username]:parsed_msg.data[0]});
                        this.setState({...parsed_msg.data[0]});
                        console.log("adding to state", {...parsed_msg.data[0]});
                    }
                    info_socket.close();
                };
                info_socket.onopen = () => {
                    backendRequestCustomSocket(info_socket, "getRequestedUserInfo", this.props.userToken, {MatrixId:this.state.addressee_matrix_id});
                };
            } else {
                console.log("have contact", {...this.props.contacts[username]});
                this.setState({...this.props.contacts[username]});
            }
        }
    }

    render () {
        let background_color = "rgb(240,240,240)";
        let accent_color = "white";

        let firstnamerus = (this.state.firstnamerus || this.props.firstnamerus);
        let lastnamerus = (this.state.lastnamerus || this.props.lastnamerus);
        let middlenamerus = (this.state.middlenamerus || this.props.middlenamerus);
        let org_namelong = (this.state.org_namelong || this.props.org_namelong);
        let titlename = (this.state.titlename || this.props.titlename);

        return (
            <Content style={{flex:1, backgroundColor:(background_color ? background_color : "white")}}>
                <View style={{height:60, padding:5, flexDirection:"row", backgroundColor:"white", elevation:15}}>
                    <View style={{flex:1, backgroundColor:"#ffffff00"}}>
                        {this.state.userphoto && this.props.userToken
                            ?
                            <Image
                                style={{ width:50, height:50, borderRadius:25}}
                                source={{
                                    uri: this.state.userphoto,
                                    method: "GET",
                                    headers: {
                                        "Authorization":this.props.userToken.toString()
                                    }
                                }}/>
                            :
                            <View style={{ borderRadius:25, width:50, height:50, justifyContent:"center", alignItems:"center", textAlign:"center", backgroundColor:"rgb(117,120,123)"}}>
                                <Text style={{fontSize:38, fontWeight:"bold", color:"#fff"}}>?</Text>
                            </View>
                        }
                    </View>
                    <View style={{flex:2 + (this.props.action_buttons != null ? 0 : 1), flexDirection:"row", justifyContent:"center", alignItems:"center", textAlign:"center", backgroundColor:"#ffffff00"}}>
                        { lastnamerus != "" && <Text style={{fontWeight:"bold", fontSize:18, color:"#000"}}>{lastnamerus}</Text>}
                        { firstnamerus != "" && <Text style={{fontWeight:"bold", fontSize:18, color:"#000"}}>{firstnamerus}</Text>}
                        { middlenamerus != "" && <Text style={{fontWeight:"bold", fontSize:18, color:"#000"}}>{middlenamerus}</Text>}
                    </View>

                    <TouchableOpacity
                        style={{alignSelf: "Right"}}
                        onPress={() => {
                            if (this.props.toggle)
                            {
                                this.props.toggle();
                            }
                        }}
                    >
                        <EvilIcons size={32} name={"close"}/>
                    </TouchableOpacity>
                </View>
                <View style={{flex:1, flexDirection:"column"}}>
                    {this.props.action_buttons}
                </View>

                <ScrollView style={{flex:1}}>

                    <SettingsInteractionCard label={"Информация:"}>
                        {
                            ["org_namelong", "titlename", "phone", "email"]
                                .filter((key) => {
                                    return (this.state[key] != null);
                                })
                                .map((existing_key, index) => {
                                    let label_start = "";
                                    switch (existing_key) {
                                        case "org_namelong":
                                            label_start = "Организация: ";
                                            break;
                                        case "titlename":
                                            label_start = "Должность: ";
                                            break;
                                        case "phone":
                                            label_start = "Телефон: ";
                                            break;
                                        case "email":
                                            label_start = "Почта: ";
                                            break;
                                    }
                                    return (
                                        <SettingsInteractionCardItem
                                            key={existing_key}
                                            top={index == 0}
                                            bottom
                                            label={label_start + this.state[existing_key]}
                                        />
                                    );
                                })
                        }
                    </SettingsInteractionCard>
                    <View style={{height:50}}/>
                </ScrollView>

            </Content>
        );
    }
}

const mapStateToProps = state => {
    return {
        //pi_overlay:         state.control.pi_overlay,
        userToken:      state.data.userToken,
        lang:           state.data.settings.lang,
        contacts:       state.data.chat.contacts
    }
};

const mapDispatchToProps = dispatch => {
    return {
        //toggle: () => dispatch(togglePersonalInfoOverlay()),
        addContacts:             (contact) => dispatch(addContacts({contact}))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(ChatUserInfoCard);
