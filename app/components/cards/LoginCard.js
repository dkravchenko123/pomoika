import React from "react";
import {
    ActivityIndicator,
    Dimensions,
    Image,
    Modal,
    Picker,
    ScrollView,
    Text,
    TextInput,
    ToastAndroid,
    TouchableOpacity,
    View
} from "react-native";
import {Button, Card, CardItem, Container, Icon} from "native-base";
import {styles} from "../../styles/login_screen_styles";
import { authoriseUser} from "../../methods/login";
import {connect} from "react-redux";
import {
    changeLang, removeData,
    setChatLogin,
    setChatPassword,
    setChatToken,
    setGuestStatus, setLogin, setPassword,
    updateUserToken
} from "../../actions/data";
import {decCounter, incCounter} from "../../reducers/reducer";
import {HTTP_URL, WS_URL} from '../../constants/backend';
import {backendRequest, extractResponse} from "../../methods/ws_requests";
import {cancel_reconnect, getws, init_timeout} from "../../methods/webSocket";
import button from "../../styles/buttons";
import field from "../../styles/fields";
import text from "../../styles/text";
import {disableNetWarn, enableNetWarn} from "../../actions/control";
import storeExport, {initializedStore} from "../../store";
import NetModalWarning from "./NetModalWarning";
import {initchat, start_client} from "../../methods/chat_client";

const {store, persistor} = storeExport();

const window = Dimensions.get("window");
const emitter = require('tiny-emitter/instance');

class LoginCard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            //language : "ru",
            login: "",
            pass: "",
            loading: true,
        };

        initchat();

        this.login_timeout = null;
        //emitter.on("test", () => {alert("test")});
    }

    componentDidMount() {
        this.props.removeData();


        console.log("have credentials: "+this.props.login+" "+this.props.password);
        if (this.props.login && this.props.password) {
            console.log("using saved credentials");
            //will try to auth with saved credentials, if not successful, in 10 seconds will just show the login screen
            this.login_timeout = setTimeout(() => {
                this.setState({loading:false});
            }, 10000);

            let login_socket = new WebSocket(WS_URL);
            login_socket.onmessage = (msg) => {
                let parsed_msg = JSON.parse(msg.data);
                console.log(parsed_msg);
                if (parsed_msg["statusCode"] == 200) {
                    clearTimeout(this.login_timeout);
                    this.props.setGuestStatus(false);
                    console.log("extracted response: "+JSON.stringify(parsed_msg));
                    this.props.updateUserToken(parsed_msg.data.bearerToken, parsed_msg.data.refreshToken);
                    if (parsed_msg.data.matrixAccessToken) this.props.setChatToken(parsed_msg.data.matrixAccessToken);
                    if (parsed_msg.data.matrixPassword && parsed_msg.data.matrixUsername) {
                        this.props.setChatPassword(parsed_msg.data.matrixPassword);
                        this.props.setChatLogin(parsed_msg.data.matrixUsername);
                        console.log(`starting client with login ${parsed_msg.data.matrixUsername} and password ${parsed_msg.data.matrixPassword}`);
                        start_client(parsed_msg.data.matrixUsername, parsed_msg.data.matrixPassword);
                    }
                    this.setState({loading:false});
                    this.props.navigation.navigate("MainScreen");
                }
                login_socket.close();
            };

            login_socket.onopen = () => {authoriseUser(login_socket, this.props.login, this.props.password, "7212abb34787", "8a4dd21d9559", Math.floor((Math.random()*1000000)).toString());};
        } else {
            this.setState({loading:false})
        }
    }

    render() {
        return (
            <View style={{flex:1, alignItems: "center"}}>
                {this.state.loading
                ?
                    <View style={{flex:1, alignItems: "center"}}>
                        <Text style={{fontSize:14}}>Авторизация</Text>
                        <ActivityIndicator style={{marginTop:10}} color={"black"} size={"large"}/>
                    </View>
                :
                    <View style={{flex:1, alignItems: "center"}}>
                        {/*<View style={[styles.login_borders]}>
                            <Text style={{marginLeft:8}}>{(this.props.lang == "en" && "Language") || (this.props.lang == "ru" && "Язык")}</Text>*/}
                        <Picker
                            selectedValue={this.props.lang}
                            onValueChange={(value, itemIndex) => {
                                this.props.ch_lang(value)
                            }}
                            style={field.picker_small}
                            //mode={"dropdown"}
                        >
                            <Picker.Item label={'Русский'} value={'ru'}/>
                            <Picker.Item label={'English'} value={'en'}/>
                        </Picker>
                        {/*</View>*/}
                        <Image style={{width:300, height:46}} resizeMode={"contain"} source={require('../../resources/rk.png')}/>
                        <Card style={styles.login_card} noShadow>
                            <CardItem>
                                <View style={{flexDirection: "column"}}>
                                    {/*<View style={[styles.login_borders]}>
                                        <Text style={{marginLeft:8}}>{(this.props.lang == "en" && "Language") || (this.props.lang == "ru" && "Язык")}</Text>
                                        <Picker
                                            selectedValue={this.props.lang}
                                            onValueChange={(value, itemIndex) => {
                                                this.props.ch_lang(value)
                                            }}
                                            style={styles.picker}
                                            //mode={"dropdown"}
                                        >
                                            <Picker.Item label={'Русский'} value={'ru'}/>
                                            <Picker.Item label={'English'} value={'en'}/>
                                        </Picker>
                                    </View>*/}
                                    <View>
                                        {(() => {
                                            switch(this.props.lang){
                                                case "en":
                                                    return (
                                                        <View>
                                                            <TextInput style={field.login} placeholder={'Login'} onChangeText={(text) => {this.setState({login:text})}}/>
                                                            <TextInput secureTextEntry={true} style={field.login} placeholder={'Password'} onChangeText={(text) => {this.setState({pass:text})}}/>
                                                        </View>
                                                    );
                                                default:
                                                    return (
                                                        <View>
                                                            <TextInput style={field.login} placeholder={'Логин'} onChangeText={(text) => {this.setState({login:text})}}/>
                                                            <TextInput secureTextEntry={true} style={field.login} placeholder={'Пароль'} onChangeText={(text) => {this.setState({pass:text})}}/>
                                                        </View>
                                                    );
                                            }
                                        })()}
                                    </View>
                                </View>
                            </CardItem>
                            <CardItem style={{marginTop:15}}>
                                <View style={{flexDirection:'column'}}>
                                    <TouchableOpacity style={[button.base, button.active]} title={'Вход'} onPress={() => {
                                        //login_socket = getws();
                                        let {login, pass} = this.state;
                                        let login_socket = new WebSocket(WS_URL);
                                        /*let timeout = setTimeout(() => {
                                            console.log("closing timed out socket");
                                            login_socket.close();
                                            login_socket = getws();
                                        }, 1000);*/
                                        //init_timeout(3000);
                                        login_socket.onmessage = (msg) => {
                                            //cancel_reconnect();
                                            let parsed_msg = JSON.parse(msg.data);
                                            console.log(parsed_msg);
                                            if (parsed_msg["statusCode"] == 200) {
                                                this.props.setGuestStatus(false);
                                                this.props.setLogin(login);
                                                this.props.setPassword(pass);
                                                console.log("extracted response: "+JSON.stringify(parsed_msg));
                                                this.props.updateUserToken(parsed_msg.data.bearerToken, parsed_msg.data.refreshToken);
                                                if (parsed_msg.data.matrixAccessToken) this.props.setChatToken(parsed_msg.data.matrixAccessToken);
                                                if (parsed_msg.data.matrixPassword && parsed_msg.data.matrixUsername) {
                                                    this.props.setChatPassword(parsed_msg.data.matrixPassword);
                                                    this.props.setChatLogin(parsed_msg.data.matrixUsername);
                                                    console.log("starting client");
                                                    start_client(parsed_msg.data.matrixUsername, parsed_msg.data.matrixPassword);
                                                }
                                                persistor.flush().then((res) => {
                                                    console.log("flus res: "+JSON.stringify(res));
                                                    this.props.navigation.navigate("MainScreen");
                                                });
                                            }
                                            login_socket.close();
                                        };

                                        //this.props.removeData("authoriseUser");

                                        //this.props.navigation.navigate("MainScreen");

                                        login_socket.onopen = () => {authoriseUser(login_socket, login, pass, "7212abb34787", "8a4dd21d9559", Math.floor((Math.random()*1000000)).toString())};
                                    }}>
                                        <Text style={{alignSelf: "center", color: "#fff"}}>
                                            {(() => {
                                                switch(this.props.lang){
                                                    case "en":
                                                        return (
                                                            'Login'
                                                        );
                                                    default:
                                                        return (
                                                            'Вход'
                                                        );
                                                }
                                            })()}
                                        </Text>
                                    </TouchableOpacity>
                                    {/*<TouchableOpacity
                                        style={button.base}
                                        title={'Регистрация'}
                                        onPress={() => {
                                            persistor.purge().then(() => {
                                                this.props.navigation.navigate('Register', {language: this.state.lang})
                                            });
                                        }}
                                    >
                                        <Text style={{alignSelf: "center"}}>
                                            {(() => {
                                                switch(this.props.lang){
                                                    case "en":
                                                        return (
                                                            'Register'
                                                        );
                                                    default:
                                                        return (
                                                            'Регистрация'
                                                        );
                                                }
                                            })()}
                                        </Text>
                                    </TouchableOpacity>*/}
                                </View>
                            </CardItem>
                            {/*<TouchableOpacity
                                onPress={() => {
                                    let login_socket = new WebSocket(WS_URL);
                                    login_socket.onmessage = (msg) => {
                                        let parsed_msg = JSON.parse(msg.data);
                                        console.log(parsed_msg);
                                        if (parsed_msg["statusCode"] == 200) {
                                            console.log("extracted response: "+JSON.stringify(parsed_msg));
                                            this.props.updateUserToken(parsed_msg.data.bearerToken);
                                            this.props.setGuestStatus(true);
                                            this.props.navigation.navigate("MainScreen");
                                        }
                                        login_socket.close();
                                    };

                                    login_socket.onopen = () => {authoriseUser(login_socket, "guest", "guest", "7212abb34787", "8a4dd21d9559", Math.floor((Math.random()*1000000)).toString())};
                                }}
                            >
                                <Text style={{textDecorationLine: 'underline'}}>
                                    {(() => {
                                        switch(this.props.lang){
                                            case "en":
                                                return (
                                                    'Login as guest'
                                                );
                                            default:
                                                return (
                                                    'Войти как гость'
                                                );
                                        }
                                    })()}
                                </Text>
                            </TouchableOpacity>*/}
                        </Card>
                    </View>
                }
            </View>
        );
    }
}


const mapStateToProps = state => {
    return {
        lang:           state.data.settings.lang,
        net_warn:       state.control.net_warn,
        login:          state.data.credentials.login,
        password:       state.data.credentials.password,
    }
};

const mapDispatchToProps = dispatch => {
    return {
        setLogin:               (login) => dispatch(setLogin({login})),
        setPassword:            (password) => dispatch(setPassword({password})),
        removeData:             (key) => dispatch(removeData({key})),
        ch_lang:                (lang) => dispatch(changeLang(lang)),
        updateUserToken:        (token, refreshToken) => dispatch(updateUserToken({token, refreshToken})),
        setGuestStatus:         (status) => dispatch(setGuestStatus({status})),
        setChatToken:           (token) => dispatch(setChatToken({token})),
        setChatLogin:           (login) => dispatch(setChatLogin({login})),
        setChatPassword:        (password) => dispatch(setChatPassword({password})),
        enableNetWarn:          () => dispatch(enableNetWarn()),
        disableNetWarn:          () => dispatch(disableNetWarn()),
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginCard);
