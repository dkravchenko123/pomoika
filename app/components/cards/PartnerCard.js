import React from "react";
import {
    ActivityIndicator, Dimensions,
    Image,
    KeyboardAvoidingView,
    Picker,
    Platform,
    Text,
    TextInput,
    ToastAndroid,
    TouchableOpacity,
    View
} from "react-native";
import {Button, Card, CardItem, Col, Grid, Row} from "native-base";
import {connect} from "react-redux";
import {toggleExpandedFact, toggleExpandedSpeaker} from "../../actions/control";
import card from "../../styles/cards";


const window = Dimensions.get("window");

class PartnerCard extends React.Component {
    render () {
        let partner = this.props.partner;

        return(
            <Card style={[card.base, {alignSelf:"center", width:window.width-30, marginTop:10, paddingVertical: 10, paddingHorizontal:10}, this.props.base_color && {backgroundColor:this.props.base_color}]}>
                <TouchableOpacity
                    style={{flex:1}}
                    onPress={() => {
                        this.props.navigation.navigate("PartnerScreen", {partner});
                    }}>
                    <Grid>
                        {partner.Imageurl &&
                            <Col size={1} style={{justifyContent: "center", alignItems: "center", marginRight: 8}}>
                                <View style={{width:60, height:60, borderRadius:30, justifyContent:'center', alignItems:"center"}}>
                                    <Image
                                        style={{height:60, width:60, borderRadius:30, resizeMode:"center"}}
                                        source={{
                                            uri:partner["Imageurl"],
                                            method: "GET",
                                            headers: {
                                                Authorization:this.props.userToken
                                            }
                                        }}
                                    />
                                </View>
                            </Col>
                        }
                        <Col size={4} >
                            <Row style={{marginBottom:3, marginTop:2}}>
                                <Text style={{fontWeight:'bold'}}>{partner["Name"]}</Text>
                            </Row>
                            <Row style={{marginBottom:3}}>
                                <Text>
                                    {
                                        ((description) => {
                                            let split_desc = description.split(" ");
                                            if (split_desc.length > 12) {
                                                return (split_desc.slice(0, 12).join(" ")+"...");
                                            } else {
                                                return (split_desc.slice(0, 12).join(" "));
                                            }
                                        })(partner["Description"])
                                    }
                                </Text>
                            </Row>
                        </Col>
                    </Grid>
                </TouchableOpacity>
            </Card>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        lang:               state.data.settings.lang,
        expanded:           state.control.expanded_speaker_id,
        userToken:          state.data.userToken
    }
};

const mapDispatchToProps = dispatch => {
    return {
        toggleExpandedSpeaker: (speaker_id) => dispatch(toggleExpandedSpeaker({speaker_id}))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(PartnerCard);
