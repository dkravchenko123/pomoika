import React from "react";
import {Card, CardItem, Col, Grid, Row} from "native-base";
import {Dimensions, Image, ScrollView, StyleSheet, Text, TouchableOpacity, View} from "react-native";
import card from "../../styles/cards";
import {setArticlesTagFilter} from "../../actions/data";
import {connect} from "react-redux";
import {fullCompare} from "../../methods/array_methods";
//import TextSize from 'react-native-text-size';

const window = Dimensions.get("window");

class ShortNewsCard extends React.Component {
    constructor(props) {
        super(props);
    }

    /*componentDidMount() {
        TextSize.measure({text:this.props.shortdesc}).then((info) => {console.log(info)});
    }*/

    render() {
        let img_w = (window.width - 60)/3;
        return (
            <Card style={card.base}>
                <Grid style={temp_styles.sn_grid}>
                    <TouchableOpacity
                        onPress={() => {
                            console.log("tapped the card");
                            this.props.navigation.navigate("FullArticleScreen", {articleid:this.props.articleid});
                        }}
                    >
                        <Row style={{marginBottom:8, marginTop:2}}>
                            <Text style={{fontWeight:"bold"}}>{this.props.title}</Text>
                        </Row>
                        <Row>
                            <Col size={1} style={{alignItems:"center", justifyContent:"center"}}>
                                {this.props.previewimgurl != "" && <Image style={{width:img_w, height:(this.props.type == 3 ? img_w*(3/2) : img_w*(2/3)), resizeMode:"cover"}} source={{uri:this.props.previewimgurl}}/>}
                            </Col>
                            <Col
                                size={2}
                                style={{paddingLeft:10}}
                                /*onLayout={(event) => {
                                    console.log("called onLayout", event.nativeEvent);
                                    if (event.nativeEvent.width != 0) {
                                    }
                                }}*/
                            >
                                <Row size={3}>
                                    <Text>{this.props.shortdesc.split(" ").slice(0,(this.props.type == 3 ? 34 : 20)).join(" ")}</Text>
                                </Row>
                            </Col>
                        </Row>
                        <Row>
                            <Text>{this.props.shortdesc.split(" ").slice((this.props.type == 3 ? 34 : 20) , this.props.shortdesc.split(" ").length).join(" ")}</Text>
                        </Row>
                    </TouchableOpacity>
                    {this.props.tags && this.props.tags.length > 0 &&
                        <Row style={{flexDirection:"row", marginTop:10}}>
                            <Text>{"Теги: "}</Text>
                            <ScrollView nestedScrollEnabled contentContainerStyle={{alignItems:"flex-start"}} style={{minHeight:24, maxHeight:116, flex:1}} horizontal={false} showsHorizontalScrollIndicator={false}>
                                {
                                    this.props.tags.map((tag) => {
                                        return (
                                            <TouchableOpacity
                                                key={tag.Tagid}
                                                onPress={() => {
                                                    if (!!this.props.articles_tag_filter && this.props.articles_tag_filter.findIndex((el) => fullCompare(tag, el)) != -1) {
                                                        this.props.setArticlesTagFilter(this.props.articles_tag_filter.dropItem(tag));
                                                    } else {
                                                        this.props.setArticlesTagFilter(
                                                            !!this.props.articles_tag_filter
                                                                ?
                                                                    this.props.articles_tag_filter.concat(tag)
                                                                :
                                                                    [tag]
                                                        )
                                                    }
                                                }}
                                            >
                                                <View
                                                    style={{
                                                        marginBottom: 6,
                                                        marginHorizontal:6,
                                                        minHeight:24,
                                                        paddingLeft:6,
                                                        paddingRight:6,
                                                        alignSelf:"flex-start",
                                                        alignItems:"flex-start",
                                                        borderRadius:12,
                                                        borderColor:"rgb(169,25,59)",
                                                        borderWidth: 1
                                                    }}
                                                >
                                                    <Text>{tag.Name}</Text>
                                                </View>
                                            </TouchableOpacity>
                                        )
                                    })
                                }
                            </ScrollView>
                        </Row>
                    }
                </Grid>
            </Card>
        );
    }
}

const temp_styles = StyleSheet.create({
    sn_card:{
        marginLeft: 15,
        marginRight: 15,
        marginTop:15,
    },
    sn_grid:{
        marginLeft: 10,
        marginRight: 10,
        marginTop: 10,
        marginBottom: 10,
    }
});

const mapStateToProps = state => {
    return {
        userToken:              state.data.userToken,
        articles_tag_filter:    state.data.articles_tag_filter
    }
};

const mapDispatchToProps = dispatch => {
    return {
        setArticlesTagFilter:         (filter) => dispatch(setArticlesTagFilter({filter}))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(ShortNewsCard);
