import React from "react";
import moment from 'moment';
import {FlatList, Image, ScrollView, StyleSheet, Text, TouchableOpacity, View} from "react-native";
import {Button, Card, Col, Container, Grid, Row} from "native-base";
import {MaterialIcons, FontAwesome, MaterialCommunityIcons} from "@expo/vector-icons";
import {getws} from "../../methods/webSocket";
import {backendRequest, backendRequestPromise} from "../../methods/ws_requests";
import button from "../../styles/buttons";
import card from "../../styles/cards";
import {LocaleConfig} from "react-native-calendars";
import {sortTime} from "../../methods/calendar_methods";

LocaleConfig.locales['ru'] = {
    monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
    monthNamesShort: ['Янв.','Фев.','Март','Апр.','Май','Июнь','Июль','Авг.','Сент.','Окт.','Нояб.','Дек.'],
    dayNames: ['Воскресенье','Понедельник','Вторник','Среда','Четверг','Пятница','Суббота'],
    dayNamesShort: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
    today: 'Сегодня'
};
LocaleConfig.locales['en'] = {
    monthNames: ['January','February','March','April','May','June','July','August','September','October','November','December'],
    monthNamesShort: ['Jan.','Feb.','Mar','Apr.','May','June','July','Aug.','Sep.','Oct.','Nov.','Dec.'],
    dayNames: ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'],
    dayNamesShort: ['Sun','Mon','Tue','Wed','Thur','Fri','Sat'],
    today: 'Today'
};


export class CalendarDayCard extends React.PureComponent {
    constructor (props) {
        super (props);

    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        console.log("CalendarDayCard did update");
    }


    render () {
        const items = this.props.items.map(elem => <Text style={{ width: 90 }}>{JSON.stringify(elem)}</Text>)
        return (
            <>
                <FlatList
                    keyExtractor={(item, index) => {return item["item_id"].toString()}}
                    data={[...this.props.items]}
                    renderItem={(el) => (
                    <>
                    <Card style={[card.base, {marginTop: 10, marginLeft:15, marginRight:15, borderRadius: 30, padding:(el.item.invite ? 30 : 15), borderBottomStartRadius: 30/*(el.item.invite ? 30 : 0)*/, borderBottomEndRadius: 30/*(el.item.invite ? 30 : 0)*/}]}>
                        {((date) => {
                            let date_arr = date.split("-");
                            let year = date_arr[0];
                            let momentDate = moment(date).format('DD MMMM');
                            let day_str = LocaleConfig.locales[this.props.language]["dayNames"][(new Date(date)).getDay()];
                            let notPlace = this.props.language === 'ru' ? 'Не указано' : 'Not indicated';
                            let day = date_arr[2];
                            if (el.item.invite) {
                                return (
                                    <>
                                        <Text style={{ alignSelf: 'center', fontSize: 20, fontWeight: 'bold', paddingBottom: 15 }}>{el.item.item_type_name}</Text>
                                        <Text style={{ alignSelf: 'center', fontSize: 18, paddingBottom: 15 }}>{el.item.name}</Text>
                                        <Text style={{ marginLeft: 20, alignSelf: 'flex-start', fontSize: 14, paddingBottom: 5 }}>{`${this.props.language === 'en' ? 'Theme:' : 'Тема:'} ${el.item.usercal_description}`}</Text>
                                        <View style={{ flexDirection: 'row', alignItems: 'center', paddingBottom: 5 }}>
                                            <MaterialCommunityIcons name="clock" color='#005989' size={14} />
                                            <Text style={{ marginLeft: 10, fontSize: 14, alignSelf: 'center' }}>
                                                {`${this.props.language === 'ru' ? 'Начало:' : 'Start:'} ${moment(el.item.date).format('DD.MM.YYYY')}`}
                                            </Text>
                                        </View>
                                        {!!el.item.guests && <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                            <FontAwesome name="users" color='#005989' size={20} />
                                            <Text style={{ marginLeft: 10, fontSize: 16, alignSelf: 'center', textAlignVertical: 'auto' }}>
                                                {`${this.props.language === 'ru' ? 'Участники' : 'Members'}: ${el.item.guests ? el.item.guests.map((elem) => (`${this.props.language === 'ru' ? elem.lastnamerus : elem.lastnameeng} ${this.props.language === 'ru' ? elem.firstnamerus : elem.firstnameeng} ${this.props.language === 'ru' ? elem.middlenamerus : ""}`)) : JSON.stringify(el.item)}`}
                                            </Text>
                                        </View>}
                                        <View style={{ justifyContent: 'flex-end', alignItems: 'flex-end', flexDirection: 'row' }}>
                                            <TouchableOpacity
                                            onPress={() => {
                                                //this.props.navigation.navigate("FactScreen", {fact: fact});
                                                backendRequestPromise(
                                                    "calendarUpdateInvite",
                                                    this.props.token,
                                                    {itemId:el.item.item_id, statusId:1}
                                                ).then(() => {
                                                    this.props.update(true);
                                                });
                                            }}
                                            >
                                                <Text style={{  paddingRight: 10, color: 'darkred', textDecorationColor: 'darkred', textDecorationStyle: 'dotted', textDecorationLine: 'underline' }}>{this.props.language === 'ru' ? 'Принять' : 'Accept'}</Text>
                                            </TouchableOpacity>
                                            <TouchableOpacity
                                            onPress={() => {

                                                backendRequestPromise(
                                                    "calendarDeclineInvite",
                                                    this.props.token,
                                                    {itemId:el.item.item_id}
                                                ).then(() => {
                                                    this.props.update(true);
                                                });
                                            }}
                                            >
                                                <Text style={{ color: 'gray', textDecorationColor: 'gray', textDecorationStyle: 'dotted', textDecorationLine: 'underline' }}>{this.props.language === 'ru' ? 'Отклонить' : 'Decline'}</Text>
                                            </TouchableOpacity>
                                        </View>
                                    </>
                                );
                            }
                            else {
                                return (
                                    <TouchableOpacity onPress={() => {this.props.navigation.navigate("CalendarItemScreen", {item: el.item })}}>
                                        <View style={{alignItems:"center", flexDirection:"row", marginVertical:10, flex:1 }}>
                                            <View style={{ flex:1, borderRightWidth: 3, borderRightColor: '#005989', paddingHorizontal: 20, paddingBottom: 10 }}>
                                                <Text style={{flex:6, fontSize:18, textAlign: 'right'}}>{momentDate}</Text>
                                                <Text style={{flex:1, fontSize:14, textAlign: 'right'}}>{day_str}</Text>
                                                <Text style={{flex:1, fontSize:14, textAlign: 'right'}}>{moment(el.item.date).format("HH:mm")}</Text>
                                            </View>
                                            <View style={{ flex:2, paddingHorizontal: 20, paddingBottom: 10}}>
                                                <Text style={{ fontSize: 20, fontWeight: 'bold', textAlign: 'left' }}>{el.item.name}</Text>
                                                <Text style={{ fontSize: 16,  textAlign: 'left' }}>
                                                    {`${el.item.description || el.item.usercal_description}`}
                                                </Text>
                                            </View>
                                        </View>
                                        {!!el.item.guests && el.item.guests.length != 0 &&
                                        <View style={{flexDirection: 'row', paddingLeft: 20, marginTop: 8}}>
                                            <>
                                                <FontAwesome name="users" color='#005989' size={16}/>
                                                <Text style={{
                                                    marginLeft: 10,
                                                    fontSize: 16,
                                                    alignSelf: 'center',
                                                    textAlignVertical: 'auto'
                                                }}>
                                                    {`${this.props.language === 'ru' ? 'Участники' : 'Members'}: ${el.item.guests ? el.item.guests.map((elem) => (`${this.props.language === 'ru' ? elem.lastnamerus : elem.lastnameeng} ${this.props.language === 'ru' ? elem.firstnamerus : elem.firstnameeng} ${this.props.language === 'ru' ? (elem.middlenamerus || "") : (elem.middlenameeng || "")}`)) : JSON.stringify(el.item)}`}
                                                </Text>
                                            </>
                                        </View>
                                        }
                                        <Text style={{ alignSelf: 'flex-end', fontSize: 12, marginRight:15, fontWeight: 'bold', color:"rgb(169,25,59)"}}>{el.item.item_type_name}</Text>
                                    {/*<View style={{ flexDirection: 'row', paddingLeft: 20, marginTop: 15 }}>
                                        <MaterialCommunityIcons name="clock" color='#005989' size={20} />
                                        <Text style={{ marginLeft: 10, fontSize: 16, alignSelf: 'center', textAlignVertical: 'auto' }}>
                                            {el.item.date.split("T")[1].split(":").slice(0,2).join(":")}
                                        </Text>
                                    </View>
                                    <View style={{ flexDirection: 'row', paddingLeft: 20, marginTop: 15 }}>
                                        <MaterialIcons name="location-on" color='#005989' size={20} />
                                        <Text style={{ marginLeft: 10, fontSize: 16, alignSelf: 'center', textAlignVertical: 'auto' }}>
                                            {`${this.props.language === 'ru' ? 'Место проведения' : 'Location'}: ${el.item.location || notPlace}`}
                                        </Text>
                                    </View>
                                    <View style={{ flexDirection: 'row', paddingLeft: 20, marginTop: 15 }}>
                                    {!!el.item.guests &&
                                        <>
                                        <FontAwesome name="users" color='#005989' size={20} />
                                        <Text style={{ marginLeft: 10, fontSize: 16, alignSelf: 'center', textAlignVertical: 'auto' }}>
                                            {`${this.props.language === 'ru' ? 'Участники' : 'Members'}: ${el.item.guests ? el.item.guests.map((elem) => (`${this.props.language === 'ru' ? elem.lastnamerus : elem.lastnameeng} ${this.props.language === 'ru' ? elem.firstnamerus : elem.firstnameeng} ${this.props.language === 'ru' ? elem.middlenamerus : ""}`)) : JSON.stringify(el.item)}`}
                                        </Text>
                                        </>
                                    }
                                    </View>*/}
                                    </TouchableOpacity>
                                );
                            }
                        })(this.props.date)}
                    </Card>
                    {/*{!el.item.invite && <TouchableOpacity
                        style={[card.base, {width: '93%',padding: 15, alignSelf:"center", marginTop:5, borderRadius: 15, borderTopStartRadius: 0, borderTopEndRadius: 0}]}
                        onPress={() => {this.props.navigation.navigate("CalendarItemScreen", {item: el.item })}}
                    >
                    <Text style={{alignSelf: "center", color: "darkred", fontSize: 20}}>
                        {this.props.language === 'ru' ? 'Открыть' : 'Open'}
                    </Text>
                    </TouchableOpacity>}*/}
                    </>
                    )}
                />
          </>
        )
    }
}
