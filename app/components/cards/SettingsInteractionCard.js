import React from "react";
import {ScrollView, Text, TouchableOpacity, View} from "react-native";
import {SimpleLineIcons, Ionicons, MaterialCommunityIcons} from "@expo/vector-icons";








export class SettingsInteractionCard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            active:true
        }
    }

    render () {
        return (
            <View>
                <View style={{flexDirection:"column", justifyContent:"center", backgroundColor:"white", width:"100%", marginTop:15, paddingBottom:10, elevation:15}}>

                    <TouchableOpacity
                        style={{flexDirection:"row", justifyContent:"space-between", alignItems:"center", width:"100%", paddingRight:20}}
                        onPress={() => {
                            if (this.props.collapsible) this.setState({active:!this.state.active});
                        }}
                    >
                        <Text style={{fontSize:14, marginVertical:12, marginHorizontal:20, color:"rgb(169,25,59)"}}>{this.props.label}</Text>
                        {this.props.collapsible && <Ionicons size={18} color={"rgb(169,25,59)"} name={this.state.active ? "ios-remove" : "ios-add"}/>}
                    </TouchableOpacity>

                    {this.state.active &&
                    <View>
                        {this.props.children != undefined && this.props.children}
                    </View>
                    }
                </View>

                <View style={{height:20}} />
            </View>
        );
    }
}

export class SettingsInteractionCardItem extends React.Component {
    constructor(props) {
        super(props);
    }

    render () {
        return (
            <TouchableOpacity
                style={{
                    height:40,
                    flexDirection:"row",
                    borderTopWidth:this.props.top ? 1 : 0,
                    justifyContent:"flex-start",
                    alignItems:"center",
                    borderBottomWidth: this.props.bottom ? 1 : 0,
                    borderColor:"rgb(220,219,216)",
                    backgroundColor:"rgb(246,246,246)",
                    paddingHorizontal:20
                }}
                onPress={() => {
                    if (this.props.onPress) this.props.onPress();
                }}
            >
                {this.props.image}
                <Text style={{
                    fontSize: 16,
                    color: "#000",
                }}>{this.props.label}</Text>
            </TouchableOpacity>
        );
    }
}
