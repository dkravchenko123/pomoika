import {ActivityIndicator, Image, PixelRatio, ScrollView, Text, TouchableOpacity, View} from "react-native";
import React from "react";
import {ActionSheet, Col, Grid, Row} from "native-base";
import {
    addContacts,
    addInvite,
    addJoined, addRooms,
    receiveData,
    removeData, removeInvite, removeJoined,
    setChatToken,
    updMessages
} from "../../actions/data";
import {connect} from "react-redux";
import {getchat} from "../../methods/chat_client";
import card from "../../styles/cards";
import {cancel_reconnect, getws, init_timeout} from "../../methods/webSocket";
import {
    backendRequestCustomSocket,
    extractManyResponses,
    extractResponse
} from "../../methods/ws_requests";
import {CHAT_URL, WS_URL} from "../../constants/backend";
import {disableNetWarn, enableNetWarn} from "../../actions/control";
import button from "../../styles/buttons";
import WebImage from "../../WebImage";

const BUTTONS = [
    {
        text: "Удалить чат",
        icon: "close"
    },
    {
        text: "Отмена",
        //icon: "exit"
    }
];

const chatbots = {
    "AccreditationFTh":"БОТ Аккредитация",
    "fth":"Бот"
};

class UserDisplayCard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            addressee_matrix_id:this.props.addressee_matrix_id
        };
    }

    componentDidMount() {
        console.log("addresse matrix id", this.state.addressee_matrix_id);
        if (this.state.addressee_matrix_id) {
            let username = this.state.addressee_matrix_id.split("@")[1].split(":")[0];
            if (!this.props.contacts.hasOwnProperty(username)) {
                console.log("don't have a contact");
                let info_socket = new WebSocket(WS_URL);
                info_socket.onmessage = (msg) => {
                    let parsed_msg = JSON.parse(msg.data);
                    console.log(parsed_msg);
                    if (parsed_msg.statusCode == 200) {
                        //this.setState({});
                        this.setState({...parsed_msg.data[0]});
                        console.log("adding to state", {...parsed_msg.data[0]});
                    }
                    info_socket.close();
                };
                info_socket.onopen = () => {
                    backendRequestCustomSocket(info_socket, "getRequestedUserInfo", this.props.userToken, {MatrixId:this.state.addressee_matrix_id});
                };
            } else {
                console.log("have contact", {...this.props.contacts[username]});
                this.setState({...this.props.contacts[username]});
            }
        }
    }

    render () {
        let firstnamerus = (this.state.firstnamerus || this.props.firstnamerus);
        let lastnamerus = (this.state.lastnamerus || this.props.lastnamerus);
        let org_namelong = (this.state.org_namelong || this.props.org_namelong);
        let titlename = (this.state.titlename || this.props.titlename);

        return (
            <View style={card.base}>
                <View style={{height:80, width:300, flexDirection:"row"}}>
                    <View style={{height:80, width:80, justifyContent:"center"}}>
                        {(this.state.userphoto || this.props.userphoto) != null
                            ?
                            <WebImage
                                style={{borderRadius:30, width:60, height:60, resizeMode:"cover", alignSelf:"center"}}
                                source={{
                                    uri: (this.state.userphoto || this.props.userphoto),
                                    method: "GET",
                                    headers: {
                                        Authorization:this.props.userToken
                                    }
                                }}
                            />
                            :
                            <View style={{borderRadius:30, width:60, height:60, alignSelf:"center", justifyContent:"center", alignItems:"center", textAlign:"center", backgroundColor:"rgb(117,120,123)"}}><Text style={{fontSize:38, fontWeight:"bold", color:"#fff"}}>?</Text></View>
                        }
                    </View>
                    <View style={{flex:1, marginTop:10, marginBottom:15, justifyContent:"space-between"}}>
                        <Text style={{fontWeight:"bold"}}>{`${firstnamerus || ""} ${lastnamerus || ""}`}</Text>
                        <Text style={{fontWeight:"bold"}}>{`${org_namelong || ""} ${titlename || ""}`}</Text>
                    </View>
                </View>
            </View>
        );
    }
}


const mapStateToProps = (state, ownProps) => {
    return {
        lang:                    state.data.settings.lang,
        userToken:               state.data.userToken,
        chatToken:               state.data.chat.token,
        joined_rooms:            state.data.chat.joined_rooms,
        invites:                 state.data.chat.invites,
        messages:                state.data.chat.messages[ownProps.room_id],
        user_id:                 state.data.chat.user_id,
        rooms:                   state.data.chat.rooms,
        contacts:                state.data.chat.contacts,
    }
};

const mapDispatchToProps = dispatch => {
    return {
        setChatToken:            (token) => dispatch(setChatToken({token})),
        updMessages:             (new_msg, room) => dispatch(updMessages({new_msg, room})),
        addInvite:               (room) => dispatch(addInvite({room})),
        addJoined:               (room) => dispatch(addJoined({room})),
        removeInvite:            (room) => dispatch(removeInvite({room})),
        removeJoined:            (room) => dispatch(removeJoined({room})),
        enableNetWarn:           () => dispatch(enableNetWarn()),
        disableNetWarn:          () => dispatch(disableNetWarn()),
        addRooms:                (room) => dispatch(addRooms({room})),
        addContacts:             (contact) => dispatch(addContacts({contact}))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(UserDisplayCard)
