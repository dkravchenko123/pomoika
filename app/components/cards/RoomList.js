import {ActivityIndicator, Dimensions, ScrollView, Text, TouchableOpacity, View} from "react-native";
import React from "react";
import {ActionSheet} from "native-base";
import {
    addAlias,
    addCanAlias,
    addInvite,
    addJoined,
    receiveData,
    removeData, removeInvite, removeJoined,
    setChatToken,
    updMessages
} from "../../actions/data";
import {connect} from "react-redux";
import {getchat} from "../../methods/chat_client";
import RoomDisplayCard from "./RoomDisplayCard";
import card from "../../styles/cards";

const window = Dimensions.get("window");

const BUTTONS = [
    {
        text: "Удалить чат",
        icon: "close"
    },
    {
        text: "Отмена",
        //icon: "exit"
    }
];

class RoomList extends React.Component {
    constructor(props) {
        super(props);

        this.client = getchat();
    }

    render () {
        let source = this.props.joined ? this.props.joined_rooms : this.props.invites;
        return (
            <View>
                <ScrollView>
                    {
                        Object.keys(source).sort((k1, k2) => cmpAliases(source[k1],source[k2])).map((key, index) => {
                            console.log("room "+key);
                            let the_props = {
                                key:key,
                                room_id: key,
                                user_id: this.props.user_id,
                                joined: this.props.joined,
                                navigation:this.props.navigation,
                                chooseChat:this.props.chooseChat
                            };
                            return (
                                <View>
                                    <RoomDisplayCard {...the_props} />
                                </View>
                            );
                        })
                    }
                </ScrollView>
            </View>
        );
    }
}

function cmpAliases(r1, r2) {
    let a1 = r1["roomAlias"];
    let a2 = r2["roomAlias"];
    if (a1 > a2) return 1;
    if (a1 < a2) return -1;
    if (a1 == a2) return 0;
}

const mapStateToProps = (state, ownProps) => {
    return {
        received_data:           state.data.received_data,
        lang:                    state.data.settings.lang,
        userToken:               state.data.userToken,
        chatToken:               state.data.chat.token,
        joined_rooms:            state.data.chat.joined_rooms,
        invites:                 state.data.chat.invites
    }
};

const mapDispatchToProps = dispatch => {
    return {
        receiveData:            (data) => dispatch(receiveData(data)),
        removeData:             (key) => dispatch(removeData({key})),
        setChatToken:           (token) => dispatch(setChatToken({token})),
        updMessages:            (new_msg, room) => dispatch(updMessages({new_msg, room})),
        addInvite:              (room) => dispatch(addInvite({room})),
        addJoined:              (room) => dispatch(addJoined({room})),
        addCanAlias:            (room, alias) => dispatch(addCanAlias({room, alias})),
        addAlias:               (room, alias) => dispatch(addAlias({room, alias})),
        removeInvite:           (room) => dispatch(removeInvite({room})),
        removeJoined:           (room) => dispatch(removeJoined({room}))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(RoomList)
