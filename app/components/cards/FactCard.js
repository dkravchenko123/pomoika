import React from "react";
import {
    ActivityIndicator, Dimensions,
    KeyboardAvoidingView, Modal,
    Picker,
    Platform,
    Text,
    TextInput,
    ToastAndroid,
    TouchableOpacity,
    View
} from "react-native";
import {Button, Card, CardItem, Col, Grid, Row} from "native-base";
import {connect} from "react-redux";
import {SimpleLineIcons, Ionicons, MaterialCommunityIcons} from "@expo/vector-icons";
import {toggleExpandedFact} from "../../actions/control";
import card from "../../styles/cards";
import FilterPage from "../pages/filter_page";
import AHWebView from "react-native-webview-autoheight";

const window = Dimensions.get("window");

class FactCard extends React.Component {
    constructor(props){
        super(props);
        this.state={
            url_open:false
        };

        this.toggleUrl = this.toggleUrl.bind(this);
    }

    toggleUrl() {
        this.setState({url_open:!this.state.url_open});
    }

    render () {
        if (this.props.expanded === this.props.fact_id) {
            return (null);
        } else {
            return(
                <Card
                    style={[card.base, {alignSelf:"center", width:window.width-30, marginTop:10, paddingVertical: 10, minHeight:(this.props.minheight ? this.props.minheight : 0), paddingHorizontal:10}, this.props.base_color && {backgroundColor:this.props.base_color}]}
                >
                    <Modal
                        animationType="fade"
                        transparent={true}
                        visible={this.state.url_open}
                        onRequestClose={() => {
                            this.toggleUrl();
                        }}
                    >
                        <View style={{flex:1}}>
                            <TouchableOpacity style={{height:window.height, backgroundColor:"#00000060"}} onPress={() => {this.toggleUrl()}}/>
                            <View style={{height:window.height*0.38, marginTop:-0.9*window.height, borderRadius:15, justifyContent:"center", padding:15, backgroundColor:(this.props.background_color ? this.props.background_color : "white")}}>
                                <AHWebView
                                    style={{/*height:wh-50,*/ width:window.width - 30}}
                                    source={{uri: this.props.fact_url}}
                                    originWhitelist={[this.props.fact_url]}
                                    scalesPageToFit={true}
                                />
                            </View>
                        </View>
                    </Modal>
                    <TouchableOpacity
                        style={{flex:1}}//, width:"100%", height:"100%"}}
                        onPress={() => {
                            //this.props.toggleExpandedFact(this.props.fact_id);
                            this.props.navigation.navigate("FactScreen", {fact: this.props.fact_obj});
                        }}>
                        <Grid /*style={{height:100}}*/>
                            <Row>
                                <Col size={4} style={{backgroundColor:"#ffffff00"}}>
                                    <Row style={{marginBottom:6, marginTop:2}}>
                                        <Col size={1} style={{alignItems:"center"}}>
                                            <Text>{this.props.fact_obj["StartDate"].split("T")[0].split("-").slice(1,3).reverse().join("/")}</Text>
                                        </Col>
                                        <Col size={4}>
                                            <Text style={{fontWeight:'bold'}}>{this.props.fact_name}</Text>
                                        </Col>
                                    </Row>
                                    <Row style={{marginBottom:3}}>
                                        <Col size={1}  style={{alignItems:"center"}}>
                                            <Text style={{fontWeight:'bold'}}>{this.props.time_start + " -\n"+this.props.time_end}</Text>
                                        </Col>
                                        <Col size={4}>
                                            <Text>
                                                {
                                                    ((description) => {
                                                        let split_desc = description.split(" ");
                                                        if (split_desc.length > 12) {
                                                            return (split_desc.slice(0, 12).join(" ")+"...");
                                                        } else {
                                                            return (split_desc.slice(0, 12).join(" "));
                                                        }
                                                    })(this.props.fact_description)
                                                }
                                            </Text>
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                            <Row>
                                <Col size={1}></Col>
                                <Col size={4}>
                                    {this.props.fact_place &&
                                        <Row style={{marginBottom:5}}>
                                            <Ionicons style={{alignSelf:"center", marginRight:3}} color={(this.props.accent_color ? this.props.accent_color : "rgb(169,25,59)")} size={14} name={"ios-pin"}/>
                                            <Text>{this.props.fact_place}</Text>
                                        </Row>
                                    }
                                    {this.props.fact_rating != null &&
                                        <Row style={{marginBottom:5}}>
                                            {((rating) => {
                                                let show = [];
                                                for (let i = 0; i < 5; i++) {
                                                    if (i <= rating-1) {
                                                        show.push(<Ionicons color={this.props.accent_color} style={{alignSelf:"center", marginRight:2}} size={16} name={"ios-star"}/>);
                                                    } else {
                                                        show.push(<Ionicons color={this.props.accent_color} style={{alignSelf:"center", marginRight:2}} size={16} name={"ios-star-outline"}/>);
                                                    }
                                                }
                                                return (show);
                                            })(this.props.fact_rating)}
                                            {/*<Ionicons style={{alignSelf:"center", marginRight:3}} size={14} name={"ios-pin"}/>*/}
                                            <Text style={{marginLeft:3}}>{this.props.fact_rating}</Text>
                                        </Row>
                                    }
                                    {this.props.fact_url != null && this.props.fact_url != "" &&
                                        <Row style={{marginBottom:5}}>
                                            <TouchableOpacity
                                                style={{flexDirection:"row"}}
                                                onPress={() => {
                                                    this.setState({url_open:true});
                                                }}
                                            >
                                                <Ionicons color={(this.props.accent_color ? this.props.accent_color : "rgb(169,25,59)")} style={{alignSelf:"center", marginRight:3}} size={20} name={"ios-play-circle"}/>
                                                <Text style={{marginLeft:5}}>{"Трансляция"}</Text>
                                            </TouchableOpacity>
                                        </Row>
                                    }
                                </Col>
                            </Row>
                        </Grid>
                    </TouchableOpacity>
                </Card>
            );
        }
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        lang:               state.data.settings.lang,
        expanded:           state.control.expanded_fact_id,
    }
};

const mapDispatchToProps = dispatch => {
    return {
        toggleExpandedFact: (fact_id) => dispatch(toggleExpandedFact({fact_id}))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(FactCard);
