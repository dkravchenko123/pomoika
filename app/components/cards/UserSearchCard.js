import React from "react";
import {Row} from "native-base";
import {Modal, ScrollView, Text, TextInput, TouchableOpacity, View} from "react-native";
import {backendRequest, backendRequestCustomSocket, backendRequestPromise} from "../../methods/ws_requests";
import {connect} from "react-redux";
import {WS_URL} from "../../constants/backend";
import {addContacts} from "../../actions/data";
import UserDisplayCard from "./UserDisplayCard";
import {ModalBottom} from "../overlays/ModalBottom";
import ChatUserInfoCard from "./ChatUserInfoCard";
import {dropIndex, fullCompare} from "../../methods/array_methods";
import {Ionicons} from '@expo/vector-icons'
import {localeStr} from "../../methods/translation_provider";


class UserSearchCard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            user_results:[],
            search_limit:40
        };
        this.search_timeout = null;
        this._closeModal = this._closeModal.bind(this);
        this._chooseUser = this._chooseUser.bind(this);
        this.inputRef = React.createRef();
    }

    _closeModal () {
        this.setState({modal_user:null});
    }

    _chooseUser (user) {
        let user_ind = this.state.user_results.findIndex((el) => fullCompare(el, user));
        console.log("index of chosen user", user_ind);
        let new_users = dropIndex([...this.state.user_results], user_ind);
        console.log("new users", new_users);
        this.props.choose_user(user);
        this.setState({user_results:new_users, modal_user:null});
    }

    render() {
        return (
            <>
            <View style={{ backgroundColor: (this.props.transparent ? 'transparent' : 'white'), position: 'relative' }}>
                {this.state.modal_user != null &&
                    <Modal
                        style={{position:"absolute", top:0, left:0, right:0, bottom:0}}
                        transparent
                        animationType="fade"
                        visible={this.state.modal_user != null}
                        onRequestClose={() => {
                            setTimeout(() => {this._closeModal()}, 200);
                        }}
                    >
                        <ModalBottom
                            closeModal={this._closeModal}
                        >
                            <ChatUserInfoCard
                                {...this.state.modal_user}
                                addressee_matrix_id={this.state.modal_user.matrix_id}
                                action_buttons={
                                    this.props.custom_action_buttons != null
                                        ?
                                            this.props.custom_action_buttons(this.state.modal_user)
                                        :
                                            [
                                                <TouchableOpacity
                                                    onPress={() => {
                                                        this._chooseUser();
                                                    }}
                                                    style={{width:100, height:30, borderRadius:15, justifyContent:"center", alignItems:"center", backgroundColor:"rgb(169,25,59)"}}
                                                >
                                                    <Text style={{color:"white"}}>Добавить</Text>
                                                </TouchableOpacity>,
                                            ]
                                }
                            />
                        </ModalBottom>
                    </Modal>
                }

                {/* <Row style={{ height:50, justifyContent:"flex-start", alignItems:"center", flex:1, borderWidth:2, borderRadius:this.props.square ? 5 : 25, borderColor:"rgba(220,219,216, 0.2)", backgroundColor:"white"}}> */}
                <View style={{flexDirection:"row", width:"100%", alignItems:"center"}}>
                    <TextInput
                        style={[{flex:1, backgroundColor: 'rgba(220,219,216, 0.6)', paddingLeft:10, fontSize: 16, minHeight: 50, borderRadius: 10 }]}
                        placeholderTextColor={'gray'}
                        ref={this.inputRef}
                        placeholder={this.props.lang === 'ru' ? "Найти участников" : 'Find participants'}
                        onChangeText={(input) => {
                            if (this.search_timeout) clearTimeout(this.search_timeout);
                            this.search_timeout = setTimeout(() => {
                                console.log("onChangeText input " + input);
                                this.setState({
                                    user_results:[],
                                });
                                if (input != "") {
                                    backendRequestPromise(
                                        "calendarGetPeople",
                                        this.props.userToken,
                                        {DesiredP:input, Count:this.state.search_limit}
                                    ).then((response) => {
                                        if (response != null) {
                                            console.log("found "+response.length);
                                            if (response.length > 0) {
                                                this.setState({
                                                    user_results:response,
                                                });
                                            }
                                        }
                                    });
                                    /*let search_socket = new WebSocket(WS_URL);
                                    search_socket.onmessage = (msg) => {
                                        let parsed_msg = JSON.parse(msg.data);
                                        console.log(parsed_msg);
                                        let response = parsed_msg.data;
                                        if (response != null) {
                                            console.log("found "+response.length);
                                            if (response.length > 0) {
                                                this.setState({
                                                    user_results:response,
                                                });
                                            }
                                        }
                                        search_socket.close();
                                    };
                                    search_socket.onopen = (() => {backendRequestCustomSocket(search_socket, "calendarGetPeople", this.props.userToken, {DesiredP:input, Count:this.state.search_limit})});*/
                                }
                            }, 30);
                        }}/>
                        <Ionicons style={{position:"absolute", right:20}} name='ios-search' size={30} color={'gray'} />
                        </View>
                {/* </Row> */}
            </View>
            {this.state.user_results.length != 0 &&
                <ScrollView nestedScrollEnabled={true} style={{marginVertical:15, maxHeight:280 }}>
                    {this.state.user_results.filter((user) => this.props.accept_anyone || !!user.matrix_id).map((user, index) => {
                        //console.log("rendering user search card", user);
                        return (
                            <TouchableOpacity
                                    key={JSON.stringify(user)}
                                onPress={() => {
                                    /*this.setState({
                                        modal_user:user
                                    });*/
                                    this._chooseUser(user);
                                }}
                            >
                                <UserDisplayCard {...user} addressee_matrix_id={user.matrix_id}/>
                            </TouchableOpacity>
                        );
                    })}
                </ScrollView>
            }
            </>
        )
    }
}

const mapStateToProps = state => {
    return {
        lang:               state.data.settings.lang,
        userToken:          state.data.userToken
    }
};

const mapDispatchToProps = dispatch => {
    return {
        addContacts:             (contact) => dispatch(addContacts({contact}))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(UserSearchCard);
