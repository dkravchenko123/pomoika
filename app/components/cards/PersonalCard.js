import React from "react";
import {
    Alert,
    ActionSheetIOS,
    ActivityIndicator,
    Dimensions,
    Image,Platform,
    ScrollView,
    StyleSheet,
    Text,
    TouchableOpacity,
    View
} from "react-native";
import {Button, Card, Col, Content, Container, Grid, Picker, Row} from "native-base";
import {Overlay} from "react-native-elements";
import {SimpleLineIcons, Ionicons, MaterialCommunityIcons} from "@expo/vector-icons";
import {get_user_info} from "../../methods/account_requests";
import {cancel_reconnect, getws, init_timeout} from "../../methods/webSocket";
import {changeLang, initEmptyForm, receiveData, removeData, setLogin, setPassword} from "../../actions/data";
import {connect} from "react-redux";
import {EventCard} from "./event_card";
import {disableNetWarn, enableNetWarn, togglePersonalInfoOverlay} from "../../actions/control";
import {backendRequest, backendRequestCustomSocket, extractResponse} from "../../methods/ws_requests";
import storeExport from "../../store";
import {WS_URL} from "../../constants/backend";
import {ButtonsOverGradient} from "../headers_footers/buttons_over_gradient";
import {BusinessCard} from "./BusinessCard";
import field from "../../styles/fields";
import {clearchat} from "../../methods/chat_client";
import {SettingsInteractionCard, SettingsInteractionCardItem} from "./SettingsInteractionCard";
import WebImage from "../../WebImage";

const {store, persistor} = storeExport();

const window = Dimensions.get("window");

const BUTTONS_RU = [
    "Мероприятия",
    "Заказы",
    "Настройки",
];


class PersonalCard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isReady: false,
            hasCache:false,
            first_name:"",
            last_name:"",
            middle_name:"",
            active_tab:0,
            new_requests_active:true,
            old_requests_active:true,
            notifications_active: true
        };
        this.change_active = this.change_active.bind(this);
    }

    change_active (num) {
        this.setState({active_tab:num})
    }

    componentDidMount() {
        if (Platform.OS != "ios" && Platform.OS != "android") {
            const brokenActionSheetOnWeb = document.querySelector(
                '#root div + div[style*="align-self: flex-start; border-color: rgb(255, 0, 0); border-width: 1px;"]',
            );
            if (brokenActionSheetOnWeb) {
                console.log("found red box");
                brokenActionSheetOnWeb.style.display = 'none'
            } else {
                console.log("have not found the red box", brokenActionSheetOnWeb);
            }
        }

        setTimeout(() => {
            this.setState({
                updateTab:true
            })
        }, 10);
        if (this.state.hasCache) {
            this.setState({
                isReady:true
            });
        } else {
            //init_timeout(3000, this.props.enableNetWarn, this.props.disableNetWarn);
            let info_socket = new WebSocket(WS_URL);
            info_socket.onmessage = (msg) => {
                //cancel_reconnect();
                let parsed_msg = JSON.parse(msg.data);
                console.log(parsed_msg);
                if (parsed_msg.statusCode == 200) {
                    let response = parsed_msg.data[0];
                    console.log(response);
                    this.setState({
                        last_name:this.props.lang == "ru" ? response.lastnamerus : response.lastnameeng,
                        first_name:this.props.lang == "ru" ? response.firstnamerus : response.firstnameeng,
                        middle_name:this.props.lang == "ru" ? response.middlenamerus : response.middlenameeng,
                        org_namelong:response.org_namelong,// || "Компания",
                        titlename:response.titlename,// || "Должность",
                        image_url:response.userphoto,
                        isReady:true
                    });
                }
                info_socket.close();
                //this.props.receiveData(parsed_msg);
            };
            //get_user_info(info_socket, this.props.userToken);
            //this.props.removeData("getUserInfo");
            info_socket.onopen = () => {backendRequestCustomSocket(info_socket, "getUserInfo", this.props.userToken);}

            let user_events_socket = new WebSocket(WS_URL);
            user_events_socket.onmessage = (msg) => {
                //cancel_reconnect();
                let parsed_msg = JSON.parse(msg.data);
                console.log(parsed_msg);
                if (parsed_msg.statusCode == 200) {
                    let response = parsed_msg.data;
                    //console.log(response);
                    if (response.length > 0) {
                        let new_user_events = {};
                        response.forEach((el) => {
                            if (!new_user_events.hasOwnProperty(el.level_id)) {
                                console.log("new levelid", el.level_id);
                                new_user_events[el.level_id] = [];
                            }
                            new_user_events[el.level_id] = new_user_events[el.level_id].concat(el);
                        });
                        this.setState({
                            user_events: new_user_events
                        });
                    }
                }
                user_events_socket.close();
                //this.props.receiveData(parsed_msg);
            };
            //get_user_info(user_events_socket, this.props.userToken);
            //this.props.removeData("getUserInfo");
            user_events_socket.onopen = () => {backendRequestCustomSocket(user_events_socket, "getUserEvents", this.props.userToken);}
        }
    }

    render() {
        return (
            <View style={{width:window.width*0.3-2, height:window.height, backgroundColor:"rgb(240,240,240)"}}>
                {this.state.isReady
                    ?
                    <View style={{width:"100%", height:window.height, backgroundColor:"rgb(246,246,246)", flex:1}}>
                        <View style={{height:110, padding:15, flexDirection:"row", backgroundColor:"white", elevation:15}}>
                            <View style={{flex:2, backgroundColor:"#ffffff00"}}>
                                {this.state.image_url && this.props.userToken
                                    ?
                                    <WebImage
                                        style={{ width:80, height:80, borderRadius:40}}
                                        source={{
                                            uri: this.state.image_url,
                                            method: "GET",
                                            headers: {
                                                "Authorization":this.props.userToken.toString()
                                            }
                                        }}/>
                                    :
                                    <View style={{ borderRadius:45, width:90, height:90, alignSelf:"center", justifyContent:"center", alignItems:"center", textAlign:"center", backgroundColor:"rgb(117,120,123)"}}><Text style={{fontSize:38, fontWeight:"bold", color:"#fff"}}>?</Text></View>
                                }
                            </View>
                            <View style={{flex:4, flexDirection:"column", justifyContent:"center", backgroundColor:"#ffffff00"}}>
                                { this.state.last_name != "" && <Text style={{fontWeight:"bold", fontSize:18, color:"#000"}}>{this.state.last_name}</Text>}
                                { this.state.first_name != "" && <Text style={{fontWeight:"bold", fontSize:18, color:"#000"}}>{this.state.first_name}</Text>}
                                { this.state.middle_name != "" && <Text style={{fontWeight:"bold", fontSize:18, color:"#000"}}>{this.state.middle_name}</Text>}
                                { this.state.org_namelong != null && this.state.titlename != null &&
                                    <View style={{flexDirection:"column"}}>
                                        <Text style={{fontSize:14, color:"#000"}}>{this.state.org_namelong+","}</Text>
                                        <Text style={{fontSize:14, color:"#000"}}>{this.state.titlename}</Text>
                                    </View>
                                }
                            </View>

                            <View style={{flex:1, flexDirection:"column", justifyContent:"flex-end", }}>
                                <TouchableOpacity
                                    onPress={() => {
                                        this.props.navigation.navigate(
                                            "AccountFullscreenWithChildren",
                                            {
                                                children: () => {
                                                    return (
                                                        <ScrollView>
                                                            {[...Array(5)].map((el, ind) => {
                                                                return <BusinessCard
                                                                    key={ind.toString()}
                                                                    data={{
                                                                        name:"Фамилия Имя Отчество",
                                                                        title:"Должность",
                                                                        email: "email@website.com",
                                                                        phone: "+79991234567",
                                                                        website: "www.website.com"
                                                                    }}
                                                                />
                                                            })}
                                                        </ScrollView>
                                                    );
                                                }
                                            });
                                    }}
                                >
                                    <View>
                                        <MaterialCommunityIcons
                                            size={40}
                                            style={{
                                                color:"#000",
                                            }}
                                            name={"account-card-details"}
                                        />
                                    </View>
                                </TouchableOpacity>
                                {/*<TouchableOpacity
                                    style={{marginBottom:-35, width:50, height:50, borderRadius:25, backgroundColor:"rgb(134,38,51)", alignItems:"center", justifyContent:'center'}}
                                    onPress={() => {
                                        console.log("settings pressed");
                                        this.props.initEmptyForm({key:"pers_setting"});
                                        this.props.initEmptyForm({key:"passport"});
                                        this.props.initEmptyForm({key:"addorgtitle"});
                                        this.props.initEmptyForm({key:"adddocuments"});
                                        this.props.initEmptyForm({key:"cars"});
                                        this.props.initEmptyForm({key:"photo_video"});
                                        //this.props.toggle();
                                        this.props.navigation.navigate("UpdatePersonalInfoCard");
                                    }}
                                >
                                    <View>
                                        <MaterialCommunityIcons
                                            size={36}
                                            style={{
                                                color:"#fff",

                                            }}
                                            name={"settings"}
                                        />
                                    </View>
                                </TouchableOpacity>*/}
                            </View>
                        </View>

                        <View zIndex={10}>
                            <ButtonsOverGradient zIndex={10} init_active={0} active_call={this.change_active} buttons={BUTTONS_RU}/>
                        </View>
                        <Content zIndex={5}  nestedScrollEnabled style={{flex:1,  flexGrow:1}}>
                            {((num) => {
                                switch(num) {
                                    case 0:
                                        return <ScrollView scrollEnabled style={{flex:1, height:"100%"}}>

                                            {this.state.user_events && Object.keys(this.state.user_events).length > 0 &&
                                                Object.keys(this.state.user_events).map((key) => {
                                                    return (
                                                        <SettingsInteractionCard
                                                            collapsible
                                                            label={this.state.user_events[key][0].level_label+":"}
                                                        >
                                                            {this.state.user_events[key].map((elem, index, arr) => {
                                                                return <TouchableOpacity style={{height:76, borderTopWidth:1, paddingVertical:3, flexDirection:"column", justifyContent:"center", alignItems:"flex-start", borderBottomWidth:(index == (arr.length - 1) ? 1 : 0), borderColor:"rgb(220,219,216)", backgroundColor:"rgb(246,246,246)"}}>
                                                                    <View style={{flexDirection:"row"}}>
                                                                        {elem.logo != null && this.props.userToken &&
                                                                        <Image
                                                                            style={{height:30, width:30, borderRadius:15, marginLeft:20, marginRight:-10, resizeMode:"cover"}}
                                                                            source={{
                                                                                uri:elem.logo,
                                                                                method: "GET",
                                                                                headers: {
                                                                                    "Authorization":this.props.userToken.toString()
                                                                                }
                                                                            }}
                                                                        />
                                                                        }
                                                                        <Text style={{
                                                                            fontSize: 16,
                                                                            color: "#000",
                                                                            marginHorizontal:20,
                                                                        }}>{elem.name}</Text>
                                                                    </View>
                                                                    <View style={{paddingHorizontal: 20, marginTop:6, width:"100%", flexDirection:"row", justifyContent:"space-between"}}>
                                                                        <Text>{elem.start_date.split("T")[0].split("-").reverse().join("/") + " - " + elem.end_date.split("T")[0].split("-").reverse().join("/")}</Text>
                                                                        <Text style={{fontWeight:"bold"}}>{"Подробнее..."}</Text>
                                                                    </View>
                                                                </TouchableOpacity>
                                                            })}
                                                        </SettingsInteractionCard>
                                                    );
                                                })
                                            }

                                            <View style={{height:150}} />
                                        </ScrollView>;
                                    case 1:
                                        return <ScrollView scrollEnabled style={{flex:1, height:"100%"}}>

                                            <SettingsInteractionCard collapsible label={"Актуальное:"}>
                                                <SettingsInteractionCardItem
                                                    top bottom
                                                    label={"Заказ съемки"}
                                                    onPress={() => {
                                                        this.props.initEmptyForm({key:"photo_video"});
                                                        this.props.navigation.navigate("UpdatePersonalInfoCard", {key:"photo_video"});
                                                    }}
                                                />

                                                <SettingsInteractionCardItem
                                                    bottom
                                                    label={"Заказ такси"}
                                                />
                                            </SettingsInteractionCard>

                                            <SettingsInteractionCard collapsible label={"Архив:"} />

                                            {/*<View style={{flexDirection:"column", justifyContent:"center", backgroundColor:"white", width:"100%", marginTop:15, paddingBottom:10, elevation:15}}>

                                                <TouchableOpacity
                                                    style={{flexDirection:"row", justifyContent:"space-between", alignItems:"center", width:"100%", paddingRight:20}}
                                                    onPress={() => {
                                                        this.setState({[`new_requests_active`]:!this.state[`new_requests_active`]});
                                                    }}
                                                >
                                                    <Text style={{fontSize:14, marginVertical:12, marginHorizontal:20, color:"rgb(169,25,59)"}}>{"Актуальное:"}</Text>
                                                    <Ionicons size={18} color={"rgb(169,25,59)"} name={this.state[`new_requests_active`] != false ? "ios-remove" : "ios-add"}/>
                                                </TouchableOpacity>

                                                {this.state[`new_requests_active`] != false &&
                                                    <View>
                                                        <TouchableOpacity
                                                            style={{height:40, borderTopWidth:1, justifyContent:"center", borderColor:"rgb(220,219,216)", backgroundColor:"rgb(246,246,246)"}}
                                                            onPress={() => {
                                                                this.props.initEmptyForm({key:"photo_video"});
                                                                this.props.navigation.navigate("UpdatePersonalInfoCard", {key:"photo_video"});
                                                            }}
                                                        >
                                                            <Text style={{
                                                                fontSize: 16,
                                                                color: "#000",
                                                                marginHorizontal:20,
                                                            }}>{"Заказ съемки"}</Text>
                                                        </TouchableOpacity>

                                                        <TouchableOpacity style={{height:40, borderTopWidth:1, justifyContent:"center", borderBottomWidth:1, borderColor:"rgb(220,219,216)", backgroundColor:"rgb(246,246,246)"}}>
                                                            <Text style={{
                                                                fontSize: 16,
                                                                color: "#000",
                                                                marginHorizontal:20,
                                                            }}>{"Заказ такси"}</Text>
                                                        </TouchableOpacity>
                                                    </View>
                                                }

                                            </View>*/}

                                            {/*<View style={{height:50}} />
                                            <View style={{flexDirection:"column", justifyContent:"center", backgroundColor:"white", width:"100%", marginTop:15, paddingBottom:10, elevation:15}}>

                                                <TouchableOpacity
                                                    style={{flexDirection:"row", justifyContent:"space-between", alignItems:"center", width:"100%", paddingRight:20}}
                                                    onPress={() => {
                                                        this.setState({[`old_requests_active`]:!this.state[`old_requests_active`]});
                                                    }}
                                                >
                                                    <Text style={{fontSize:14, marginVertical:12, marginHorizontal:20, color:"rgb(169,25,59)"}}>{"Архив:"}</Text>
                                                    <Ionicons size={18} color={"rgb(169,25,59)"} name={this.state[`old_requests_active`] != false ? "ios-remove" : "ios-add"}/>
                                                </TouchableOpacity>

                                            </View>*/}

                                            <View style={{height:150}} />
                                        </ScrollView>;
                                    case 2:
                                        return <ScrollView scrollEnabled style={{flex:1, height:"100%"}}>

                                            <SettingsInteractionCard collapsible label={"Обновление данных:"}>
                                                <SettingsInteractionCardItem
                                                    top bottom
                                                    label={"Персональные данные"}
                                                    onPress={() => {
                                                        this.props.initEmptyForm({key:"pers_setting"});
                                                        this.props.navigation.navigate("UpdatePersonalInfoCard", {key:"pers_setting"});
                                                    }}
                                                />
                                                <SettingsInteractionCardItem
                                                    bottom
                                                    label={"Паспортная информация"}
                                                    onPress={() => {
                                                        this.props.initEmptyForm({key:"passport"});
                                                        this.props.navigation.navigate("UpdatePersonalInfoCard", {key:"passport"});
                                                    }}
                                                />
                                                <SettingsInteractionCardItem
                                                    bottom
                                                    label={"Организация"}
                                                    onPress={() => {
                                                        this.props.initEmptyForm({key:"addorgtitle"});
                                                        this.props.navigation.navigate("UpdatePersonalInfoCard", {key:"addorgtitle"});
                                                    }}
                                                />

                                            </SettingsInteractionCard>

                                            <SettingsInteractionCard collapsible label={"Чат:"}>
                                                <SettingsInteractionCardItem
                                                    top bottom
                                                    label={"Фильтры"}
                                                    onPress={() => {
                                                        Alert.alert("Ошибка", "Нет доступных фильтров!");
                                                    }}
                                                />
                                                <SettingsInteractionCardItem
                                                    bottom
                                                    image={<Ionicons style={{marginRight: 8}} size={16} name={this.state.notifications_active != false ? "ios-radio-button-on" : "ios-radio-button-off"}/>}
                                                    label={this.state.notifications_active != false ? "Уведомления включены" : "Уведомления выключены"}
                                                    onPress={() => {
                                                        this.setState({notifications_active: !this.state.notifications_active});
                                                    }}
                                                />
                                            </SettingsInteractionCard>



                                            <SettingsInteractionCard label={"Язык:"}>
                                                <View style={{backgroundColor:"rgb(246,246,246)", width:"100%", borderTopWidth:1, borderBottomWidth:1, borderColor:"rgb(220,219,216)"}}>
                                                    {Platform.OS == "ios"
                                                        ?
                                                        <TouchableOpacity
                                                            onPress={() => {
                                                                /*if (Platform.OS == 'ios') ActionSheetIOS.showActionSheetWithOptions(
                                                                    {
                                                                        options: ["Русский", "English"],
                                                                    },
                                                                    (buttonIndex) => {
                                                                        if (buttonIndex == 0) this.props.ch_lang("ru");
                                                                        if (buttonIndex == 1) this.props.ch_lang("en");
                                                                    },
                                                                );*/
                                                            }}
                                                        >
                                                            <View
                                                                style={{
                                                                    marginHorizontal: 20,
                                                                    height: 40,
                                                                    borderTopWidth: 1,
                                                                    justifyContent: "center", /*borderBottomWidth:1,*/
                                                                    borderColor: "rgb(220,219,216)",
                                                                    backgroundColor: "rgb(246,246,246)"
                                                                }}
                                                            >
                                                                <Text>{this.props.lang == "ru" ? "Русский" : "English"}</Text>
                                                            </View>
                                                        </TouchableOpacity>
                                                        :
                                                        <Picker
                                                            selectedValue={this.props.lang}
                                                            onValueChange={(itemValue, itemIndex) => {
                                                                if (itemValue != "0") this.props.ch_lang(itemValue);
                                                            }}
                                                            style={{
                                                                marginHorizontal: 20,
                                                                height: 40,
                                                                borderTopWidth: 1,
                                                                justifyContent: "center", /*borderBottomWidth:1,*/
                                                                borderColor: "rgb(220,219,216)",
                                                                backgroundColor: "rgb(246,246,246)"
                                                            }}
                                                        >
                                                            <Picker.Item
                                                                value={"ru"}
                                                                label={"Русский"}
                                                                style={{paddingLeft: 20}}
                                                            />
                                                            <Picker.Item
                                                                value={"en"}
                                                                label={"English"}
                                                                style={{paddingLeft: 20}}
                                                            />
                                                        </Picker>
                                                    }
                                                </View>
                                            </SettingsInteractionCard>

                                            {/*<View style={{flexDirection:"column", justifyContent:"center", backgroundColor:"white", width:"100%", marginTop:15, paddingBottom:10, elevation:15}}>
                                                <Text style={{fontSize:14, marginVertical:12, marginHorizontal:20, color:"rgb(169,25,59)"}}>{"Обновление данных:"}</Text>

                                                <TouchableOpacity
                                                    onPress={() => {
                                                        this.props.initEmptyForm({key:"pers_setting"});
                                                        this.props.navigation.navigate("UpdatePersonalInfoCard", {key:"pers_setting"});
                                                    }}
                                                    style={{height:40, borderTopWidth:1, justifyContent:"center",  borderColor:"rgb(220,219,216)", backgroundColor:"rgb(246,246,246)"}}
                                                >
                                                    <Text style={{
                                                        fontSize: 16,
                                                        color: "#000",
                                                        marginHorizontal:20,
                                                    }}>{"Персональные данные"}</Text>
                                                </TouchableOpacity>

                                                <TouchableOpacity
                                                    onPress={() => {
                                                        this.props.initEmptyForm({key:"passport"});
                                                        this.props.navigation.navigate("UpdatePersonalInfoCard", {key:"passport"});
                                                    }}
                                                    style={{height:40, borderTopWidth:1, justifyContent:"center",  borderColor:"rgb(220,219,216)", backgroundColor:"rgb(246,246,246)"}}
                                                >
                                                    <Text style={{
                                                        fontSize: 16,
                                                        color: "#000",
                                                        marginHorizontal:20,
                                                    }}>{"Паспортная информация"}</Text>
                                                </TouchableOpacity>

                                                <TouchableOpacity
                                                    onPress={() => {
                                                        this.props.initEmptyForm({key:"addorgtitle"});
                                                        this.props.navigation.navigate("UpdatePersonalInfoCard", {key:"addorgtitle"});
                                                    }}
                                                    style={{height:40, borderTopWidth:1, justifyContent:"center", borderBottomWidth:1, borderColor:"rgb(220,219,216)", backgroundColor:"rgb(246,246,246)"}}
                                                >
                                                    <Text style={{
                                                        fontSize: 16,
                                                        color: "#000",
                                                        marginHorizontal:20,
                                                    }}>{"Организация"}</Text>
                                                </TouchableOpacity>

                                            </View>*/}

                                            {/*<View style={{flexDirection:"column", justifyContent:"center", backgroundColor:"white", width:"100%", marginTop:15, paddingBottom:10, elevation:15}}>
                                                <Text style={{fontSize:14, marginVertical:12, marginHorizontal:20, color:"rgb(169,25,59)"}}>{"Язык:"}</Text>
                                                <View style={{backgroundColor:"rgb(246,246,246)", width:"100%", borderTopWidth:1, borderBottomWidth:1, borderColor:"rgb(220,219,216)"}}>
                                                    {Platform.OS == "ios"
                                                        ?
                                                            <TouchableOpacity
                                                                onPress={() => {
                                                                    if (Platform.OS == 'ios') ActionSheetIOS.showActionSheetWithOptions(
                                                                        {
                                                                            options: ["Русский", "English"],
                                                                        },
                                                                        (buttonIndex) => {
                                                                            if (buttonIndex == 0) this.props.ch_lang("ru");
                                                                            if (buttonIndex == 1) this.props.ch_lang("en");
                                                                        },
                                                                    );
                                                                }}
                                                            >
                                                                <View
                                                                    style={{
                                                                        marginHorizontal: 20,
                                                                        height: 40,
                                                                        borderTopWidth: 1,
                                                                        justifyContent: "center",
                                                                        borderColor: "rgb(220,219,216)",
                                                                        backgroundColor: "rgb(246,246,246)"
                                                                    }}
                                                                >
                                                                    <Text>{this.props.lang == "ru" ? "Русский" : "English"}</Text>
                                                                </View>
                                                            </TouchableOpacity>
                                                        :
                                                            <Picker
                                                                selectedValue={this.props.lang}
                                                                onValueChange={(itemValue, itemIndex) => {
                                                                    if (itemValue != "0") this.props.ch_lang(itemValue);
                                                                }}
                                                                style={{
                                                                    marginHorizontal: 20,
                                                                    height: 40,
                                                                    borderTopWidth: 1,
                                                                    justifyContent: "center",
                                                                    borderColor: "rgb(220,219,216)",
                                                                    backgroundColor: "rgb(246,246,246)"
                                                                }}
                                                            >
                                                                <Picker.Item
                                                                    value={"ru"}
                                                                    label={"Русский"}
                                                                    style={{paddingLeft: 20}}
                                                                />
                                                                <Picker.Item
                                                                    value={"en"}
                                                                    label={"English"}
                                                                    style={{paddingLeft: 20}}
                                                                />
                                                            </Picker>
                                                    }
                                                </View>

                                            </View>*/}

                                            <View
                                                style={{width:"100%", height:40, marginTop:4}}
                                            >
                                                <TouchableOpacity
                                                    style={{width:"100%", height:40, flexDirection:"row", justifyContent:"flex-start", alignItems:"center"}}
                                                    onPress={() => {
                                                        console.log("logout pressed");
                                                        this.props.setLogin("");
                                                        this.props.setPassword("");
                                                        clearchat();
                                                        persistor.purge().then(() => {
                                                            this.props.navigation.navigate("Login");
                                                        });
                                                    }}
                                                >
                                                    <Text style={{
                                                        fontSize: 18,
                                                        color: "rgb(134,38,51)",
                                                        marginHorizontal:20,
                                                    }}>Выйти</Text>
                                                </TouchableOpacity>
                                            </View>

                                            <View style={{height:150}} />
                                        </ScrollView>;
                                }
                            })(this.state.active_tab)}

                        </Content>
                    </View>
                    :
                    <View style={{marginTop:10}}>
                        <ActivityIndicator size={"small"} color={"#000"}/>
                    </View>
                }
            </View>
        );
    }

}

const mapStateToProps = state => {
    return {
        userToken:      state.data.userToken,
        lang:           state.data.settings.lang
    }
};

const mapDispatchToProps = dispatch => {
    return {
        ch_lang:            (lang) => dispatch(changeLang(lang)),
        setLogin:           (login) => dispatch(setLogin({login})),
        setPassword:        (password) => dispatch(setPassword({password})),
        receiveData:        (data) => dispatch(receiveData(data)),
        toggle:             () => dispatch(togglePersonalInfoOverlay()),
        initEmptyForm:      (data) => dispatch(initEmptyForm(data)),
        removeData:         (key) => dispatch(removeData({key})),
        enableNetWarn:      () => dispatch(enableNetWarn()),
        disableNetWarn:     () => dispatch(disableNetWarn()),
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(PersonalCard);
