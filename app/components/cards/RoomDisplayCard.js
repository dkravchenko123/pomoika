import {ActivityIndicator, Image, PixelRatio, ScrollView, Text, TouchableOpacity, View} from "react-native";
import React from "react";
import {ActionSheet, Col, Grid, Row} from "native-base";
import {
    addAlias,
    addCanAlias, addContacts,
    addInvite,
    addJoined, addRooms,
    receiveData,
    removeData, removeInvite, removeJoined, setChatDms,
    setChatToken,
    updMessages
} from "../../actions/data";
import {connect} from "react-redux";
import {getchat} from "../../methods/chat_client";
import card from "../../styles/cards";
import {cancel_reconnect, getws, init_timeout} from "../../methods/webSocket";
import {
    backendRequestCustomSocket,
    extractManyResponses,
    extractResponse
} from "../../methods/ws_requests";
import {CHAT_URL, WS_URL} from "../../constants/backend";
import {disableNetWarn, enableNetWarn} from "../../actions/control";
import button from "../../styles/buttons";
import {dropIndex, fullCompare} from "../../methods/array_methods";
import WebImage from "../../WebImage";

const BUTTONS = [
    {
        text: "Удалить чат",
        icon: "close"
    },
    {
        text: "Отмена",
        //icon: "exit"
    }
];

const chatbots = {
    "AccreditationFTh":"БОТ Аккредитация",
    "fth":"Бот"
};

class RoomDisplayCard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            avatar:this.props.rooms && this.props.rooms.hasOwnProperty(this.props.room_id) ? this.props.rooms[this.props.room_id].avatar : null,
            full_name:this.props.rooms && this.props.rooms.hasOwnProperty(this.props.room_id) ? this.props.rooms[this.props.room_id].full_name : null,
            last_sender:null,
            loading_name:false,
            loading_msgs:false,
            loading_room_name:false,

            name_req_id: Math.floor(Math.random()*99999),
            room_name_req_id: Math.floor(Math.random()*99999),
            msgs_req_id: Math.floor(Math.random()*99999)
        };

        this.room_search_string = "";
        //this.room_search_string = "";
        this.loading = false;
        this.new_state = {};
        this.client = getchat();
        this.is_not_group = false;
        this.is_dm = false;
        this._getName = this._getName.bind(this);
    }

    _getName() {
        if (!this.props.rooms.hasOwnProperty(this.props.room_id)) {
            this.loading = true;
            let dm_room_socket = new WebSocket(WS_URL);
            dm_room_socket.onmessage = (msg) => {
                this.loading = false;
                //cancel_reconnect();
                let parsed_msg = JSON.parse(msg.data);
                console.log(parsed_msg);
                //this.props.receiveData(parsed_msg);
                if (parsed_msg.statusCode == 200) {
                    let msg_data = parsed_msg.data;
                    //console.log("using ",msg_data);
                    if (!msg_data.room_type) {
                        let new_dms = this.props.dms;
                        console.log("is a dm, current dms:", this.props.dms);
                        if (/*new_dms.length > 0 && */new_dms.findIndex((el) => el == msg_data.addressee_matrix_id) == -1) {
                            this.props.setChatDms(new_dms.concat(msg_data.addressee_matrix_id));
                        }
                    }
                    this.props.addRooms({[this.props.room_id]:{
                            addressee_matrix_id: msg_data.addressee_matrix_id,
                            full_name: msg_data.room_name,
                            avatar: msg_data.image_url,
                            room_type: msg_data.room_type
                        }});
                    this.setState({
                        loading_name: false,
                        addressee_matrix_id: msg_data.addressee_matrix_id,
                        full_name: msg_data.room_name,
                        avatar: msg_data.image_url,
                        room_type: msg_data.room_type
                    });


                    //get contact
                    if (!this.props.contacts.hasOwnProperty(msg_data.addressee_matrix_id)) {
                        let name_socket = new WebSocket(WS_URL);
                        name_socket.onmessage = (msg) => {
                            let parsed_msg = JSON.parse(msg.data);
                            console.log(parsed_msg);
                            if (parsed_msg.statusCode == 200) {
                                this.props.addContacts({[msg_data.addressee_matrix_id.split("@")[1].split(":")[0]]: parsed_msg.data[0]});
                            }
                            name_socket.close();
                        };
                        name_socket.onopen = () => {
                            backendRequestCustomSocket(name_socket, "getRequestedUserInfo", this.props.userToken, {MatrixId: msg_data.addressee_matrix_id});
                        };
                    }

                    //this.forceUpdate();
                } else {
                    console.log("got wrong status code", parsed_msg.statusCode);
                }
                dm_room_socket.close();
            };
            dm_room_socket.onopen = () => {
                backendRequestCustomSocket(dm_room_socket, "chatGetRoomInfo", this.props.userToken, {matrix_room_id: this.props.room_id}/*, this.state.room_name_req_id*/);
            };
        } else {
            this.setState({
                loading_name: false,
                addressee_matrix_id: this.props.rooms[this.props.room_id].addressee_matrix_id,
                full_name: this.props.rooms[this.props.room_id].full_name,
                avatar: this.props.rooms[this.props.room_id].avatar,
                room_type: this.props.rooms[this.props.room_id].room_type
            });
        }
    }

    shouldComponentUpdate(nextProps, nextState, nextContext) {
        if (this.props.rooms.hasOwnProperty(this.props.room_id) && nextProps.rooms.hasOwnProperty(this.props.room_id)) {
            let old_room = this.props.rooms[this.props.room_id];
            let new_room = nextProps.rooms[this.props.room_id];
            console.log("should room update? ", old_room.avatar, new_room.avatar, old_room.full_name, new_room.full_name, old_room.avatar != new_room.avatar || old_room.full_name != new_room.full_name);
            return (old_room.avatar != new_room.avatar || old_room.full_name != new_room.full_name);
        } else {
            console.log("should room update? ", this.props.rooms.hasOwnProperty(this.props.room_id), nextProps.rooms.hasOwnProperty(this.props.room_id), "idk, will update regardless");
            return true;
        }
    }

    componentWillMount() {
        let {messages, user_id, room_id, joined} = this.props;
        let source = this.props.joined ? this.props.joined_rooms : this.props.invites;
        let search_term_roomname = (source[room_id] ? ( source[room_id]["roomAlias"] || source[room_id]["canonicalAlias"]) : null);
        if (search_term_roomname) {
            let dm_check = search_term_roomname.split("-");
            if ((dm_check.length == 3 && dm_check[0] == "DM") || (dm_check.length == 3 && dm_check[0]=="#DM")) {
                console.log(search_term_roomname + " is a dm room");
                this.is_not_group = true;
            }
        }
    }

    componentDidMount() {
        let {messages, user_id, room_id, joined} = this.props;
        console.log("omg my matrix id is "+user_id);

        this._getName();
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        let {messages, user_id, room_id, joined} = this.props;

        let source = this.props.joined ? this.props.joined_rooms : this.props.invites;

        if (!this.props.rooms.hasOwnProperty(this.props.room_id) && !this.loading
            /*(this.props.rooms[this.props.room_id] != prevProps.rooms[this.props.room_id])*/) {
                console.log("did update, want more");
                this._getName();
        }
    }

    render () {
        let {messages, user_id, room_id, joined} = this.props;
        let source = this.props.joined ? this.props.joined_rooms : this.props.invites;
        return (
            <View style={card.base}>
                <TouchableOpacity
                    delayLongPress={500}
                    onPress={() => {
                        console.log("opening "+room_id);
                        if (joined) {
                            /*this.props.navigation.navigate(
                                "MessageScreen",
                                {
                                    room_id:room_id,
                                    user_id:user_id,
                                    full_name:
                                        this.state.full_name
                                            ?
                                                this.state.full_name
                                            :
                                                source[room_id] ? (source[room_id]["roomAlias"] || source[room_id]["canonicalAlias"]) : room_id,
                                    avatar: this.state.avatar,
                                    is_not_group:this.is_not_group,
                                    addressee_matrix_id: this.state.addressee_matrix_id
                                });*/
                            this.props.chooseChat(
                                {
                                    room_id:room_id,
                                    user_id:user_id,
                                    full_name:
                                        this.state.full_name
                                            ?
                                            this.state.full_name
                                            :
                                            source[room_id] ? (source[room_id]["roomAlias"] || source[room_id]["canonicalAlias"]) : room_id,
                                    avatar: this.state.avatar,
                                    is_not_group:this.is_not_group,
                                    addressee_matrix_id: this.state.addressee_matrix_id
                                }
                            );
                        }
                    }}
                    onLongPress={() => {
                        //alert("long press");
                        /*ActionSheet.show(
                            {
                                options:BUTTONS,
                                cancelButtonIndex:1
                            },
                            buttonIndex => {
                                switch(buttonIndex){
                                    case 0:
                                        this.client.leave(room_id);
                                        this.props.removeJoined(room_id);
                                        if (this.props.dms.length > 0) {
                                            this.props.setChatDms(this.props.dms.dropItem(this.state.addressee_matrix_id));
                                        }
                                }

                            }
                        )*/
                    }}
                >
                    <View style={{height:80, width:300, flexDirection:"row"}}>
                            <View style={{height:80, width:80, justifyContent:"center"}}>
                                {this.props.rooms && this.props.rooms.hasOwnProperty(this.props.room_id) && this.props.rooms[this.props.room_id].avatar
                                    ?
                                        <WebImage
                                            style={{borderRadius:30, width:60, height:60, resizeMode:"cover", alignSelf:"center"}}
                                            source={{
                                                uri: this.props.rooms[this.props.room_id].avatar,//this.state.avatar,
                                                method: "GET",
                                                headers: {
                                                    Authorization:this.props.userToken
                                                }
                                            }}
                                        />
                                    :
                                    <View style={{borderRadius:30, width:60, height:60, alignSelf:"center", justifyContent:"center", alignItems:"center", textAlign:"center", backgroundColor:"rgb(117,120,123)"}}><Text style={{fontSize:38, fontWeight:"bold", color:"#fff"}}>?</Text></View>
                                }
                            </View>
                            {joined && this.props.joined_rooms[this.props.room_id].hasOwnProperty("unread") && this.props.joined_rooms[this.props.room_id].unread.length > 0 &&
                                <View
                                    style={{
                                        width:20,
                                        height:20,
                                        borderRadius:10,
                                        alignSelf:"flex-start",
                                        marginLeft:-30,
                                        marginTop:10,
                                        backgroundColor:"rgb(169,25,59)",
                                        justifyContent:"center",
                                        alignItems:"center"
                                    }}
                                >
                                    <Text style={{color:"white"}}>{this.props.joined_rooms[this.props.room_id].unread.length}</Text>
                                </View>
                            }
                            <View style={{flex:1, marginTop:10, marginBottom:15, justifyContent:"space-between"}}>
                                {this.props.rooms && this.props.rooms.hasOwnProperty(this.props.room_id)//this.state.full_name
                                    ?
                                        <Text style={{fontWeight:"bold"}}>{this.props.rooms[this.props.room_id].full_name}</Text>
                                    :
                                        /*chatbots.hasOwnProperty(source[room_id] ? (source[room_id]["roomAlias"] || source[room_id]["canonicalAlias"]) : room_id)
                                            ?
                                                <Text style={{fontWeight:"bold"}}>{chatbots[source[room_id] ? (source[room_id]["roomAlias"] || source[room_id]["canonicalAlias"]) : room_id]}</Text>
                                            :
                                                <Text style={{fontWeight:"bold"}}>{(source[room_id]["roomAlias"] ?  source[room_id]["roomAlias"] : source[room_id]["canonicalAlias"])/!*source[room_id] ? (source[room_id]["roomAlias"] || source[room_id]["canonicalAlias"]) : room_id*!/}</Text>*/
                                        <View
                                            style={{
                                                width:"70%",
                                                height:16,
                                                borderRadius:8,
                                                backgroundColor:"#5c5c5c"
                                            }}
                                        />
                                }
                                {joined && messages != null && messages.length>0 &&
                                    <View style={{flexDirection:"row"}}>
                                        {((msg) => {
                                            let text = msg.text;
                                            let time = msg.createdAt;

                                            let is_bot = text.slice(0, "[BOT_MESSAGE]".length) == "[BOT_MESSAGE]";

                                            if (is_bot) {
                                                let parsed_text = JSON.parse(text.split("[BOT_MESSAGE] ")[1]).message;
                                                return (
                                                    <Text numberOfLines={1}>{`${new Date(time).getHours()}:${((new Date(time).getMinutes() < 10)&&"0") + (new Date(time).getMinutes())} - ${parsed_text}`}</Text>
                                                );
                                            } else {
                                                return (
                                                    <Text numberOfLines={1}>{`${new Date(time).getHours()}:${((new Date(time).getMinutes() < 10)&&"0") + (new Date(time).getMinutes())} - ${text}`}</Text>
                                                );
                                            }
                                        })(messages[0])}
                                    </View>
                                }
                                {!joined &&
                                    <View
                                        style={{flexDirection:"row"}}
                                    >
                                        <TouchableOpacity
                                            onPress={() => {
                                                this.client.joinRoom(room_id, null, () => {this.props.removeInvite(room_id)});
                                            }}
                                        >
                                            <View
                                                style={this.state.active_tab == 0 ? [button.header, button.active] : button.header}
                                            >
                                                <Text style={[this.state.active_tab == 0 && {color:"white"}, {fontSize:14*PixelRatio.getFontScale()}]}>{"Принять"}</Text>
                                            </View>
                                        </TouchableOpacity>
                                        <TouchableOpacity
                                            style={{marginLeft:20}}
                                            onPress={() => {
                                                this.client.leave(room_id, () => {this.props.removeInvite(room_id);});
                                            }}
                                        >
                                            <View
                                                style={this.state.active_tab == 0 ? [button.header, button.active] : button.header}
                                            >
                                                <Text style={[this.state.active_tab == 0 && {color:"white"}, {fontSize:14*PixelRatio.getFontScale()}]}>{"Отклонить"}</Text>
                                            </View>
                                        </TouchableOpacity>
                                    </View>
                                }
                            </View>
                    </View>
                </TouchableOpacity>
            </View>
        );
    }
}



const mapStateToProps = (state, ownProps) => {
    return {
        lang:                    state.data.settings.lang,
        userToken:               state.data.userToken,
        chatToken:               state.data.chat.token,
        joined_rooms:            state.data.chat.joined_rooms,
        invites:                 state.data.chat.invites,
        messages:                state.data.chat.messages[ownProps.room_id],
        user_id:                 state.data.chat.user_id,
        rooms:                   state.data.chat.rooms,
        contacts:                state.data.chat.contacts,
        dms:                     state.data.chat.dms
    }
};

const mapDispatchToProps = dispatch => {
    return {
        setChatToken:            (token) => dispatch(setChatToken({token})),
        updMessages:             (new_msg, room) => dispatch(updMessages({new_msg, room})),
        addInvite:               (room) => dispatch(addInvite({room})),
        addJoined:               (room) => dispatch(addJoined({room})),
        addCanAlias:             (room, alias) => dispatch(addCanAlias({room, alias})),
        addAlias:                (room, alias) => dispatch(addAlias({room, alias})),
        removeInvite:            (room) => dispatch(removeInvite({room})),
        removeJoined:            (room) => dispatch(removeJoined({room})),
        enableNetWarn:           () => dispatch(enableNetWarn()),
        disableNetWarn:          () => dispatch(disableNetWarn()),
        addRooms:                (room) => dispatch(addRooms({room})),
        addContacts:             (contact) => dispatch(addContacts({contact})),
        setChatDms:              (dms) => dispatch(setChatDms({dms}))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(RoomDisplayCard)
