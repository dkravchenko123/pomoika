import React from "react";
import {Dimensions, Image, PixelRatio, Text, TouchableOpacity, View} from "react-native";
import {Ionicons} from "@expo/vector-icons";
import {
    changeLang,
    receiveData,
    removeData,
    setAvailableFilter,
    setCurrentFilter,
    setEventId, setEventJson, setProgramReady,
    updateUserToken
} from "../../actions/data";
import {closeFilterView, disableNetWarn, enableNetWarn, toggleCredCard, toggleRegForm} from "../../actions/control";
import {connect} from "react-redux";
import button from "../../styles/buttons";


const window = Dimensions.get("window");


class EventCard extends React.Component {
    constructor(props){
        super(props);

    }



    render () {
        let background_color = "white";
        let accent_color = "rgb(134,38,51)";
        let Text_color = "black";
        return (
            <View style={{justifyContent:"space-between", width:window.width-30, backgroundColor:background_color, borderRadius: 15}}>
                <View style={{paddingLeft:15, paddingRight:15, paddingTop:15, alignItems:"center", flexDirection:"row", marginBottom:10}}>
                    {this.props.logo &&
                        <Image
                            style={{height:70, width:70, borderRadius:35, marginRight:10, resizeMode:"center"}}
                            source={{
                                uri:this.props.logo,
                                method: "GET",
                                headers: {
                                    Authorization:this.props.userToken
                                }
                            }}
                        />
                    }
                    <Text style={{fontWeight:"bold", fontSize:18, flex:1, color:Text_color, flexWrap:"wrap"}}>{this.props.name}</Text>
                </View>
                <View style={{flexDirection:"row", paddingHorizontal:15, alignItems:"center"}}>
                    <Text style={{flex:1, color:Text_color}}>{this.props.description_short}</Text>
                    {this.props.description_full &&
                        <TouchableOpacity
                            onPress={() => {
                                this.props.navigation.navigate("WebViewScreen", {uri:this.props.description_full});
                            }}
                        >
                            <Ionicons name={"ios-arrow-forward"} size={28} color={Text_color}/>
                        </TouchableOpacity>
                    }
                </View>
                <View style={{marginVertical:8}}>
                    <View style={{paddingLeft:15, flexDirection:"row"}}>
                        <View style={{width:18, alignItems:"center"}}>
                            <Ionicons name={"ios-calendar"} size={17} color={accent_color}/>
                        </View>
                        <Text style={{color:Text_color}}>{" Даты проведения: "+this.props.startdate.split("T")[0].split("-").join("/") + " - " + this.props.enddate.split("T")[0].split("-").join("/")}</Text>
                    </View>
                    {this.props.place_name &&
                        <View style={{paddingLeft:15, marginTop:4, flexDirection:"row"}}>
                            <View style={{width:18, alignItems:"center"}}>
                                <Ionicons name={"ios-pin"} size={17} color={accent_color}/>
                            </View>
                            <Text style={{color:Text_color}}>{" Место проведения: "+this.props.place_name}</Text>
                        </View>
                    }
                </View>
                {this.props.is_past
                    ?
                        <View style={{paddingHorizontal:20, marginTop:10}}>
                            <View style={{flexDirection:"row", height:30, justifyContent:"space-between", alignItems:"center", marginBottom:10,}}>
                                <TouchableOpacity
                                    style={[button.header, {width:"100%", marginBottom:10, flexDirection:"row", flex:1, alignItems:"center", justifyContent:"center", height:30, borderRadius:23, borderWidth:1, borderColor:accent_color}]}
                                    onPress={() => {
                                        this.props.navigation.navigate("PhotobankWebViewScreen", {uri:this.props.photobank_link});
                                    }}
                                >
                                    <Text style={{color:Text_color, fontSize:14*PixelRatio.getFontScale()}}>Фотобанк</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={{flexDirection:"row", height:30, justifyContent:"space-between", alignItems:"center", marginBottom:10,}}>
                                <TouchableOpacity
                                    style={[button.header, {width:"100%", marginBottom:10, flexDirection:"row", flex:1, alignItems:"center", justifyContent:"center", height:30, borderRadius:23, borderWidth:1, borderColor:accent_color}]}
                                    onPress={() => {
                                        this.props.navigation.navigate(
                                            "HomeFullscreenWithChildren",
                                            {
                                                children: () => {
                                                    return (
                                                        <Text>Записи Докладов</Text>
                                                    );
                                                }
                                            });
                                    }}
                                >
                                    <Text style={{color:Text_color, fontSize:14*PixelRatio.getFontScale()}}>Записи докладов</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={{flexDirection:"row", height:30, justifyContent:"space-between", alignItems:"center", marginBottom:10,}}>
                                <TouchableOpacity
                                    style={[button.header, {width:"100%", marginBottom:10, flexDirection:"row", flex:1, alignItems:"center", justifyContent:"center", height:30, borderRadius:23, borderWidth:1, borderColor:accent_color}]}
                                    onPress={() => {
                                        this.props.navigation.navigate(
                                            "HomeFullscreenWithChildren",
                                            {
                                                children: () => {
                                                    return (
                                                        <Text>Цитаты</Text>
                                                    );
                                                }
                                            });
                                    }}
                                >
                                    <Text style={{color:Text_color, fontSize:14*PixelRatio.getFontScale()}}>Цитаты</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    :
                        <View style={{paddingHorizontal:20, marginTop:10}}>
                            <View style={{flexDirection:"row", height:30, justifyContent:"space-between", alignItems:"center", marginBottom:10}}>
                                <TouchableOpacity
                                    style={[button.header, {alignItems:"center", justifyContent:"center", width:100, height:30, borderRadius:23, borderWidth:1, borderColor:accent_color}]}
                                    onPress={() => {
                                        this.props.navigation.navigate(
                                            "HomeFullscreenWithChildren",
                                            {
                                                children: () => {
                                                    return (
                                                        <Text>Программа</Text>
                                                    );
                                                }
                                            });
                                    }}
                                >
                                    <Text style={{color:Text_color, fontSize:14*PixelRatio.getFontScale()}}>Программа</Text>
                                </TouchableOpacity>
                                <TouchableOpacity
                                    style={[button.header, {alignItems:"center", justifyContent:"center", width:100, height:30, borderRadius:23, borderWidth:1, borderColor:accent_color}]}
                                    onPress={() => {
                                        this.props.navigation.navigate(
                                            "HomeFullscreenWithChildren",
                                            {
                                                children: () => {
                                                    return (
                                                        <Text>Участники</Text>
                                                    );
                                                }
                                            });
                                    }}
                                >
                                    <Text style={{color:Text_color, fontSize:14*PixelRatio.getFontScale()}}>Участники</Text>
                                </TouchableOpacity>
                                <TouchableOpacity
                                    style={[button.header, {alignItems:"center", justifyContent:"center", width:100, height:30, borderRadius:23, borderWidth:1, borderColor:accent_color}]}
                                    onPress={() => {
                                        this.props.navigation.navigate("PhotobankWebViewScreen", {uri:this.props.photobank_link});
                                    }}
                                >
                                    <Text style={{color:Text_color, fontSize:14*PixelRatio.getFontScale()}}>Фотобанк</Text>
                                </TouchableOpacity>
                            </View>
                            <TouchableOpacity
                                style={[button.header, {width:"100%", marginBottom:10, flexDirection:"row", flex:1, alignItems:"center", justifyContent:"center", height:30, borderRadius:23, borderWidth:1, borderColor:accent_color}]}
                                onPress={() => {
                                    this.props.navigation.navigate(
                                        "HomeFullscreenWithChildren",
                                        {
                                            children: () => {
                                                return (
                                                    <Text>Подача заявки</Text>
                                                );
                                            }
                                        });
                                }}
                            >
                                <Text style={{color:Text_color, fontSize:14*PixelRatio.getFontScale()}}>Подать заявку</Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                style={[button.header, {width:"100%", marginBottom:25, flexDirection:"row", flex:1, alignItems:"center", justifyContent:"center", height:30, borderRadius:23, borderWidth:1, borderColor:accent_color}]}
                                onPress={() => {
                                    if (this.props.eventid != this.props.cur_id) {
                                        this.props.removeData("getUserEventMenu");
                                        this.props.removeData("getEventFilters");
                                        this.props.removeData("getEventFacts");
                                        this.props.setCurrentFilter({});
                                        this.props.setAvailableFilter({});
                                        this.props.closeFilterView();
                                        this.props.setEventJson({
                                            description_full:    this.props.description_full,
                                            description_short:   this.props.description_short,
                                            enddate:             this.props.enddate,
                                            eventid:             this.props.eventid,
                                            logo:                this.props.logo,
                                            name:                this.props.name,
                                            nameen:              this.props.nameen,
                                            permissionid:        this.props.permissionid,
                                            place_name:          this.props.place_name,
                                            startdate:           this.props.startdate,
                                            style:               this.props.style,
                                        });
                                        this.props.setEventId(this.props.eventid);
                                        this.props.setProgramReady(false);
                                    }

                                    this.props.navigation.navigate("Event");
                                }}
                            >
                                <Text style={{color:Text_color, fontSize:14*PixelRatio.getFontScale()}}>Открыть мероприятие</Text>
                            </TouchableOpacity>
                        </View>
                }

                {/*<View style={{flexDirection:"row", flex:1, alignItems:"center", marginHorizontal:15, justifyContent:"space-around"}}>

                </View>*/}
            </View>
            /*<Card style={{marginTop: 10, marginLeft:10, marginRight:10, height:100}}>
                <Grid>
                    <Col size={1}>
                        {this.props.data.img &&
                            <Image style={{marginLeft: 15, marginTop: 15, width: 70, height: 70, borderRadius: 35}}
                                   resizeMode={"cover"} source={this.props.data.img}/>
                        }
                    </Col>
                    <Col size={3}>
                        <Row size={1} style={{marginTop:10, marginLeft:10}}>
                            <Text style={{alignSelf: "center", fontWeight:"bold", fontSize:18}}>{this.props.data.name}</Text>
                        </Row>
                        <Row size={2} style={{marginLeft:10}}>
                            {this.props.data.status &&
                                <Text style={{alignSelf: "center", color: "#0078D7"}}>{this.props.data.status}</Text>
                            }
                        </Row>
                        <Row size={1} style={{marginBottom:5, marginLeft:10}}>
                            {this.props.data.contact &&
                                <Text style={{alignSelf: "center", fontSize: 12}}>{this.props.data.contact}</Text>
                            }
                        </Row>
                    </Col>
                </Grid>
            </Card>*/
        )
    }
}

const mapStateToProps = state => {
    return {
        received_data:      state.data.received_data,
        lang:               state.data.settings.lang,
        cred_card:          state.control.cred_card,
        userToken:          state.data.userToken,
        cur_id:             state.data.event.event_id
    }
};

const mapDispatchToProps = dispatch => {
    return {
        removeData:             (key) => dispatch(removeData({key})),
        receiveData:            (data) => dispatch(receiveData(data)),
        ch_lang:                (lang) => dispatch(changeLang(lang)),
        updateUserToken:        (token) => dispatch(updateUserToken({token})),
        toggleRegForm:          () => dispatch(toggleRegForm()),
        toggleCredCard:         () => dispatch(toggleCredCard()),
        setEventId:             (event_id) => dispatch(setEventId({event_id})),
        setCurrentFilter:       (new_filter) => dispatch(setCurrentFilter({current_filter: new_filter})),
        setAvailableFilter:     (new_filter) => dispatch(setAvailableFilter({available_filter: new_filter})),
        closeFilterView:        () => dispatch(closeFilterView()),
        enableNetWarn:          () => dispatch(enableNetWarn()),
        disableNetWarn:         () => dispatch(disableNetWarn()),
        setEventJson:           (event_json) => dispatch(setEventJson({event_json})),
        setProgramReady:        (isReady) => dispatch(setProgramReady({isReady}))
    }
};


export default connect(mapStateToProps, mapDispatchToProps)(EventCard);
