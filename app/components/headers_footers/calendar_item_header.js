import React from "react";
import {Button, Header, Input, Item} from "native-base";
import {styles} from "../../styles/header_footer_styles";
import {Image, Platform, StatusBar, Text, View} from "react-native";
import {togglePersonalInfoOverlay, toggleSearch} from "../../actions/control";
import {connect} from "react-redux";
import {SimpleLineIcons, Ionicons, MaterialIcons} from "@expo/vector-icons";
import {backendRequest, backendRequestCustomSocket} from "../../methods/ws_requests";
import {getws} from "../../methods/webSocket";
import {calRemoveItem, setCalendarNeedsUpdate} from "../../actions/data";
import {WS_URL} from "../../constants/backend";


class CalendarItemHeader extends React.Component {
    constructor (props) {
        super (props);

    }

    render() {
        let back_color = "";
        switch (this.props.event_id) {
            case (172): //ВЭФ
                back_color = "#007BFF";
                break;
            case (2): //ПМЭФ
                back_color = "#283E65";
                break;
            case (1):
                back_color = "#0072BC";
                break;
            default:
                back_color = "#ffffff";
        }
        let logo_url = "";
        switch (this.props.event_id) {
            case (172): //ВЭФ
                logo_url = require("../../resources/vostok.png");
                break;
            case (2): //ПМЭФ
                logo_url = require("../../resources/pmef.png");
                break;
            case (1): //РИФ
                logo_url = require("../../resources/rif.png");
                break;
            default:
                logo_url = require("../../resources/rk.png");
        }
        let text_color = "";
        switch (this.props.event_id) {
            case (172): //ВЭФ
                text_color = "#fff";
                break;
            case (2): //ПМЭФ
                text_color = "#fff";
                break;
            case (1): //РИФ
                text_color = "#fff";
                break;
            default:
                text_color = "#000";
        }
        let item = this.props.item;
        return (
            <Header style={{backgroundColor: back_color, justifyContent: "space-between", marginTop: Platform.OS === 'ios' ? 0 : StatusBar.currentHeight}}>
                <View style={{flexDirection: "row", width: 60, justifyContent: "space-between"}}>
                    <Button
                        transparent
                        onPress={() => {
                            this.props.navigation.goBack();
                        }}
                    >
                        <Ionicons size={28} style={styles.header_nav_button}
                                  name="ios-arrow-back"/>
                    </Button>
                </View>
                <Image style={{width: 200, height: 24, alignSelf: "center", marginBottom: 5}}
                       resizeMode={"contain"} source={logo_url}/>
                <View style={{
                    flexDirection: "row",
                    width: 70,
                    justifyContent: "space-between",
                    marginRight: 5
                }}>
                    <Button
                        transparent
                        onPress={() => {
                            this.props.navigation.navigate("CalendarEditItemScreen", {item})
                        }}
                    >
                        <MaterialIcons size={28} style={styles.header_nav_button}
                                  name="edit"/>
                    </Button>
                    <Button
                        transparent
                        onPress={() => {
                            let cal_socket = new WebSocket(WS_URL);
                            cal_socket.onopen = () => {
                                backendRequestCustomSocket(
                                    cal_socket,
                                    "calendarRemoveItem",
                                    this.props.userToken,
                                    {
                                        itemId:item.item_id,
                                    }
                                );
                                this.props.calRemoveItem(item.item_id);
                                this.props.setCalendarNeedsUpdate(true);
                                this.props.navigation.goBack();
                                cal_socket.close();
                            };
                        }}
                    >
                        <Ionicons size={32} style={styles.header_nav_button} name="ios-trash"/>
                    </Button>
                </View>
            </Header>
        );
    }
}

const mapStateToProps = state => {
    return {
        lang:               state.data.settings.lang,
        userToken:          state.data.userToken,
        event_id:           state.data.event_id
    }
};

const mapDispatchToProps = dispatch => {
    return {
        //toggle: () => dispatch(togglePersonalInfoOverlay()),
        calRemoveItem:              (item_id) => dispatch(calRemoveItem(item_id)),
        setCalendarNeedsUpdate:     (needs_update) => dispatch(setCalendarNeedsUpdate({needs_update}))

    }
};

export default connect(mapStateToProps, mapDispatchToProps)(CalendarItemHeader);
