import React from "react";
import {Button, Header, Input, Item} from "native-base";
import {styles} from "../../styles/header_footer_styles";
import {Image, Platform, StatusBar, Text, View} from "react-native";
import {togglePersonalInfoOverlay, toggleSearch} from "../../actions/control";
import {connect} from "react-redux";
import {SimpleLineIcons, Ionicons} from "@expo/vector-icons";


class SubscreenHeader extends React.Component {
    render() {
        let event_id = this.props.is_event_screen ? this.props.event_id : -1;

        /*let back_color = "";
        switch (this.props.event_id) {
            case (172): //ВЭФ
                back_color = "#007BFF";
                break;
            case (2): //ПМЭФ
                back_color = "#283E65";
                break;
            case (1):
                back_color = "#0072BC";
                break;
            default:
                back_color = "#ffffff";
        }
        let logo_url = "";
        switch (this.props.event_id) {
            case (172): //ВЭФ
                logo_url = require("../../resources/vostok.png");
                break;
            case (2): //ПМЭФ
                logo_url = require("../../resources/pmef.png");
                break;
            case (1): //РИФ
                logo_url = require("../../resources/rif.png");
                break;
            default:
                logo_url = require("../../resources/rk.png");
        }
        let text_color = "";
        switch (this.props.event_id) {
            case (172): //ВЭФ
                text_color = "#fff";
                break;
            case (2): //ПМЭФ
                text_color = "#fff";
                break;
            case (1): //РИФ
                text_color = "#fff";
                break;
            default:
                text_color = "#000";
        }*/
        let RightButton = this.props.right_button;
        return (
            <Header
                style={{
                    height:60,
                    backgroundColor: "white",
                    justifyContent: "space-between",
                    marginTop: Platform.OS === 'ios' ? 0 : StatusBar.currentHeight,
                    elevation:0,
                    borderBottomWidth: 2,
                    borderBottomColor: "rgb(220,219,216)"
                }}
            >
                <View style={{flexDirection: "row", width: 60, justifyContent: "space-between"}}>
                    <Button
                        transparent
                        onPress={() => {
                            if (this.props.custom_back) {
                                this.props.custom_back();
                            } else if (this.props.pi_overlay) {
                                this.props.togglePIO();
                            } else {
                                this.props.navigation.goBack();
                            }
                        }}
                    >
                        <Ionicons size={28} style={styles.header_nav_button}
                                  name="ios-arrow-back"/>
                    </Button>
                </View>
                {(this.props.is_event_screen && this.props.event_json)
                    ?
                    <Image
                        style={{width: 200, height: 31, alignSelf: "center", marginBottom: 5}}
                        resizeMode={"contain"}
                        source={{
                            uri:this.props.event_json.logo,
                            method: "GET",
                            headers: {
                                Authorization:this.props.userToken
                            }
                        }}
                    />
                    :
                    <Image
                        style={{width: 200, height: 24, alignSelf: "center", marginBottom: 5}}
                        resizeMode={"contain"} source={require("../../resources/rk.png")}
                    />
                }
                <View style={{
                    flexDirection: "row",
                    width: 70,
                    justifyContent: "flex-end",
                    marginRight: 5
                }}>
                {/*{this.props.right_button
                    ?
                        <RightButton navigation={this.props.navigation} />
                    :
                        <Button
                            transparent
                            onPress={() => {
                                if (this.props.menu_fun) {
                                    this.props.menu_fun();
                                }
                            }}
                        >
                            <Ionicons
                                size={28}
                                style={styles.header_nav_button}
                                name="ios-menu"
                            />
                        </Button>
                }*/}
                </View>

            </Header>
        );
    }
}


const mapStateToProps = (state, ownProps) => {
    return {
        search_active:      state.control.search_active,
        pi_overlay:         state.control.pi_overlay,
        event_id:           state.data.event.event_id,
        event_json:         state.data.event.event_json
    }
};


const mapDispatchToProps = dispatch => {
    return {
        toggleSearch:       () => dispatch(toggleSearch()),
        togglePIO:          () => dispatch(togglePersonalInfoOverlay()),

    }
};


export default connect(mapStateToProps, mapDispatchToProps)(SubscreenHeader);
