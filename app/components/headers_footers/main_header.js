import React from "react";
import {Button, Header, Input, Item} from "native-base";
import {styles} from "../../styles/header_footer_styles";
import {Image, Platform, StatusBar, Text, View} from "react-native";
import {toggleFilterView, togglePersonalInfoOverlay, toggleSearch} from "../../actions/control";
import {connect} from "react-redux";
import {SimpleLineIcons, Ionicons} from "@expo/vector-icons";



class DefaultHeader extends React.Component {
    render() {
        //if (this.props.is_event_screen) console.log("event_json " + this.props.event_json);
        let event_id = this.props.is_event_screen ? this.props.event_id : -1;

        /*switch (event_id) {
            case (172): //ВЭФ
                back_color = "#007BFF";
                break;
            case (2): //ПМЭФ
                back_color = "#283E65";
                break;
            case (1):
                back_color = "#0072BC";
                break;
            default:
                back_color = "#ffffff";
        }*/
        /*let logo_url = "";
        switch (event_id) {
            case (172): //ВЭФ
                logo_url = require("../../resources/vostok.png");
                break;
            case (2): //ПМЭФ
                logo_url = require("../../resources/pmef.png");
                break;
            case (1): //РИФ
                logo_url = require("../../resources/rif.png");
                break;
            default:
                logo_url = require("../../resources/rk.png");
        }*/
        /*let text_color = "";
        switch (event_id) {
            case (172): //ВЭФ
                text_color = "#fff";
                break;
            case (2): //ПМЭФ
                text_color = "#fff";
                break;
            case (1): //РИФ
                text_color = "#fff";
                break;
            default:
                text_color = "#000";
        }*/
        if (this.props.search_active) {
            return (
                <Header
                    style={{
                        height: (Platform.OS == "ios" ? 64 : 56),
                        //width:"100%",
                        backgroundColor: "white",
                        justifyContent: "space-between",
                        marginTop: Platform.OS === 'ios' ? 0 : StatusBar.currentHeight,
                        //shadowOpacity:0,
                        elevation:0,
                        borderBottomWidth: 2,
                        borderBottomColor: "rgb(220,219,216)"
                    }}
                >
                    <Item style={{
                        borderColor: "#000",
                        flex: 1,
                        marginBottom: 8,
                        marginTop: 8,
                        marginRight: 10,
                        paddingLeft: 12
                    }} rounded>
                        <Ionicons active size={24} style={styles.header_nav_button} name="ios-search"/>
                        <Input placeholder={"Найти"}/>
                    </Item>
                    <Button transparent onPress={() => {
                        this.props.toggleSearch();
                    }}><Text style={{fontSize: 17}}>{"Отмена"}</Text></Button>
                </Header>
            );
        } else {
            return (
                <Header
                    style={{
                        height: (Platform.OS == "ios" ? 64 : 56),
                        //width:"100%",
                        backgroundColor: "white",
                        justifyContent: "space-between",
                        marginTop: Platform.OS === 'ios' ? 0 : StatusBar.currentHeight,
                        //shadowOpacity:0,
                        elevation:0,
                        borderBottomWidth: 2,
                        borderBottomColor: "rgb(220,219,216)"
                    }}
                >
                    {/* <View style={{flexDirection: "row", width: 60, justifyContent: "space-between"}}>
                        <Button
                            transparent
                            onPress={() => {
                                console.log("back pressed");
                                if (this.props.custom_back) {
                                    this.props.custom_back();
                                } else if (this.props.filter_view) {
                                    this.props.toggleFilterView();
                                } else {
                                    this.props.navigation.goBack();
                                    if (this.props.pi_overlay) {
                                        this.props.togglePIO();
                                    }
                                }
                            }}
                        >
                            <Ionicons size={28} style={styles.header_nav_button}
                                      name="ios-arrow-back"/>
                        </Button>
                        <Button transparent><Ionicons size={28} style={styles.header_nav_button}
                                                      name="ios-arrow-forward"/></Button>
                    </View> */}
                    {(this.props.is_event_screen && this.props.event_json)
                        ?
                            <Image
                                style={{width: 200, height: 31, alignSelf: "center", marginBottom: 5}}
                                resizeMode={"contain"}
                                source={{
                                    uri:this.props.event_json.logo,
                                    method: "GET",
                                    headers: {
                                        Authorization:this.props.userToken
                                    }
                                }}
                            />
                        :
                        <Image
                            style={{width: 200, height: 24, alignSelf: "center", marginBottom: 5}}
                            resizeMode={"contain"} source={require("../../resources/logo_roscongress_Building_Trust_ru.jpg")}
                        />
                    }
                    {/* <View style={{
                        flexDirection: "row",
                        width: 70,
                        justifyContent: "space-between",
                        marginRight: 5
                    }}>
                        <Button transparent onPress={() => {
                            this.props.toggleSearch();
                        }}>
                            <Ionicons size={28} style={styles.header_nav_button} name="ios-search"/>
                        </Button>
                        <Button
                            transparent
                            onPress={() => {
                                if (this.props.menu_fun) {
                                    this.props.menu_fun();
                                }
                            }}
                        >
                            <Ionicons
                                size={28}
                                style={styles.header_nav_button}
                                name="ios-menu"
                            />
                        </Button>
                    </View> */}
                </Header>
            );
        }
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        search_active:      state.control.search_active,
        pi_overlay:         state.control.pi_overlay,
        event_id:           state.data.event.event_id,
        event_json:         state.data.event.event_json,
        filter_view:        state.control.event_filter_view,
    }
};


const mapDispatchToProps = dispatch => {
    return {
        toggleSearch:       () => dispatch(toggleSearch()),
        togglePIO:          () => dispatch(togglePersonalInfoOverlay()),
        toggleFilterView:   () => dispatch(toggleFilterView()),
    }
};


export default connect(mapStateToProps, mapDispatchToProps)(DefaultHeader);
