import React from "react";
import {PixelRatio, Text, TouchableOpacity, View} from "react-native";
import MainHeader from "./main_header";
import button from "../../styles/buttons";
import { LinearGradient } from 'expo-linear-gradient';


export class ButtonsOverGradient extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            active_tab:this.props.init_active ? this.props.init_active : 0
        }
    }

    render () {
        let button_labels = this.props.buttons;
        return (
            <View style={{width:"100%", height:50/*height:"100%"*/}}>
                <View style={{backgroundColor:"#00000000", width:window.width*0.3, height:50}} zIndex={5}>
                    <View
                        style={{backgroundColor:"#00000000",/*"rgb(240,240,240)"*/ height:50, flexDirection:"row", justifyContent:"space-between", alignItems:"center", marginHorizontal:10}}
                    >
                        <TouchableOpacity
                            onPress={() => {
                                if (this.props.active_call) this.props.active_call(0);
                                this.setState({active_tab:0})
                            }}
                        >
                            <View
                                style={this.state.active_tab == 0 ? [button.header, button.active] : button.header}
                            >
                                <Text style={[this.state.active_tab == 0 && {color:"white"}, {fontSize:14*PixelRatio.getFontScale()}]}>{button_labels[0]}</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={() => {
                                if (this.props.active_call) this.props.active_call(1);
                                this.setState({active_tab:1})
                            }}
                        >
                            <View
                                style={this.state.active_tab == 1 ? [button.header, button.active] : button.header}
                            >
                                <Text style={[this.state.active_tab == 1 && {color:"white"}, {fontSize:14*PixelRatio.getFontScale()}]}>{button_labels[1]}</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={() => {
                                if (this.props.active_call) this.props.active_call(2);
                                this.setState({active_tab:2})
                            }}
                        >
                            <View
                                style={this.state.active_tab == 2 ? [button.header, button.active] : button.header}
                            >
                                <Text style={[this.state.active_tab == 2 && {color:"white"}, {fontSize:14*PixelRatio.getFontScale()}]}>{button_labels[2]}</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>

                {/*<LinearGradient
                    colors={["rgb(240,240,240)", "rgba(240,240,240,0.8)", 'transparent']}
                    zIndex={4}
                    start={[0.5, 0.8]}
                    end={[0.5, 1]}
                    style={{
                        marginTop:-50,
                        height:50,
                        width:"100%"
                    }}
                />*/}
            </View>
        );
    }
}

