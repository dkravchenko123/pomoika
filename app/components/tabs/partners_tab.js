import {Button, Container, Segment, Tab} from "native-base";
import {ScrollView, Text, View} from "react-native";
import {FactCard} from "../cards/FactCard";
import React from "react";
import {receiveData} from "../../actions/data";
import {connect} from "react-redux";
import {Ionicons, MaterialIcons} from "@expo/vector-icons";
import PartnerCard from "../cards/PartnerCard";



class PartnersTab extends React.Component {
    componentDidUpdate(prevProps, prevState, snapshot) {
        console.log("PartnersTab updated");
    }

    render () {
        let background_color = "rgb(240,240,240)"; //this.props.event_json.style ? (this.props.event_json.style.Primary_color ? this.props.event_json.style.Primary_color : null) : null;
        let accent_color = null;     //this.props.event_json.style ? (this.props.event_json.style.Accent_color ? this.props.event_json.style.Accent_color : "white") : null;
        return (
            <Container>
                <ScrollView style={{flex:1, backgroundColor:background_color}}>
                    {this.props.filters["Partners"] && this.props.filters["Partners"].length != 0 &&
                        this.props.filters["Partners"].map((partner, index) => {
                            return <PartnerCard
                                key={index}
                                partner={partner}
                                base_color={background_color ? "white" : null}
                                navigation={this.props.navigation}
                            />;
                        })
                    }
                    {!(this.props.filters["Partners"] && this.props.filters["Partners"].length != 0) &&
                        <Text>No partners!</Text>
                    }
                    <View style={{height:120}} />
                </ScrollView>
            </Container>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        lang:               state.data.settings.lang,
        filters:            state.data.event.filters,
        event_json:              state.data.event.event_json,
    }
};

export default connect(mapStateToProps)(PartnersTab);
