/*
import React from "react";
import {Button, Container, Content, ScrollableTab, Tabs, Tab, TabHeading} from "native-base";
import {ScrollView, Text, TouchableOpacity, View} from "react-native";
import {styles} from "../styles/my_events_styles";
import {EventCard} from "./event_card";

*/

import React from "react";
import {ActivityIndicator, Image, ScrollView, Text, View} from "react-native";
import {Card, Container, Content} from "native-base";
import {EventCard} from "../cards/event_card";
import {getws} from "../../methods/webSocket";
import {get_events} from "../../methods/account_requests";
import {receiveData} from "../../actions/data";
import {connect} from "react-redux";



class MyEventsTab extends React.Component {
    constructor (props) {
        super(props);
        this.state = {
            isReady:false
        };

    }


    render() {
        /*let event_list = {};
        let isReady = false;
//        if (!this.state.isReady) {
            for (let elem of  this.props.received_data) {
                console.log(elem);
                if (elem.hasOwnProperty("AskedMethod")) {
                    console.log("we have AskedMethod:"+elem.AskedMethod);
                    if (elem.AskedMethod == "get_events") {
                        /!*this.setState({
                            isReady:true,
                            events:elem.Data.Data
                        });*!/
                        event_list = elem.Data.Data.map((eventInfo) =>
                            <EventCard key={JSON.stringify(eventInfo)} data={{name:eventInfo.EventName}}/>
                        );
                        isReady=true;
                    }
                }

            }*/
  //      }
 /*       if (this.state.events) {
            event_list = this.state.events.map((eventInfo) =>
                <EventCard key={JSON.stringify(eventInfo)} data={{name:eventInfo.EventName}}/>
            );
        }*/
        return null;
        return(
            <Content scrollEnabled={true}>
                {isReady
                    ?
                    <ScrollView style={{marginBottom:140}}>
                        {event_list}
                    </ScrollView>
                    :
                    <View style={{marginTop: 10}}>
                        <ActivityIndicator size={"small"} color={"#000"} />
                    </View>
                }
            </Content>
        )
    }
}

const mapStateToProps = state => {
    return {
        received_data: state.data.received_data
    }
};

const mapDispatchToProps = dispatch => {
    return {
        receiveData: (data) => dispatch(receiveData(data))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(MyEventsTab);

/*

export class MyEventsTab extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            show_old_events:false,
            show_future_events:true
        }
    }


    render() {
        return (
            <Content scrollEnabled={true}>
                <View style={{flexDirection: "row", justifyContent: "center", paddingTop: 18}}>
                    <TouchableOpacity
                        style={(this.state.show_old_events ? styles.active_button : styles.inactive_button)}
                        onPress={() => {this.setState({show_old_events:!this.state.show_old_events})}}
                    >
                        <Text style={(this.state.show_old_events ? {fontWeight:"bold"} : {})}>
                            {"Прошедшие"}
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={(this.state.show_future_events ? styles.active_button : styles.inactive_button)}
                        onPress={() => {this.setState({show_future_events:!this.state.show_future_events})}}
                    >
                        <Text style={(this.state.show_future_events ? {fontWeight:"bold"} : {})}>
                            {"Предстоящие"}
                        </Text>
                    </TouchableOpacity>
                </View>
                <ScrollView style={{marginBottom:140}}>
                    {(() => {if(this.state.show_old_events) {
                        return(
                            <EventCard data={{img:require("../resources/1.png"), name:"Первый Форум", status:"Ваша заявка на рассмотрении", contact:"info@forum1.ru"}}/>
                        )}})()}

                    {(() => {if(this.state.show_old_events) {
                        return(
                        <EventCard data={{img:require("../resources/2.png"), name:"Второй Форум", status:"Вы приглашены!", contact:"info@forum2.ru"}}/>
                        )}})()}


                    {(() => {if(this.state.show_future_events) {
                        return(
                            <EventCard data={{img:require("../resources/3.png"), name:"Третий Форум", status:"Заявка одобрена", contact:"info@forum3.ru"}}/>
                        )}})()}

                    {(() => {if(this.state.show_future_events) {
                        return(
                            <EventCard data={{img:require("../resources/4.png"), name:"Четвертый Форум", status:"Требуется оплата", contact:"info@forum4.ru"}}/>
                        )}})()}

                    {(() => {if(this.state.show_future_events) {
                        return(
                            <EventCard data={{img:require("../resources/5.png"), name:"Пятый Форум", status:"Заявка одобрена", contact:"info@forum5.ru"}}/>
                        )}})()}
                </ScrollView>
            </Content>
        )
    }
}*/
