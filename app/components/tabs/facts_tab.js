import {Button, Container, Segment, Tab} from "native-base";
import {Dimensions, FlatList, ScrollView, Text, TouchableOpacity, View} from "react-native";
import FactCard from "../cards/FactCard";
import React from "react";
import {receiveData, setCurDate} from "../../actions/data";
import {connect} from "react-redux";
import {Ionicons, MaterialIcons} from "@expo/vector-icons";
import {intersect, sameArr} from "../../methods/array_methods";
import {applyFilter} from "../../methods/filter_parsing";
import {toggleFilterView} from "../../actions/control";

const window = Dimensions.get("window");

class FactsTab extends React.Component {
    constructor(props) {
        super(props);
        this.prev_date = [];
        this.next_date = [];
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        console.log("FactsTab updated, new fact_array length "+(this.props.fact_array ? this.props.fact_array.length : "n/a"));
    }

    getTime = (date) => {
        let all_time = date.split("T")[1].split("Z")[0].split(":").slice(0, 2);
        return all_time.join(":");
    };
    getPlace = (ID) => {
        if (this.places_list.hasOwnProperty(ID)) {
            console.log("place "+ID+" is "+this.places_list[ID]["PlaceDescription"]);
            return this.places_list[ID]["PlaceDescription"];
        } else {
            console.log("couldn't find place "+ID);
            return null;
        }
    };

    render () {
        let background_color = "rgb(240,240,240)"; //this.props.event_json.style ? (this.props.event_json.style.Primary_color ? this.props.event_json.style.Primary_color : null) : null;
        let accent_color = null;     //this.props.event_json.style ? (this.props.event_json.style.Accent_color ? this.props.event_json.style.Accent_color : "white") : null;
        let {filters, fact_array, id_map, current_filter, available_filter, dates_array, cur_date} = this.props.event;
        return (
            <Container>
                <View style={{width:window.width, height:50, flexDirection:"row"}}>
                    <View style={{flex:1, justifyContent:"flex-end", flexDirection:"row", alignItems:"center", paddingBottom:4}}>
                        <TouchableOpacity
                            style={{justifyContent:"center", alignSelf:"center", marginRight:20}}
                            onPress={() => {
                                if (this.prev_date) this.props.setCurDate([...this.prev_date])
                            }}
                        >
                            <Ionicons size={28} name={"ios-arrow-back"} color={"black"}/>
                        </TouchableOpacity>
                    </View>

                    <Segment style={{flex:3, flexDirection:"row", backgroundColor:"#ffffff00", alignItems:"center"}}>
                        {dates_array.length > 0 &&
                            ((d_arr, c_date) => {
                                console.log("cur date is: "+JSON.stringify(cur_date));
                                let c_index = d_arr.findIndex((el) => (el[0]==c_date[0] && el[1]==c_date[1] && el[2]==c_date[2]));
                                console.log(JSON.stringify(c_date)+" is at "+c_index);
                                //show first two choices
                                if (c_index == 0) {
                                    return (d_arr.slice(0,2).map((date, index) => {
                                        console.log("mapping date: "+JSON.stringify(date));
                                        if (index == 0) {
                                            this.prev_date = null;
                                            return ([
                                                <View style={{width:80, height:30, borderRightWidth:0, borderColor: "black", borderWidth:1, alignSelf:"center"}} />,
                                                <Button
                                                    key={0}
                                                    style={{width:80, height:30, justifyContent: "center", borderColor: "black", backgroundColor:  accent_color ? accent_color : "black"}}
                                                    first
                                                    active
                                                    onPress={() => {this.props.setCurDate([...date])}}
                                                >
                                                    <Text style={{color:"white"}}>{date.slice(1,3).reverse().join(".")}</Text>
                                                </Button>]);
                                        } else {
                                            this.next_date = [...date];
                                            return (<Button
                                                key={1}
                                                style={{width:80, height:30, justifyContent: "center", borderColor: "black"}}
                                                onPress={() => {this.props.setCurDate([...date])}}
                                            >
                                                <Text>{date.slice(1,3).reverse().join(".")}</Text>
                                                {/*<Ionicons name={"ios-arrow-forward"} />*/}
                                            </Button>);
                                        }
                                    }));
                                }

                                //show last two choices
                                if (c_index == (d_arr.length-1)){
                                    return (d_arr.slice(d_arr.length-2,d_arr.length).map((date, index) => {
                                        if (index == 1) {
                                            this.next_date = null;
                                            return ([<Button
                                                key={1}
                                                style={{width:80, height:30,borderRightWidth:0, justifyContent: "center", borderColor: "black", backgroundColor:  accent_color ? accent_color : "black"}}
                                                active
                                                onPress={() => {this.props.setCurDate([...date])}}
                                            >
                                                <Text style={{color:"white"}}>{date.slice(1,3).reverse().join(".")}</Text>
                                            </Button>,
                                            <View style={{width:80, height:30, borderLeftWidth:0, borderColor: "black", borderWidth:1, alignSelf:"center"}} />]);
                                        } else {
                                            this.prev_date = [...date];
                                            return (<Button
                                                key={0}
                                                style={{width:80, height:30, borderRightWidth:0, justifyContent: "center", borderColor: "black"}}
                                                onPress={() => {this.props.setCurDate([...date])}}
                                                first
                                            >
                                                {/*<Ionicons name={"ios-arrow-back"} />*/}
                                                <Text>{date.slice(1,3).reverse().join(".")}</Text>
                                            </Button>);
                                        }
                                    }));
                                }

                                //we are somewhere in the middle of dates array, show three choices
                                return (d_arr.slice(c_index-1,c_index+2).map((date, index) => {
                                    if (index == 0) {
                                        this.prev_date = [...date];
                                        return (<Button
                                            key={0}
                                            style={{width:80, height:30, borderRightWidth:0, justifyContent: "center", borderColor: "black"}}
                                            first
                                            onPress={() => {this.props.setCurDate([...date])}}
                                        >
                                            {/*<Ionicons name={"ios-arrow-back"} />*/}
                                            <Text>{date.slice(1,3).reverse().join(".")}</Text>
                                        </Button>);
                                    }
                                    if (index == 1) {
                                        return (<Button
                                            key={1}
                                            style={{width:80, height:30, justifyContent: "center", borderColor: "black", backgroundColor:  accent_color ? accent_color : "black"}}
                                            active
                                            onPress={() => {}}
                                        >
                                            <Text style={{color:"white"}}>{date.slice(1,3).reverse().join(".")}</Text>
                                        </Button>);
                                    }
                                    if (index == 2) {
                                        this.next_date = [...date];
                                        return (<Button
                                            key={2}
                                            style={{width:80, height:30, borderLeftWidth:0, justifyContent: "center", borderColor: "black"}}
                                            last
                                            onPress={() => {this.props.setCurDate([...date])}}
                                        >
                                            <Text>{date.slice(1,3).reverse().join(".")}</Text>
                                            {/*<Ionicons name={"ios-arrow-forward"} />*/}
                                        </Button>);
                                    }
                                }));
                            })(dates_array, cur_date)
                        }
                        {/*<Button style={{width:80, height:30, justifyContent: "center", borderColor:"black"}} first active><Ionicons name={"ios-arrow-back"} /><Text>{"1"}</Text></Button>
                        <Button style={{width:80, height:30, justifyContent: "center", borderColor:"black"}}><Text>{"2"}</Text></Button>
                        <Button style={{width:80, height:30, justifyContent: "center", borderColor:"black"}}><Text>{"3"}</Text><Ionicons name={"ios-arrow-forward"} /></Button>*/}
                    </Segment>

                    <View style={{flex:1, flexDirection:"row", justifyContent:"space-between", alignItems:"center", paddingBottom:4}}>
                        <TouchableOpacity
                            style={{justifyContent:"center", alignSelf:"center", marginLeft:20}}
                            onPress={() => {
                                if (this.next_date) this.props.setCurDate([...this.next_date])
                            }}
                        >
                            <Ionicons size={28} name={"ios-arrow-forward"} color={"black"}/>
                        </TouchableOpacity>
                        <TouchableOpacity style={{width:30, justifyContent:"center", alignSelf:"center"}} last onPress={() => {this.props.toggleFilterView()}}>
                            <Ionicons size={28} name={"ios-options"} color={"black"}/>
                        </TouchableOpacity>
                    </View>
                </View>
                {fact_array.length != 0 &&
                    <FlatList
                        style={{flex:1, backgroundColor:(background_color ? background_color : "white")}}
                        data={applyFilter(fact_array, id_map, current_filter)}
                        keyExtractor={(el) => {return(el["FactID"].toString())}}
                        renderItem={(el) => {
                            let fact = el.item;
                            let index = el.index;
                            if (fact["StartDate"].split("T")[0] != cur_date.join("-")) {
                                //console.log("wrong date: "+fact["StartDate"].split("T")[0]+"!="+cur_date.join("-"));
                                return null;
                            }
                            let fact_place_name = (fact["FactPlaceName"] && fact["FactPlaceName"].length != 0 ? fact["FactPlaceName"] : null);
                            return <FactCard
                                key={index}
                                //props for collapsed view
                                time_start={this.getTime(fact["StartDate"])}
                                time_end={this.getTime(fact["EndDate"])}
                                fact_name={fact["FactName"]}
                                fact_description={fact["FactDescription"]}
                                fact_place={fact_place_name}
                                //props for expanded view
                                fact_url={fact["FactStreamURL"]}
                                speakers={fact["Speakers"]}
                                fact_id={fact["FactID"]}
                                fact_rating={fact["FactRating"]}

                                fact_obj={fact}
                                navigation={this.props.navigation}

                                base_color={background_color ? "white" : null}
                                background_color={background_color}
                                accent_color={accent_color}
                            />
                        }}
                    />
                }
            </Container>
        );
    }
}

/*function intersect(a, b) {
    let t;
    if (b.length > a.length) {t = b; b = a; a = t;} // indexOf to loop over shorter
    return a.filter(function (e) {
        return b.indexOf(e) > -1;
    });
}

function sameArr(a, b) {
    if (a.length != b.length) return false;
    if (intersect(a,b).length != b.length) return false;
    return true;
}*/

const mapStateToProps = (state, ownProps) => {
    return {
        lang:               state.data.settings.lang,
        event:              state.data.event,
        event_json:         state.data.event.event_json,
    }
};


const mapDispatchToProps = dispatch => {
    return {
        toggleFilterView:           () => dispatch(toggleFilterView()),
        setCurDate:                 (cur_date) => dispatch(setCurDate({cur_date}))
    }
};


export default connect(mapStateToProps, mapDispatchToProps)(FactsTab);
