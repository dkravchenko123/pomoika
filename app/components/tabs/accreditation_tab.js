import React from "react";
import {Button, Card, CardItem, CheckBox, Container, Grid, ListItem, Row, ScrollableTab, Tabs, Tab, TabHeading} from "native-base";
import {ScrollView, Text, View} from "react-native";


export class AccreditationTab extends React.Component {
    constructor(props){
        super(props);
    }

    render() {
        return (
            <Container>
                <Card style={{marginLeft:15, marginRight:15, marginTop:15}}>
                            <CardItem style={{justifyContent: "space-between"}}><Text>{"Анкета"}</Text><Button bordered light><Text>{"Загрузить"}</Text></Button></CardItem>
                            <CardItem style={{justifyContent: "space-between"}}><Text>{"Паспорт"}</Text><Button bordered light><Text>{"Загрузить"}</Text></Button></CardItem>
                            <CardItem style={{justifyContent: "flex-start"}}><CheckBox checked={true} /><Text>{"Стать спикером"}</Text></CardItem>
                            <Button><Text>{"Отправить"}</Text></Button>
                </Card>
            </Container>
        )
    }
}