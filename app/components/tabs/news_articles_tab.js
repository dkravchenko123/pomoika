import React from "react";
import {
    ActivityIndicator,
    FlatList,
    Image, RefreshControl,
    ScrollView,
    Text,
    View,
} from "react-native";
import {Button, Card, Container} from "native-base";
import {EventCard} from "../cards/event_card";
import {cancel_reconnect, getws, init_timeout} from "../../methods/webSocket";
import {get_events} from "../../methods/account_requests";
import {disableNetWarn, enableNetWarn, toggleNewsOverlay} from "../../actions/control";
import {connect} from "react-redux";
import NewsOverlay from "../overlays/news_overlay";
import ShortNewsCard from "../cards/ShortNewsCard";
import {backendRequest, backendRequestCustomSocket, extractResponse} from "../../methods/ws_requests";
import {addArticlesTag, receiveData, removeData, setArticles, setArticlesTagFilter} from "../../actions/data";
import {CalendarDayCard} from "../cards/CalendarDayCard";
import {WS_URL} from "../../constants/backend";


class NewsArticlesTab extends React.Component {
    constructor (props) {
        super(props);
        this.state = {
            needs_update: true,
            //date: new Date("2019-09-10")
            start_id:null,
            will_load:true,
            tag_type_id:this.props.type == 1 ? "4" : "5"
        };

        this.news_events = [];
        this._onRefresh = this._onRefresh.bind(this);
    }

    componentDidMount() {
        if (!this.props.articles || !this.props.articles.hasOwnProperty(this.props.type) || !this.props.articles[this.props.type].length > 0) {
            this._onRefresh();
        } else {
            let existing_data = this.props.articles[this.props.type];
            this.setState({
                needs_update:false,
                start_id: existing_data[existing_data.length - 1]["articleid"]
            });
        }
    }

    _onRefresh() {
        this.props.setArticlesTagFilter([]);
        let refresh_socket = new WebSocket(WS_URL);
        refresh_socket.onmessage = (msg) => {
            let parsed_msg = JSON.parse(msg.data);
            console.log(parsed_msg);
            let new_data = parsed_msg.data;
            if (new_data) {
                this._updateTags(new_data);
                let new_articles = {...this.props.articles};
                new_articles[this.props.type] = new_data;
                this.props.setArticles(new_articles);
                this.setState({needs_update:false, start_id: new_data[new_data.length - 1]["articleid"]});
            }
            refresh_socket.close();
        };
        this.setState({needs_update:true, start_id:null});
        refresh_socket.onopen = () => {backendRequestCustomSocket(refresh_socket, "getArticlesTitles", this.props.userToken, {articleType:this.props.type});};
    }

    _nextDate () {
        let refresh_socket = new WebSocket(WS_URL);
        refresh_socket.onmessage = (msg) => {
            let parsed_msg = JSON.parse(msg.data);
            console.log(parsed_msg);
            let new_data = parsed_msg.data;
            if (new_data) {
                this._updateTags(new_data);
                let new_articles = {...this.props.articles};
                new_articles[this.props.type] = [ ...new_articles[this.props.type], ...(new_data.slice(1, new_data.length))];
                this.props.setArticles(new_articles);
                this.setState({needs_update:false, start_id: new_data[new_data.length - 1]["articleid"]});
            }
            refresh_socket.close();
        };
        refresh_socket.onopen = () => {backendRequestCustomSocket(refresh_socket, "getArticlesTitles", this.props.userToken, {StartId:this.state.start_id, articleType:this.props.type});};
        this.setState({needs_update:true})
    }

    _updateTags (articles) {
        if (articles.length > 0) {
            for (let a = 0; a < articles.length; a++) {
                if (articles[a].tags && articles[a].tags.length > 0) {
                    articles[a].tags.forEach((tag) => {this.props.addArticlesTag(tag)});
                }
            }
        }
    }

    render() {
        return(
            <Container style={{backgroundColor:"rgb(240,240,240)"}}>
                {this.props.articles && this.props.articles[this.props.type] && this.props.articles[this.props.type].length > 0
                    ?
                        <View style={{flex:1}}>
                            {this.props.articles[this.props.type].length > 0 &&
                                <FlatList
                                    //ref={(ref) => {this.flatlist_ref=ref}}
                                    contentContainerStyle={{paddingBottom:60, paddingTop:50}}
                                    refreshing={this.state.needs_update}
                                    onRefresh={this._onRefresh}
                                    progressViewOffset={50}
                                    keyExtractor={(el) => {return el.articleid.toString()}}
                                    data={this.props.articles[this.props.type]}
                                    renderItem={(el, index) => {
                                        if (this.props.articles_tag_filter && this.props.articles_tag_filter.length > 0) {
                                            if (el.item.tags && el.item.tags.length > 0) {
                                                for (let f_t_ind = 0; f_t_ind < this.props.articles_tag_filter.length; f_t_ind++){
                                                    if (el.item.tags.findIndex((t) => t.Tagid == this.props.articles_tag_filter[f_t_ind].Tagid) != -1) {
                                                        return <ShortNewsCard {...el.item} navigation={this.props.navigation} type={this.props.type}/>;
                                                    }
                                                }
                                                console.log("no filter match");
                                                return <View/>;
                                            } else {
                                                console.log("no filters", el);
                                                return <View/>;
                                            }
                                            console.log("my tag ids", my_tag_ids);
                                        }

                                        return <ShortNewsCard {...el.item} navigation={this.props.navigation} type={this.props.type}/>;
                                    }}
                                    onEndReached={() => {
                                        if (!this.props.articles_tag_filter || this.props.articles_tag_filter.length == 0) {
                                            this._nextDate();
                                        }
                                    }}
                                    onEndReachedThreshold={0.5}
                                />
                            }
                        </View>
                    :
                        <ScrollView refreshControl={<RefreshControl progressViewOffset={50} refreshing={this.state.needs_update} onRefresh={this._onRefresh}/>}>
                            <View style={{height:10}} />
                        </ScrollView>
                }

            </Container>
        )
    }
}

const mapStateToProps = state => {
    return {
        received_data:          state.data.received_data,
        userToken:              state.data.userToken,
        articles:               state.data.articles,
        articles_tag_filter:    state.data.articles_tag_filter
    }
};

const mapDispatchToProps = dispatch => {
    return {
        receiveData:            (data) => dispatch(receiveData(data)),
        removeData:             (key) => dispatch(removeData({key})),
        toggle:                 () => dispatch(toggleNewsOverlay()),
        enableNetWarn:          () => dispatch(enableNetWarn()),
        disableNetWarn:         () => dispatch(disableNetWarn()),
        setArticles:            (articles) => dispatch(setArticles({articles})),
        addArticlesTag:         (tag) => dispatch(addArticlesTag({tag})),
        setArticlesTagFilter:   (filter) => dispatch(setArticlesTagFilter({filter}))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(NewsArticlesTab);
