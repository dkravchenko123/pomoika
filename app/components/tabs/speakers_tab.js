import {Button, Container, Segment, Tab} from "native-base";
import {FlatList, ScrollView, Text, View} from "react-native";
import {FactCard} from "../cards/FactCard";
import React from "react";
import {receiveData} from "../../actions/data";
import {connect} from "react-redux";
import {Ionicons, MaterialIcons} from "@expo/vector-icons";
import SpeakerCard from "../cards/SpeakerCard";



class SpeakersTab extends React.Component {
    render () {
        let background_color = "rgb(240,240,240)"; //this.props.event_json.style ? (this.props.event_json.style.Primary_color ? this.props.event_json.style.Primary_color : null) : null;
        let accent_color = null;     //this.props.event_json.style ? (this.props.event_json.style.Accent_color ? this.props.event_json.style.Accent_color : "white") : null;
        return (
            <Container>
                <View style={{flex:1, backgroundColor:background_color}}>
                {/*<ScrollView style={{flex:1}}>
                    {this.props.speakers_array && this.props.speakers_array.length != 0 &&
                        this.props.speakers_array.map((speaker, index) => {
                            return <SpeakerCard
                                        key={index}
                                        speaker={speaker}

                                        navigation={this.props.navigation}
                            />;
                        })
                    }
                    {!(this.props.speakers_array && this.props.speakers_array.length != 0) &&
                        <Text>No speakers!</Text>
                    }
                    <View style={{height:120}} />
                </ScrollView>*/}
                {this.props.speakers_array && this.props.speakers_array.length != 0 &&
                    <FlatList
                        data={this.props.speakers_array}
                        keyExtractor={(el) => el["SpeakerID"].toString()}
                        renderItem={(el) => {
                            let speaker = el.item;
                            return (
                                <SpeakerCard
                                    speaker={speaker}
                                    navigation={this.props.navigation}
                                    base_color={background_color ? "white" : null}
                                />
                            );
                        }}
                    />
                }
                </View>
            </Container>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        lang:               state.data.settings.lang,
        speakers_array:     state.data.event.speakers_array,
        event_json:         state.data.event.event_json,
    }
};

export default connect(mapStateToProps)(SpeakersTab);
