import React from "react";
import {
    Alert,
    ActivityIndicator,
    Image,
    KeyboardAvoidingView,
    Text,
    TextInput,
    ToastAndroid,
    TouchableOpacity,
    View
} from "react-native";
import {Button, Card, Col, Container, Content, Footer, FooterTab, Grid, Header, Icon, Item, Input, Row} from "native-base";
import {SimpleLineIcons, Ionicons} from "@expo/vector-icons";
import {connect} from "react-redux";
import mime from 'react-native-mime-types';
import * as FileSystem from 'expo-file-system';
import {ModalCenterCard} from "./modal_center_card";


const b64 = require("base64-js");


export class FileMessageOverlay extends React.Component {
    constructor(props) {
        super(props);
        this.state={
            text_input:""
        };
        this.sending = false;
    }

    render() {
        return (
            /*<KeyboardAvoidingView behavior="padding" enabled style={{position:"absolute", top:0, left:0, right:0, bottom:0, justifyContent: "center", alignItems: "center", backgroundColor:"#0000002f"}} zIndex={100}>
                <Card style={{height:60}}>
                    {this.props.file_uri &&

                        <Text>{this.props.file_name}</Text>
                    }
                    <View style={{width:300, height:40, borderTopWidth: 2, borderColor:"#3c3c3c", flexDirection: "row"}}>
                        <TextInput placeholder={"Type your message..."} style={{width:240}} value={this.state.text_input} onChangeText={(val) => {this.setState({text_input:val})}}/>
                        <TouchableOpacity
                            onPress={() => {
                                //let format = this.props.image_uri.split(".").slice(-1)[0].toLowerCase();
                                let format = mime.contentType(this.props.file_name.split("/").slice(-1)[0]);
                                console.log("format of ", this.props.file_name, format);
                                try {
                                    FileSystem.readAsStringAsync(this.props.file_uri, {encoding: FileSystem.EncodingType.Base64}).then((file_string) => {
                                        console.log("file string length", file_string.length);
                                        this.props.client.uploadContent(b64.toByteArray(file_string),
                                        {
                                            type: format,
                                            includeFilename: true,
                                            name: this.props.file_name
                                        })
                                        .done((url) => {
                                            console.log(url);
                                            let content = {
                                                msgtype: "m.file",
                                                body: this.state.text_input,
                                                info: {
                                                    mimetype: format
                                                },
                                                url: url
                                            };
                                            this.props.client.sendMessage(this.props.room_id, content);
                                            this.props.trigger_overlay();
                                        })
                                    });
                                } catch (err) {
                                    console.log(err);
                                }
                            }}
                        >
                            <View style={{height:40, width:60, justifyContent:"center", alignItems:"center", marginBottom:4}}>
                                <Text style={{color:"#58aae1", alignSelf: "center"}}>Send</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </Card>
            </KeyboardAvoidingView>*/
            <ModalCenterCard close_fun={this.props.trigger_overlay}>
                {this.props.file_uri &&

                <Text>{this.props.file_name}</Text>
                }
                {this.props.file_uri.length > 1000000 &&
                    <Text style={{color:"red"}}>!!Файлы данного размера не поддерживаются!!</Text>
                }
                <View style={{width:300, height:40, borderTopWidth: 2, borderColor:"#3c3c3c", flexDirection: "row", alignSelf:"center"}}>
                    <TextInput placeholder={"Комментарий..."} style={{width:240}} value={this.state.text_input} onChangeText={(val) => {this.setState({text_input:val})}}/>
                    <TouchableOpacity
                        onPress={() => {
                            if (!this.sending) {
                                let format = mime.contentType(this.props.file_name.split("/").slice(-1)[0]);
                                console.log("format of ", this.props.file_name, format);
                                this.sending = true;
                                this.forceUpdate();
                                try {
                                    //FileSystem.readAsStringAsync(this.props.file_uri, {encoding: FileSystem.EncodingType.Base64}).then((file_string) => {
                                        let file_string = this.props.file_uri.split(",")[1];
                                        console.log("file string length", file_string.length);
                                        if (file_string.length > 1000000) {
                                            //Alert.alert("Внимание!", "Выбран файл слишком большого размера!");
                                            this.props.trigger_overlay();
                                            this.sending = false;
                                            this.forceUpdate();
                                            return;
                                        }
                                        this.props.client.uploadContent(b64.toByteArray(file_string),
                                            {
                                                type: format,
                                                includeFilename: true,
                                                name: this.props.file_name
                                            })
                                            .done((url) => {
                                                console.log(url);
                                                let content = {
                                                    msgtype: "m.file",
                                                    body: `${this.state.text_input}\n${this.props.file_name}`,
                                                    info: {
                                                        mimetype: format
                                                    },
                                                    url: url
                                                };
                                                this.sending = false;
                                                this.props.client.sendMessage(this.props.room_id, content);
                                                this.props.trigger_overlay();
                                            })
                                    //});
                                } catch (e) {
                                    Alert.alert("Внимание!", "Произошла ошибка при загрузке");
                                    this.props.trigger_overlay();
                                    this.sending = false;
                                    console.log("encountered an error", e);
                                }
                            }
                        }}
                    >
                        <View style={{height:40, width:60, justifyContent:"center", alignItems:"center", marginBottom:4}}>
                            {this.sending
                                ?
                                    <ActivityIndicator style={{alignSelf:"center"}} size={"small"} color={"#58aae1"} />
                                :
                                    <Ionicons style={{alignSelf:"center"}} name={"md-send"} color={"rgb(134,38,51)"} size={36}/>
                            }
                        </View>
                    </TouchableOpacity>
                </View>
            </ModalCenterCard>
        );
    }
}
