import React from "react";
import {
    BackHandler,
    Image,
    Keyboard,
    KeyboardAvoidingView, Platform,
    Text,
    TextInput,
    ToastAndroid,
    TouchableOpacity,
    View
} from "react-native";
import {Button, Card, Col, Container, Content, Footer, FooterTab, Grid, Header, Icon, Item, Input, Row} from "native-base";
import {SimpleLineIcons, Ionicons} from "@expo/vector-icons";
import {connect} from "react-redux";
import mime from 'react-native-mime-types';
import * as FileSystem from 'expo-file-system';


const b64 = require("base64-js");


export class ModalCenterCard extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        Keyboard.dismiss();
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    }

    handleBackPress = () => {
        if (this.props.close_fun) this.props.close_fun();
        Keyboard.dismiss();
        return true;
    };

    componentWillUnmount() {
        this.backHandler.remove();
    }

    render() {
        return (
            <KeyboardAvoidingView  behavior="padding" enabled style={{position:"absolute", top:0, left:0, right:0, bottom:0, justifyContent: "center", alignItems: "center", backgroundColor:"#0000002f"}} zIndex={100}>
                <Card style={{padding:10, borderRadius:10}}>
                    {this.props.children}
                </Card>
            </KeyboardAvoidingView>
        );
    }
}
