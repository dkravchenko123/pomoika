import {Text} from "react-native";
import React from "react";

export class Text extends React.Component {
    constructor(props){
        super(props);
    }

    render(){
        let passed_props = {...this.props};
        return (
            <Text {...passed_props}></Text>
        );
    }
}
