import React from "react";
import {Button} from "native-base";
import {View} from "react-native";
import {SimpleLineIcons, Ionicons, MaterialIcons} from "@expo/vector-icons";


export class UpdateButton extends React.Component {
    constructor(props) {
        super(props);

    }

    render() {
        return (
            <View style={{flex:1, width:"100%", justifyContent: "center", alignItems: "center"}}>
                <Button
                    style={{alignSelf: "center"}}
                    transparent
                    onPress={() => {
                        this.props.action();
                    }}
                >
                    <MaterialIcons size={35} name="update" />
                </Button>
            </View>
        );
    }
}