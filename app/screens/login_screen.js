import React from 'react';
import {Button, Image, Platform, ScrollView, StyleSheet, Text, TextInput, View} from 'react-native';
import ImageResizeMode from 'react-native/Libraries/Image/ImageResizeMode';
import {Card, CardItem} from 'native-base';
import LoginCard from "../components/cards/LoginCard";
import {
    changeLang,
    clearForm,
    receiveData,
    removeData, setChatLogin, setChatPassword,
    setChatToken,
    setGuestStatus,
    updateUserToken
} from "../actions/data";
import {decrementFormCounter, disableNetWarn, enableNetWarn, incrementFormCounter} from "../actions/control";
import {connect} from "react-redux";
import {getws} from "../methods/webSocket";
import NetModalWarning from "../components/cards/NetModalWarning";
import {initializedStore} from "../store";
import {Notifications} from "expo";

//const emitter = require("tiny-emitter/instance");
//emitter.on("connection_lost", () => {initializedStore.dispatch(enableNetWarn())});

export default class LoginScreen extends React.Component {
    componentDidMount() {
        if (Platform.OS != "ios" && Platform.OS != "android") {
            const brokenActionSheetOnWeb = document.querySelector(
                '#root div + div[style*="border-color: rgb(255, 0, 0);"]',
            );
            if (brokenActionSheetOnWeb) {
                console.log("found red box");
                brokenActionSheetOnWeb.style.display = 'none'
            } else {
                console.log("have not found the red box", brokenActionSheetOnWeb);
            }
        }
    }

    render() {
        return (
            <ScrollView keyboardShouldPersistTaps="never" contentContainerStyle={{flexGrow : 1, justifyContent : 'center'}}>
                <NetModalWarning />
                {/*<View style={{height:80}} />*/}
                <View style={styles.container}>
                    <View style={{width:300}}>
                        {/*<Image style={{width:300, height:46}} resizeMode={"contain"} source={require('../resources/rk.png')}/>*/}
                        <LoginCard navigation={this.props.navigation}/>
                    </View>
                </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width:"100%",
        height:"100%",
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',

    },
});




