import React from "react";
import {Button, Card, Container, Content, Footer, FooterTab, Header, Icon, Item, Input} from "native-base";
import {connect} from "react-redux";
import SubscreenHeader from "../../components/headers_footers/subscreen_header";
import NetModalWarning from "../../components/cards/NetModalWarning";
import DrawerContent from "../../components/cards/DraweContent";
import Drawer from "react-native-drawer";
import {addContacts} from "../../actions/data";
import ChatUserInfoCard from "../../components/cards/ChatUserInfoCard";
import {Text, TouchableOpacity} from "react-native";
import button from "../../styles/buttons";

class ChatDMInfoScreen extends React.Component {
    constructor(props) {
        super(props);
    }

    componentWillUnmount() {
        if (this.didBlurSubscription) this.didBlurSubscription.remove();
    }

    render () {
        let background_color = "rgb(240,240,240)";
        let accent_color = "white";

        return (
            <Container>
                {/* <SubscreenHeader custom_back={this.props.toggle}/> */}
                <Content style={{flex:1, backgroundColor:(background_color ? background_color : "white")}}>
                    <ChatUserInfoCard
                        toggle={this.props.toggle}
                        addressee_matrix_id={this.props.addressee_matrix_id}
                        action_buttons={[
                            <TouchableOpacity
                                style={[button.header, button.active, {alignSelf:"left", marginTop:20, marginLeft:20, width:220}]}
                            >
                                <Text style={{color:"white", fontSize:12}}>Заблокировать</Text>
                            </TouchableOpacity>
                        ]}
                    />
                </Content>
            </Container>
        );
    }
}

const mapStateToProps = state => {
    return {
        //pi_overlay:         state.control.pi_overlay,
        userToken:      state.data.userToken,
        lang:           state.data.settings.lang,
        contacts:       state.data.chat.contacts
    }
};

const mapDispatchToProps = dispatch => {
    return {
        //toggle: () => dispatch(togglePersonalInfoOverlay()),
        addContacts:             (contact) => dispatch(addContacts({contact}))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(ChatDMInfoScreen);
