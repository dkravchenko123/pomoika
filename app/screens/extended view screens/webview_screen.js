import React from "react";
import {Dimensions, Image, Platform, Text, ToastAndroid, TouchableOpacity, View, WebView} from "react-native";
import {Button, Card, Container, Content, Footer, FooterTab, Header, Icon, Item, Input} from "native-base";
import {SimpleLineIcons, Ionicons} from "@expo/vector-icons";
import {styles} from "../../styles/header_footer_styles";

import AccountTab from "../../components/pages/account_page";
import {HomePage} from "../../components/pages/home_page";
import {SearchScreen} from "../../components/pages/search_page";
import {togglePersonalInfoOverlay} from "../../actions/control";
import {connect} from "react-redux";
import PI_Overlay from "../../components/overlays/personal_info_overlay";
import EventPage from "../../components/pages/facts_sponsors_partners_page";
import DefaultHeader from "../../components/headers_footers/main_header";
import SubscreenHeader from "../../components/headers_footers/subscreen_header";
import AHWebView from "react-native-webview-autoheight";
import Drawer from "react-native-drawer";
import DrawerContent from "../../components/cards/DraweContent";



class WebViewScreen extends React.Component {
    constructor(props) {
        super(props);

        this.drawer = new React.createRef();
        this.menu_fun = this.menu_fun.bind(this);
        this.close_drawer = this.close_drawer.bind(this);
    }

    menu_fun () {
        this.drawer.open();
    }

    close_drawer () {
        this.drawer.close();
    }

    componentDidMount() {
        this.didBlurSubscription = this.props.navigation.addListener(
            'willBlur',
            payload => {
                if (this.close_drawer) this.close_drawer();
                //console.log('willBlur', payload);
            }
        );
    }

    componentWillUnmount() {
        if (this.didBlurSubscription) this.didBlurSubscription.remove();
    }

    render () {
        const ww = Dimensions.get("window").width;
        const wh = Dimensions.get("window").height;
        let uri = this.props.navigation.getParam('uri', null);
        console.log("opening "+uri);
        let event_screen = this.props.navigation.getParam('event_screen', false);
        if (uri.slice(0,3) == "www") {
            uri = "https://" + uri;
        }

        return (
            <Drawer
                content={<DrawerContent is_event={this.props.navigation.getParam("is_event", null)} navigation={this.props.navigation} open_facts={this.open_facts} close_drawer={this.close_drawer}/>}
                ref={(r) => this.drawer = r}
                openDrawerOffset={0.0}
                side={"right"}
                acceptPan
                negotiatePan
            >
                <Container>
                    <SubscreenHeader custom_back={this.props.navigation.getParam("custom_back", null)} menu_fun={this.menu_fun} navigation={this.props.navigation} is_event_screen={event_screen}/>
                    <Content scrollEnabled={true}>
                        {(uri.slice(0,4) == "http")
                            ?
                                <AHWebView
                                    useWebKit
                                    style={{/*height:wh-50,*/ width:ww}}
                                    source={{uri: uri}}
                                    originWhitelist={[uri]}
                                    scalesPageToFit={Platform.OS == "ios"}
                                    scrollEnabled
                                />
                            :
                                <AHWebView
                                    useWebKit
                                    style={{/*height:wh-50, */width:ww}}
                                    source={{html: uri}}
                                    originWhitelist={['*']}
                                    scalesPageToFit={Platform.OS == "ios"}
                                    scrollEnabled
                                />
                        }

                    </Content>
                </Container>
            </Drawer>
        );
    }
}

const mapStateToProps = state => {
    return {
        pi_overlay: state.control.pi_overlay,
        search_active:      state.control.search_active,
    }
};

const mapDispatchToProps = dispatch => {
    return {
        //toggle: () => dispatch(togglePersonalInfoOverlay()),
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(WebViewScreen);
