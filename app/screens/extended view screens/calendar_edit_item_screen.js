import React from "react";
import {
    ActionSheetIOS,
    Image,
    KeyboardAvoidingView, Platform,
    ScrollView,
    Text,
    TextInput,
    ToastAndroid,
    TouchableOpacity,
    View
} from "react-native";
import {
    Button,
    Card,
    Col,
    Container,
    Content,
    Footer,
    FooterTab,
    Grid,
    Header,
    Icon,
    Item,
    Input,
    Row,
    Picker
} from "native-base";
import {SimpleLineIcons, Ionicons} from "@expo/vector-icons";
import {styles} from "../../styles/header_footer_styles";
import {connect} from "react-redux";
import SubscreenHeader from "../../components/headers_footers/subscreen_header";
import {cancel_reconnect, getws, init_timeout} from "../../methods/webSocket";
import DatePicker from "react-native-datepicker";
import {new_user} from "../../methods/register";
import {backendRequest, backendRequestCustomSocket, extractResponse} from "../../methods/ws_requests";
import {receiveData, removeData, setCalendarNeedsUpdate} from "../../actions/data";
import Autocomplete from "react-native-autocomplete-input";
import NetModalWarning from "../../components/cards/NetModalWarning";
import field from "../../styles/fields";
import DrawerContent from "../../components/cards/DraweContent";
import Drawer from "react-native-drawer";

import button from "../../styles/buttons";
import card from "../../styles/cards";
import UserSearchField from "../../components/forms/static_fields/UserSearchField";
import {WS_URL} from "../../constants/backend";
import UserSearchCard from "../../components/cards/UserSearchCard";
import UserDisplayCard from "../../components/cards/UserDisplayCard";

class CalendarEditItemScreen extends React.Component {
    constructor (props) {
        super (props);
        let item = this.props.navigation.getParam("item", null);
        this.state = {
            item_id:                item.item_id,
            date:                   item.date ? new Date( Platform.OS == "android" ? item.date.toString().slice(0, 15).split("T").join(" ") : item.date) : new Date(),
            date_display:           item.date ? item.date.toString() : null,
            user_id:                item.user_id ? item.user_id.toString() : null,
            fact_id:                item.fact_id ? item.fact_id.toString() : null,
            item_types:[],
            item_type_id:           item.item_type_id ? item.item_type_id.toString() : null,
            event_id:               item.event_id ? item.event_id.toString() : null,
            name:                   item.name ? item.name.toString() : null,
            description:            item.description ? item.description.toString() : null,
            user_results:[],
            guests:                 item.guests ? item.guests : [],
            searching:false,

            //user search
            user_search_active:     false,
            user_search_query:      "",
            user_search_results:    [],
        };

        this.drawer = new React.createRef();
        this.menu_fun = this.menu_fun.bind(this);
        this.close_drawer = this.close_drawer.bind(this);
        this.choose_user = this.choose_user.bind(this);
    }

    menu_fun () {
        this.drawer.open();
    }

    close_drawer () {
        this.drawer.close();
    }

    choose_user (user) {
        this.setState({
            guests: [ ...this.state.guests, user],
        });
    }

    componentDidMount() {
        let type_socket = new WebSocket(WS_URL);

        type_socket.onmessage = (msg) => {
            let parsed_msg = JSON.parse(msg.data);
            console.log(parsed_msg);
            if (parsed_msg.statusCode == 200) {
                let response = parsed_msg.data.sort((el1, el2) => el1.itemtypeid - el2.itemtypeid);
                //console.log(response);
                this.setState({
                    item_types:response,
                    item_type_id:this.state.item_type_id ? this.state.item_type_id : response[0].itemtypeid
                });
            }
            type_socket.close();
        };

        type_socket.onopen = () => {
            backendRequestCustomSocket(type_socket, "calendarGetItemtype", this.props.userToken);
        };

        this.didBlurSubscription = this.props.navigation.addListener(
            'willBlur',
            payload => {
                if (this.close_drawer) this.close_drawer();
                //console.log('willBlur', payload);
            }
        );
    }

    componentWillUnmount() {
        if (this.didBlurSubscription) this.didBlurSubscription.remove();
    }


    render () {
        return (
            <Drawer
                content={<DrawerContent navigation={this.props.navigation} open_facts={this.open_facts} close_drawer={this.close_drawer}/>}
                ref={(r) => this.drawer = r}
                openDrawerOffset={0.0}
                side={"right"}
                acceptPan
                negotiatePan
            >
                <KeyboardAvoidingView style={{flex:1}} enabled behavior={"padding"}>
                    <Container>
                        <NetModalWarning />
                        <SubscreenHeader menu_fun={this.menu_fun} navigation={this.props.navigation}/>
                        <Content>
                            <View style={[card.base, {padding:15}]}>
                                <Row style={{justifyContent:"center", flex:1}}>
                                    <Text style={{fontWeight:"bold", fontSize:20}}>{"Изменить событие"}</Text>
                                </Row>
                                <Row style={{marginTop:15, height:this.state.show ? 280 : 50, justifyContent:"center", flex:1, borderWidth:2, borderRadius:5, borderColor:"black", backgroundColor:"#fff"}}>
                                    {Platform.OS == "android"
                                        ?
                                        <DatePicker
                                            locale={this.props.lang == "ru" ? "ru-RU" : "en-EN"}
                                            mode="datetime"
                                            is24Hour={true}
                                            display="default"
                                            placeholder={"Время"}
                                            date={this.state.date}
                                            //value={this.state.date}
                                            onDateChange={(val) => {
                                                this.setState({date: val});
                                                //this.setState({date: val.split(" ").join("T")+":00Z"});
                                            }}
                                            style={[styles.login_picker, {
                                                marginTop: 5,
                                                paddingRight: 10,
                                                borderColor: "#000",
                                                width: "100%"
                                            }]}
                                            customStyles={{
                                                placeholderText: {
                                                    alignSelf: "flex-start",
                                                    paddingLeft: 7
                                                },
                                                dateText: {
                                                    alignSelf: "flex-start",
                                                    paddingLeft: 7
                                                },
                                                dateInput: {
                                                    borderColor: "white"
                                                },
                                            }}
                                            /*onChange={(event, date) => {
                                                date = date || this.state.date;

                                                this.setState({
                                                    show: false,//Platform.OS === 'ios' ? true : false,
                                                    date,
                                                });
                                            }}*/
                                            iconComponent={<Ionicons style={{marginHorizontal: 5}} size={30}
                                                                     name={"ios-calendar"}/>}
                                        />
                                        :
                                        <View style={{flex:1, justifyContent:"center", padding:10}}>
                                            <TouchableOpacity
                                                style={{flexDirection:"row", justifyContent:"space-between", alignItems:"center"}}
                                                onPress={() => {
                                                    this.setState({show:!this.state.show});
                                                }}
                                            >
                                                <Text>
                                                    {((date) => {
                                                        return(`${date.getHours()}:${(date.getMinutes()<10 ? "0" : "")+date.getMinutes().toString()} ${[date.getDate(), date.getMonth()+1, date.getFullYear()].join("/")}`);
                                                    })(this.state.date)}
                                                </Text>
                                                <Ionicons style={{marginHorizontal: 5}} size={30} name={this.state.show ? "ios-checkmark" :"ios-calendar"}/>
                                            </TouchableOpacity>
                                            {this.state.show &&
                                            <DateTimePicker
                                                locale={this.props.lang == "ru" ? "ru-RU" : "en-EN"}
                                                mode="datetime"
                                                is24Hour={true}
                                                display="default"
                                                placeholder={"Время"}
                                                //date={this.state.date}
                                                value={this.state.date}
                                                /*onDateChange={(val) => {
                                                    this.setState({date: val});
                                                    //this.setState({date: val.split(" ").join("T")+":00Z"});
                                                }}*/
                                                style={[styles.login_picker, {
                                                    marginTop: 5,
                                                    paddingRight: 10,
                                                    borderColor: "#000",
                                                    width: "100%"
                                                }]}
                                                customStyles={{
                                                    placeholderText: {
                                                        alignSelf: "flex-start",
                                                        paddingLeft: 7
                                                    },
                                                    dateText: {
                                                        alignSelf: "flex-start",
                                                        paddingLeft: 7
                                                    },
                                                    dateInput: {
                                                        borderColor: "white"
                                                    },
                                                }}
                                                onChange={(event, date) => {
                                                    //console.log("date change event", event);
                                                    date = date || this.state.date;

                                                    this.setState({
                                                        //show: false,//Platform.OS === 'ios' ? true : false,
                                                        date,
                                                    });
                                                }}
                                                iconComponent={<Ionicons style={{marginHorizontal: 5}} size={30}
                                                                         name={"ios-calendar"}/>}
                                            />
                                            }
                                        </View>
                                    }
                                </Row>
                                {this.state.item_types.length > 0 &&
                                    <Row style={{
                                        marginTop: 10,
                                        height: 50,
                                        justifyContent: "center",
                                        flex: 1,
                                        borderWidth: 2,
                                        borderRadius: 5,
                                        borderColor: "black",
                                        backgroundColor: "#fff"
                                    }}>
                                        {Platform.OS == "ios"
                                            ?
                                            <TouchableOpacity
                                                onPress={() => {
                                                    if (Platform.OS == 'ios') ActionSheetIOS.showActionSheetWithOptions(
                                                        {
                                                            options: this.state.item_types.map((item) => {
                                                                return item.itemtypename
                                                            }),
                                                        },
                                                        (buttonIndex) => {
                                                            this.setState({item_type_id: this.state.item_types[buttonIndex].itemtypeid});
                                                        },
                                                    );
                                                }}
                                            >
                                                <View
                                                    style={[styles.picker, {
                                                        borderColor: "#000",
                                                        flexDirection: "row",
                                                        flex: 1,
                                                        alignItems: "center",
                                                        justifyContent: "center"
                                                    }]}
                                                >
                                                    <Text>{this.state.item_types[this.state.item_type_id].itemtypename}</Text>
                                                </View>
                                            </TouchableOpacity>
                                            :
                                            <Picker
                                                placeholder={"Тип"}
                                                selectedValue={this.state.item_type_id}
                                                onValueChange={(value, itemIndex) => {
                                                    this.setState({item_type_id: value});
                                                }}
                                                style={[styles.picker, {
                                                    borderColor: "#000",
                                                    flexDirection: "row",
                                                    flex: 1
                                                }]}
                                            >
                                                {this.state.item_types.map((item) => {
                                                    return <Picker.Item key={item.itemtypeid} label={item.itemtypename}
                                                                        value={item.itemtypeid}/>
                                                })}
                                            </Picker>
                                        }
                                    </Row>
                                }
                                <Row style={{paddingLeft:8, marginTop:10, height:50, justifyContent:"flex-start", alignItems:"center", flex:1, borderWidth:2, borderRadius:5, borderColor:"black", backgroundColor:"#fff"}}>
                                    <TextInput
                                        style={[styles.login_text_input, styles.login_borders]}
                                        placeholder={"Название события"}
                                        value={this.state.name}
                                        onChangeText={(val) => {
                                            this.setState({name:val});
                                        }}
                                    />
                                </Row>
                                <Row style={{paddingLeft:8, marginTop:10, height:50, justifyContent:"flex-start", alignItems:"center", flex:1, borderWidth:2, borderRadius:5, borderColor:"black", backgroundColor:"#fff"}}>
                                    <TextInput
                                        style={[styles.login_text_input, styles.login_borders]}
                                        placeholder={"Описание"}
                                        value={this.state.description}
                                        onChangeText={(val) => {
                                            this.setState({description:val});
                                        }}
                                    />
                                </Row>

                                {/*<UserSearchField choose_user={this.choose_user} />*/}
                                <UserSearchCard accept_anyone square choose_user={this.choose_user} />

                                {this.state.guests.length != 0 &&
                                <Row style={{paddingLeft:0, marginTop:10, minHeight:50, maxHeight:140, marginHorizontal:5, flexDirection:"column", justifyContent:"flex-start", alignItems:"center", flex:1, borderWidth:2, borderRadius:5, borderColor:"black", backgroundColor:"#fff"}}>
                                    <Text style={{fontWeight:"bold", fontSize:16, color:"black"}}>Участники:</Text>
                                    <ScrollView nestedScrollEnabled={true} style={{paddingHorizontal:0, marginHorizontal:0, maxHeight:120, backgroundColor:"#fff"}}>
                                        {this.state.guests.map((el, index) => {
                                            console.log("viewing guest", el);
                                            return (
                                                <TouchableOpacity
                                                    key={el["firstnamerus"]||el["firstnameeng"]}
                                                    style={{flexDirection:"row", flex:1, alignItems:"center"}}
                                                    onPress={() => {
                                                        let new_guests = [ ...this.state.guests.slice(0, index), ...this.state.guests.slice(index + 1, this.state.guests.length)];
                                                        this.setState({guests: new_guests});
                                                    }}
                                                >
                                                    {/*<Text>{el["firstnamerus"]+" "+el["lastnamerus"]}</Text>
                                                    <Ionicons style={{marginLeft:5}} name={"ios-close"} size={14}/>*/}
                                                    <UserDisplayCard {...el} addressee_matrix_id={el.matrix_id}/>
                                                </TouchableOpacity>
                                            );
                                        })}
                                    </ScrollView>
                                </Row>
                                }

                                <TouchableOpacity
                                    style={[button.header, button.active, {alignSelf:"center", marginTop:20}]}
                                    onPress={() => {
                                        let cal_socket = new WebSocket(WS_URL);
                                        cal_socket.onopen = () => {
                                            backendRequestCustomSocket(
                                                cal_socket,
                                                "calendarUpdateItem",
                                                this.props.userToken,
                                                {
                                                    itemId:this.state.item_id,
                                                    //user_id:this.state.user_id,
                                                    date:this.state.date,
                                                    factId:this.state.fact_id,
                                                    itemTypeId:parseInt(this.state.item_type_id),
                                                    eventId:this.state.event_id,
                                                    name:this.state.name,
                                                    description:this.state.description,
                                                    guestIds:this.state.guests.map((g) => [parseInt(g.userid), 0])
                                                }
                                            );
                                            this.props.setCalendarNeedsUpdate(true);
                                            this.props.navigation.navigate("CalendarPage");
                                        };
                                    }}
                                >
                                    <Text style={{alignSelf: "center", color: "white"}}>
                                        {(() => {
                                            switch(this.props.lang){
                                                case "en":
                                                    return (
                                                        'Save'
                                                    );
                                                default:
                                                    return (
                                                        'Сохранить'
                                                    );
                                            }
                                        })()}
                                    </Text>
                                </TouchableOpacity>

                            </View>
                        </Content>
                    </Container>
                </KeyboardAvoidingView>
            </Drawer>
        );
    }
}

const mapStateToProps = state => {
    return {
        lang:               state.data.settings.lang,
        received_data:      state.data.received_data,
        userToken:          state.data.userToken
    }
};

const mapDispatchToProps = dispatch => {
    return {
        //toggle: () => dispatch(togglePersonalInfoOverlay()),
        receiveData:            (data) => dispatch(receiveData(data)),
        removeData:             (key) => dispatch(removeData({key})),
        setCalendarNeedsUpdate: (needs_update) => dispatch(setCalendarNeedsUpdate({needs_update}))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(CalendarEditItemScreen);
