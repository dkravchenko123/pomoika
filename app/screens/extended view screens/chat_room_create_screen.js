import {ActivityIndicator, Alert, FlatList, ScrollView, Text, TextInput, TouchableOpacity, View} from "react-native";
import React from "react";
import {
    addInvite,
    addJoined,
    receiveData,
    removeData,
    setChatToken,
    updMessages
} from "../../actions/data";
import {Card, Container, Content, Icon, Row} from "native-base";
import {connect} from "react-redux";
import {getchat} from "../../methods/chat_client";
import SubscreenHeader from "../../components/headers_footers/subscreen_header";
import {backendRequest, backendRequestCustomSocket, extractResponse} from "../../methods/ws_requests";
import {cancel_reconnect, getws, init_timeout} from "../../methods/webSocket";
import {disableNetWarn, enableNetWarn} from "../../actions/control";
import NetModalWarning from "../../components/cards/NetModalWarning";
import card from "../../styles/cards";
import field from "../../styles/fields";
import DrawerContent from "../../components/cards/DraweContent";
import Drawer from "react-native-drawer";
import button from "../../styles/buttons";
import UserSearchField from "../../components/forms/static_fields/UserSearchField";
import {WS_URL} from "../../constants/backend";
import UserSearchCard from "../../components/cards/UserSearchCard";
import UserDisplayCard from "../../components/cards/UserDisplayCard";
import {AntDesign, EvilIcons} from "@expo/vector-icons";

class RoomCreationScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            room_name: "",
            user_search_input:'',
            search_limit:10,
            user_results:[],
            users_chosen:[],
            searching:false
        };


        this.client = getchat();

        this.drawer = new React.createRef();
        this.menu_fun = this.menu_fun.bind(this);
        this.close_drawer = this.close_drawer.bind(this);
        this.choose_user = this.choose_user.bind(this);
    }

    menu_fun () {
        this.drawer.open();
    }

    close_drawer () {
        this.drawer.close();
    }

    choose_user (user) {
        this.setState({
            users_chosen: [ ...this.state.users_chosen, user]
        });
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.state.searching) {
            let response = extractResponse(this.props.received_data, "calendarGetPeople");

            if (response != null) {
                console.log("found "+response.length);
                if (response.length > 0) {
                    this.setState({
                        user_results:response,
                        searching:false,
                    });
                }
            }
        }
    }

    render () {
        return (
            <Container style={{height:window.height}}>
                {/* <NetModalWarning /> */}
                {/* <SubscreenHeader  custom_back={this.props.toggle}/> */}

                <Content style={{height:window.height}}>
                    <View style={{padding:15, height:window.height}}>
                        <View
                            style={{
                                //height: 60,
                                //borderBottomWidth: 2,
                                borderBottomColor: "rgb(220,219,216)",
                                flexDirection: "row",
                                justifyContent: "space-between",
                            }}
                        >
                            <View style={{
                                alignSelf: "center",
                                alignItems: "center",
                                textAlign: "center",
                                marginLeft: 10
                            }}>
                                <Text>Создание группового чата</Text>
                            </View>

                            <TouchableOpacity
                                style={{alignSelf: "Right"}}
                                onPress={() => {
                                    if (this.props.toggle)
                                    {
                                        this.props.toggle();
                                    }
                                }}
                            >
                                <EvilIcons size={32} name={"close"}/>
                            </TouchableOpacity>
                        </View>

                        <Row style={{paddingLeft:15, marginTop:10, height:50, justifyContent:"flex-start", alignItems:"center", flex:1, /*borderWidth:2,*/ borderRadius:10/*25*/, /*borderColor:"black",*/ backgroundColor:"#f2f3f4"/*"#fff"*/}}>
                            <TextInput
                                style={{flex:1, flexDirection:"row", alignItems:"center", margin:15}}
                                //placeholderTextColor={"#000"}
                                placeholder={"Название группы"}
                                onChangeText={(val) => {this.setState({room_name:val})}}
                            />

                        </Row>

                        {/*<UserSearchField choose_user={this.choose_user} />*/}
                        <UserSearchCard group z-index={100} zIndex={100} choose_user={this.choose_user} />

                        {this.state.users_chosen.length > 0 &&
                            <View z-index={90} zIndex={90} style={{flexDirection:"column"}}>
                                <Text style={{alignSelf:"center", fontSize:16}}>{"Пользователи:"}</Text>
                                <FlatList
                                    style={{marginLeft:0, marginTop:8}}
                                    data={this.state.users_chosen}
                                    keyExtractor={(el) => el.user_id}
                                    renderItem={(el) => {
                                        let user = el.item;
                                        return (

                                            <TouchableOpacity
                                                onPress={() => {
                                                    let new_users = [ ...this.state.users_chosen.slice(0, el.index), ...this.state.users_chosen.slice(el.index + 1, this.state.users_chosen.length)];
                                                    this.setState({users_chosen: new_users});
                                                }}
                                            >
                                                <UserDisplayCard addressee_matrix_id={user.matrix_id}/>
                                            </TouchableOpacity>
                                        );
                                    }}
                                />
                            </View>
                        }

                        <TouchableOpacity
                            z-index={92} zIndex={92}
                            style={[button.header, button.active, {alignSelf:"center", marginTop:20}]}
                            onPress={() => {
                                if (this.state.room_name.length > 0) {
                                    this.client.createRoom({"room_alias_name":this.state.room_name.split(" ").join("")})
                                        .then((resp_res) => {
                                            console.log("resp_res"+ JSON.stringify(resp_res));
                                            if (resp_res.room_id) {
                                                let info_socket = new WebSocket(WS_URL);
                                                info_socket.onopen = () => {
                                                    backendRequestCustomSocket(info_socket, "chatCreateRoom", this.props.userToken, {matrix_room_id:resp_res.room_id, room_name:this.state.room_name});
                                                };
                                                info_socket.onmessage = () => {
                                                    info_socket.close()
                                                };
                                                this.state.users_chosen.forEach((user, index, arr) => {
                                                    console.log("inviting "+user.firstnamerus + " " + user.lastnamerus + " to "+resp_res.room_id);
                                                    this.client.invite(resp_res.room_id, user.matrix_id)
                                                        .then(() => {
                                                            if (index == arr.length-1) this.props.toggle();
                                                        });
                                                });
                                            }
                                        });
                                } else {
                                    Alert.alert("Внимание!", "Введите название группы!");
                                }

                            }}
                        >
                            <Text style={{color:"white"}}>Создать</Text>
                        </TouchableOpacity>

                    </View>
                </Content>
            </Container>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        received_data:           state.data.received_data,
        lang:                    state.data.settings.lang,
        userToken:               state.data.userToken,
        //chatToken:               state.data.chat.token,
        //joined_rooms:            state.data.chat.joined_rooms,
        //invites:                 state.data.chat.invites
    }
};

const mapDispatchToProps = dispatch => {
    return {
        receiveData:            (data) => dispatch(receiveData(data)),
        removeData:             (key) => dispatch(removeData({key})),
        setChatToken:           (token) => dispatch(setChatToken({token})),
        updMessages:            (new_msg, room) => dispatch(updMessages({new_msg, room})),
        addInvite:              (room) => dispatch(addInvite({room})),
        addJoined:              (room) => dispatch(addJoined({room})),
        enableNetWarn:          () => dispatch(enableNetWarn()),
        disableNetWarn:          () => dispatch(disableNetWarn()),
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(RoomCreationScreen)
