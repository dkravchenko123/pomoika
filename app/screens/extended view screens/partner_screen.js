import React from "react";
import {Image, Text, ToastAndroid, TouchableOpacity, View} from "react-native";
import {Button, Card, Container, Content, Footer, FooterTab, Header, Icon, Item, Input} from "native-base";
import {SimpleLineIcons, Ionicons} from "@expo/vector-icons";
import {styles} from "../../styles/header_footer_styles";

import AccountTab from "../../components/pages/account_page";
import {HomePage} from "../../components/pages/home_page";
import {SearchScreen} from "../../components/pages/search_page";
import {togglePersonalInfoOverlay} from "../../actions/control";
import {connect} from "react-redux";
import PI_Overlay from "../../components/overlays/personal_info_overlay";
import EventPage from "../../components/pages/facts_sponsors_partners_page";
import DefaultHeader from "../../components/headers_footers/main_header";
import SubscreenHeader from "../../components/headers_footers/subscreen_header";
import NetModalWarning from "../../components/cards/NetModalWarning";
import DrawerContent from "../../components/cards/DraweContent";
import Drawer from "react-native-drawer";
import card from "../../styles/cards";


class PartnerScreen extends React.Component {
    constructor(props) {
        super(props);

        this.drawer = new React.createRef();
        this.menu_fun = this.menu_fun.bind(this);
        this.close_drawer = this.close_drawer.bind(this);
    }

    menu_fun () {
        this.drawer.open();
    }

    close_drawer () {
        this.drawer.close();
    }

    componentDidMount() {
        this.didBlurSubscription = this.props.navigation.addListener(
            'willBlur',
            payload => {
                if (this.close_drawer) this.close_drawer();
                //console.log('willBlur', payload);
            }
        );
    }

    componentWillUnmount() {
        if (this.didBlurSubscription) this.didBlurSubscription.remove();
    }

    render () {
        let background_color = "rgb(240,240,240)"; //this.props.event_json.style ? (this.props.event_json.style.Primary_color ? this.props.event_json.style.Primary_color : null) : null;
        let accent_color = "white"; //this.props.event_json.style ? (this.props.event_json.style.Accent_color ? this.props.event_json.style.Accent_color : "white") : null;

        let partner = this.props.navigation.getParam('partner', null);
        return (
            <Drawer
                content={<DrawerContent navigation={this.props.navigation} open_facts={this.open_facts} close_drawer={this.close_drawer}/>}
                ref={(r) => this.drawer = r}
                openDrawerOffset={0.0}
                side={"right"}
                acceptPan
                negotiatePan
            >
                <Container>
                    <NetModalWarning />
                    <SubscreenHeader menu_fun={this.menu_fun} is_event_screen navigation={this.props.navigation}/>
                    <Content style={{paddingTop:0, paddingHorizontal:0, backgroundColor:(background_color ? background_color : "white")}}>
                        <View style={[card.base, {padding:15, justifyContent:"space-between", backgroundColor:(background_color ? "white" : "rgb(220,219,216)"), borderRadius: 15}]}>
                            <Text style={{fontWeight:"bold", alignSelf:"center", fontSize:22}}>{partner["Name"]}</Text>
                            {partner.Imageurl &&
                                <Image
                                    style={{height:180, width:"100%", resizeMode:"center", paddingVertical:10}}
                                    source={{
                                        uri:partner["Imageurl"],
                                        method: "GET",
                                        headers: {
                                            Authorization:this.props.userToken
                                        }
                                    }}
                                />
                            }
                            <Text>{partner["Description"]}</Text>
                            {partner["URL"] != null && partner["URL"].length > 0 &&
                                <TouchableOpacity
                                    onPress={() => {
                                        this.props.navigation.navigate("WebViewScreen", {uri:partner["URL"], event_screen:true});
                                    }}
                                >
                                    <Text style={{textDecorationLine: 'underline', color:"blue"}}>{partner["URL"]}</Text>
                                </TouchableOpacity>
                            }
                        </View>
                    </Content>
                </Container>
            </Drawer>
        );
    }
}

const mapStateToProps = state => {
    return {
        pi_overlay:         state.control.pi_overlay,
        search_active:      state.control.search_active,
        event_json:         state.data.event.event_json,
    }
};

const mapDispatchToProps = dispatch => {
    return {
        //toggle: () => dispatch(togglePersonalInfoOverlay()),
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(PartnerScreen);
