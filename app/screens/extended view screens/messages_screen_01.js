import {
    Dimensions,
    Image,
    Keyboard,
    KeyboardAvoidingView,
    Linking,
    Platform,
    Share,
    Text,
    TextInput,
    TouchableOpacity,
    View
} from "react-native";
import {ActionSheet, Button, Picker, Root} from "native-base";
import * as Permissions from 'expo-permissions';
import * as ImagePicker from 'expo-image-picker';
import * as DocumentPicker from 'expo-document-picker';
import React from "react";
import {Bubble, GiftedChat, Message, Time, MessageImage} from 'react-native-gifted-chat';
import {
    addContacts,
    clearUnread,
    receiveData,
    removeData,
    setChatToken,
    sortMessages,
    updMessages
} from "../../actions/data";
import {FontAwesome, Entypo, AntDesign, SimpleLineIcons, Ionicons, MaterialCommunityIcons, MaterialIcons} from "@expo/vector-icons";
import {connect} from "react-redux";
import {getchat} from "../../methods/chat_client";
import SubscreenHeader from "../../components/headers_footers/subscreen_header";
import {ImageMessageOverlay} from "../../components/overlays/image_message_overlay";
import {FileMessageOverlay} from "../../components/overlays/FileMessageOverlay";
import {styles} from "../../styles/header_footer_styles";
import MainFooter from "../../components/headers_footers/main_footer";
import NetModalWarning from "../../components/cards/NetModalWarning";
import DrawerContent from "../../components/cards/DraweContent";
import Drawer from "react-native-drawer";
import {WS_URL} from "../../constants/backend";
import {backendRequestCustomSocket} from "../../methods/ws_requests";
import WebImage from "../../WebImage";
import ChatGroupInfoScreen from "./chat_group_info_screen";
import ChatDMInfoScreen from "./chat_dm_info_screen";

const window = Dimensions.get("window");

const IMAGE_BUTTONS = [
    {
        text: "Открыть камеру",
        icon: "camera"
    },
    {
        text: "Выбрать фото из галереи",
        icon: "folder"
    },
    {
        text: "Выбрать файл",
        icon: "add"
    }
];

const LONG_PRESS_BUTTONS = [
    {
        text: "Share",
        icon: "share"
    },
    {
        text: "Cancel",
        icon: "close"
    }
];

class MessageScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            room_id:this.props.room_id || null,
            user_id:this.props.user_id,
            full_name:this.props.full_name || null,
            avatar:this.props.avatar || null,
            is_not_group:this.props.is_not_group || null,
            addressee_matrix_id:this.props.addressee_matrix_id || null,
            input_height:36,

            dm_info_active:false,
            group_info_active:false,
        };

        this.chat_ref = new React.createRef();
        this.client = getchat();
        this._trigger_image_overlay = this._trigger_image_overlay.bind(this);
        this._trigger_file_overlay = this._trigger_file_overlay.bind(this);
        this.right_button = this.right_button.bind(this);
        this.renderBubble = this.renderBubble.bind(this);
    }

    componentDidMount() {
        if (Platform.OS != "ios" && Platform.OS != "android") {
            const brokenActionSheetOnWeb = document.querySelector(
                '#root div + div[style*="border-color: rgb(255, 0, 0);"]',
            );
            if (brokenActionSheetOnWeb) {
                console.log("found red box");
                brokenActionSheetOnWeb.style.display = 'none'
            } else {
                console.log("have not found the red box", brokenActionSheetOnWeb);
            }
        }

        this.props.clearUnread(this.state.room_id);
        this.props.sortMessages(this.state.room_id);
        console.log("addresse matrix id", this.state.addressee_matrix_id);
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if(prevState.input_height != this.state.input_height){
            if (this.chat_ref) this.chat_ref.resetInputToolbar()
        }
    }

    toggleDMInfo = () => {
        this.setState({dm_info_active:!this.state.dm_info_active});
    };

    toggleGroupInfo = () => {
        this.setState({group_info_active:!this.state.group_info_active});
    };

    right_button = (props) => {
        return (null);
        /*return (
            <Button
                transparent
                onPress={() => {
                    props.navigation.navigate("AddUserScreen", {room_id:this.state.room_id});
                }}
            >
                <AntDesign size={28} style={styles.header_nav_button} name={"addusergroup"}/>
            </Button>
        );*/
    };

    _trigger_image_overlay () {
        Keyboard.dismiss();
        this.setState({image_message_overlay:!this.state.image_message_overlay});
    }
    _trigger_file_overlay () {
        Keyboard.dismiss();
        this.setState({file_message_overlay:!this.state.file_message_overlay});
    }

    onSend(messages) {
        this.setState({input_height:36});
        messages.forEach((message) => {
            let content = {
                "body": message.text,
                "msgtype": "m.text"
            };
            this.client.sendEvent(this.state.room_id, "m.room.message", content, "", (err, res) => {
                console.log(err);
            });
            //this.props.updMessages({new_msg:message, room:this.state.room_id});
        });
        console.log("sending " + JSON.stringify(messages));
        //this.forceUpdate();
    }

    renderMessageImage = (msgProps) => {
        console.log(msgProps);
        return (
            <TouchableOpacity
                accessibilityRole='link'
                href={msgProps.currentMessage.image}
                target='_blank'
                /*onPress={() => {
                    //console.log("touched message image", msgProps.currentMessage);
                    Linking.openURL(msgProps.currentMessage.image);
                }}*/
            >
                <View pointerEvents={"none"}>
                    <MessageImage {...msgProps}/>
                </View>
            </TouchableOpacity>
        );
    };

    renderBubble (props) {
        console.log("rendering message bubble", props);
        let new_props = {...props};
        let contact = this.props.contacts[props.currentMessage.user.name];
        if (!contact) {
            new_props = {
                ...new_props,
                currentMessage:{
                    ...props.currentMessage,
                    user:{
                        ...props.currentMessage.user,
                        name:""
                    }
                }
            };
            let name_socket = new WebSocket(WS_URL);
            name_socket.onmessage = (msg) => {
                let parsed_msg = JSON.parse(msg.data);
                console.log(parsed_msg);
                if (parsed_msg.statusCode == 200) {
                    this.props.addContacts({[props.currentMessage.user.name]:parsed_msg.data[0]});
                    new_props = {
                        ...new_props,
                        currentMessage:{
                            ...props.currentMessage,
                            user:{
                                ...props.currentMessage.user,
                                name:`${parsed_msg.data[0].firstnamerus} ${parsed_msg.data[0].lastnamerus}`
                            }
                        }
                    };
                }
                name_socket.close();
            };
            name_socket.onopen = () => {
                backendRequestCustomSocket(name_socket, "getRequestedUserInfo", this.props.userToken, {MatrixId: props.currentMessage.user["_id"]});
            };
        } else {
            new_props = {
                ...new_props,
                currentMessage:{
                    ...props.currentMessage,
                    user:{
                        ...props.currentMessage.user,
                        name:`${contact.firstnamerus} ${contact.lastnamerus}`
                    }
                }
            };
        }

        //console.log("message bubble props", props);
        let is_bot = props.currentMessage.text.slice(0, "[BOT_MESSAGE]".length) == "[BOT_MESSAGE]";
        let alignment = props.position == "left" ? "flex-start" : "flex-end";
        let has_file = props.currentMessage.file != null;

        if (is_bot) {
            let parsed_msg = JSON.parse(props.currentMessage.text.split("[BOT_MESSAGE] ")[1]);
            let options = parsed_msg.options;
            new_props = {...new_props, currentMessage:{...props.currentMessage, text:parsed_msg.message}};

            return (
                <View>
                    <Bubble
                        {...new_props}
                        wrapperStyle={{
                            right: {
                                marginRight:window.width/20,
                                backgroundColor: "rgb(134,38,51)",
                                maxWidth:window.width/5
                            },
                            left: {
                                marginLeft:window.width/20,
                                backgroundColor: "rgb(117,120,123)",
                                maxWidth:window.width/5
                            }
                        }}
                        textStyle={{
                            left: {
                                color: 'white',
                            },
                        }}
                        renderTime={(props) => <Time {...props} timeFormat={"H:mm"}/>}
                        //renderTime={(props) => (new Bubble).renderTime({...props, timeFormat:"H:mm"})}
                    />
                    {has_file && <TouchableOpacity
                        style={{alignSelf:alignment, marginHorizontal:window.width/10, marginTop:10, padding:10, borderRadius:10, width:100, backgroundColor:"rgb(220,219,216)"}}
                        accessibilityRole='link'
                        href={props.currentMessage.file}
                        target='_blank'
                    >
                        <Text style={{flexWrap:"wrap", alignSelf:"center"}}>{"Скачать"}</Text>
                    </TouchableOpacity>}
                    {options.map((el) => {
                        return (
                            <TouchableOpacity
                                style={{marginVertical:10, padding:10, borderRadius:10, backgroundColor:"rgb(220,219,216)"}}
                                onPress={() => this.onSend([{text:el}])}
                            >
                                <Text style={{flexWrap:"wrap", alignSelf:"center"}}>{el}</Text>
                            </TouchableOpacity>
                        );
                    })}
                </View>
            );
        } else {
            return (
                <View>
                    <Bubble
                        {...new_props}
                        wrapperStyle={{
                            right: {
                                marginRight:window.width/20,
                                backgroundColor: "rgb(134,38,51)",
                                maxWidth:window.width/5
                            },
                            left: {
                                marginLeft:window.width/20,
                                backgroundColor: "rgb(117,120,123)",
                                maxWidth:window.width/5
                            }
                        }}
                        textStyle={{
                            left: {
                                color: 'white',
                                flexWrap:"wrap"
                            },
                            right: {
                                flexWrap:"wrap"
                            },
                        }}
                        renderTime={(props) => <Time {...props} timeFormat={"H:mm"}/>}
                        //renderTime={(props) => (new Bubble({...new_props})).renderTime({...props, timeFormat:"LTS"})}
                    />
                    {has_file && <TouchableOpacity
                        style={{alignSelf:alignment, marginHorizontal:window.width/10, marginVertical:10, padding:10, borderRadius:10, width:100, backgroundColor:"rgb(220,219,216)"}}
                        accessibilityRole='link'
                        href={props.currentMessage.file}
                        target='_blank'
                    >
                        <Text style={{flexWrap:"wrap", alignSelf:"center"}}>{"Скачать"}</Text>
                    </TouchableOpacity>}
                </View>
            );
        }
    }



    render () {
        console.log("lets see some f's in the chat for "+this.state.room_id);
        console.log(this.props.messages);
        return (
            <View style={{height:window.height, width:window.width*0.7, justifyContent:"flex-end"}}>
                {this.state.group_info_active &&
                <ChatGroupInfoScreen room_id={this.state.room_id} toggle={this.toggleGroupInfo}/>
                }
                {this.state.dm_info_active &&
                <ChatDMInfoScreen addressee_matrix_id={this.state.addressee_matrix_id} toggle={this.toggleDMInfo}/>
                }
                {!this.state.group_info_active && !this.state.dm_info_active &&
                    <TouchableOpacity
                        onPress={() => {
                            if (this.state.is_not_group) {
                                //this.props.navigation.navigate("ChatDMInfoScreen", {addressee_matrix_id: this.state.addressee_matrix_id});
                                this.toggleDMInfo();
                            } else {
                                //this.props.navigation.navigate("ChatGroupInfoScreen", {room_id:this.state.room_id});//{...this.state});
                                this.toggleGroupInfo();
                            }
                        }}
                    >
                        <View style={{height:60, width:window.width*0.7, flexDirection:"row", justifyContent:"space-between", backgroundColor:"white", elevation:0, borderBottomWidth: 2, borderBottomColor: "rgb(220,219,216)"}}>
                            <View style={{justifyContent:"flex-start", flexDirection:"row"}}>
                                <View style={{marginLeft:10, height:60, width:60, justifyContent:"center", alignSelf:"center"}}>
                                    {this.state.avatar != null
                                        ?
                                        <WebImage
                                            style={{borderRadius:25, width:50, height:50, resizeMode:"cover", alignSelf:"center"}}
                                            source={{
                                                uri: this.state.avatar,
                                                method: "GET",
                                                headers: {
                                                    "Authorization":this.props.userToken
                                                }
                                            }}
                                        />
                                        :
                                        <View style={{borderRadius:25, width:50, height:50, alignSelf:"center", justifyContent:"center", alignItems:"center", textAlign:"center", backgroundColor:"rgb(117,120,123)"}}><Text style={{fontSize:32, fontWeight:"bold", color:"#fff"}}>?</Text></View>
                                    }
                                </View>
                                <View style={{flex:1, marginLeft:10, alignSelf:"center"}}>
                                    <Text>{this.props.rooms.hasOwnProperty(this.state.room_id) && this.props.rooms[this.state.room_id].full_name}</Text>
                                </View>
                            </View>
                            <View style={{
                                height: 38,
                                flexDirection: "row",
                                alignSelf: "center",
                                marginRight: 20
                            }}>
                                <TouchableOpacity
                                    style={{alignSelf: "center", marginRight:15}}
                                    onPress={() => {
                                        if (this.state.is_not_group) {
                                            //this.props.navigation.navigate("ChatDMInfoScreen", {addressee_matrix_id: this.state.addressee_matrix_id});
                                            this.toggleDMInfo();
                                        } else {
                                            //this.props.navigation.navigate("ChatGroupInfoScreen", {room_id:this.state.room_id});//{...this.state});
                                            this.toggleGroupInfo();
                                        }
                                    }}
                                >
                                    <Entypo size={28} name={"dots-three-vertical"}/>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </TouchableOpacity>
                }
                {this.state.image_message_overlay &&
                <ImageMessageOverlay zIndex={1000} trigger_overlay={this._trigger_image_overlay} room_id={this.state.room_id} client={this.client} image_uri={this.state.image_uri} image64={this.state.image_base64}/>
                }
                {this.state.file_message_overlay &&
                <FileMessageOverlay zIndex={1000} trigger_overlay={this._trigger_file_overlay} room_id={this.state.room_id} client={this.client} file_uri={this.state.file_uri} file_name={this.state.file_name} file_type={this.state.file_type}/>
                }

                {!this.state.group_info_active && !this.state.dm_info_active &&
                    <View
                        pointerEvents={this.state.image_message_overlay || this.state.file_message_overlay ? "none" : "auto"}
                        style={{height: window.height-60, width: window.width * 0.7}}
                    >
                        <GiftedChat
                            style={{height: window.height-60, width: window.width * 0.7}}
                            ref={(r) => {
                                this.chat_ref = r
                            }}
                            messages={this.props.messages}
                            onSend={messages => this.onSend(messages)}
                            user={{
                                _id: this.state.user_id,
                            }}
                            inverted={true}
                            renderMessage={(props) => {
                                console.log("message props", props);
                                if (!props.nextMessage.hasOwnProperty("_id")) this.props.clearUnread(this.state.room_id);
                                return <Message {...props} />
                            }}
                            renderMessageImage={this.renderMessageImage}
                            renderUsernameOnMessage={true}
                            //onLongPress={this.onLongPress}
                            renderBubble={this.renderBubble}
                            minInputToolbarHeight={
                                Platform.OS == "ios" ? 60 : ((h) => {
                                    if (h > 36) {
                                        if (h >= 80) {
                                            console.log("minInputToolbarHeight", 104);
                                            return 104
                                        } else {
                                            console.log("minInputToolbarHeight", h + 24);
                                            return h + 24
                                        }
                                    } else {
                                        console.log("minInputToolbarHeight", 60);
                                        return 60;
                                    }
                                })(this.state.input_height)
                            }
                            renderInputToolbar={() => {
                                return (
                                    <View
                                        style={{
                                            position: "absolute",
                                            bottom: 50,
                                            right: 0,
                                            width: window.width * 0.7,
                                            flexDirection: "row",
                                            flex: 1,
                                            minHeight: 60,
                                            maxHeight: Platform.OS == "ios" ? 60 : 104,
                                            backgroundColor: "rgb(240,240,240)",
                                            paddingBottom: 8, paddingTop: 8,
                                            borderColor: "rgb(220,219,216)",
                                            borderTopWidth: 2,
                                            alignSelf: "flex-end",
                                            alignItems: "flex-end"
                                        }}
                                    >
                                        <View
                                            style={{
                                                flexDirection: "row",
                                                justifyContent: "space-between",
                                                flex: 1,
                                                height: this.state.input_height ? this.state.input_height + 8 : 44,
                                                minHeight: 44,
                                                maxHeight: Platform.OS == "ios" ? 44 : 88,
                                                backgroundColor: "white",
                                                elevation: 0,
                                                borderColor: "rgb(220,219,216)",
                                                borderRadius: 22,
                                                borderWidth: 2,
                                                marginHorizontal: 5,
                                                paddingVertical: 6,
                                                paddingRight: 6
                                            }}
                                        >
                                            <TextInput
                                                onContentSizeChange={(event) => {
                                                    this.setState({input_height: Platform.OS == "ios" ? 36 : event.nativeEvent.contentSize.height})
                                                }}
                                                multiline={Platform.OS == "ios" ? false : true}
                                                ref={(r) => {
                                                    this.input_ref = r
                                                }}
                                                style={{
                                                    minHeight: 36,
                                                    maxHeight: Platform.OS == "ios" ? 36 : 80,
                                                    height: this.state.input_height || 36,
                                                    flex: 1,
                                                    alignSelf: "center",
                                                    paddingHorizontal: 5,
                                                    flexWrap: "wrap",
                                                    fontSize: 18
                                                }}
                                                onChangeText={(val) => {
                                                    this.setState({message_input: val});
                                                }}
                                            />
                                        </View>
                                        <TouchableOpacity
                                            style={{
                                                marginRight: 5,
                                                paddingLeft: 4,
                                                width: 40,
                                                height: 40,
                                                alignSelf: "center",
                                                backgroundColor: "#00000000",
                                                justifyContent: "center",
                                                alignItems: "center"
                                            }}
                                            onPress={() => {
                                                if (this.state.message_input) {
                                                    this.onSend([{text: this.state.message_input}]);
                                                    if (this.input_ref) this.input_ref.clear();
                                                    this.setState({message_input: ""});
                                                }
                                            }}
                                        >
                                            <Ionicons name={"md-send"} color={"rgb(134,38,51)"} size={36}/>
                                        </TouchableOpacity>
                                    </View>
                                );
                            }}
                            renderActions={() => {
                                return (
                                    <TouchableOpacity
                                        style={{alignSelf: "center"}}
                                        onPress={() => {
                                            Keyboard.dismiss();
                                        }}
                                    >
                                        <MaterialCommunityIcons name={"plus-circle-outline"} size={30}
                                                                style={{marginLeft: 5, alignSelf: "flex-start"}}/>
                                    </TouchableOpacity>
                                );
                            }}
                        />
                    </View>
                }
            </View>
        );
    }



    _askPermission = async (type, failureMessage) => {
        const { status, permissions } = await Permissions.askAsync(type);

        if (status === 'denied') {
            alert(failureMessage);
        }
    };

    _pickImage = async () => {
        //Keyboard.dismiss();
        //await this._askPermission(Permissions.CAMERA, 'We need the camera-roll permission to read pictures from your phone...');
        //await this._askPermission(Permissions.CAMERA_ROLL, 'We need the camera-roll permission to read pictures from your phone...');
        let pickerResult = await ImagePicker.launchImageLibraryAsync({
            maxWidth:1000,
            maxHeight:1000,
            quality:0.4,
            allowsEditing: true,
            mediaType: ImagePicker.MediaTypeOptions.Image,
            base64:true
        });

        console.log("pickerResult", pickerResult);
        if (pickerResult.didCancel) {return}

        this.setState({
            image_message_overlay:true,
            image_uri:pickerResult.uri,
        });
    };
}



const mapStateToProps = (state, ownProps) => {
    return {
        lang:                    state.data.settings.lang,
        messages:                state.data.chat.messages[ownProps.room_id || null],
        userToken:               state.data.userToken,
        contacts:                state.data.chat.contacts,
        rooms:                   state.data.chat.rooms,
        user_id:                 state.data.chat.user_id,
    }
};

const mapDispatchToProps = dispatch => {
    return {
        /*receiveData:            (data) => dispatch(receiveData(data)),
        removeData:             (key) => dispatch(removeData({key})),
        setChatToken:           (token) => dispatch(setChatToken({token}))*/
        clearUnread:             (room) => dispatch(clearUnread({room})),
        updMessages:             (new_msg, room) => dispatch(updMessages({new_msg, room})),
        addContacts:             (contact) => dispatch(addContacts({contact})),
        sortMessages:            (room) => dispatch(sortMessages({room}))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(MessageScreen);
