import React from 'react';
import {
    ActivityIndicator, Dimensions, FlatList, Image, ImageBackground,
    KeyboardAvoidingView, RefreshControl,
    ScrollView,
    StyleSheet, Text, TouchableOpacity,
    View
} from 'react-native';
import {changeLang, receiveData, setAvailableFilter, setCurrentFilter, setEventId} from "../../actions/data";
import {connect} from "react-redux";
import {cancel_reconnect, getws, init_timeout} from "../../methods/webSocket";
import {backendRequest, extractResponse} from "../../methods/ws_requests";
import {removeData, updateUserToken} from "../../actions/data";
import {closeFilterView, disableNetWarn, enableNetWarn, toggleCredCard, toggleRegForm} from "../../actions/control";
import {isLater, parseCalItems} from "../../methods/calendar_methods";
import SubscreenHeader from "../../components/headers_footers/subscreen_header";
import card from "../../styles/cards";
import MainFooter from "../../components/headers_footers/main_footer";
import NetModalWarning from "../../components/cards/NetModalWarning";
import EventCard from "../../components/cards/event_card";
import DrawerContent from "../../components/cards/DraweContent";
import {SimpleLineIcons, Ionicons, MaterialCommunityIcons} from "@expo/vector-icons";
import {WS_URL} from "../../constants/backend";

const window = Dimensions.get("window");

class ChooseEventIdScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            needs_update:true,
            future_active:true,
            past_active:false,
        };

        this.events_past = [];
        this.events_future = [];

        this.drawer = new React.createRef();
        this._onRefresh = this._onRefresh.bind(this);
    }
    componentDidMount() {
        this.events_past = [];
        this.events_future = [];
        let event_socket = new WebSocket(WS_URL);
        event_socket.onmessage = (msg) => {
            let parsed_msg = JSON.parse(msg.data);
            console.log(parsed_msg);
            let response = parsed_msg.data;
            if (response != null) {
                response.forEach((el) => {
                    if (new Date(el.enddate) < new Date()) {
                        this.events_past = this.events_past.concat(el);
                    } else {
                        this.events_future = this.events_future.concat(el);
                    }
                });
            }
            this.setState({
                needs_update:false,
            });
            event_socket.close();
        };
        event_socket.onopen = () => {backendRequest(
            event_socket,
            "getEvents",
            this.props.userToken
        )};

    }

    _onRefresh() {
        this.events_past = [];
        this.events_future = [];
        let event_socket = new WebSocket(WS_URL);
        event_socket.onmessage = (msg) => {
            let parsed_msg = JSON.parse(msg.data);
            console.log(parsed_msg);
            let response = parsed_msg.data;
            if (response != null) {
                response.forEach((el) => {
                    if (new Date(el.enddate) < new Date()) {
                        this.events_past = this.events_past.concat(el);
                    } else {
                        this.events_future = this.events_future.concat(el);
                    }
                });
            }
            this.setState({
                needs_update:false,
            });
            event_socket.close();
        };
        event_socket.onopen = () => {backendRequest(
            event_socket,
            "getEvents",
            this.props.userToken
        );}
    }

    render() {
        console.log("window.width "+window.width );
        return (
                <View style={{flex:1, width: window.width, justifyContent:"space-between", backgroundColor:"rgb(240,240,240)"}}>
                    <NetModalWarning />
                    {(this.events_future.length > 0 || this.events_past.length > 0)
                        ?
                            <ScrollView style={{paddingTop:0, width:window.width}} refreshControl={<RefreshControl progressViewOffset={50} refreshing={this.state.needs_update} onRefresh={this._onRefresh}/>}>
                                <View style={{height:50}}/>
                                <TouchableOpacity
                                    onPress={() => {
                                        this.setState({future_active:!this.state.future_active});
                                    }}
                                    style={{height:40, borderTopWidth:1, flexDirection:"row", justifyContent:"space-between", alignItems:"center", borderBottomWidth:((this.state.future_active && this.events_future.length > 0) ? 1 : 0), borderColor:"rgb(220,219,216)", backgroundColor:"rgb(246,246,246)"}}
                                >
                                    <Text style={{
                                        fontSize: 16,
                                        color: "#000",
                                        marginHorizontal:20,
                                    }}>{"Ближайшие мероприятия:"}</Text>
                                    <View style={{alignItems:"center", justifyContent:"center", height:40, width:40, marginRight:20}}>
                                        <Ionicons size={18} color={"black"} name={this.state.future_active ? "ios-remove" : "ios-add"}/>
                                    </View>
                                </TouchableOpacity>
                                {this.state.future_active && this.events_future.length > 0 && this.events_future.map((elem) => {
                                    return (
                                        <View style={[card.base, {marginHorizontal:15, marginBottom:15,  alignSelf:"center"}]}>
                                            {elem.background
                                                ?
                                                    <ImageBackground imageStyle={{ borderRadius: 15 }} source={{uri:elem.background_url}} style={{width:360, height:180, resizeMode:"contain", alignSelf:"center"}}>
                                                        <EventCard {...elem} navigation={this.props.navigation}/>
                                                    </ImageBackground>
                                                :
                                                    <EventCard  {...elem} navigation={this.props.navigation}/>
                                            }
                                        </View>
                                    );
                                })}

                                <TouchableOpacity
                                    onPress={() => {
                                        this.setState({past_active:!this.state.past_active});
                                    }}
                                    style={{height:40, borderTopWidth:1, flexDirection:"row", justifyContent:"space-between", alignItems:"center", borderBottomWidth: 1, borderColor:"rgb(220,219,216)", backgroundColor:"rgb(246,246,246)"}}
                                >
                                    <Text style={{
                                        fontSize: 16,
                                        color: "#000",
                                        marginHorizontal:20,
                                    }}>{"Прошедшие мероприятия:"}</Text>
                                    <View style={{alignItems:"center", justifyContent:"center", height:40, width:40, marginRight:20}}>
                                        <Ionicons size={18} color={"black"} name={this.state.past_active ? "ios-remove" : "ios-add"}/>
                                    </View>
                                </TouchableOpacity>
                                {this.state.past_active && this.events_past.length > 0 && this.events_past.map((elem) => {
                                    return (
                                        <View style={[card.base, {marginHorizontal:15, marginBottom:15,  alignSelf:"center"}]}>
                                            {elem.background
                                                ?
                                                <ImageBackground imageStyle={{ borderRadius: 15 }} source={{uri:elem.background_url}} style={{width:360, height:180, resizeMode:"contain", alignSelf:"center"}}>
                                                    <EventCard {...elem} is_past navigation={this.props.navigation}/>
                                                </ImageBackground>
                                                :
                                                <EventCard  {...elem} is_past navigation={this.props.navigation}/>
                                            }
                                        </View>
                                    );
                                })}

                                <View style={{height:80}} />
                            </ScrollView>
                        :
                            <ScrollView refreshControl={<RefreshControl progressViewOffset={50} refreshing={this.state.needs_update} onRefresh={this._onRefresh}/>}>
                                <View style={{height:10}} />
                            </ScrollView>
                    }
                </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'flex-start',
        flexDirection: "column"
    },
});

const mapStateToProps = state => {
    return {
        received_data:      state.data.received_data,
        lang:               state.data.settings.lang,
        cred_card:          state.control.cred_card,
        userToken:          state.data.userToken,

        login:              state.data.credentials.login,
        password:           state.data.credentials.password
    }
};

const mapDispatchToProps = dispatch => {
    return {
        removeData:             (key) => dispatch(removeData({key})),
        receiveData:            (data) => dispatch(receiveData(data)),
        ch_lang:                (lang) => dispatch(changeLang(lang)),
        updateUserToken:        (token) => dispatch(updateUserToken({token})),
        toggleRegForm:          () => dispatch(toggleRegForm()),
        toggleCredCard:         () => dispatch(toggleCredCard()),
        setEventId:             (event_id) => dispatch(setEventId({event_id})),
        setCurrentFilter:       (new_filter) => dispatch(setCurrentFilter({current_filter: new_filter})),
        setAvailableFilter:     (new_filter) => dispatch(setAvailableFilter({available_filter: new_filter})),
        closeFilterView:        () => dispatch(closeFilterView()),
        enableNetWarn:          () => dispatch(enableNetWarn()),
        disableNetWarn:          () => dispatch(disableNetWarn()),
    }
};


export default connect(mapStateToProps, mapDispatchToProps)(ChooseEventIdScreen);
