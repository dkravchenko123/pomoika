import React from "react";
import {Container, ScrollableTab, Tabs, Tab, TabHeading} from "native-base";
import {Dimensions, Text, ScrollView, View, WebView, Image} from "react-native";
import MainHeader from "../../components/headers_footers/main_header.js";
import AHWebView from "react-native-webview-autoheight";


//styles
import tab from "../../styles/tabs";
import DrawerContent from "../../components/cards/DraweContent";
import Drawer from "react-native-drawer";
import ChooseEventIdScreen from "../../screens/extended view screens/choose_event_id_screen";
import {cancel_reconnect, getws, init_timeout} from "../../methods/webSocket";
import {backendRequest, backendRequestCustomSocket, extractResponse} from "../../methods/ws_requests";
import {receiveData, removeData} from "../../actions/data";
import {disableNetWarn, enableNetWarn, toggleNewsOverlay} from "../../actions/control";
import {connect} from "react-redux";
import SubscreenHeader from "../../components/headers_footers/subscreen_header";
import {WS_URL} from "../../constants/backend";

const window = Dimensions.get("window");

class FullArticleScreen extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            needs_update:true,
            articleid: this.props.navigation.getParam("articleid", null)
        };

        this.drawer = new React.createRef();
        this.menu_fun = this.menu_fun.bind(this);
        this.close_drawer = this.close_drawer.bind(this);

    }

    componentDidMount() {
        let article_socket = new WebSocket(WS_URL);
        //this.props.removeData("getArticle");
        //init_timeout(3000);
        article_socket.onmessage = (msg) => {
            //cancel_reconnect();
            let parsed_msg = JSON.parse(msg.data);
            console.log(parsed_msg);
            this.props.receiveData(parsed_msg);
            let new_data = parsed_msg.data;
            if (new_data) {
                console.log(new_data);
                this.setState({
                    needs_update:false,
                    ...new_data
                });
                this.props.removeData("getArticle");
            }
            article_socket.close();
        };
        article_socket.onopen = () => {
            backendRequestCustomSocket(article_socket, "getArticle", this.props.userToken, {articleid:this.state.articleid});
        };

        this.didBlurSubscription = this.props.navigation.addListener(
            'willBlur',
            payload => {
                if (this.close_drawer) this.close_drawer();
                //console.log('willBlur', payload);
            }
        );
    }

    componentWillUnmount() {
        if (this.didBlurSubscription) this.didBlurSubscription.remove();
    }

    /*componentDidUpdate () {
        if (this.state.needs_update) {
            let new_data = extractResponse(this.props.received_data, "getArticle");
            if (new_data) {
                console.log(new_data);
                this.setState({
                    needs_update:false,
                    ...new_data
                });
                this.props.removeData("getArticle");
            }
        }
    }*/

    menu_fun () {
        this.drawer.open();
    }

    close_drawer () {
        this.drawer.close();
    }

    render () {
        return (
            <Drawer
                content={<DrawerContent navigation={this.props.navigation} open_facts={this.open_facts} close_drawer={this.close_drawer}/>}
                ref={(r) => this.drawer = r}
                openDrawerOffset={0.0}
                side={"right"}
                acceptPan
                negotiatePan
            >
                <View style={{flex:1,/*height:"100%"*/}}>
                    {/*<MainHeader menu_fun={this.menu_fun} navigation={this.props.navigation}/>*/}
                    <SubscreenHeader menu_fun={this.menu_fun} navigation={this.props.navigation}/>
                    <ScrollView showsHorizontalScrollIndicator={false} showsVerticalScrollIndicator={false}>
                        {this.state.mainimgurl && <Image style={{width:window.width-30, height:(2/3) * (window.width-30), alignSelf:"center", resizeMode:"cover", marginTop:15}} source={{uri:this.state.mainimgurl}}/>}

                        {this.state.title &&
                        <View style={{flex:1, marginLeft:20, marginRight:20, marginTop:15}}>
                            <Text style={{fontWeight:"bold", fontSize:20}}>{this.state.title}</Text>
                        </View>
                        }

                        {this.state.publicdate &&
                        <View style={{flex:1, marginLeft:20, marginRight:20, marginTop:8}}>
                            <Text style={{fontSize:12}}>{this.state.publicdate.split(":").slice(0,2).join(":").split("-").join("/").split("T").join(" ")}</Text>
                        </View>
                        }

                        {(this.state.description && this.state.articletypeid != 3) &&
                            <View style={{flex:1, marginLeft:20, marginRight:20, marginTop:8}}>
                                <Text style={{fontSize:16}}>{this.state.description}</Text>
                            </View>
                        }
                        {(this.state.description && this.state.articletypeid == 3) &&
                            <View style={{marginLeft:20, marginRight:20, marginTop:8}}>
                                <AHWebView
                                    useWebKit
                                    style={{/*height:window.height, */width:window.width-40}}
                                    source={{html: this.state.description}}
                                    originWhitelist={['*']}
                                    scalesPageToFit={true}
                                />
                            </View>
                        }
                    </ScrollView>
                </View>
            </Drawer>
        );
    }
}

const mapStateToProps = state => {
    return {
        received_data:          state.data.received_data,
        userToken:              state.data.userToken
    }
};

const mapDispatchToProps = dispatch => {
    return {
        receiveData:            (data) => dispatch(receiveData(data)),
        removeData:             (key) => dispatch(removeData({key})),
        toggle:                 () => dispatch(toggleNewsOverlay()),
        enableNetWarn:          () => dispatch(enableNetWarn()),
        disableNetWarn:          () => dispatch(disableNetWarn()),
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(FullArticleScreen);
