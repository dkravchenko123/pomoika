import React from "react";
import {Alert, Image, PixelRatio, Text, ToastAndroid, TouchableOpacity, View} from "react-native";
import {Button, Card, Col, Container, Content, Footer, FooterTab, Grid, Header, Icon, Item, Input, Row} from "native-base";
import {SimpleLineIcons, Ionicons} from "@expo/vector-icons";
import {styles} from "../../styles/header_footer_styles";

import AccountTab from "../../components/pages/account_page";
import {HomePage} from "../../components/pages/home_page";
import {SearchScreen} from "../../components/pages/search_page";
import {togglePersonalInfoOverlay} from "../../actions/control";
import {connect} from "react-redux";
import PI_Overlay from "../../components/overlays/personal_info_overlay";
import EventPage from "../../components/pages/facts_sponsors_partners_page";
import DefaultHeader from "../../components/headers_footers/main_header";
import SubscreenHeader from "../../components/headers_footers/subscreen_header";
import {backendRequest, backendRequestCustomSocket} from "../../methods/ws_requests";
import {getws} from "../../methods/webSocket";
import NetModalWarning from "../../components/cards/NetModalWarning";
import DrawerContent from "../../components/cards/DraweContent";
import Drawer from "react-native-drawer";
import button from "../../styles/buttons";
import card from "../../styles/cards";
import {WS_URL} from "../../constants/backend";
import {setCalendarNeedsUpdate} from "../../actions/data";


class FactScreen extends React.Component {
    constructor (props) {
        super (props);

        this.separated_speakers = {
            default:[]
        };
        this.fact = this.props.navigation.getParam('fact', null);

        this.drawer = new React.createRef();
        this.menu_fun = this.menu_fun.bind(this);
        this.close_drawer = this.close_drawer.bind(this);
    }

    menu_fun () {
        this.drawer.open();
    }

    close_drawer () {
        this.drawer.close();
    }

    componentWillMount() {
        //console.log("Rendered fact: "+ JSON.stringify(fact));
        [...this.fact["Speakers"]].sort((sp1, sp2) => {return afterSort(sp1,sp2, this.props.speakers_array);}).forEach((sp) => {
            if ((this.props.lang == "ru" && sp["SpeakerRole"].length > 0) || !sp["SpeakerRole"+this.props.lang.toUpperCase()]) {
                if (this.separated_speakers.hasOwnProperty(sp["SpeakerRole"])) {
                    this.separated_speakers[sp["SpeakerRole"]].push(sp);
                } else {
                    this.separated_speakers[sp["SpeakerRole"]] = [sp];
                }
            }
            if (this.props.lang == "en" && sp["SpeakerRoleEN"].length > 0) {
                if (this.separated_speakers.hasOwnProperty(sp["SpeakerRoleEN"])) {
                    this.separated_speakers[sp["SpeakerRoleEN"]].push(sp);
                } else {
                    this.separated_speakers[sp["SpeakerRoleEN"]] = [sp];
                }
            }
        });
    }

    componentDidMount() {
        this.didBlurSubscription = this.props.navigation.addListener(
            'willBlur',
            payload => {
                if (this.close_drawer) this.close_drawer();
                //console.log('willBlur', payload);
            }
        );
    }

    componentWillUnmount() {
        if (this.didBlurSubscription) this.didBlurSubscription.remove();
    }

    render () {
        let background_color = "rgb(240,240,240)";
        let accent_color = "white";

        return (
            <Drawer
                content={<DrawerContent navigation={this.props.navigation} open_facts={this.open_facts} close_drawer={this.close_drawer}/>}
                ref={(r) => this.drawer = r}
                openDrawerOffset={0.0}
                side={"right"}
                acceptPan
                negotiatePan
            >
                <Container>
                    <NetModalWarning />
                    <SubscreenHeader menu_fun={this.menu_fun} is_event_screen navigation={this.props.navigation}/>
                    <Content style={{paddingTop:0, paddingBottom:60, paddingHorizontal:0, backgroundColor:(background_color ? background_color : "white")}}>
                        <Grid style={[card.base, {padding:15, justifyContent:"space-between", backgroundColor:(background_color ? "white" : "rgb(220,219,216)"), borderRadius: 15}]}>
                            <Row style={{marginLeft:15, marginRight:30, flex:1, alignItems:"center", justifyContent:"center"}}>{/*<View style={{width:30}}></View>*/}<Text style={{fontWeight:"bold", fontSize:24}}>{this.fact["FactName"]}</Text></Row>
                            <Row style={{marginTop:15, marginLeft:15, marginRight:30, flex:1, alignItems:"center", justifyContent:"space-between"}}>{/*<View style={{width:30}}><Ionicons name={"ios-clock"} size={20}/></View>*/}

                                <Text>
                                    {this.fact["StartDate"].split("Z")[0].split("T")[0].split("-").slice(1,3).reverse().join("/")+ " " + this.fact["StartDate"].split("Z")[0].split("T")[1].split(":").slice(0,2).join(":")+"-"+this.fact["EndDate"].split("Z")[0].split("T")[1].split(":").slice(0,2).join(":")}
                                </Text>

                                {(this.fact["FactPlaceName"] != null && this.fact["FactPlaceName"].length > 0 ) &&
                                    <View>
                                        <Text style={{color:"black"}}>{this.fact["FactPlaceName"]}</Text>
                                        <Ionicons style={{alignSelf:"center", marginRight:3}} color={(this.props.accent_color ? this.props.accent_color : "rgb(169,25,59)")} size={14} name={"ios-pin"}/>
                                    </View>
                                }
                            </Row>
                            <Row style={{marginTop:15, marginLeft:15, marginRight:30, flex:1, alignItems:"center"}}>{/*<View style={{width:30}}><Ionicons name={"ios-information-circle-outline"} size={20}/></View>*/}<Text>{this.fact["FactDescription"]}</Text></Row>
                        </Grid>
                        {this.fact["Speakers"].length != 0 &&
                            <Grid style={[card.base, {marginTop:10, padding:15, paddingBottom:20, justifyContent:"space-between", backgroundColor:(background_color ? "white" : "rgb(220,219,216)"), borderRadius: 15}]}>
                                {this.fact["Speakers"].length != 0 && {/*Object.keys(this.separated_speakers).length > 1*/} &&
                                    <View style={{marginLeft:15, marginRight:30, flex:1, alignItems:"flex-start", flexDirection:"column"}}>
                                        {/*<Col style={{width:30}}><Ionicons name={"ios-person"} size={20}/></Col>*/}
                                        <Text style={{fontSize:22, fontWeight:"bold", marginBottom:5}}>Участники</Text>

                                        <View>
                                            {
                                                Object.keys(this.separated_speakers).map((key) => {
                                                    return(
                                                        <Col key={key} style={{flexDirection:"column"}}>
                                                            {key!="default" && <View style={{marginBottom:8}}><Text style={{fontWeight:"bold", fontSize:18}} >{((key == "default" && this.separated_speakers[key].length > 0) ? "Участники дискуссии:" : key)}</Text></View>}
                                                            <View style={{flexDirection: "column"}}>
                                                                {this.separated_speakers[key].map((speaker_obj, index) => {
                                                                    let speaker = this.props.speakers_array.find((el) => el["SpeakerID"] == speaker_obj["SpeakerID"]);
                                                                    return (
                                                                        <View style={{flexDirection: "row"}}>
                                                                            <TouchableOpacity
                                                                                style={{flexDirection:"row", alignItems:"center"}}
                                                                                key={index}
                                                                                onPress={() => {
                                                                                    this.props.navigation.navigate("SpeakerScreen", {speaker});
                                                                                }}
                                                                            >
                                                                                {/*{speaker_obj["SpeakerRole"] != null && speaker_obj["SpeakerRole"].length != 0 &&
                                                                                <Text
                                                                                    style={{fontWeight: "bold"}}>{speaker_obj["SpeakerRole"] + ": "}</Text>
                                                                                }*/}
                                                                                <View style={{height:50, width:50, borderRadius:25, marginRight:4, marginBottom:10, borderColor:background_color, borderWidth:0, justifyContent:"center", alignItems:"center"}}>
                                                                                    <Image style={{height:50, width:50, borderRadius:25, resizeMode:"cover"}} source={{uri:speaker["SpeakerUserImgURL"]}}/>
                                                                                </View>
                                                                                <View style={{flexDirection:"column", width:"80%"}}>
                                                                                    <Text style={{flexWrap:"wrap", fontWeight:"bold", fontSize:14*PixelRatio.getFontScale(), marginBottom:2, alignSelf:"center"}}>{speaker["SpeakerFirstName"] + " " + speaker["SpeakerLastName"]}</Text>
                                                                                    <Text style={{flexWrap:"wrap", fontSize:14*PixelRatio.getFontScale(), marginBottom:2, alignSelf:"center"}}>{speaker["SpeakerDescription"]}</Text>
                                                                                </View>
                                                                            </TouchableOpacity>
                                                                        </View>
                                                                    );
                                                                })}
                                                            </View>
                                                        </Col>
                                                    );
                                                })
                                            }
                                        </View>
                                    </View>
                                }
                                {false && this.fact["Speakers"].length != 0 && Object.keys(this.separated_speakers).length === 1 &&
                                    <Row style={{marginTop:15, marginLeft:15, marginRight:30, flex:1, alignItems:"center"}}><View style={{width:30}}><Ionicons name={"ios-person"} size={20}/></View>
                                        <View style={{flexDirection:"column"}}>
                                            {this.fact["Speakers"].sort((sp1, sp2) => {return afterSort(sp1,sp2, this.props.speakers_array);}).map((speaker_obj, index) => {
                                                let speaker = this.props.speakers_array.find((el) => el["SpeakerID"]==speaker_obj["SpeakerID"])
                                                return (
                                                    <TouchableOpacity
                                                        key={index}
                                                        onPress={() => {
                                                            this.props.navigation.navigate("SpeakerScreen", {speaker});
                                                        }}
                                                    >
                                                        {speaker_obj["SpeakerRole"] != null && speaker_obj["SpeakerRole"].length != 0 &&
                                                            <Text style={{fontWeight:"bold"}}>{speaker_obj["SpeakerRole"] + ": "}</Text>
                                                        }
                                                        <Text>{speaker["SpeakerFirstName"]+" "+speaker["SpeakerLastName"]}</Text>
                                                    </TouchableOpacity>
                                                );
                                            })}
                                        </View>
                                    </Row>
                                }
                                {/*<Text>{JSON.stringify(this.separated_speakers)}</Text>*/}
                            </Grid>
                        }
                        <View style={[card.base, {borderRadius:15, padding:15, alignItems:"center", flexDirection:"row", justifyContent:"center", flexWrap:"wrap", marginTop:10, backgroundColor:(background_color ? "white" : "rgb(220,219,216)")}]}>
                            {this.fact["FactStreamURL"] != null && this.fact["FactStreamURL"].length > 0 &&
                                <TouchableOpacity
                                    onPress={() => {
                                        this.props.navigation.navigate("WebViewScreen", {uri: this.fact["FactStreamURL"]});
                                    }}
                                >
                                    <View
                                        style={[button.header, {width:120, marginRight:12, marginTop:6}]}
                                    >
                                        <Text
                                            style={[ {fontSize: 12 * PixelRatio.getFontScale()}]}>{"Трансляция"}</Text>
                                    </View>
                                </TouchableOpacity>
                            }
                            <TouchableOpacity
                                onPress={() => {
                                }}
                            >
                                <View
                                    style={[button.header, {width:120, marginRight:12, marginTop:6}]}
                                >
                                    <Text style={[{fontSize:12*PixelRatio.getFontScale()}]}>{"К мероприятию"}</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={() => {
                                    Alert.alert(
                                        "Добавить событие",
                                        'Добавить событие в календарь?',
                                        [
                                            {
                                                text: 'Нет',
                                                onPress: () => console.log('Cancel Pressed'),
                                            },
                                            {
                                                text: 'Да', onPress: () => {
                                                    let cal_socket = new WebSocket(WS_URL);
                                                    cal_socket.onopen = () => {
                                                        backendRequestCustomSocket(
                                                            cal_socket,
                                                            "calendarNewItem",
                                                            this.props.userToken,
                                                            {
                                                                date:this.fact["StartDate"],
                                                                itemtypeid:0,
                                                                factid:this.fact["FactID"],
                                                                eventid:this.props.event_id,
                                                                name:this.fact["FactName"],
                                                                description:this.fact["FactDescription"]
                                                            }
                                                        );
                                                        cal_socket.close();
                                                    };
                                                    this.props.setCalendarNeedsUpdate(true);
                                                }},
                                        ],
                                    );
                                }}
                            >
                                <View
                                    style={[button.header, {width:120, marginRight:12, marginTop:6}]}
                                >
                                    <Text style={[{fontSize:12*PixelRatio.getFontScale()}]}>{"В календарь"}</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={() => {
                                }}
                            >
                                <View
                                    style={[button.header, {width:120, marginRight:12, marginTop:6}]}
                                >
                                    <Text style={[{fontSize:12*PixelRatio.getFontScale()}]}>Голосование</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={() => {
                                }}
                            >
                                <View
                                    style={[button.header, {width:120, marginRight:12, marginTop:6}]}
                                >
                                    <Text style={[{fontSize:12*PixelRatio.getFontScale()}]}>Задать вопрос</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                        <View style={{height:60}} />
                    </Content>
                </Container>
            </Drawer>
        );
    }
}

function afterSort(sp1, sp2, og_speakers) {
    let i1 = og_speakers.findIndex((el) => el["SpeakerID"]==sp1["SpeakerID"]);
    let i2 = og_speakers.findIndex((el) => el["SpeakerID"]==sp2["SpeakerID"]);

    if (sp2["SpeakerSortPriority"] < sp1["SpeakerSortPriority"]) return 1;
    if (sp2["SpeakerSortPriority"] > sp1["SpeakerSortPriority"]) return 1;
    if (i2 < i1) return 1;
    if (i2 > i1) return -1;
    if (i2 == i1) return 0;
}

const mapStateToProps = state => {
    return {
        lang:               state.data.settings.lang,
        speakers_array:     state.data.event.speakers_array,
        userToken:          state.data.userToken,
        event_id:           state.data.event.event_id,
        event_json:         state.data.event.event_json,
    }
};

const mapDispatchToProps = dispatch => {
    return {
        //toggle: () => dispatch(togglePersonalInfoOverlay()),
        setCalendarNeedsUpdate: (needs_update) => dispatch(setCalendarNeedsUpdate({needs_update}))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(FactScreen);
