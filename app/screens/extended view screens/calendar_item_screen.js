import React from "react";
import {Image, ScrollView, Text, ToastAndroid, TouchableOpacity, View} from "react-native";
import moment from 'moment';
import {Button, Card, Col, Container, Content, Footer, FooterTab, Grid, Header, Icon, Item, Input, Row} from "native-base";
import {MaterialIcons, FontAwesome, MaterialCommunityIcons} from "@expo/vector-icons";
import {styles} from "../../styles/header_footer_styles";
import {connect} from "react-redux";
import SubscreenHeader from "../../components/headers_footers/subscreen_header";
import {backendRequest, backendRequestCustomSocket, backendRequestPromise} from "../../methods/ws_requests";
import {getws} from "../../methods/webSocket";
import CalendarItemHeader from "../../components/headers_footers/calendar_item_header";
import NetModalWarning from "../../components/cards/NetModalWarning";
import DrawerContent from "../../components/cards/DraweContent";

import button from "../../styles/buttons";
import card from "../../styles/cards";
import UserDisplayCard from "../../components/cards/UserDisplayCard";
import {WS_URL} from "../../constants/backend";
import {isLater, parseCalItems} from "../../methods/calendar_methods";
import {setCalendarNeedsUpdate} from "../../actions/data";

class CalendarItemScreen extends React.Component {
    constructor (props) {
        super (props);
    }

    render () {
        let item = this.props.navigation.getParam('item', null);
        console.log("showing item", item);
        let show_fact_info = (item.event_id != null) && item.event_id == this.props.event.event_id;
        let fact = null;
        if (show_fact_info) {
            fact = this.props.event.program.find((f) => f.FactID == item.fact_id);
            if (fact) {
                //item = {...item, ...fact};
                console.log("found fact", fact);
            }
        }
        //console.log("calendar item", item);
        return (
                <Container>
                    <NetModalWarning />
                    <CalendarItemHeader navigation={this.props.navigation} item={item}/>
                    <Content style={{flex:1, backgroundColor:"rgb(240,240,240)"}}>
                        <View style={[card.base, {padding:15, marginTop: 10, marginLeft:15, marginRight:15, borderRadius: 30}]}>
                            <>
                                <Text style={{ alignSelf: 'center', fontSize: 20, fontWeight: 'bold', paddingBottom: 15 }}>{item.item_type_name || 'Приглашение'}</Text>
                                <Text style={{ alignSelf: 'center', fontSize: 18, paddingBottom: 15 }}>{item.name}</Text>
                                <Text style={{ marginLeft: 24, alignSelf: 'flex-start', fontSize: 14, paddingBottom: 5 }}>{`${item.description}`}</Text>
                                <View style={{ flexDirection: 'row', alignItems: 'center', paddingBottom: 5 }}>
                                    <MaterialCommunityIcons name="clock" color='#005989' size={14} />
                                    <Text style={{ marginLeft: 10, fontSize: 14, alignSelf: 'center' }}>
                                        {`${this.props.lang === 'ru' ? 'Начало:' : 'Start:'} ${moment(item.date).format('DD.MM.YYYY')}`}
                                    </Text>
                                </View>
                                {!!item.guests && item.guests.length > 0 && <View style={{ flexDirection: 'row', alignItems: 'center', paddingBottom: 5 }}>
                                    <FontAwesome name="users" color='#005989' size={14} />
                                    <Text style={{ marginLeft: 10, fontSize: 14, alignSelf: 'center', textAlignVertical: 'auto' }}>
                                        {`${this.props.lang === 'ru' ? 'Участники' : 'Members'}: ${item.guests ? item.guests.map((elem) => (`${this.props.lang === 'ru' ? elem.lastnamerus : elem.lastnameeng} ${this.props.lang === 'ru' ? elem.firstnamerus : elem.firstnameeng} ${this.props.lang === 'ru' ? elem.middlenamerus : ""}`)) : JSON.stringify(item)}`}
                                    </Text>
                                </View>}
                                {item.invite && <View style={{ justifyContent: 'flex-end', alignItems: 'flex-end', flexDirection: 'row' }}>
                                    <TouchableOpacity
                                        onPress={() => {
                                            //this.props.navigation.navigate("FactScreen", {fact: fact});
                                            backendRequestPromise(
                                                "calendarUpdateInvite",
                                                this.props.token,
                                                {itemId:item.item_id, statusId:1}
                                            ).then(() => {
                                                this.props.update(true);
                                            });
                                        }}
                                    >
                                        <Text style={{  paddingRight: 10, color: 'darkred', textDecorationColor: 'darkred', textDecorationStyle: 'dotted', textDecorationLine: 'underline' }}>{this.props.lang === 'ru' ? 'Принять' : 'Accept'}</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                        onPress={() => {

                                            backendRequestPromise(
                                                "calendarDeclineInvite",
                                                this.props.token,
                                                {itemId:item.item_id}
                                            ).then(() => {
                                                this.props.update(true);
                                            });
                                        }}
                                    >
                                        <Text style={{ color: 'gray', textDecorationColor: 'gray', textDecorationStyle: 'dotted', textDecorationLine: 'underline' }}>{this.props.lang === 'ru' ? 'Отклонить' : 'Decline'}</Text>
                                    </TouchableOpacity>
                                </View>}
                            </>
                            {/*<Row style={{justifyContent:"center"}}>
                                <Text style={{fontWeight:"bold", fontSize:20}}>{item["item_type_name"]||(item.invite && "Приглашение")}</Text>
                            </Row>
                            <Row style={{justifyContent:"center", marginTop:4}}>
                                <Text style={{fontWeight:"bold", fontSize:18}}>{item["name"]}</Text>
                            </Row>
                            <Row style={{justifyContent:"flex-start", marginTop:6}}>
                                <Text style={{fontSize:14}}>{item["description"]||item["usercal_description"]}</Text>
                            </Row>
                            <Row style={{justifyContent:"flex-start", marginTop:10}}>
                                <Col size={1}>
                                    <Text style={{fontWeight:"bold"}}>Начало:</Text>

                                </Col>
                                <Col size={4}>

                                    {((date) => {
                                        let full_date = new Date(date);
                                        return (<Text>{full_date.toLocaleString("ru-RU", { timeZone: 'UTC' }).split(":").slice(0,2).join(":")}</Text>);
                                    })(item["date"])}

                                </Col>
                            </Row>

                            {item.guests && item.guests.length != 0 &&
                                <Row style={{paddingLeft:0, marginTop:10, minHeight:50, maxHeight:120, marginHorizontal:5, flexDirection:"column", justifyContent:"flex-start", alignItems:"center", flex:1, borderWidth:2, borderRadius:5, borderColor:"black", backgroundColor:"#fff"}}>
                                    <Text style={{fontWeight:"bold", fontSize:16, color:"black"}}>Участники:</Text>
                                    <ScrollView nestedScrollEnabled={true} style={{paddingHorizontal:0, marginHorizontal:0, maxHeight:120, backgroundColor:"#fff"}}>
                                        {item.guests.map((el, index) => {
                                            return (
                                                <TouchableOpacity
                                                    style={{flexDirection:"row", flex:1, alignItems:"center"}}
                                                    onPress={() => {
                                                        //
                                                    }}
                                                >
                                                    <UserDisplayCard {...el} addressee_matrix_id={el.matrix_id}/>
                                                </TouchableOpacity>
                                            );
                                        })}
                                    </ScrollView>
                                </Row>
                            }

                            {fact != null &&
                            <Row style={{justifyContent:"center", marginTop:6}}>
                                <TouchableOpacity
                                    onPress={() => {
                                        this.props.navigation.navigate("FactScreen", {fact: fact});
                                    }}
                                >
                                    <Text style={{fontSize:14, fontWeight:"bold"}}>{"Подробнее о событии"}</Text>
                                </TouchableOpacity>
                            </Row>
                            }*/}

                            {item.invite &&
                            <Row style={{justifyContent:"center", alignItems:"center", marginTop:6}}>
                                <TouchableOpacity
                                    style={[button.base, {
                                        width:100,
                                        height:34,
                                        borderRadius:17,
                                        justifyContent:"center", alignItems:"center",
                                        marginHorizontal:10
                                    }]}
                                    onPress={() => {

                                        backendRequestPromise(
                                            "calendarDeclineInvite",
                                            this.props.userToken,
                                            {itemId:item.item_id}
                                        ).then(() => {
                                            this.props.setCalendarNeedsUpdate(true);
                                            this.props.navigation.navigate("CalendarPage");
                                        });
                                        //this.props.navigation.navigate("FactScreen", {fact: fact});
                                        /*let invite_socket = new WebSocket(WS_URL);
                                        invite_socket.onmessage = (msg) => {
                                            invite_socket.close();
                                            this.props.setCalendarNeedsUpdate(true);
                                            this.props.navigation.navigate("CalendarPage");
                                        };
                                        invite_socket.onopen = (()=>{
                                            backendRequestCustomSocket(invite_socket, "calendarDeclineInvite", this.props.userToken, {itemId:item.item_id});
                                        });*/
                                    }}
                                >
                                    <Text style={{fontSize:14, fontWeight:"bold"}}>{"Отклонить"}</Text>
                                </TouchableOpacity>
                                <TouchableOpacity
                                    style={[button.base, {
                                        width:100,
                                        height:34,
                                        borderRadius:17,
                                        justifyContent:"center", alignItems:"center",
                                        marginHorizontal:10
                                    }]}
                                    onPress={() => {
                                        //this.props.navigation.navigate("FactScreen", {fact: fact});
                                        backendRequestPromise(
                                            "calendarUpdateInvite",
                                            this.props.userToken,
                                            {itemId:item.item_id, statusId:1}
                                        ).then(() => {
                                            this.props.setCalendarNeedsUpdate(true);
                                            this.props.navigation.navigate("CalendarPage");
                                        });
                                        /*let invite_socket = new WebSocket(WS_URL);
                                        invite_socket.onmessage = (msg) => {
                                            invite_socket.close();
                                            this.props.setCalendarNeedsUpdate(true);
                                            this.props.navigation.navigate("CalendarPage");
                                        };
                                        invite_socket.onopen = (()=>{
                                            backendRequestCustomSocket(invite_socket, "calendarUpdateInvite", this.props.userToken, {itemId:item.item_id, statusId:1});
                                        });*/
                                    }}
                                >
                                    <Text style={{fontSize:14, fontWeight:"bold"}}>{"Принять"}</Text>
                                </TouchableOpacity>
                            </Row>
                            }

                        </View>
                    </Content>
                </Container>
        );
    }
}

const mapStateToProps = state => {
    return {
        lang:               state.data.settings.lang,
        event:              state.data.event,
        userToken:          state.data.userToken
    }
};

const mapDispatchToProps = dispatch => {
    return {
        //toggle: () => dispatch(togglePersonalInfoOverlay()),
        setCalendarNeedsUpdate: (needs_update) => dispatch(setCalendarNeedsUpdate({needs_update}))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(CalendarItemScreen);
