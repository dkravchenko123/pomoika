import React from "react";
import {Image, PixelRatio, ScrollView, Text, TextInput, ToastAndroid, TouchableOpacity, View} from "react-native";
import {
    Button,
    Card,
    Container,
    Content,
    Footer,
    FooterTab,
    Header,
    Icon,
    Item,
    Input,
    Row,
    ActionSheet
} from "native-base";
import {connect} from "react-redux";
import SubscreenHeader from "../../components/headers_footers/subscreen_header";
import NetModalWarning from "../../components/cards/NetModalWarning";
import DrawerContent from "../../components/cards/DraweContent";
import Drawer from "react-native-drawer";
import * as Permissions from 'expo-permissions';
import * as ImagePicker from 'expo-image-picker';
import card from "../../styles/cards";
import {BusinessCard} from "../../components/cards/BusinessCard";
import {SimpleLineIcons, Ionicons, MaterialCommunityIcons} from "@expo/vector-icons";
import {SettingsInteractionCard, SettingsInteractionCardItem} from "../../components/cards/SettingsInteractionCard";
import {getchat} from "../../methods/chat_client";
import {ModalCenterCard} from "../../components/overlays/modal_center_card";
import UserSearchField from "../../components/forms/static_fields/UserSearchField";
import {WS_URL} from "../../constants/backend";
import {backendRequestCustomSocket} from "../../methods/ws_requests";
import button from "../../styles/buttons";
import {addContacts, addRooms, removeJoined} from "../../actions/data";
import * as DocumentPicker from "expo-document-picker";
import {ModalBottom} from "../../components/overlays/ModalBottom";
import ChatUserInfoCard from "../../components/cards/ChatUserInfoCard";
import AddUserScreen from "./chat_add_user_screen";
import {AntDesign, EvilIcons} from "@expo/vector-icons";

const IMAGE_BUTTONS = [
    {
        text: "Открыть камеру",
        icon: "camera"
    },
    {
        text: "Выбрать фото из галереи",
        icon: "folder"
    }
];

class ChatGroupInfoScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            room_id:this.props.room_id,
            modal_user:null,
            is_admin:false,

            user_search_active:false,
            rename_active:false,
            change_photo_active:false
        };

        this.roomObject = null;
        this._updateRoomInfo = this._updateRoomInfo.bind(this);
        this._toggleAddUser = this._toggleAddUser.bind(this);
        this._toggleRename = this._toggleRename.bind(this);
        this._toggleChangePhoto = this._toggleChangePhoto.bind(this);
        this._pickImage = this._pickImage.bind(this);
        this._closeModal = this._closeModal.bind(this);

        this.client = getchat();
    }

    _closeModal () {
        this.setState({modal_user:null});
    }

    _toggleAddUser() {
        this.setState({add_user_active:!this.state.add_user_active});
    }

    _toggleRename() {
        this.setState({rename_active:!this.state.rename_active});
    }

    _toggleChangePhoto() {
        this.setState({change_photo_active:!this.state.change_photo_active});
    }

    componentDidMount() {
        if (!this.props.rooms.hasOwnProperty(this.state.room_id)) {
            this._updateRoomInfo();
        } else {
            this.setState({...this.props.rooms[this.state.room_id]});
        }

        this.roomObject = this.client.getRooms().find((room) => room.roomId == this.state.room_id);
        if (this.roomObject != undefined) {
            let is_admin = this.roomObject.getJoinedMembers().reduce((acc, cur) => {
                if (cur.userId == this.props.user_id) {
                    return cur.powerLevel == 100;
                } else {
                    return acc;
                }
            }, false);
            console.log("current user admin status: ", is_admin);
            this.setState({
                is_admin: is_admin
            });
        }
    }

    _updateRoomInfo () {
        let room_socket = new WebSocket(WS_URL);
        room_socket.onmessage = (msg) => {
            //cancel_reconnect();
            let parsed_msg = JSON.parse(msg.data);
            console.log(parsed_msg);
            //this.props.receiveData(parsed_msg);
            if (parsed_msg.statusCode == 200) {
                let msg_data = parsed_msg.data;
                //console.log("using ",msg_data);
                this.props.addRooms({[this.state.room_id]:{
                        full_name: msg_data.room_name,
                        avatar: msg_data.image_url,
                        room_type: msg_data.room_type
                    }});
                this.setState({
                    full_name: msg_data.room_name,
                    avatar: msg_data.image_url,
                    room_type: msg_data.room_type
                });
            } else {
                console.log("got wrong status code", parsed_msg.statusCode);
            }
            room_socket.close();
        };
        room_socket.onopen = () => {
            backendRequestCustomSocket(room_socket, "chatGetRoomInfo", this.props.userToken, {matrix_room_id: this.state.room_id}/*, this.state.room_name_req_id*/);
        };
    }

    render () {
        let background_color = "rgb(240,240,240)"; //this.props.event_json.style ? (this.props.event_json.style.Primary_color ? this.props.event_json.style.Primary_color : null) : null;
        let accent_color = "white"; //this.props.event_json.style ? (this.props.event_json.style.Accent_color ? this.props.event_json.style.Accent_color : "white") : null;

        return (
            <Drawer
                content={<DrawerContent navigation={this.props.navigation} open_facts={this.open_facts} close_drawer={this.close_drawer}/>}
                ref={(r) => this.drawer = r}
                openDrawerOffset={0.0}
                side={"right"}
                acceptPan
                negotiatePan
            >
                <Container>
                    {/* <SubscreenHeader custom_back={this.props.toggle}/> */}

                    <Content style={{flex:1, backgroundColor:(background_color ? background_color : "white")}}>
                        <View style={{height:60, padding:5, flexDirection:"row", backgroundColor:"white", elevation:15}}>
                            <View style={{flex:2, backgroundColor:"#ffffff00"}}>
                                {this.state.avatar && this.props.userToken
                                    ?
                                    <Image
                                        style={{ width:50, height:50, borderRadius:25}}
                                        source={{
                                            uri: this.state.avatar,
                                            method: "GET",
                                            headers: {
                                                "Authorization":this.props.userToken.toString()
                                            }
                                        }}/>
                                    :
                                    <View style={{ borderRadius:25, width:50, height:50, justifyContent:"center", alignItems:"center", textAlign:"center", backgroundColor:"rgb(117,120,123)"}}>
                                        <Text style={{fontSize:38, fontWeight:"bold", color:"#fff"}}>?</Text>
                                    </View>
                                }
                            </View>
                            <View style={{flex:4, flexDirection:"column", justifyContent:"center", textAlign:"left", backgroundColor:"#ffffff00"}}>
                                { this.state.full_name != "" && <Text style={{fontWeight:"bold", fontSize:18, color:"#000"}}>{this.state.full_name}</Text>}

                                { this.state.org_namelong != null && this.state.titlename != null &&
                                <View style={{flexDirection:"column"}}>
                                    <Text style={{fontSize:14, color:"#000"}}>{this.state.org_namelong+","}</Text>
                                    <Text style={{fontSize:14, color:"#000"}}>{this.state.titlename}</Text>
                                </View>
                                }
                            </View>
                            <TouchableOpacity
                                style={{alignSelf: "Right"}}
                                onPress={() => {
                                    if (this.props.toggle)
                                    {
                                        this.props.toggle();
                                    }
                                }}
                            >
                                <EvilIcons size={32} name={"close"}/>
                            </TouchableOpacity>
                        </View>
                        <ScrollView style={{flex:1, backgroundColor:(background_color ? background_color : "white")}}>

                            <SettingsInteractionCard label={"Управление:"}>
                                {this.state.is_admin &&
                                    <View>
                                        <SettingsInteractionCardItem
                                            onPress={() => {
                                                this._toggleRename();
                                            }}
                                            top
                                            bottom
                                            label={`Переименовать чат`}
                                        />
                                        <SettingsInteractionCardItem
                                            onPress={() => {
                                                this._pickImage();
                                            }}
                                            label={`Изменить фото`}
                                        />
                                    </View>
                                }
                                <SettingsInteractionCardItem
                                    onPress={() => {
                                        this.client.leave(this.state.room_id);
                                        this.props.removeJoined(this.state.room_id);
                                        this.props.navigation.navigate("MessageScreen");
                                    }}
                                    top
                                    bottom
                                    label={`Покинуть чат`}
                                />
                            </SettingsInteractionCard>


                            <SettingsInteractionCard label={"Пользователи:"}>
                                {this.state.room_type == "group" && <SettingsInteractionCardItem
                                    onPress={() => {
                                        //this.props.navigation.navigate("AddUserScreen", {room_id:this.state.room_id});
                                        this.setState({user_search_active:true, modal_user:null, change_photo_active:false, rename_active:false})
                                    }}
                                    top bottom
                                    label={`+ Добавить пользователя`}
                                />}
                                {this.client.getRooms()
                                    .filter((room) => room.roomId == this.state.room_id)
                                        .map((room) => {
                                            return (
                                                <View>
                                                    {room.getJoinedMembers()
                                                        .filter((member) => member.name != this.props.username)
                                                            .map((filtered_member) => {
                                                                console.log("filtered member", filtered_member);
                                                                let image = null;
                                                                let label_builder = "";
                                                                if (this.props.contacts.hasOwnProperty(filtered_member.name)) {
                                                                    label_builder = label_builder.concat([
                                                                        this.props.contacts[filtered_member.name].lastnamerus,
                                                                        this.props.contacts[filtered_member.name].firstnamerus,
                                                                        this.props.contacts[filtered_member.name].middlenamerus].join(" ")
                                                                    );

                                                                    if (this.props.contacts[filtered_member.name].userphoto) {
                                                                        image = <Image
                                                                            style={{
                                                                                width: 30,
                                                                                height: 30,
                                                                                borderRadius: 15,
                                                                                marginRight:10,
                                                                            }}
                                                                            source={{
                                                                                uri: this.props.contacts[filtered_member.name].userphoto,
                                                                                method: "GET",
                                                                                headers: {
                                                                                    "Authorization": this.props.userToken.toString()
                                                                                }
                                                                            }}/>;
                                                                    }
                                                                } else {
                                                                    console.log("don't have a contact");
                                                                    let info_socket = new WebSocket(WS_URL);
                                                                    info_socket.onmessage = (msg) => {
                                                                        let parsed_msg = JSON.parse(msg.data);
                                                                        console.log(parsed_msg);
                                                                        if (parsed_msg.statusCode == 200) {
                                                                            this.props.addContacts({[filtered_member.name]:parsed_msg.data[0]});
                                                                        }
                                                                        info_socket.close();
                                                                    };
                                                                    info_socket.onopen = () => {
                                                                        backendRequestCustomSocket(info_socket, "getRequestedUserInfo", this.props.userToken, {MatrixId:filtered_member.userId});
                                                                    };
                                                                }

                                                                return (
                                                                    <SettingsInteractionCardItem
                                                                        key={filtered_member.name}
                                                                        image={image}
                                                                        onPress={() => {
                                                                            //this.props.navigation.navigate("ChatDMInfoScreen", {addressee_matrix_id: filtered_member["userId"]});
                                                                            this.setState({
                                                                                modal_user:filtered_member["userId"]
                                                                            });
                                                                        }}
                                                                        bottom
                                                                        label={label_builder || "..."}
                                                                    />
                                                                );
                                                            })}
                                                </View>
                                            );
                                        })}
                            </SettingsInteractionCard>
                            <View style={{height:50}}/>
                        </ScrollView>
                    </Content>

                    {(this.state.user_search_active || this.state.modal_user != null || this.state.rename_active || this.state.change_photo_active) &&
                        <TouchableOpacity
                            onPress={() => {
                                this.setState({
                                    modal_user:null,
                                    user_search_active:false,
                                    rename_active:false,
                                    change_photo_active:false
                                });
                            }}
                            style={{position:"absolute", top:0, bottom:0, left:0, right:0, width:"100%", height:"100%", backgroundColor:"#00000050"}}
                        />
                    }

                    {this.state.user_search_active &&
                        <View
                            //closeModal={this._closeModal}
                            style={{position:"absolute", bottom:0, paddingBottom:50, width:400, height:400, left:"30%", backgroundColor:"white", borderTopLeftRadius:15, borderTopRightRadius:15, paddingTop:15, borderWidth:2, borderBottomWidth:0, borderColor:"black"}}
                        >
                            <AddUserScreen room_id={this.state.room_id} toggle={() => {this.setState({user_search_active:false})}} />
                        </View>
                    }

                    {this.state.modal_user != null &&
                        <View
                            //closeModal={this._closeModal}
                            style={{position:"absolute", bottom:0, width:400, height:400, left:"30%", backgroundColor:"white", borderTopLeftRadius:15, borderTopRightRadius:15, paddingTop:15, borderWidth:2, borderBottomWidth:0, borderColor:"black"}}
                        >
                            <ChatUserInfoCard
                                addressee_matrix_id={this.state.modal_user}
                                action_buttons={[
                                    (this.state.is_admin ? <TouchableOpacity
                                        onPress={() => {
                                            //this._chooseUser();
                                            this.client.kick(this.state.room_id, this.state.modal_user, null, () => {this._updateRoomInfo()});

                                            this.setState({
                                                modal_user:null,
                                                user_search_active:false,
                                                rename_active:false,
                                                change_photo_active:false
                                            });

                                            this._updateRoomInfo();
                                        }}
                                        style={{width:100, height:30, borderRadius:15, justifyContent:"center", alignItems:"center", backgroundColor:"rgb(169,25,59)"}}
                                    >
                                        <Text style={{color:"white"}}>Удалить</Text>
                                    </TouchableOpacity> : null)
                                ]}
                            />
                        </View>
                    }

                    {this.state.rename_active &&
                        <ModalCenterCard close_fun={this._toggleRename}>
                            <View style={{flexDirection:"row", justifyContent:"flex-start", width:300}}>
                                <View style={{flexDirection:"row", flexGrow:1, paddingLeft:8, marginBottom:10, height:50, justifyContent:"flex-start", alignItems:"center", width:280, borderWidth:2, borderRadius:5, borderColor:"black", backgroundColor:"#fff"}}>
                                    <TextInput
                                        style={{flex:1, height:50, paddingLeft:8, flexDirection:"row", alignItems:"center"}}
                                        //placeholderTextColor={"#000"}
                                        placeholder={"Название комнаты"}
                                        onChangeText={(val) => {this.setState({room_name_input:val})}}
                                    />

                                </View>
                            </View>
                            <View
                                style={{flexDirection:"row", justifyContent:"space-between", width:280, alignSelf:"center"}}
                            >
                                <TouchableOpacity
                                    onPress={() => {
                                        this.setState({room_name_input:""});
                                        this._toggleRename();
                                    }}
                                >
                                    <View
                                        style={button.header}
                                    >
                                        <Text style={{fontSize:14*PixelRatio.getFontScale()}}>{"Отмена"}</Text>
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                    onPress={() => {
                                        let rename_socket = new WebSocket(WS_URL);
                                        rename_socket.onopen = () => {
                                            backendRequestCustomSocket(rename_socket, "chatEditRoom", this.props.userToken, {matrix_room_id: this.state.room_id, room_name:this.state.room_name_input});
                                        };
                                        rename_socket.onmessage = () => {
                                            this.setState({room_name_input:""});
                                            this._updateRoomInfo();
                                            this._toggleRename();
                                            rename_socket.close();
                                        };
                                    }}
                                >
                                    <View
                                        style={button.header}
                                    >
                                        <Text style={{fontSize:14*PixelRatio.getFontScale()}}>{"Сохранить"}</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </ModalCenterCard>
                    }

                    {this.state.change_photo_active &&
                        <ModalCenterCard close_fun={this._toggleChangePhoto}>
                            {this.state.image_uri && <Image style={{width:300, height:260, resizeMode:"contain"}} source={{uri:this.state.image_uri}} />}
                            <View
                                style={{flexDirection:"row", justifyContent:"space-between", width:"75%", alignSelf:"center"}}
                            >
                                <TouchableOpacity
                                    onPress={() => {
                                        this._toggleChangePhoto();
                                    }}
                                >
                                    <View
                                        style={button.header}
                                    >
                                        <Text style={{fontSize:14*PixelRatio.getFontScale()}}>{"Отмена"}</Text>
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                    onPress={() => {
                                        let rename_socket = new WebSocket(WS_URL);
                                        rename_socket.onopen = () => {
                                            backendRequestCustomSocket(rename_socket, "chatEditRoom", this.props.userToken, {matrix_room_id: this.state.room_id, room_avatar_id:this.state.image_id});
                                        };
                                        rename_socket.onmessage = () => {
                                            this._toggleChangePhoto();
                                            this._updateRoomInfo();
                                            rename_socket.close();
                                        };
                                    }}
                                >
                                    <View
                                        style={button.header}
                                    >
                                        <Text style={{fontSize:14*PixelRatio.getFontScale()}}>{"Сохранить"}</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </ModalCenterCard>
                    }
                </Container>
            </Drawer>
        );
    }

    _askPermission = async (type, failureMessage) => {
        const { status, permissions } = await Permissions.askAsync(type);

        if (status === 'denied') {
            alert(failureMessage);
        }
    };

    _pickImage = async () => {
        await this._askPermission(Permissions.CAMERA, 'We need the camera-roll permission to read pictures from your phone...');
        await this._askPermission(Permissions.CAMERA_ROLL, 'We need the camera-roll permission to read pictures from your phone...');
        let pickerResult = await ImagePicker.launchImageLibraryAsync({
            //maxWidth:1000,
            //maxHeight:1000,
            quality:0.5,
            allowsEditing: true,
            mediaType: ImagePicker.MediaTypeOptions.Image,
            base64:true
        });

        console.log("pickerResult.didCancel", pickerResult);
        if (pickerResult.didCancel) {return}

        let img_socket = new WebSocket(WS_URL);
        img_socket.onmessage = (msg) => {
            //cancel_reconnect();
            let parsed_msg = JSON.parse(msg.data);
            console.log(parsed_msg);

            if (parsed_msg.statusCode != 200) {
                alert("Произошла ошибка при загрузке изображения!");
                this.setState({image_id:parsed_msg["data"]['file_id']});
                //return <View style={{flex:1, backgroundColor:"red"}} />
            } else {
                this.setState({image_id:parsed_msg["data"]['file_id']});
                console.log("Изображение загружено успешно!");
            }
            img_socket.close();
        };
        //console.log("image length in base64 = "+pickerResult.base64.length);
        //console.log("file type "+pickerResult.uri.split(".").slice(-1)[0]);
        img_socket.onopen = () => {
            backendRequestCustomSocket(
                img_socket,
                "fileUpload",
                this.props.userToken,
                {
                    fileRaw:pickerResult.uri.split(",")[1],
                    fileType:pickerResult.uri.split(";")[0].split("/")[1].replace("e", "")
                }
            );
            this.setState({
                change_photo_active:true,
                image_uri:""+pickerResult.uri,
            });
            console.log(pickerResult.uri);
        };
    };
}



const mapStateToProps = state => {
    return {
        //pi_overlay:         state.control.pi_overlay,
        userToken:      state.data.userToken,
        lang:           state.data.settings.lang,
        username:       state.data.chat.login,
        contacts:       state.data.chat.contacts,
        rooms:          state.data.chat.rooms,
        user_id:        state.data.chat.user_id,
    }
};

const mapDispatchToProps = dispatch => {
    return {
        //toggle: () => dispatch(togglePersonalInfoOverlay()),
        addRooms:                (room) => dispatch(addRooms({room})),
        removeJoined:            (room) => dispatch(removeJoined({room})),
        addContacts:             (contact) => dispatch(addContacts({contact}))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(ChatGroupInfoScreen);
