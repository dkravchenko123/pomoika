import React from "react";
import {
    ActivityIndicator,
    Alert,
    Dimensions,
    FlatList,
    Image,
    PixelRatio, RefreshControl,
    ScrollView,
    Text,
    ToastAndroid,
    TouchableOpacity,
    View
} from "react-native";
import {Button, Card, Col, Container, Content, Footer, FooterTab, Grid, Header, Icon, Item, Input, Row} from "native-base";
import {SimpleLineIcons, Ionicons} from "@expo/vector-icons";
import {styles} from "../../styles/header_footer_styles";

import AccountTab from "../../components/pages/account_page";
import {HomePage} from "../../components/pages/home_page";
import {SearchScreen} from "../../components/pages/search_page";
import {disableNetWarn, enableNetWarn, toggleFilterView, togglePersonalInfoOverlay} from "../../actions/control";
import {connect} from "react-redux";
import PI_Overlay from "../../components/overlays/personal_info_overlay";
import EventPage from "../../components/pages/facts_sponsors_partners_page";
import DefaultHeader from "../../components/headers_footers/main_header";
import SubscreenHeader from "../../components/headers_footers/subscreen_header";
import {backendRequest, backendRequestCustomSocket} from "../../methods/ws_requests";
import {cancel_reconnect, getws, init_timeout} from "../../methods/webSocket";
import NetModalWarning from "../../components/cards/NetModalWarning";
import Drawer from "react-native-drawer";
import DrawerContent from "../../components/cards/DraweContent";
import MainHeader from "../../components/headers_footers/main_header.js"
import card from "../../styles/cards";
import button from "../../styles/buttons";
import {applyFilter, dateSort, parseFilterToArray} from "../../methods/filter_parsing";
import {
    receiveData,
    removeData, setAvailableFilter, setCurDate, setCurrentFilter, setDatesArray,
    setFactArray,
    setFilters,
    setIdMap,
    setProgram, setProgramReady,
    setSpeakersArray
} from "../../actions/data";
import PartnerCard from "../../components/cards/PartnerCard";
import FactCard from "../../components/cards/FactCard";
import {WS_URL} from "../../constants/backend";
//import AutoScrolling from "react-native-auto-scrolling";

const window = Dimensions.get("window");

class EventScreen extends React.Component {
    constructor (props) {
        super (props);
        this.state = {
            got_facts: false,
            got_filters:false,
            description_open:false,
            ready:false,

            last_shown_date: null,
            program_active:false,
            partners_active:false,

            cardheight:0
        };

        this.separated_speakers = {
            default:[]
        };
        this.fact = this.props.navigation.getParam('fact', null);
        this.drawer = new React.createRef();
        this.menu_fun = this.menu_fun.bind(this);
        this.close_drawer = this.close_drawer.bind(this);
        this._onRefresh  = this._onRefresh.bind(this);
        this.cardlayout = this.cardlayout.bind(this);
    }

    componentDidMount() {
        if (this.props.program_ready) {
            this.setState({ready:true});
        } else {
            this._onRefresh();
        }

        this.didBlurSubscription = this.props.navigation.addListener(
            'willBlur',
            payload => {
                if (this.close_drawer) this.close_drawer();
                //console.log('willBlur', payload);
            }
        );
    }

    componentWillUnmount() {
        if (this.didBlurSubscription) this.didBlurSubscription.remove();
    }

    _onRefresh() {
        this.setState({ready:false});
        if (this.props.event_id) {
            let prog_socket = new WebSocket(WS_URL);
            let filter_socket = new WebSocket(WS_URL);
            prog_socket.onmessage = (msg) => {
                prog_socket.close();
                let parsed_msg = JSON.parse(msg.data);
                console.log(parsed_msg);
                if (parsed_msg.statusCode == 200) {
                    this.setState({got_facts: true});
                    this.program = [...parsed_msg["data"]];
                    console.log("now we got facts");
                    if (this.state.got_filters) {
                        let res = parseFilterToArray(this.filters, this.program);
                        this.props.setProgram(this.program);
                        this.props.setFilters(this.filters);
                        this.props.setFactArray(res.res_array);
                        this.props.setIdMap(res.id_map);
                        this.props.setSpeakersArray(res.speakers_array);
                        if (res.dates_array.length > 0) {
                            this.props.setDatesArray(dateSort(res.dates_array));
                            this.props.setCurDate(res.dates_array[0])
                        }
                        this.props.setProgramReady(true);
                        this.setState({ready:true});
                        this.forceUpdate();
                    }
                }
            };
            prog_socket.onopen = () => {
                backendRequestCustomSocket(prog_socket, "getEventFacts", this.props.userToken, {event_id: this.props.event_id.toString()});
            };

            filter_socket.onmessage = (msg) => {
                let parsed_msg = JSON.parse(msg.data);
                console.log(parsed_msg);

                this.setState({got_filters: true});
                this.filters = {...parsed_msg["data"]};
                console.log("now we got filters");
                if (this.state.got_facts) {
                    let res = parseFilterToArray(this.filters, this.program);
                    this.props.setProgram(this.program);
                    this.props.setFilters(this.filters);
                    this.props.setFactArray(res.res_array);
                    this.props.setIdMap(res.id_map);
                    this.props.setSpeakersArray(res.speakers_array);
                    if (res.dates_array.length > 0) {
                        this.props.setDatesArray(dateSort(res.dates_array));
                        this.props.setCurDate(res.dates_array[0])
                    }
                    //this.props.removeData("getEventFilters");
                    this.props.setProgramReady(true);
                    this.setState({ready:true});
                    this.forceUpdate();
                }

                filter_socket.close();
            };
            filter_socket.onopen = () => {
                backendRequestCustomSocket(filter_socket, "getEventFilters", this.props.userToken, {event_id: this.props.event_id.toString()});
            };
        }
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.props.event_id != null && ((prevProps.event_id && prevProps.event_id != this.props.event_id) || !prevProps.event_id)) {
            console.log("event_id changed");
            this.setState({
                got_facts:false,
                got_filters:false,
                failed:false,
                ready:false
            }, this._onRefresh);
            return;
        }
    }

    menu_fun () {
        this.drawer.open();
    }

    close_drawer () {
        this.drawer.close();
    }

    cardlayout (height) {
        if (this.state.cardheight < height) this.setState({cardheight:height})
    }

    render () {
        console.log("description_open: "+this.state.description_open);
        console.log(this.props.event_json);
        console.log("this.state.got_facts && this.state.got_filters == "+(this.state.got_facts && this.state.got_filters));
        let background_color = this.props.event_json.style ? (this.props.event_json.style.Primary_color ? this.props.event_json.style.Primary_color : null) : null;

        let accent_color = this.props.event_json.style ? (this.props.event_json.style.Accent_color ? this.props.event_json.style.Accent_color : "white") : null;
        return (
            <Drawer
                content={<DrawerContent is_event navigation={this.props.navigation} open_facts={this.open_facts} close_drawer={this.close_drawer}/>}
                ref={(r) => this.drawer = r}
                openDrawerOffset={0.0}
                side={"right"}
                acceptPan
                negotiatePan

            >
                <View style={{flex:1}}>
                    <MainHeader is_event_screen={this.props.event_id != ""} menu_fun={this.menu_fun} navigation={this.props.navigation}/>
                    <Container>
                        <NetModalWarning />
                        <Content
                            style={{backgroundColor:"rgb(240,240,240)"}}
                            refreshControl={
                                <RefreshControl
                                    progressViewOffset={50}
                                    refreshing={this.props.event_id && (!this.state.ready)}
                                    onRefresh={this._onRefresh}
                                />
                            }
                        >
                            {this.props.event_id
                                ?
                                    this.state.ready
                                        &&
                                            <View style={{flex:1}}>
                                                <View style={{justifyContent:"space-between", backgroundColor:"white", width:window.width, elevation:15, paddingBottom:10}}>
                                                    <View style={{flexDirection:"row", alignItems:"center", marginTop:15, marginHorizontal:15}}>
                                                        <View style={{height:90, width:90, borderRadius:45, alignItems:"center",marginRight:12, marginBottom:12, /*elevation:5*/}}>
                                                            {this.props.event_json.logo &&
                                                                <Image
                                                                    style={{height:90, width:90, borderRadius:45, resizeMode:"center"}}
                                                                    source={{
                                                                        uri:this.props.event_json.logo,
                                                                        method: "GET",
                                                                        headers: {
                                                                            Authorization:this.props.userToken
                                                                        }
                                                                    }}
                                                                />
                                                            }
                                                        </View>
                                                        <Text style={{fontWeight:"bold", flexWrap:"wrap", flex:1, fontSize:18}}>{this.props.event_json.name}</Text>
                                                    </View>

                                                    {this.props.event_json.description_short.length > 0 &&
                                                            <View style={{alignItems:"center", marginBottom:15, paddingHorizontal:20}}>
                                                                <Text>{this.props.event_json.description_short}</Text>
                                                                {this.props.event_json.description_full &&
                                                                <TouchableOpacity
                                                                    style={{alignSelf:"flex-end"}}
                                                                    onPress={() => {
                                                                        this.props.navigation.navigate("WebViewScreen", {
                                                                            uri: this.props.event_json.description_full,
                                                                            event_screen: true
                                                                        });
                                                                    }}
                                                                >
                                                                    <Text style={{fontWeight: "bold"}}>Подробнее...</Text>
                                                                </TouchableOpacity>
                                                                }
                                                            </View>
                                                    }
                                                    <View style={{justifyContent:"space-between", paddingHorizontal:20, marginBottom:15, alignSelf:"center"}}>
                                                        <View style={{flexDirection:"column", justifyContent:"space-between"/*, marginBottom:10*/}}>
                                                            <View style={{flexDirection:"row", justifyContent:"center"}}>
                                                                <View style={{opacity:((this.props.program_ready) ? 1 : 0.4)}}>
                                                                    <TouchableOpacity
                                                                        style={[button.header, {marginRight:15, width:120, height:30, borderRadius:23, borderWidth:1, backgroundColor:accent_color, borderColor:accent_color}]}
                                                                        onPress={() => {
                                                                            if (this.props.program_ready) this.props.navigation.navigate("FactsAndSponsors");
                                                                        }}
                                                                    >
                                                                        <Text style={{color:"white", fontSize:14*PixelRatio.getFontScale()}}>Программа</Text>
                                                                    </TouchableOpacity>
                                                                </View>
                                                                <TouchableOpacity
                                                                    style={[button.header, {marginRight:15, width:120, height:30, borderRadius:23, borderWidth:1, backgroundColor:accent_color, borderColor:accent_color}]}
                                                                >
                                                                    <Text style={{color:"white", fontSize:14*PixelRatio.getFontScale()}}>Карта</Text>
                                                                </TouchableOpacity>
                                                            </View>
                                                        </View>
                                                    </View>
                                                </View>



                                                {this.props.program_ready && applyFilter(this.props.fact_array, this.props.id_map, []).length != 0 &&     //this.props.fact_array.length != 0 &&
                                                    <View style={{flexDirection:"column", justifyContent:"center", backgroundColor:"white", width:"100%", marginTop:25, paddingBottom:20, elevation:15}}>
                                                        <View
                                                            style={{height:40, borderTopWidth:1, flexDirection:"row", justifyContent:"space-between", alignItems:"center", borderColor:"rgb(220,219,216)"}}
                                                        >
                                                            <Text style={{
                                                                fontSize: 16,
                                                                color: "#000",
                                                                marginHorizontal:20,
                                                            }}>{"Ближайшие сессии:"}</Text>
                                                        </View>
                                                        <ScrollView
                                                            nestedScrollEnabled={true}
                                                            showsHorizontalScrollIndicator={false}
                                                            horizontal
                                                            style={{borderTopWidth:1, borderBottomWidth:1, borderColor:"rgb(220,219,216)", backgroundColor:"rgb(246,246,246)"}}
                                                            contentContainerStyle={{paddingVertical:4, alignItems:"center"}}
                                                        >
                                                            {applyFilter(this.props.fact_array, this.props.id_map, []).slice(0, 10).map((item, index) => {
                                                                let fact = item;

                                                                let fact_place_name = (fact["FactPlaceName"] && fact["FactPlaceName"].length != 0 ? fact["FactPlaceName"] : null);
                                                                return <ScrollView contentContainerStyle={{paddingBottom:10}}/*style={{height:240}}*/><FactCard
                                                                    key={index}
                                                                    //props for collapsed view
                                                                    time_start={this.getTime(fact["StartDate"])}
                                                                    time_end={this.getTime(fact["EndDate"])}
                                                                    fact_name={fact["FactName"]}
                                                                    fact_description={fact["FactDescription"]}
                                                                    fact_place={fact_place_name}
                                                                    //props for expanded view
                                                                    fact_url={fact["FactStreamURL"]}
                                                                    speakers={fact["Speakers"]}
                                                                    fact_id={fact["FactID"]}
                                                                    fact_rating={fact["FactRating"]}

                                                                    fact_obj={fact}
                                                                    navigation={this.props.navigation}

                                                                    //cardlayout={this.cardlayout}
                                                                    //minheight={this.state.cardheight ? this.state.cardheight : null}

                                                                    base_color={background_color ? "white" : null}
                                                                    background_color={background_color}
                                                                    accent_color={accent_color}
                                                                /></ScrollView>
                                                            })}
                                                            <TouchableOpacity
                                                                style={{
                                                                    width:50, height:50,marginHorizontal:80,
                                                                }}
                                                                onPress={() => {
                                                                    if (this.props.program_ready) this.props.navigation.navigate("FactsAndSponsors");
                                                                }}
                                                            >
                                                                <Text style={{
                                                                    fontSize: 32,
                                                                    color: "#000",
                                                                }}>...</Text>
                                                            </TouchableOpacity>
                                                        </ScrollView>
                                                    </View>


                                                }

                                                {this.props.filters["Partners"] && this.props.filters["Partners"].length != 0 &&

                                                    <View style={{flexDirection:"column", justifyContent:"center", backgroundColor:"white", width:"100%", marginTop:25, paddingBottom:20, elevation:15}}>

                                                        {/*<Text style={{fontSize:14, marginVertical:12, marginHorizontal:20, color:"rgb(169,25,59)"}}>{"Создать новый заказ:"}</Text>*/}

                                                        <View
                                                            style={{height:40, borderTopWidth:1, flexDirection:"row", justifyContent:"space-between", alignItems:"center", borderColor:"rgb(220,219,216)"}}
                                                        >
                                                            <Text style={{
                                                                fontSize: 16,
                                                                color: "#000",
                                                                marginHorizontal:20,
                                                            }}>{"Партнеры мероприятия:"}</Text>
                                                        </View>
                                                            <ScrollView horizontal style={{borderTopWidth:1, borderBottomWidth:1, borderColor:"rgb(220,219,216)", backgroundColor:"rgb(246,246,246)"}}
                                                                        contentContainerStyle={{paddingVertical:10}}
                                                                        showsHorizontalScrollIndicator={false}>
                                                                {/*<AutoScrolling style={{height:140, width:window.width-60}}>*/}

                                                                {this.props.filters["Partners"].map((partner, index, arr) => {
                                                                    return (
                                                                        <TouchableOpacity
                                                                            onPress={() => {
                                                                                this.props.navigation.navigate("PartnerScreen", {partner});
                                                                            }}
                                                                        >
                                                                            <View
                                                                                style={{
                                                                                    //borderRadius: 15,
                                                                                    //backgroundColor: "rgb(220,219,216)",
                                                                                    width: 140,
                                                                                    height: 100,
                                                                                    justifyContent: "space-around",
                                                                                    alignItems: "center",
                                                                                    marginRight: (index != (arr.length - 1) ? 10 : 0)
                                                                                }}
                                                                            >
                                                                                {/*<Text style={{fontWeight:"bold"}}>{partner.Name}</Text>*/}
                                                                                <Image
                                                                                    style={{
                                                                                        width: 140,
                                                                                        height: 100,
                                                                                        borderRadius: 15,
                                                                                        resizeMode: "center"
                                                                                    }}
                                                                                    source={{
                                                                                        uri: partner["Imageurl"],
                                                                                        method: "GET",
                                                                                        headers: {
                                                                                            Authorization: this.props.userToken
                                                                                        }
                                                                                    }}
                                                                                />
                                                                            </View>
                                                                        </TouchableOpacity>
                                                                    );
                                                                })}
                                                            </ScrollView>
                                                    </View>
                                                }
                                                <View style={{height:150}}/>
                                            </View>
                                :
                                    <View>
                                        <Text style={{fontSize: 28, color:"black", alignSelf:"center"}}>Мероприятие не выбрано!</Text>
                                        <TouchableOpacity
                                            style={[button.header, {width:116, height:30, borderRadius:23, borderWidth:1, alignSelf:"center"}, button.active]}
                                            onPress={() => {
                                                this.props.navigation.navigate("HomeTab");
                                            }}
                                        >
                                            <Text>Выбрать</Text>
                                        </TouchableOpacity>
                                    </View>
                            }
                        </Content>
                    </Container>
                </View>
            </Drawer>
        );
    }

    getTime = (date) => {
        let all_time = date.split("T")[1].split("Z")[0].split(":").slice(0, 2);
        return all_time.join(":");
    };
}

const mapStateToProps = state => {
    return {
        lang:               state.data.settings.lang,
        received_data:      state.data.received_data,
        speakers_array:     state.data.event.speakers_array,
        userToken:          state.data.userToken,
        event_id:           state.data.event.event_id,
        event_json:         state.data.event.event_json,
        filters:            state.data.event.filters,
        program_ready:      state.data.event.program_ready,
        fact_array:         state.data.event.fact_array,
        id_map:             state.data.event.id_map
    }
};

const mapDispatchToProps = dispatch => {
    return {
        //toggle: () => dispatch(togglePersonalInfoOverlay()),
        removeData:              (key) => dispatch(removeData({key})),
        receiveData:             (data) => dispatch(receiveData(data)),
        setProgram:              (program) => dispatch(setProgram({program})),
        setFilters:              (filters) => dispatch(setFilters({filters})),
        setFactArray:            (fact_array) => dispatch(setFactArray({fact_array})),
        setSpeakersArray:        (speakers_array) => dispatch(setSpeakersArray({speakers_array})),
        setIdMap:                (id_map) => dispatch(setIdMap({id_map})),
        toggleFilterView:        () => dispatch(toggleFilterView()),
        setCurrentFilter:        (current_filter) => dispatch(setCurrentFilter({current_filter})),
        setAvailableFilter:      (available_filter) => dispatch(setAvailableFilter({available_filter})),
        setDatesArray:           (dates_array) => dispatch(setDatesArray({dates_array})),
        setCurDate:              (cur_date) => dispatch(setCurDate({cur_date})),
        enableNetWarn:           () => dispatch(enableNetWarn()),
        disableNetWarn:          () => dispatch(disableNetWarn()),
        setProgramReady:         (isReady) => dispatch(setProgramReady({isReady}))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(EventScreen);
