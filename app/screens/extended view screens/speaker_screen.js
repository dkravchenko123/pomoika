import React from "react";
import {Image, PixelRatio, Text, ToastAndroid, TouchableOpacity, View} from "react-native";
import {Button, Card, Container, Content, Col, Footer, FooterTab, Grid, Header, Icon, Item, Input, Row} from "native-base";
import {SimpleLineIcons, Ionicons} from "@expo/vector-icons";
import {styles} from "../../styles/header_footer_styles";

import AccountTab from "../../components/pages/account_page";
import {HomePage} from "../../components/pages/home_page";
import {SearchScreen} from "../../components/pages/search_page";
import {togglePersonalInfoOverlay} from "../../actions/control";
import {connect} from "react-redux";
import PI_Overlay from "../../components/overlays/personal_info_overlay";
import EventPage from "../../components/pages/facts_sponsors_partners_page";
import DefaultHeader from "../../components/headers_footers/main_header";
import SubscreenHeader from "../../components/headers_footers/subscreen_header";
import NetModalWarning from "../../components/cards/NetModalWarning";
import DrawerContent from "../../components/cards/DraweContent";
import Drawer from "react-native-drawer";
import button from "../../styles/buttons";
import card from "../../styles/cards";


class SpeakerScreen extends React.Component {
    constructor(props) {
        super(props);

        this.drawer = new React.createRef();
        this.menu_fun = this.menu_fun.bind(this);
        this.close_drawer = this.close_drawer.bind(this);
    }

    menu_fun () {
        this.drawer.open();
    }

    close_drawer () {
        this.drawer.close();
    }

    componentDidMount() {
        this.didBlurSubscription = this.props.navigation.addListener(
            'willBlur',
            payload => {
                if (this.close_drawer) this.close_drawer();
                //console.log('willBlur', payload);
            }
        );
    }

    componentWillUnmount() {
        if (this.didBlurSubscription) this.didBlurSubscription.remove();
    }

    render () {
        let background_color = "rgb(240,240,240)";
        let accent_color = "white";

        let speaker = this.props.navigation.getParam('speaker', null);
        return (
            <Drawer
                content={<DrawerContent navigation={this.props.navigation} open_facts={this.open_facts} close_drawer={this.close_drawer}/>}
                ref={(r) => this.drawer = r}
                openDrawerOffset={0.0}
                side={"right"}
                acceptPan
                negotiatePan
            >
                <Container>
                    <NetModalWarning />
                    <SubscreenHeader menu_fun={this.menu_fun} is_event_screen navigation={this.props.navigation}/>
                    <Content style={{paddingTop:0, paddingHorizontal:0, backgroundColor:(background_color ? background_color : "white")}}>
                        <View style={[card.base, {justifyContent:"space-between", backgroundColor:(background_color ? "white" : "rgb(220,219,216)"), borderRadius: 15}]}>
                            <Grid style={{padding:15}}>
                                <Row>

                                        <Image style={{height:120, width:120, borderRadius:60, resizeMode:"cover"}} source={{uri:speaker["SpeakerUserImgURL"]}}/>

                                    <Col size={1} style={{marginLeft:15}}>
                                        <Text style={{fontSize:24}}>{speaker["SpeakerFirstName"] + " " + speaker["SpeakerLastName"]}</Text>
                                        <Text>{speaker["SpeakerDescription"]}</Text>
                                    </Col>
                                </Row>
                                <Row>
                                    {speaker["SpeakerRating"] != null &&
                                        <Grid style={{padding: 15, flexDirection:"column"}}>
                                            <Text style={{fontWeight:"bold", marginBottom:8}}>Рейтинг спикера:</Text>
                                            <View style={{flexDirection:"row"}}>
                                                {((rating) => {
                                                    let show = [];
                                                    for (let i = 0; i < 5; i++) {
                                                        if (i <= rating-1) {
                                                            show.push(<Ionicons color={this.props.accent_color} style={{alignSelf:"center", marginRight:2}} size={16} name={"ios-star"}/>);
                                                        } else {
                                                            show.push(<Ionicons color={this.props.accent_color} style={{alignSelf:"center", marginRight:2}} size={16} name={"ios-star-outline"}/>);
                                                        }
                                                    }
                                                    return (show);
                                                })(speaker["SpeakerRating"])}
                                                <Text style={{marginLeft:3}}>{speaker["SpeakerRating"]}</Text>
                                            </View>
                                            <View style={{flexDirection:"row", paddingHorizontal:30, justifyContent:"space-between"}}>
                                                <TouchableOpacity
                                                    onPress={() => {
                                                    }}
                                                >
                                                    <View
                                                        style={[button.header, {width:120, marginRight:12, marginTop:20}]}
                                                    >
                                                        <Text style={[{fontSize:12*PixelRatio.getFontScale()}]}>Оценить</Text>
                                                    </View>
                                                </TouchableOpacity>
                                                <TouchableOpacity
                                                    onPress={() => {
                                                    }}
                                                >
                                                    <View
                                                        style={[button.header, {width:120, marginRight:12, marginTop:20}]}
                                                    >
                                                        <Text style={[{fontSize:12*PixelRatio.getFontScale()}]}>Задать вопрос</Text>
                                                    </View>
                                                </TouchableOpacity>
                                            </View>
                                        </Grid>
                                    }
                                </Row>


                            </Grid>
                        </View>

                        {speaker["Sessions"].length != 0 &&
                            <View style={[card.base, {
                                justifyContent: "space-between",
                                backgroundColor: (background_color ? "white" : "rgb(220,219,216)"),
                                borderRadius: 15,
                                marginTop:10
                            }]}>
                                <View style={{padding:15, width:"100%"}}>
                                    <Text style={{fontWeight:"bold", marginBottom:8}}>Сессии:</Text>
                                    <View style={{width:"100%"}}>
                                        {speaker["Sessions"].map((fact, index) => {
                                            return (
                                                <TouchableOpacity
                                                    style={{marginBottom:5, flexDirection:"row", flex:1}}
                                                    key={index}
                                                    onPress={() => {
                                                        this.props.navigation.navigate("FactScreen", {fact});
                                                    }}
                                                >
                                                    <View style={{flex:1}}>
                                                        {fact["StartDate"] != "" && <Text style={{fontWeight:"bold"}}>{fact["StartDate"].split("T")[0].split("-").reverse().join("/")+"  "}</Text>}
                                                    </View>
                                                    <View style={{flex:3}}>
                                                        <Text>{fact["FactName"]}</Text>
                                                    </View>
                                                </TouchableOpacity>
                                            );
                                        })}
                                    </View>
                                </View>
                            </View>
                        }


                    </Content>
                </Container>
            </Drawer>
        );
    }
}

const mapStateToProps = state => {
    return {
        pi_overlay:         state.control.pi_overlay,
        search_active:      state.control.search_active,
        event_json:         state.data.event.event_json,
    }
};

const mapDispatchToProps = dispatch => {
    return {
        //toggle: () => dispatch(togglePersonalInfoOverlay()),
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(SpeakerScreen);
