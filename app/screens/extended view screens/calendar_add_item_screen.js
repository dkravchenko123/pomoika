import React from "react";
import {
    Alert, Dimensions,
    KeyboardAvoidingView, Platform,
    ScrollView,
    Text,
    TextInput,
    TouchableOpacity,
    View
} from "react-native";
import {
    Container,
    Content,
    Row,
    Picker
} from "native-base";
import { Ionicons } from "@expo/vector-icons";
import { styles } from "../../styles/header_footer_styles";
import { connect } from "react-redux";
import SubscreenHeader from "../../components/headers_footers/subscreen_header";
import DatePicker from 'react-native-datepicker'
import {
    backendRequestPromise,
} from "../../methods/ws_requests";
import { setCalendarNeedsUpdate } from "../../actions/data";
import NetModalWarning from "../../components/cards/NetModalWarning";
import DrawerContent from "../../components/cards/DraweContent";
import Drawer from "react-native-drawer";
import button from "../../styles/buttons";
import card from "../../styles/cards";
import UserSearchCard from "../../components/cards/UserSearchCard";
import UserDisplayCard from "../../components/cards/UserDisplayCard";
import { localeStr } from "../../methods/translation_provider";
import { ModalCenterCard } from "../../components/overlays/modal_center_card";
import { FontAwesome } from "@expo/vector-icons";
import ShortUserDisplayCard from "../../components/cards/ShortUserDisplayCard";

const window = Dimensions.get("window");

let DateTimePicker;
/*
if (Platform.OS == "ios") {
    DateTimePicker = require("@react-native-community/datetimepicker").default;
}
*/

class CalendarEditItemScreen extends React.Component {
    constructor(props) {
        super(props);
        this.is_new = this.props.navigation.getParam("is_new", false);
        let item = this.props.navigation.getParam("item", null);
        if (!item) {
            item = {
                item_id: null,
                item_types: [],
                guests: [],
                user_id: null,
                fact_id: this.props.navigation.getParam('fact_id', null),
                item_type_id: 1,
                event_id: this.props.navigation.getParam('event_id', null),
                name: null,
                description: null,
                theme: null,
                place: null,
                place_time: null,
                place_on_site: false,
            };
        } else {
            console.log(
                "date manipulation",
                item.date,
                item.date.toString().slice(0, 16).split("T").join(" "),
                new Date(item.date),
                new Date(item.date.toString().slice(0, 16).split("T").join(" ")),
                //new Date(item.date.toString().slice(0, 16).split("T").join(" ")).toISOString()
            );
        }

        this.state = {
            item_id: item.item_id,
            date:
                item.date
                    ?
                    new Date(new Date(item.date).getTime() + (new Date()).getTimezoneOffset() * 60000)
                    :
                    new Date(),
            date_display: item.date ? item.date.toString() : null,
            user_id: item.user_id ? item.user_id.toString() : null,
            fact_id: item.fact_id ? item.fact_id.toString() : null,
            item_types: [],
            item_type_id: item.item_type_id ? item.item_type_id : null,
            event_id: item.event_id ? item.event_id.toString() : null,
            name: item.name ? item.name.toString() : null,
            description: item.description ? item.description.toString() : null,
            theme: item.theme ? item.theme.toString() : null,
            place: item.place ? item.place.toString() : null,
            place_time: item.place_time ? item.place_time.toString() : null,
            place_on_site: item.place_on_site ? item.place_on_site : false,
            place_time_id: null,

            guests: item.guests ? item.guests : [],

            user_results: [],
            searching: false,
            show: false,
            //user search
            user_search_active: false,
            user_search_query: "",
            user_search_results: [],

            available_places: null,

            place_modal: false,
            time_modal: false,
            time_picker: false,
            date_picker: false,
        };

        this.drawer = new React.createRef();
        this.menu_fun = this.menu_fun.bind(this);
        this.close_drawer = this.close_drawer.bind(this);
        this.choose_user = this.choose_user.bind(this);
    }

    menu_fun() {
        this.drawer.open();
    }

    close_drawer() {
        this.drawer.close();
    }

    choose_user(user) {
        if (this.state.guests.findIndex((el) => el.userid == user.userid) == -1) {
            this.setState({guests: [...this.state.guests, user]});
        }
    }

    componentDidMount() {
        backendRequestPromise(
            "calendarGetItemtype",
            this.props.bearerToken
        ).then((resp) => {
            let response = resp.sort((el1, el2) => el1.itemtypeid - el2.itemtypeid);
            //console.log(response);
            this.setState({
                item_types: response,
                item_type_id: this.state.item_type_id ? this.state.item_type_id : response[0].itemtypeid
            });
        });

        if (this.props.event_id != null) {
            this._getPlaces();
        }

        this.didBlurSubscription = this.props.navigation.addListener(
            'willBlur',
            payload => {
                if (this.close_drawer) this.close_drawer();
                //console.log('willBlur', payload);
            }
        );
    }

    toggleState = (key) => {
        this.setState({ [key]: !this.state[key] });
    };

    _getPlaces = () => {
        backendRequestPromise(
            "getEventPlaces",
            this.props.bearerToken,
            {
                event_id: this.props.event_id
            }
        ).then((res) => {
            if (res) {
                console.log("event places", res.rooms);
                this.setState({ available_places: res.rooms });
            }
        }).catch((err) => { })
    };

    timeModal = () => {
        return (
            <View
                style={{
                    flexDirection: "column",
                    width: 300,
                    //minHeight: 200,
                    padding: 15,
                    alignItems: "center"
                }}
            >
                <Row style={{ justifyContent: "center", flex: 1 }}>
                    <View style={{ width: 40 }} />
                    <Text style={{ fontWeight: "bold", fontSize: 20 }}>{localeStr("calendar_choose_time", this.props.lang)}</Text>
                    <View style={{ width: 40 }}>
                        <TouchableOpacity
                            style={{ alignSelf: "flex-start" }}
                            onPress={() => {
                                this.toggleState("date_picker")
                            }}
                        >
                            <Ionicons size={40} name={"ios-close"} />
                        </TouchableOpacity>
                    </View>
                </Row>

                <Row zIndex={6} style={{ marginTop: 10, height: 50, width: '100%', justifyContent: "center",  borderWidth: 2, borderRadius: 5, borderColor: "rgba(220,219,216, 0.2)", backgroundColor: "rgba(220,219,216, 0.2)" }}>
                    <View  style={{ backgroundColor: "rgba(220,219,216, 0)", flex: 1, flexDirection:"column", justifyContent:"space-between", }}>
                        <TouchableOpacity
                            zIndex={5}
                            style={[{
                                borderColor: "rgba(220,219,216, 0.2)",
                                flexDirection: "row",
                                //flex: 1,
                                width:"100%",
                                backgroundColor: "transparent",
                                borderRadius: 5,
                                paddingRight:25,
                                paddingLeft: 15,
                                color: 'gray',
                                justifyContent:"space-between",
                                alignItems:"center",
                                height:50
                            }]}
                            onPress={() => {
                                this.toggleState("date_picker");
                            }}
                        >
                            <Text>
                                {((date) => {
                                    return (`${[date.getDate(), date.getMonth() + 1, date.getFullYear()].join("/")}`);
                                })(this.state.date)}
                            </Text>
                            <Ionicons color={'#005989'} style={{ marginHorizontal: 5 }} size={30} name={this.state.date_picker ? "md-arrow-dropup" : "md-arrow-dropdown"} />
                        </TouchableOpacity>
                        {this.state.date_picker &&
                        <View
                            zIndex={4}
                            style={{
                                marginTop: -4,
                                //borderColor: "#000",
                                borderTopColor: "#fff",
                                borderWidth: 2,
                                backgroundColor: "white",
                                width: "100%",
                                borderColor: "rgba(220,219,216, 0.2)"
                            }}
                        >
                            <DateTimePicker
                                zIndex={4}
                                locale={this.props.lang == "ru" ? "ru-RU" : "en-EN"}
                                mode="date"
                                //date={this.state.date}
                                //value={new Date(this.state.date.getTime() + (new Date()).getTimezoneOffset() * 60000)}
                                value={this.state.date}
                                style={[styles.login_picker, {
                                    marginTop: 5,
                                    alignSelf:"center",
                                    width: "100%",
                                }]}
                                customStyles={{
                                    placeholderText: {
                                        alignSelf: "flex-start",
                                        paddingLeft: 7
                                    },
                                    dateText: {
                                        alignSelf: "flex-start",
                                        paddingLeft: 7
                                    },
                                    dateInput: {
                                        borderColor: "white"
                                    },
                                }}
                                onChange={(event, date) => {
                                    console.log("date picket response type", typeof (date), date);

                                    this.setState({
                                        //show: false,//Platform.OS === 'ios' ? true : false,
                                        date
                                    });
                                }}
                                iconComponent={<Ionicons style={{ marginHorizontal: 5 }} size={30}
                                                         name={"ios-calendar"} />}
                            />
                        </View>
                        }
                    </View>
                </Row>


                <Row zIndex={3} style={{ marginTop: 10, height: 50, width: '100%', justifyContent: "center",  borderWidth: 2, borderRadius: 5, borderColor: "rgba(220,219,216, 0.2)", backgroundColor: "rgba(220,219,216, 0.2)" }}>
                    <View  style={{ backgroundColor: "rgba(220,219,216, 0)", flex: 1, flexDirection:"column", justifyContent:"space-between",}}>
                        <TouchableOpacity
                            zIndex={3}
                            style={[{
                                borderColor: "rgba(220,219,216, 0.2)",
                                flexDirection: "row",
                                //flex: 1,
                                width:"100%",
                                backgroundColor: "transparent",
                                borderRadius: 5,
                                paddingRight:25,
                                paddingLeft: 15,
                                color: 'gray',
                                justifyContent:"space-between",
                                alignItems:"center",
                                height:50
                            }]}
                            onPress={() => {
                                this.toggleState("time_picker");
                            }}
                        >
                            <Text>
                                {((date) => {
                                    //return(new Date(date.getTime() - (new Date()).getTimezoneOffset() * 60000).toISOString().slice(0, 16).split("T").join(" "));
                                    //return(date.toString());
                                    return (`${date.getHours()}:${(date.getMinutes() < 10 ? "0" : "") + date.getMinutes().toString()}`);
                                })(this.state.date)}
                            </Text>
                            <Ionicons color={'#005989'} style={{ marginHorizontal: 5 }} size={30} name={this.state.time_picker ? "md-arrow-dropup" : "md-arrow-dropdown"} />
                        </TouchableOpacity>
                        {this.state.time_picker &&
                        <View
                            zIndex={2}
                            style={{
                                marginTop: -4,
                                //borderColor: "#000",
                                borderTopColor: "#fff",
                                borderWidth: 2,
                                backgroundColor: "white",
                                borderColor: "rgba(220,219,216, 0.2)"
                            }}
                        >
                            <DateTimePicker
                                zIndex={2}
                                locale={this.props.lang == "ru" ? "ru-RU" : "en-EN"}
                                mode="time"
                                is24Hour={true}
                                display="default"
                                placeholder={"Время"}
                                //date={this.state.date}
                                //value={new Date(this.state.date.getTime() + (new Date()).getTimezoneOffset() * 60000)}
                                value={this.state.date}
                                /*onDateChange={(val) => {
                                    this.setState({date: val});
                                    //this.setState({date: val.split(" ").join("T")+":00Z"});
                                }}*/
                                style={[styles.login_picker, {
                                    marginTop: 5,
                                    paddingRight: 10,
                                    alignSelf:"center",
                                    width: "100%",
                                }]}
                                customStyles={{
                                    placeholderText: {
                                        alignSelf: "flex-start",
                                        paddingLeft: 7
                                    },
                                    dateText: {
                                        alignSelf: "flex-start",
                                        paddingLeft: 7
                                    },
                                    dateInput: {
                                        borderColor: "white"
                                    },
                                }}
                                onChange={(event, date) => {
                                    console.log("date picket response type", typeof (date), date);

                                    this.setState({
                                        //show: false,//Platform.OS === 'ios' ? true : false,
                                        date
                                    });
                                }}
                                iconComponent={<Ionicons color={'#005989'} style={{ marginHorizontal: 5 }} size={30}
                                                         name={"ios-calendar"} />}
                            />
                        </View>
                        }
                    </View>
                </Row>
            </View>
        );
    };

    timeButton = () => {
        return (
            <TouchableOpacity
                style={[card.base, { width: 320, padding: 15, alignSelf: "center", marginTop: 5, borderRadius: 15, borderTopStartRadius: 0, borderTopEndRadius: 0 }]}
                onPress={() => {
                    this.toggleState("time_modal");
                }}
            >
                <Text style={{ alignSelf: "center", color: "rgb(169,25,59)" }}>
                    {localeStr(
                        "calendar_time_button_label",
                        this.props.lang
                    )}
                </Text>
            </TouchableOpacity>
        );
    };

    placeModal = () => {
        return (
            <View
                style={{
                    flexDirection: "column",
                    width: 300,
                    height: this.state.place_on_site ? 200 : 140,
                    //maxHeight:0.7*window.height,
                    justifyContent:"center",
                    padding: 15,
                    alignItems: "center"
                }}
            >{/*
                <Row style={{ justifyContent: "space-between", flex: 1, marginBottom:8, minHeight:40, width:"100%"}}>
                    <View style={{width:40}}/>
                    <Text style={{ fontWeight: "bold", fontSize: 20, marginTop:10}}>{localeStr("calendar_choose_place", this.props.lang)}</Text>
                    <View style={{width:40, height:40, flexDirection:"row", justifyContent:"flex-end"}}>
                        <TouchableOpacity
                            style={{}}
                            onPress={() => {
                                this.toggleState("place_modal")
                            }}
                        >
                            <Ionicons size={40} name={"ios-close"} />
                        </TouchableOpacity>
                    </View>
                </Row>*/}

                <Row style={{ marginTop: 10, height: 50, width: '100%', justifyContent: "center",  borderWidth: 2, borderRadius: 5, borderColor: "rgba(220,219,216, 0.2)" }}>
                    <View style={{ backgroundColor: "rgba(220,219,216, 0.2)", flex: 1, flexDirection:"row", justifyContent:"space-between", paddingLeft: 10 }}>

                        <Picker
                            style={[{
                                borderColor: "rgba(220,219,216, 0.2)",
                                flexDirection: "row",
                                //flex: 1,
                                width:"100%",
                                backgroundColor: "transparent",
                                borderRadius: 5,
                                paddingRight:25,
                                //paddingLeft: 15,
                                color: 'gray',
                                justifyContent:"flex-start"
                            }]}
                            //placeholder={"Тип"}
                            selectedValue={this.state.place_on_site}
                            onValueChange={(value, itemIndex) => {
                                this.setState({ place_on_site: value, place: null });
                            }}
                            customStyles={{
                                placeholderText: {
                                    alignSelf: "flex-start",
                                    paddingLeft: 7
                                },
                                dateText: {
                                    alignSelf: "flex-start",
                                    paddingLeft: 15,
                                    color: 'gray',
                                    fontSize: 16,
                                },
                                dateInput: {
                                    borderColor: "rgba(220,219,216, 0.2)",
                                    borderWidth: 0,
                                }
                            }}
                            iconComponent={<Ionicons style={{ marginHorizontal: 5 }} size={30} color={"#005989"}
                                                     name={"ios-calendar"} />}
                        >
                            {[
                                localeStr("calendar_place_on_site", this.props.lang),
                                localeStr("calendar_place_off_site", this.props.lang)
                            ].map((item, index) => {
                                return <Picker.Item key={item} label={item} value={index == 0} />
                            })}
                        </Picker>
                        <FontAwesome
                            name='sort-down'
                            size={25}
                            color='#005989'
                            style={[{ right: 25, top: 5, position:"absolute" }]}
                        />
                    </View>
                    {/*}*/}
                </Row>

                {this.state.place_on_site
                    ?
                    <>
                        <Row style={{ marginTop: 10, height: 50, width: '100%', justifyContent: "center",  borderWidth: 2, borderRadius: 5, borderColor: "rgba(220,219,216, 0.2)" }}>
                            <View style={{ backgroundColor: "rgba(220,219,216, 0.2)", flex: 1, flexDirection:"row", justifyContent:"space-between", paddingLeft: 10 }}>

                                <Picker
                                    style={[{
                                        borderColor: "rgba(220,219,216, 0.2)",
                                        flexDirection: "row",
                                        //flex: 1,
                                        width:"100%",
                                        backgroundColor: "transparent",
                                        borderRadius: 5,
                                        paddingRight:25,
                                        //paddingLeft: 15,
                                        color: 'gray',
                                        justifyContent:"flex-start"
                                    }]}
                                    //placeholder={"Тип"}
                                    selectedValue={this.state.place}
                                    onValueChange={(value, itemIndex) => {
                                        let index = this.state.available_places.findIndex((e) => e.id == value);
                                        this.setState({ place: value });
                                        if (index >= 0) {
                                            backendRequestPromise(
                                                "getPlaceTimetable",
                                                this.props.bearerToken,
                                                {
                                                    Place_id: value
                                                }
                                            ).then((res) => {

                                                if (res) {
                                                    let new_places = [...this.state.available_places];
                                                    console.log("new places", new_places, index);
                                                    new_places[index].place_timetable = res.periods;
                                                    this.setState({ available_places: new_places });
                                                }
                                            });
                                        }
                                    }}
                                >
                                    {!!this.state.available_places && this.state.available_places.length > 0
                                        ?
                                        [
                                            <Picker.Item key={"default"} label={"--Место--"} value={null} />,
                                            (!!this.state.available_places && this.state.available_places.length > 0 &&
                                                this.state.available_places.map((item, index) => {
                                                    return <Picker.Item key={item.id} label={item.name} value={item.id} />
                                                })
                                            )
                                        ]
                                        :
                                        <Picker.Item key={"default"} label={"--Место--"} value={null} />
                                    }
                                </Picker>
                                <FontAwesome
                                    name='sort-down'
                                    size={25}
                                    color='#005989'
                                    style={[{ right: 25, top: 5, position:"absolute" }]}
                                />
                            </View>
                        </Row>
                        <Row style={{ marginTop: 10, height: 50, width: '100%', justifyContent: "center",  borderWidth: 2, borderRadius: 5, borderColor: "rgba(220,219,216, 0.2)" }}>
                            <View style={{ backgroundColor: "rgba(220,219,216, 0.2)", flex: 1, flexDirection:"row", justifyContent:"space-between", paddingLeft: 10 }}>

                                <Picker
                                    style={[{
                                        borderColor: "rgba(220,219,216, 0.2)",
                                        flexDirection: "row",
                                        //flex: 1,
                                        width:"100%",
                                        backgroundColor: "transparent",
                                        borderRadius: 5,
                                        paddingRight:25,
                                        //paddingLeft: 15,
                                        color: 'gray',
                                        justifyContent:"flex-start"
                                    }]}
                                    //placeholder={"Тип"}
                                    selectedValue={this.state.place_time_id}
                                    onValueChange={(value, itemIndex) => {
                                        this.setState({ place_time_id: value });
                                        if (this.state.place != null) {
                                            let place_index = this.state.available_places.findIndex((el) => {
                                                return el.id == this.state.place;
                                            });

                                            console.log("found place", place_index);

                                            if (place_index != -1 && this.state.available_places[place_index].hasOwnProperty("place_timetable")) {
                                                let time_obj = this.state.available_places[place_index].place_timetable.find((el) => el.period_id == value);
                                                console.log("time_obj", time_obj);
                                                if (time_obj != null) {
                                                    this.setState({ date: new Date(new Date(time_obj.start).getTime() + (new Date()).getTimezoneOffset() * 60000) });
                                                }
                                            }
                                        }
                                    }}
                                >
                                    {(() => {
                                        let return_arr = [
                                            <Picker.Item key={"default"} label={"--Время--"} value={null} />
                                        ];

                                        if (this.state.place != null) {
                                            let place_index = this.state.available_places.findIndex((el) => {
                                                return el.id == this.state.place
                                            });

                                            console.log("found place", place_index);

                                            if (place_index != -1 && this.state.available_places[place_index].hasOwnProperty("place_timetable")) {
                                                //console.log("place timetable", this.state.available_places[place_index].place_timetable);
                                                return_arr = [...return_arr, this.state.available_places[place_index].place_timetable.map((item, index) => {
                                                    let date_1 = new Date(new Date(item.start).getTime() + new Date().getTimezoneOffset() * 60000);
                                                    let date_2 = new Date(new Date(item.end).getTime() + new Date().getTimezoneOffset() * 60000);
                                                    let readable_time_1 = `${date_1.getHours()}:${(date_1.getMinutes() < 10 ? "0" : "") + date_1.getMinutes().toString()} ${[date_1.getDate(), date_1.getMonth() + 1, date_1.getFullYear()].join("/")}`;
                                                    let readable_time_2 = `${date_2.getHours()}:${(date_2.getMinutes() < 10 ? "0" : "") + date_2.getMinutes().toString()} ${[date_2.getDate(), date_2.getMonth() + 1, date_2.getFullYear()].join("/")}`;
                                                    return <Picker.Item key={item.period_id} label={readable_time_1 + " - " + readable_time_2} value={item.period_id} />
                                                })];
                                            }

                                        }

                                        return (return_arr);
                                    })()}
                                </Picker>
                                <FontAwesome
                                    name='sort-down'
                                    size={25}
                                    color='#005989'
                                    style={[{ right: 25, top: 5, position:"absolute" }]}
                                />
                            </View>
                        </Row>
                    </>
                    :
                    <Row style={{ marginTop: 10, height: 50, width: '100%', justifyContent: "center",  borderWidth: 2, borderRadius: 5, borderColor: "rgba(220,219,216, 0.2)" }}>
                        <View style={{ backgroundColor: "rgba(220,219,216, 0.2)", flex: 1, flexDirection:"row", justifyContent:"space-between", paddingLeft: 10 }}>

                            <TextInput
                                style={[{
                                    borderColor: "rgba(220,219,216, 0.2)",
                                    flexDirection: "row",
                                    //flex: 1,
                                    width:"90%",
                                    backgroundColor: "transparent",
                                    borderRadius: 5,
                                    fontSize:16,
                                    paddingLeft: 10,
                                    color: 'gray',
                                    justifyContent:"space-between"
                                }]}
                                placeholder={localeStr("calendar_item_place_name_placeholder", this.props.lang)}
                                value={this.state.place}
                                onChangeText={(val) => {
                                    this.setState({ place: val });
                                }}
                            />
                        </View>
                    </Row>
                }
            </View>
        );
    };

    placeButton = () => {
        return (
            <TouchableOpacity
                style={[card.base, { width: 320, padding: 15, alignSelf: "center", marginTop: 5, borderRadius: 15, borderTopStartRadius: 0, borderTopEndRadius: 0 }]}
                onPress={() => {
                    this.toggleState("place_modal");
                }}
            >
                <Text style={{ alignSelf: "center", color: "rgb(169,25,59)" }}>
                    {localeStr(
                        "calendar_place_button_label",
                        this.props.lang
                    )}
                </Text>
            </TouchableOpacity>
        );
    };

    guestsModal = () => {
        return (
            <View
                style={{
                    flexDirection: "column",
                    width: 300,
                    minHeight:120,
                    maxHeight:0.7*window.height,
                    justifyContent:"center",
                    padding: 15,
                    alignItems: "center"
                }}
            >
                <UserSearchCard accept_anyone square choose_user={this.choose_user} />
            </View>
        );
    };

    guestsButton = () => {
        return (
            <TouchableOpacity
                style={[card.base, { width: 320, padding: 15, alignSelf: "center", marginTop: 5, borderRadius: 15, borderTopStartRadius: 0, borderTopEndRadius: 0 }]}
                onPress={() => {
                    this.toggleState("guests_modal");
                }}
            >
                <Text style={{ alignSelf: "center", color: "rgb(169,25,59)" }}>
                    {localeStr(
                        "calendar_guest_button_label",
                        this.props.lang
                    )}
                </Text>
            </TouchableOpacity>
        );
    };

    componentWillUnmount() {
        if (this.didBlurSubscription) this.didBlurSubscription.remove();
    }

    render() {
        return (
            <Drawer
                content={<DrawerContent navigation={this.props.navigation} open_facts={this.open_facts} close_drawer={this.close_drawer} />}
                ref={(r) => this.drawer = r}
                openDrawerOffset={0.0}
                side={"right"}
                acceptPan
                negotiatePan
            >
                <KeyboardAvoidingView style={{ flex: 1 }} enabled behavior={"padding"}>
                    <Container style={{ backgroundColor: "rgba(220,219,216)" }}>
                        <NetModalWarning />
                        <SubscreenHeader menu_fun={this.menu_fun} navigation={this.props.navigation} />

                        {!!this.state.place_modal &&
                        <ModalCenterCard bottom_button={this.placeButton}>
                            {this.placeModal()}
                        </ModalCenterCard>
                        }

                        {!!this.state.time_modal &&
                        <ModalCenterCard bottom_button={this.timeButton}>
                            {this.timeModal()}
                        </ModalCenterCard>
                        }


                        {!!this.state.guests_modal &&
                        <ModalCenterCard bottom_button={this.guestsButton}>
                            {this.guestsModal()}
                        </ModalCenterCard>
                        }


                        <Content style={{ flex: 1, backgroundColor: "rgba(220,219,216,0.2)", paddingTop: 20, paddingBottom:60 }} contentContainerStyle={{paddingBottom:60}}>
                            <View style={[card.base, { padding: 15, borderRadius: 15, borderBottomStartRadius: 0, borderBottomEndRadius: 0 }]}>
                                <Row style={{ justifyContent: "center", flex: 1 }}>
                                    <Text style={{ fontWeight: "bold", fontSize: 20 }}>{localeStr(this.is_new ? "calendar_create_card_label" : "calendar_edit_card_label", this.props.lang)}</Text>
                                </Row>

                                <Row style={{ paddingLeft: 8, marginTop: 10, height: 50, justifyContent: "flex-start", alignItems: "center", flex: 1, borderWidth: 2, borderRadius: 5, borderColor: "rgba(220,219,216, 0.2)", backgroundColor: "rgba(220,219,216, 0.2)" }}>
                                    <TouchableOpacity
                                        onPress={() => {
                                            this.toggleState("place_modal");
                                        }}
                                    >
                                        <View
                                            style={[styles.picker, {
                                                borderColor: "#000",
                                                flexDirection: "row",
                                                flex: 1,
                                                alignItems: "center",
                                                justifyContent: "center"
                                            }]}
                                        >
                                            <Text style={[{ color: 'gray', paddingLeft: 10, fontSize: 16 }]}>
                                                {(() => {
                                                    if (this.state.place == null) return localeStr("calendar_item_place_placeholder", this.props.lang);

                                                    if (this.state.place_on_site) {
                                                        if (!!this.state.available_places) {
                                                            let place_found = this.state.available_places.find((el) => el.id == this.state.place);
                                                            if (place_found != null) {
                                                                return place_found["name"];
                                                            }
                                                        }
                                                    } else {
                                                        return this.state.place;
                                                    }

                                                    return localeStr("calendar_item_place_placeholder", this.props.lang);
                                                })()}
                                            </Text>
                                        </View>
                                    </TouchableOpacity>
                                </Row>

                                <TouchableOpacity
                                    onPress={() => {
                                        if (this.state.place_on_site) Alert.alert(localeStr("attention", this.props.lang), localeStr("calendar_manual_time_not_available", this.props.lang));
                                    }}
                                    pointerEvents={this.state.place_on_site ? "auto" : "box-none"}
                                >
                                    <Row style={{ marginTop: 15, height: this.state.show ? 280 : 50, justifyContent: "center", flex: 1, borderWidth: 2, borderRadius: 5, borderColor: "rgba(220,219,216, 0.2)", backgroundColor: "rgba(220,219,216, 0.2)" }}>
                                        {Platform.OS == "android"
                                            ?
                                            <DatePicker
                                                locale={this.props.lang == "ru" ? "ru-RU" : "en-EN"}
                                                mode="datetime"
                                                is24Hour={true}
                                                display="default"
                                                placeholder={"Время"}
                                                date={this.state.date}
                                                //value={this.state.date}
                                                onDateChange={(val) => {
                                                    //console.log("setting date", val);
                                                    this.setState({ date: val});
                                                    //this.setState({date: val.split(" ").join("T")+":00Z"});
                                                }}
                                                style={[{
                                                    paddingTop: 5,
                                                    paddingRight: 15,
                                                    // borderColor: "#000",
                                                    width: "100%",
                                                    backgroundColor: "rgba(220,219,216, 0.2)",
                                                }]}
                                                customStyles={{
                                                    placeholderText: {
                                                        alignSelf: "flex-start",
                                                        paddingLeft: 7
                                                    },
                                                    dateText: {
                                                        alignSelf: "flex-start",
                                                        paddingLeft: 15,
                                                        color: 'gray',
                                                        fontSize: 16,
                                                    },
                                                    dateInput: {
                                                        borderColor: "rgba(220,219,216, 0.2)",
                                                        borderWidth: 0,
                                                    }
                                                }}

                                                iconComponent={<Ionicons style={{ marginHorizontal: 5 }} size={30} color={"#005989"}
                                                                         name={"ios-calendar"} />}
                                            />
                                            :
                                            <View style={{ flex: 1, justifyContent: "center", padding: 10 }}>
                                                <TouchableOpacity
                                                    style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}
                                                    onPress={() => {
                                                        //this.setState({show:!this.state.show});
                                                        this.toggleState("time_modal");
                                                    }}
                                                >
                                                    <Text>
                                                        {((date) => {
                                                            //return(new Date(date.getTime() - (new Date()).getTimezoneOffset() * 60000).toISOString().slice(0, 16).split("T").join(" "));
                                                            //return(date.toString());
                                                            return (`${date.getHours()}:${(date.getMinutes() < 10 ? "0" : "") + date.getMinutes().toString()} ${[date.getDate(), date.getMonth() + 1, date.getFullYear()].join("/")}`);
                                                        })(this.state.date)}
                                                    </Text>
                                                    <Ionicons color={'#005989'} style={{ marginHorizontal: 5, paddingBottom:5 }} size={30} name={this.state.show ? "ios-checkmark" : "ios-calendar"} />
                                                </TouchableOpacity>
                                            </View>
                                        }
                                    </Row>
                                </TouchableOpacity>
                                {/*{this.state.item_types.length > 0 &&
                                    <Row style={{ marginTop: 10, height: 50, width: '100%', justifyContent: "center", flex: 1, borderWidth: 2, borderRadius: 5, borderColor: "rgba(220,219,216, 0.2)" }}>
                                        <View style={{ backgroundColor: "rgba(220,219,216, 0.2)", flex: 1, paddingLeft: 10 }}>

                                            <Picker
                                                placeholder={"Тип"}
                                                selectedValue={this.state.item_type_id}
                                                onValueChange={(value, itemIndex) => {
                                                    this.setState({ item_type_id: value });
                                                }}
                                                style={[{
                                                    borderColor: "rgba(220,219,216, 0.2)",
                                                    flexDirection: "row",
                                                    flex: 1,
                                                    backgroundColor: "transparent",
                                                    borderRadius: 5,
                                                    paddingLeft: 15,
                                                    color: 'gray',
                                                }]}
                                            >
                                                {this.state.item_types.map((item) => {
                                                    return <Picker.Item key={item.itemtypeid} label={item.itemtypename} value={item.itemtypeid} />
                                                })}
                                            </Picker>
                                            <FontAwesome
                                                name='sort-down'
                                                size={25}
                                                color='#005989'
                                                style={[{ right: 25, top: 5, position: 'absolute' }]}
                                            />
                                        </View>
                                    </Row>
                                }*/}
                                <Row style={{ paddingLeft: 8, marginTop: 10, height: 50, justifyContent: "flex-start", alignItems: "center", flex: 1, borderWidth: 2, borderRadius: 5, borderColor: "rgba(220,219,216, 0.2)", backgroundColor: "rgba(220,219,216, 0.2)" }}>
                                    <TextInput
                                        style={[{ flex: 1, color: 'gray', paddingLeft: 10, fontSize: 16 }]}
                                        placeholder={localeStr("calendar_item_theme_placeholder", this.props.lang)}
                                        value={this.state.name}
                                        onChangeText={(val) => {
                                            this.setState({ name: val });
                                        }}
                                    />
                                </Row>

                                {/*<Row style={{ paddingLeft: 8, marginTop: 10, height: 50, justifyContent: "flex-start", alignItems: "center", flex: 1, borderWidth: 2, borderRadius: 5, borderColor: "rgba(220,219,216, 0.2)", backgroundColor: "rgba(220,219,216, 0.2)" }}>
                                    <TextInput
                                        style={[{ flex: 1, color: 'gray', paddingLeft: 10, fontSize: 16 }]}
                                        placeholder={localeStr("calendar_item_theme_placeholder", this.props.lang)}
                                        value={this.state.theme}
                                        onChangeText={(val) => {
                                            this.setState({ theme: val });
                                        }}
                                    />
                                </Row>*/}

                                <Row style={{ padding: 8, marginTop: 10, minHeight: 50, justifyContent: "flex-start", alignItems: "center", flex: 1, borderWidth: 2, borderRadius: 5, borderColor: "rgba(220,219,216, 0.2)", backgroundColor: "rgba(220,219,216, 0.2)" }}>
                                    <TextInput
                                        multiline
                                        style={[{ flex: 1, color: 'gray', paddingLeft: 10, fontSize: 16 }]}
                                        placeholder={localeStr("calendar_item_description_placeholder", this.props.lang)}
                                        value={this.state.description}
                                        onChangeText={(val) => {
                                            this.setState({ description: val });
                                        }}
                                    />
                                </Row>

                                <TouchableOpacity
                                    style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}
                                    onPress={() => {
                                        //this.setState({show:!this.state.show});
                                        this.toggleState("guests_modal");
                                    }}
                                >
                                    <Row style={{ marginTop: 15, height: 50, padding:10, alignItems:"center", justifyContent: "center", flex: 1, borderWidth: 2, borderRadius: 5, borderColor: "rgba(220,219,216, 0.2)", backgroundColor: "rgba(220,219,216, 0.2)" }}>
                                        <Text style={[{ flex: 1, color: 'gray', paddingLeft: 10, fontSize: 16 }]}>
                                            {localeStr("calendar_search_label", this.props.lang)}
                                        </Text>
                                        <Ionicons color={'#005989'} style={{ marginHorizontal: 5, paddingBottom:5 }} size={30} name='ios-search' />
                                    </Row>
                                </TouchableOpacity>

                                {/*<UserSearchCard accept_anyone square choose_user={this.choose_user} />*/}
                                {this.state.guests.length != 0 &&
                                <Row style={{paddingLeft:15, paddingTop:8, marginTop: 10, minHeight: 50, /*maxHeight: 200,*/ marginHorizontal: 0, flexDirection: "column", justifyContent: "flex-start", alignItems: "center", flex: 1, borderWidth: 2, borderRadius: 5, borderColor: "rgba(220,219,216, 0.2)", backgroundColor: "rgba(220,219,216, 0.2)" }}>
                                    <Text style={{ fontWeight: "bold", fontSize: 16, color: "gray" }}>Участники:</Text>
                                    <ScrollView nestedScrollEnabled={true} showsHorizontalScrollIndicator={false} style={{alignSelf:"flex-start", marginVertical: 10, /*maxHeight: 120, */backgroundColor: "#ffffff00" }}>
                                        {this.state.guests.map((el, index, arr) => {
                                            //console.log("guest", el);
                                            return (
                                                <TouchableOpacity
                                                    key={JSON.stringify(el)}
                                                    style={{ alignItems: "center", flexDirection:"row", height:40 }}
                                                    onPress={() => {
                                                        let new_guests = [...this.state.guests.slice(0, index), ...this.state.guests.slice(index + 1, this.state.guests.length)];
                                                        this.setState({ guests: new_guests });
                                                    }}
                                                >
                                                    {/*<Text>{el["firstnamerus"]+" "+el["lastnamerus"]}</Text>
                                                    <Ionicons style={{marginLeft:5}} name={"ios-close"} size={14}/>*/}
                                                    {/*<UserDisplayCard {...el} addressee_matrix_id={el.matrix_id} />*/}
                                                    <Ionicons style={{marginHorizontal:10}} name={"md-close"} size={14}/>
                                                    <ShortUserDisplayCard {...el} last={index==arr.length-1}/>
                                                </TouchableOpacity>
                                            );
                                        })}
                                    </ScrollView>
                                </Row>
                                }
                            </View>
                            <TouchableOpacity
                                style={[card.base, { width: '92.5%', padding: 15, alignSelf: "center", marginTop: 5, borderRadius: 15, borderTopStartRadius: 0, borderTopEndRadius: 0 }]}
                                onPress={() => {
                                    let place_props = (this.state.place_on_site && this.state.place != null && this.state.place_time_id != null)
                                        ?
                                        {
                                            //PlaceOnSite: this.state.place_on_site,
                                            PeriodId: this.state.place_time_id
                                        }
                                        :
                                        {
                                            //PlaceOnSite:this.state.place_on_site,
                                            PlaceDescription: this.state.place
                                        };

                                    if (this.is_new) {
                                        backendRequestPromise(
                                            "calendarNewItem",
                                            this.props.bearerToken,
                                            {
                                                date: new Date(this.state.date - (new Date()).getTimezoneOffset() * 60000).toISOString(),
                                                factid: this.state.fact_id,
                                                itemtypeid: parseInt(this.state.item_type_id),
                                                eventid: this.state.event_id,
                                                name: this.state.name,
                                                description: this.state.description,
                                                guestIds: this.state.guests.map((g) => [parseInt(g.userid), 0]),
                                                MeetingTheme: this.state.theme,
                                                ...place_props
                                            }
                                        ).then(() => {
                                            this.props.setCalendarNeedsUpdate(true);
                                            //this.props.navigation.goBack();
                                            this.props.navigation.navigate("CalendarPage");
                                        }).catch((err) => {
                                            Alert.alert("Внимание", `Произошла ошибка при создании встречи:\n${err.statusDescription}`);
                                        });
                                    } else {
                                        backendRequestPromise(
                                            "calendarUpdateItem",
                                            this.props.bearerToken,
                                            {
                                                itemId: this.state.item_id,
                                                date: new Date(this.state.date - (new Date()).getTimezoneOffset() * 60000).toISOString(),
                                                factId: this.state.fact_id,
                                                itemTypeId: parseInt(this.state.item_type_id),
                                                eventId: this.state.event_id,
                                                name: this.state.name,
                                                description: this.state.description,
                                                guestIds: this.state.guests.map((g) => [parseInt(g.userid), 0]),
                                                MeetingTheme: this.state.theme,
                                                ...place_props
                                            }
                                        ).then(() => {
                                            this.props.setCalendarNeedsUpdate(true);
                                            //this.props.navigation.goBack();
                                            this.props.navigation.navigate("CalendarPage");
                                        }).catch((err) => {
                                            Alert.alert("Внимание", "Произошла ошибка при создании встречи");
                                        });
                                    }


                                    /*let cal_socket = new WebSocket(WS_URL);
                                    cal_socket.onopen = () => {
                                        backendRequestCustomSocket(
                                            cal_socket,
                                            "calendarNewItem",
                                            this.props.bearerToken,
                                            {
                                                //user_id:this.state.user_id,
                                                date:this.state.date,
                                                factid:this.state.fact_id,
                                                itemtypeid:parseInt(this.state.item_type_id),
                                                eventid:this.state.event_id,
                                                name:this.state.name,
                                                description:this.state.description,
                                                guestIds:this.state.guests.map((g) => [parseInt(g.userid), 0])
                                            }
                                        );
                                        this.props.setCalendarNeedsUpdate(true);
                                        this.props.navigation.goBack();
                                        cal_socket.close();
                                    };*/
                                }}
                            >
                                <Text style={{ alignSelf: "center", color: "darkred", fontSize: 20 }}>
                                    {localeStr(
                                        this.is_new ? "calendar_create_button_label" : "calendar_edit_button_label",
                                        this.props.lang
                                    )}
                                </Text>
                            </TouchableOpacity>
                            {/*<View style={{height:60}} />*/}
                        </Content>
                    </Container>
                </KeyboardAvoidingView>
            </Drawer>
        );
    }
}

const mapStateToProps = state => {
    return {
        lang: state.data.settings.lang,
        bearerToken: state.data.userToken,
        event_id: state.data.event.event_id,
    }
};

const mapDispatchToProps = dispatch => {
    return {
        //toggle: () => dispatch(togglePersonalInfoOverlay()),
        setCalendarNeedsUpdate: (needs_update) => dispatch(setCalendarNeedsUpdate({ needs_update }))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(CalendarEditItemScreen);
