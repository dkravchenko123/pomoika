import {ActivityIndicator, Alert, FlatList, ScrollView, Text, TextInput, TouchableOpacity, View} from "react-native";
import React from "react";
import {
    addInvite,
    addJoined,
    receiveData,
    removeData,
    setChatToken,
    updMessages
} from "../../actions/data";
import {Card, Container, Content, Icon, Picker, Row} from "native-base";
import {connect} from "react-redux";
import {getchat} from "../../methods/chat_client";
import SubscreenHeader from "../../components/headers_footers/subscreen_header";
import {cancel_reconnect, getws, init_timeout} from "../../methods/webSocket";
import {backendRequest, backendRequestCustomSocket, extractResponse} from "../../methods/ws_requests";
import {isLater, parseCalItems} from "../../methods/calendar_methods";
import {disableNetWarn, enableNetWarn} from "../../actions/control";
import NetModalWarning from "../../components/cards/NetModalWarning";
import card from "../../styles/cards";
import field from "../../styles/fields";
import DrawerContent from "../../components/cards/DraweContent";
import Drawer from "react-native-drawer";
import {styles} from "../../styles/header_footer_styles";
import button from "../../styles/buttons";
import {SimpleLineIcons, Ionicons} from "@expo/vector-icons";
import UserSearchField from "../../components/forms/static_fields/UserSearchField";
import {WS_URL} from "../../constants/backend";
import UserSearchCard from "../../components/cards/UserSearchCard";
import UserDisplayCard from "../../components/cards/UserDisplayCard";
import {AntDesign, EvilIcons} from "@expo/vector-icons";

class PerconalChatCreationScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            user_search_input:'',
            search_limit:20,
            user_results:[],
            user_chosen:{},
            searching:false
        };

        this.client = getchat();

        this.drawer = new React.createRef();
        this.menu_fun = this.menu_fun.bind(this);
        this.close_drawer = this.close_drawer.bind(this);
        this.choose_user = this.choose_user.bind(this);
    }

    menu_fun () {
        this.drawer.open();
    }

    close_drawer () {
        this.drawer.close();
    }

    choose_user (user) {
        console.log("user chosen", user);
        this.setState({
            user_chosen: {...user}
        });
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.state.searching) {
            let response = extractResponse(this.props.received_data, "calendarGetPeople");

            if (response != null) {
                console.log("found "+response.length);
                if (response.length > 0) {
                    this.setState({
                        user_results:response,
                        searching:false,
                    });
                }
            }
        }
    }

    componentDidMount() {
        this.didBlurSubscription = this.props.navigation.addListener(
            'willBlur',
            payload => {
                if (this.close_drawer) this.close_drawer();
                //console.log('willBlur', payload);
            }
        );
    }

    componentWillUnmount() {
        if (this.didBlurSubscription) this.didBlurSubscription.remove();
    }

    renderChooseButton = (user) => {return [
        <TouchableOpacity
            onPress={() => {
                if (user.hasOwnProperty("matrix_id")) {
                    let shortid = user.matrix_id.split("@")[1].split(":")[0];
                    let my_shortid = this.props.user_id.split("@")[1].split(":")[0];
                    if (this.props.dms.length > 0 && this.props.dms.findIndex((el) => el == user.matrix_id) != -1) {
                        Alert.alert("Внимание!", "Чат с выбранным пользователем уже существует!");
                        return;
                    } else {
                        console.log("trying to create "+"DM-"+shortid+"-"+my_shortid+(new Date()).toString());
                        this.client.createRoom({"room_alias_name":"DM-"+shortid+"-"+my_shortid+(new Date()).valueOf()})
                            .then((resp_res) => {
                                console.log("resp_res"+ JSON.stringify(resp_res));
                                if (resp_res.room_id) {
                                    let info_socket = new WebSocket(WS_URL);
                                    info_socket.onopen = () => {
                                        backendRequestCustomSocket(info_socket, "chatCreateDMRoom", this.props.userToken, {matrix_room_id:resp_res.room_id, users:[this.props.user_id, user.matrix_id]});
                                    };
                                    info_socket.onmessage = (msg) => {
                                        console.log(msg.data);
                                        info_socket.close()
                                    };
                                    console.log("inviting " + user.firstnamerus + " " + user.lastnamerus + " to " + resp_res.room_id);
                                    this.client.invite(
                                        resp_res.room_id,
                                        user.matrix_id,
                                        () => {
                                            this.client.setPowerLevel(resp_res.room_id, user.matrix_id, 100/*, null, () => {
                                                this.props.navigation.goBack();
                                            }*/)
                                        }
                                    ).then(() => {
                                        this.props.toggle();
                                    })
                                }
                            })
                            .catch((err) => {
                                Alert.alert("Внимание!", "Возникла ошибка!");
                            });
                    }
                } else {
                    Alert.alert("Внимание!", "Выберите пользователя!");
                }
            }}
            style={{width:100, height:30, borderRadius:15, justifyContent:"center", alignItems:"center", backgroundColor:"rgb(169,25,59)"}}
        >
            <Text style={{color:"white"}}>Написать</Text>
        </TouchableOpacity>
    ]};

    render () {
        return (
            <Container style={{height:window.height}}>
                {/* <SubscreenHeader custom_back={this.props.toggle}/> */}
                <Content style={{height:window.height}}>
                    <View style={{padding:15, height:window.height}}>
                        <View
                            style={{
                                //height: 60,
                                //borderBottomWidth: 2,
                                borderBottomColor: "rgb(220,219,216)",
                                flexDirection: "row",
                                justifyContent: "space-between",
                            }}
                        >
                            <View style={{
                                alignSelf: "center",
                                alignItems: "center",
                                textAlign: "center",
                                marginLeft: 10
                            }}>
                                <Text>Создание чата</Text>
                            </View>

                            <TouchableOpacity
                                style={{alignSelf: "Right"}}
                                onPress={() => {
                                    if (this.props.toggle)
                                    {
                                        this.props.toggle();
                                    }
                                }}
                            >
                                <EvilIcons size={32} name={"close"}/>
                            </TouchableOpacity>
                        </View>

                        {/*<UserSearchField choose_user={this.choose_user} />*/}
                        <UserSearchCard choose_user={this.choose_user} custom_action_buttons={this.renderChooseButton}/>

                        {(this.state.user_chosen.hasOwnProperty("firstnamerus") || this.state.user_chosen.hasOwnProperty("firstnameeng")) &&
                            <View style={{flexDirection:"column", marginTop:8, marginLeft:0}}>
                                <Text style={{alignSelf:"center", fontSize:16}}>{"Пользователь:"}</Text>
                                <TouchableOpacity
                                    onPress={() => {
                                        //let new_users = [ ...this.state.users_chosen.slice(0, el.index), ...this.state.users_chosen.slice(el.index + 1, this.state.users_chosen.length)];
                                        this.setState({user_chosen: {}});
                                        //alert("remove "+el.index);
                                    }}
                                >
                                    <UserDisplayCard addressee_matrix_id={this.state.user_chosen.matrix_id}/>
                                </TouchableOpacity>
                            </View>
                        }

                        {/*<TouchableOpacity
                            style={[button.header, button.active, {alignSelf:"center", marginTop:20}]}
                            onPress={() => {
                                if (this.state.user_chosen.hasOwnProperty("matrix_id")) {
                                    let shortid = this.state.user_chosen.matrix_id.split("@")[1].split(":")[0];
                                    let my_shortid = this.props.user_id.split("@")[1].split(":")[0];
                                    if (this.props.dms.length > 0 && this.props.dms.findIndex((el) => el == this.state.user_chosen.matrix_id) != -1) {
                                        Alert.alert("Внимание!", "Чат с выбранным пользователем уже существует!");
                                        return;
                                    } else {
                                        console.log("trying to create "+"DM-"+shortid+"-"+my_shortid+(new Date()).toString());
                                        this.client.createRoom({"room_alias_name":"DM-"+shortid+"-"+my_shortid+(new Date()).valueOf()})
                                            .then((resp_res) => {
                                                console.log("resp_res"+ JSON.stringify(resp_res));
                                                if (resp_res.room_id) {
                                                    let info_socket = new WebSocket(WS_URL);
                                                    info_socket.onopen = () => {
                                                        backendRequestCustomSocket(info_socket, "chatCreateDMRoom", this.props.userToken, {matrix_room_id:resp_res.room_id, users:[this.props.user_id, this.state.user_chosen.matrix_id]});
                                                    };
                                                    info_socket.onmessage = (msg) => {
                                                        console.log(msg.data);
                                                        info_socket.close()
                                                    };
                                                    console.log("inviting " + this.state.user_chosen.firstnamerus + " " + this.state.user_chosen.lastnamerus + " to " + resp_res.room_id);
                                                    this.client.invite(resp_res.room_id, this.state.user_chosen.matrix_id)
                                                        .then(() => {
                                                            this.props.navigation.goBack();
                                                        });
                                                }
                                            })
                                            .catch((err) => {
                                                Alert.alert("Внимание!", "Возникла ошибка!");
                                            });
                                    }
                                } else {
                                    Alert.alert("Внимание!", "Выберите пользователя!");
                                }
                            }}
                        >
                            <Text style={{color:"white"}}>Создать</Text>
                        </TouchableOpacity>*/}

                    </View>
                </Content>
            </Container>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        received_data:           state.data.received_data,
        lang:                    state.data.settings.lang,
        userToken:               state.data.userToken,
        user_id:                 state.data.chat.user_id,
        dms:                     state.data.chat.dms,
        leave:                   state.data.chat.leave
        //chatToken:               state.data.chat.token,
        //joined_rooms:            state.data.chat.joined_rooms,
        //invites:                 state.data.chat.invites
    }
};

const mapDispatchToProps = dispatch => {
    return {
        receiveData:            (data) => dispatch(receiveData(data)),
        removeData:             (key) => dispatch(removeData({key})),
        setChatToken:           (token) => dispatch(setChatToken({token})),
        updMessages:            (new_msg, room) => dispatch(updMessages({new_msg, room})),
        addInvite:              (room) => dispatch(addInvite({room})),
        addJoined:              (room) => dispatch(addJoined({room})),
        enableNetWarn:          () => dispatch(enableNetWarn()),
        disableNetWarn:          () => dispatch(disableNetWarn()),
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(PerconalChatCreationScreen)
