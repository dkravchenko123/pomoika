import {createStackNavigator} from "react-navigation";
import PhotobankWebViewScreen from "../screens/extended view screens/photobank_webview_screen";
import {HomePage} from "../components/pages/home_page";
import FullArticleScreen from "../screens/extended view screens/full_article_screen";
import WebViewScreen from "../screens/extended view screens/webview_screen";
import FullscreenWithChildren from "../screens/extended view screens/full_screen_with_children";

export const HomeRoute = createStackNavigator(
    {
        HomeTab:                             {screen: HomePage},
        FullArticleScreen:                   {screen: FullArticleScreen},
        PhotobankWebViewScreen:              {screen: PhotobankWebViewScreen},
        HomeWebViewScreen:                   {screen: WebViewScreen},
        HomeFullscreenWithChildren:          {screen: FullscreenWithChildren}
    },
    {
        initialRouteName: "HomeTab",
        headerMode:"none"
    }
);
