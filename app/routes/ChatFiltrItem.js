import React from 'react';
import { SafeAreaView, TouchableOpacity, FlatList, StyleSheet, Text, View } from 'react-native';
import Constants from 'expo-constants';
import { createAppContainer } from 'react-navigation';

const DATA = [
  {
    id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
    title: 'First Item',
    selected: false,
  },
  {
    id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
    title: 'Second Item',
    selected: true,
  },
  {
    id: '58694a0f-3da1-471f-bd96-145571e29d72',
    title: 'Third Item',
    selected: true,
  },
];

function Item({ id, title, selected }) {
  return (
    <TouchableOpacity
      onPress={() => { this.setState({selected : !this.state.selected})}}
      style={[
        styles.item,
        { backgroundColor: selected ? '#6e3b6e' : '#f9c2ff' },
      ]}
    >
      <Text style={styles.title}>{title}</Text>
    </TouchableOpacity>
  );
}

export class ChatFiltrItem extends React.Component {
  render() {
    return (
      <View>
        <Text>ChatFiltrItem... {this.props.TitleEmpty}</Text>
        <SafeAreaView style={styles.container}>
          <FlatList
            data={DATA}
            renderItem={({ item }) =>
              <Item
                id={item.id}
                title={item.title}
                selected={item.selected}
              />}
            keyExtractor={item => item.id}
          />
        </SafeAreaView>
      </View>
    );
  }
}

export const AppContainer_ChatFiltrItem = createAppContainer(ChatFiltrItem);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: Constants.statusBarHeight,
  },
  item: {
    backgroundColor: '#f9c2ff',
    padding: 5,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  title: {
    fontSize: 16,
  },
});
