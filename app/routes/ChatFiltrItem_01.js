import React from 'react';
import { Text, View } from 'react-native';
import { Card, Container, Content, Icon, Row, Header, List, ListItem, CheckBox, Body } from 'native-base';
import { createAppContainer } from 'react-navigation';

export class ChatFiltrItem extends React.Component {
  render() {
    return (
      <View>
        {/* <Row style={{paddingLeft:15, margin:10, flexDirection:"column", justifyContent:"flex-start", alignItems:"left", flex:1, borderRadius:10, backgroundColor:"#f2f3f4"}}>*/}
        <Row style={{paddingLeft:15, margin:10, flexDirection:"column", justifyContent:"flex-start", alignItems:"left", flex:1}}>
            <Text>ChatFiltrItem... {this.props.TitleEmpty}</Text>
          <ListItem>
            <CheckBox checked={true} />
            <Text>Daily Stand Up</Text>
          </ListItem>
          <ListItem>
            <CheckBox checked={false} />
            <Text>Discussion with Client</Text>
          </ListItem>
          <ListItem>
            <CheckBox checked={false} color="green"/>
            <Text>Finish list Screen</Text>
          </ListItem>
          <List>
            <ListItem>
              <Text>Aaron Bennet</Text>
            </ListItem>
            <ListItem selected>
              <Text>Ali Connors</Text>
            </ListItem>
            <ListItem>
              <Text>Bradley Horowitz</Text>
            </ListItem>
          </List>
        </Row>

        <Content>
          <List>
            <ListItem selected>
                <Text>Simon Mignolet</Text>
            </ListItem>
            <ListItem>
                <Text>Nathaniel Clyne</Text>
            </ListItem>
            <ListItem>
                <Text>Dejan Lovren</Text>
            </ListItem>
          </List>
        </Content>
      </View>
    );
  }
}

export const AppContainer_ChatFiltrItem = createAppContainer(ChatFiltrItem);
//    return <h1>Hello,</h1>;
