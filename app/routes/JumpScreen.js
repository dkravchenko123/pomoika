﻿import React from 'react';
import { Text, View } from 'react-native';
import { createAppContainer } from 'react-navigation';
import { Linking } from 'expo';

export class JumpScreen extends React.Component {
  render() {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <Text>JumpScreen</Text>
      </View>
    );
  }

  componentDidMount() {
    console.log('JumpScreen.===========================<<<<<<< componentDidMount');
    console.log('===js.0');
    Linking.getInitialURL().then((url) => {
        if (url)
        {
            console.log('===js.1');
            let ScreenDefault;

            console.log('Initial url is: ' + url);
            let IndexOf = url.lastIndexOf('?');
            console.log('IndexOf 1: ' + IndexOf);
            IndexOf = url.indexOf('?');
            console.log('IndexOf 2: ' + IndexOf);

            console.log('===js.2');
            IndexOf = url.lastIndexOf('?');
            if (IndexOf >= 0)
            {
                console.log('===js.3');
                let paramsUrl = url.slice(IndexOf + 1, url.length)
                console.log('paramsUrl: ' + paramsUrl);
                let paramsUrlArray = paramsUrl.split('&');
                console.log('paramsUrlArray: ' + paramsUrlArray);
                console.log('===js.4');
                console.table(paramsUrlArray);
                console.log('===js.5');
                //IndexOf = paramsUrlArray.indexOf('screen=');
                //console.log('IndexOf screen: ' + IndexOf);
                console.log('===js.6');
                paramsUrlArray.map((item, key) => console.log(item));
                console.log('===js.7');

                let SearchScreen = (paramsUrl + '&').match(/screen=(\w+)&/i);
                let SearchLogin = (paramsUrl + '&').match(/login=(\w+)&/i);
                let SearchPassword = (paramsUrl + '&').match(/password=(\w+)&/i);
                console.table(SearchScreen);
                console.table(SearchLogin);
                console.table(SearchPassword);
                console.log('===js.8');
                console.log(SearchScreen);
                console.log(SearchLogin);
                console.log(SearchPassword);
                console.log('===js.9');
                if (SearchScreen)
                {
                    console.log('===js.10');
                    console.log('SearchScreen[0]: ' + SearchScreen[0]);
                    console.log('SearchScreen[1]: ' + SearchScreen[1]);
                    ScreenDefault = SearchScreen[1];
                }
                console.log('screen 1: ' + ScreenDefault);
                console.log('===js.11');
                
                //let url = new URL(url);
                //let ScreenDefault = url.searchParams.get('screen');
                //ScreenDefault = 'ChatScreen';
                //ScreenDefault = 'CalendarScreen';
            }
            console.log('===js.12');

            if (ScreenDefault === undefined)
            {
                //ScreenDefault = 'ChatScreen';
                //ScreenDefault = 'CalendarScreen';
                ScreenDefault = 'TestScreen1';
                //ScreenDefault = 'TestScreen2';
            }

            console.log('screen 2: ' + ScreenDefault);
            this.props.navigation.navigate(ScreenDefault);
        }
    });

    //console.log(props.match.path);
    console.log('===js.13');
    console.log('JumpScreen.===========================>>>>>>> componentDidMount');
  }

  componentWillUnmount() {
    console.log('JumpScreen.=========================== .componentWillUnmount');
  }

}

export const AppContainer_JumpScreen = createAppContainer(JumpScreen);
