import React from 'react';
import { Text, View } from 'react-native';
import { Container } from "native-base";
import { createAppContainer } from 'react-navigation';
import { createMaterialTopTabNavigator  } from 'react-navigation-tabs';
//import {ChatRoute} from "./ChatRoute";
import {AppContainer_ChatRoute} from "./ChatRoute";
//import {CalendarRoute} from "./CalendarRoute";
import {AppContainer_CalendarRoute} from "./CalendarRoute";

class HomeScreen extends React.Component {
  render() {
    return (
      <AppContainer_ChatRoute />
/*
      <Container>
          <ChatRoute/>
      </Container>
*/
/*
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <Text>Home!</Text>
      </View>
*/
    );
  }
}

class SettingsScreen extends React.Component {
  render() {
    return (
      <AppContainer_CalendarRoute />
/*
      <Container>
          <CalendarRoute/>
      </Container>
*/
/*
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <Text>Settings!</Text>
      </View>
*/
    );
  }
}

export const MainTopNavigator = createMaterialTopTabNavigator ({
  Home: HomeScreen,
  Settings: SettingsScreen,
});
//export default createAppContainer(TabNavigator);


/*
export const AppContainer = createAppContainer(RootStack);
// Now AppContainer is the main component for React to render
//export default AppContainer;
*/
