import {createStackNavigator} from "react-navigation";
import CalendarPage from "../components/pages/calendar_page";
import UpdatePersonalInfoCard from "../components/cards/UpdatePersonalInfoCard";
import {AccountPage} from "../components/pages/account_page";
import FullscreenWithChildren from "../screens/extended view screens/full_screen_with_children";



export const AccountRoute = createStackNavigator(
    {
        AccountPage:                       {screen: AccountPage},
        UpdatePersonalInfoCard:            {screen: UpdatePersonalInfoCard},
        AccountFullscreenWithChildren:     {screen: FullscreenWithChildren}
    },
    {
        initialRouteName: "AccountPage",
        headerMode:"none"
    }
);
