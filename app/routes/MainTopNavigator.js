import React from 'react';
import { Text, View } from 'react-native';
import { Container } from "native-base";
import { createAppContainer } from 'react-navigation';
import { createMaterialTopTabNavigator  } from 'react-navigation-tabs';
//import {ChatRoute} from "./ChatRoute";
import {AppContainer_ChatRoute} from "./ChatRoute";
//import {CalendarRoute} from "./CalendarRoute";
import {AppContainer_CalendarRoute} from "./CalendarRoute";

class ChatScreen extends React.Component {
  render() {
    return (
/*
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <Text>Chat</Text>
      </View>
*/
/*
      <Container>
          <ChatRoute/>
      </Container>
*/
      <AppContainer_ChatRoute />
    );
  }
}

class CalendarScreen extends React.Component {
  render() {
    return (
/*
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <Text>Calendar!</Text>
      </View>
*/
/*
      <Container>
          <CalendarRoute/>
      </Container>
*/
      <AppContainer_CalendarRoute />
    );
  }
}

export const MainTopNavigator = createMaterialTopTabNavigator ({
  Chat: ChatScreen,
  Calendar: CalendarScreen,
});
//export default createAppContainer(TabNavigator);
