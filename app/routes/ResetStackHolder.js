import { NavigationActions, StackActions } from 'react-navigation';

let _navigators = {};

function setNavigator(navigatorRef, routeName) {
    _navigators[routeName] = navigatorRef;
}

function navigateMainNavigator(routeName, params) {
    _navigators[routeName].dispatch(
        NavigationActions.navigate({
            routeName,
            params,
        }),
    );
}

function navigateAndReset(routeName, params) {
    if (_navigators[routeName]) {
        _navigators[routeName].dispatch(
            StackActions.reset({
                index: 0,
                actions: [
                    NavigationActions.navigate({
                        routeName,
                        params,
                    }),
                ],
            })
        );
    } else {
        _navigators["MainFooter"].dispatch(NavigationActions.navigate({routeName}));
    }

}

// add other navigation functions that you need and export them

export default {
    setNavigator,
    navigateMainNavigator,
    navigateAndReset
};
