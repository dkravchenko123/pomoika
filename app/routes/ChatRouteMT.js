import React from 'react';
import { Text, View } from 'react-native';
//import { createAppContainer } from 'react-navigation';
import { createMaterialTopTabNavigator  } from 'react-navigation-tabs';
import {ChatRoute} from "./ChatRoute";
import {CalendarRoute} from "./CalendarRoute";

class HomeScreen extends React.Component {
  render() {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <Text>Home!</Text>
      </View>
    );
  }
}

class SettingsScreen extends React.Component {
  render() {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <Text>Settings!</Text>
      </View>
    );
  }
}

export const TabNavigator = createMaterialTopTabNavigator ({
  Home: HomeScreen,
  Settings: SettingsScreen,
});

//export default createAppContainer(TabNavigator);
