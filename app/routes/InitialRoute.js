import {createStackNavigator, createSwitchNavigator} from "react-navigation";
import LoginScreen from "../screens/login_screen";
import RegisterScreen from "../screens/register_screen";
//import MainScreen from "../routes/";
import {MainFooter} from "./MainFooterNavigator";
import {ChatRoute} from "./ChatRoute";
import {ChatFiltr} from "./ChatFiltr";
//import {ChatRouteX} from "./ChatRouteX";
//import {TabNavigator} from "./ChatRouteB";
//import {TabNavigator} from "./ChatRouteMB";
import {CalendarRoute} from "./CalendarRoute";
//import {MainTopNavigator} from "./MainTopNavigator";
import {JumpScreen} from "./JumpScreen";
//import {TabNavigator} from "./ChatRouteMT";
import {TestScreen1} from "./TestScreen1";
import {TestScreen2} from "./TestScreen2";
import {MenuScreen} from "./cl/menu";

import {AgendaScreen} from "./cl/agenda";
import {CalendarsScreen} from "./cl/calendars";
import {CalendarsList} from "./cl/CalendarsList";
import {ExpandableCalendarScreen} from "./cl/expandableCalendar";
import {HorizontalCalendarList} from "./cl/horizontalCalendarList";

CalendarRoute.navigationOptions = ({ navigation }) => {
    const tabBarVisible = false;
    return { tabBarVisible };
};

const authRoute = createStackNavigator(
    {
        Login:              {screen: LoginScreen},
        Register:           {screen: RegisterScreen},
    },{
        initialRouteName: "Login",
        headerMode:"none"
    }
);

export const InitNavigator = createSwitchNavigator(
    {
        Login:           {screen: authRoute},
        //MainScreen:    {screen: MainScreen},
        //MainScreen:    {screen: ChatRoute},
        //MainScreen:    {screen: ChatRouteX},
        //MainScreen:    {screen: MainTopNavigator},
        //MainScreen:    {screen: CalendarRoute},
        //MainScreen:    {screen: TabNavigator},
        MainScreen:      {screen: JumpScreen},
        ChatScreen:      {screen: ChatRoute},
        CalendarScreen:  {screen: CalendarRoute},
        ChatFiltrScreen: {screen: ChatFiltr},
        TestScreen1:     {screen: TestScreen1},
        TestScreen2:     {screen: TestScreen2},
        TestScreen3:     {screen: AgendaScreen},
        TestScreen4:     {screen: CalendarsScreen},
        TestScreen5:     {screen: CalendarsList},
        TestScreen6:     {screen: ExpandableCalendarScreen},
        TestScreen7:     {screen: HorizontalCalendarList},
    },
    {
        initialRouteName: "CalendarScreen",
        headerMode:"none",
    }
);

const prevGetStateForAction = MainFooter.router.getStateForAction;

MainFooter.router.getStateForAction = (action, state) => {

    //console.log("navigating: "+action.type+" "+JSON.stringify(state));
    if (
        action.type === 'Navigation/BACK'
        && state
    ) {
        let prev_state = prevGetStateForAction(action, state);
        if (prev_state.routes[prev_state.index].routeName === 'Login') {
            return null;
        }
    }

    return prevGetStateForAction(action, state);
};
