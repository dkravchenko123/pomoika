import React, { Component } from 'react';
import { Text, View } from 'react-native';
import { Card, Container, Content, Icon, Row, Header, List, ListItem, CheckBox, Body, Left, Right } from 'native-base';
import { createAppContainer } from 'react-navigation';

export class ChatFiltrItem extends React.Component {
  render() {
    return (
      <Container>
        <Header />
        <Content>
          <List>
            <ListItem selected>
              <Left>
                <Text>Simon Mignolet</Text>
              </Left>
              <Right>
                <Text>...</Text>
              </Right>
            </ListItem>
            <ListItem>
             <Left>
                <Text>Nathaniel Clyne</Text>
              </Left>
              <Right>
                <Text>...</Text>
              </Right>
            </ListItem>
            <ListItem selected>
              <Left>
                <Text>Dejan Lovren</Text>
              </Left>
              <Right>
                <Text>...</Text>
              </Right>
            </ListItem>
          </List>
        </Content>
      </Container>
    );
  }
}

export const AppContainer_ChatFiltrItem = createAppContainer(ChatFiltrItem);
//    return <h1>Hello,</h1>;
