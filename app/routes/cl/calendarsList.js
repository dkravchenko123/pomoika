import React, {Component} from 'react';
import {CalendarList} from 'react-native-calendars';

export class CalendarsList extends Component {
  
  render() {
    return (
      <CalendarList current={'2012-05-16'} pastScrollRange={4} futureScrollRange={4}/>
    );
  }
}
