import {createStackNavigator} from "react-navigation";
import FactScreen from "../screens/extended view screens/fact_screen";
import SpeakerScreen from "../screens/extended view screens/speaker_screen";
import PartnerScreen from "../screens/extended view screens/partner_screen";
import FactsAndSponsors from "../components/pages/facts_sponsors_partners_page";
import EventScreen from "../screens/extended view screens/event_screen";
import WebViewScreen from "../screens/extended view screens/webview_screen";
import FullscreenWithChildren from "../screens/extended view screens/full_screen_with_children";

export const EventRoute = createStackNavigator(
    {
        EventScreen:            {screen: EventScreen},
        FactsAndSponsors:       {screen: FactsAndSponsors},
        FactScreen:             {screen: FactScreen},
        SpeakerScreen:          {screen: SpeakerScreen},
        PartnerScreen:          {screen: PartnerScreen},
        WebViewScreen:          {screen: WebViewScreen},
        FullscreenWithChildren: {screen: FullscreenWithChildren}
    },
    {
        initialRouteName: "EventScreen",
        headerMode:"none"
    }
);
