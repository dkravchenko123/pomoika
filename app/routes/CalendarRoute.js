import { createAppContainer, createStackNavigator } from "react-navigation";
import CalendarItemScreen from "../screens/extended view screens/calendar_item_screen";
import CalendarEditItemScreen from "../screens/extended view screens/calendar_add_item_screen";
import CalendarPage from "../components/pages/calendar_page";

export const CalendarRoute = createStackNavigator(
    {
        CalendarPage:                   {screen: CalendarPage},
        CalendarItemScreen:             {screen: CalendarItemScreen},
        CalendarEditItemScreen:          {screen: CalendarEditItemScreen},
    },
    {
        initialRouteName: "CalendarPage",
        headerMode:"none",
    }
);

export const AppContainer_CalendarRoute = createAppContainer(CalendarRoute);
