import {createStackNavigator} from "react-navigation";
import MessageScreen from "../screens/extended view screens/messages_screen";
import RoomCreationScreen from "../screens/extended view screens/chat_room_create_screen";
import PersonalChatCreationScreen from "../screens/extended view screens/chat_personal_create_screen";
import AddUserScreen from "../screens/extended view screens/chat_add_user_screen";
import ChatDMInfoScreen from "../screens/extended view screens/chat_dm_info_screen";
import ChatGroupInfoScreen from "../screens/extended view screens/chat_group_info_screen";
import ChatPageX from "../components/pages/chat_pageX";



export const ChatRouteX = createStackNavigator(
    {
        ChatPageX:                       {screen: ChatPageX},
        MessageScreen:                  {screen: MessageScreen},
        RoomCreationScreen:             {screen: RoomCreationScreen},
        PersonalChatCreationScreen:     {screen: PersonalChatCreationScreen},
        AddUserScreen:                  {screen: AddUserScreen},
        ChatDMInfoScreen:               {screen: ChatDMInfoScreen},
        ChatGroupInfoScreen:            {screen: ChatGroupInfoScreen}
    },
    {
        initialRouteName: "ChatPageX",
        headerMode:"none"
    }
);
