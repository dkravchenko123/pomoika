﻿import React from 'react';
import { Text, View } from 'react-native';
import { createAppContainer } from 'react-navigation';
//import { createMaterialBottomTabNavigator  } from 'react-navigation-tabs';
import { createMaterialBottomTabNavigator  } from 'react-navigation-material-bottom-tabs';
import { Linking } from 'expo';

class Welcome extends React.Component {
  render() {
    console.log('===w.1');
    const msg = this.props.url;
    console.log(this.props.name);
    console.log(msg);
    console.log('===w.2');
    return <h1>Hello, {this.props.name}</h1>;
  }
}

class HomeScreen extends React.Component {
  render() {
    const msg = 'zas 15';
    console.log('===hs.1');
    console.log(msg);
    console.log('===hs.2');

    Linking.getInitialURL().then((url) => {
        if (url)
        {
            let ScreenDefault;

            console.log('Initial url is: ' + url);
            let IndexOf = url.lastIndexOf('?');
            console.log('IndexOf 1: ' + IndexOf);
            IndexOf = url.indexOf('?');
            console.log('IndexOf 2: ' + IndexOf);

            IndexOf = url.lastIndexOf('?');
            if (IndexOf >= 0)
            {
                
                let paramsUrl = url.slice(IndexOf + 1, url.length)
                console.log('paramsUrl: ' + paramsUrl);
                let paramsUrlArray = paramsUrl.split('&');
                console.log('paramsUrlArray: ' + paramsUrlArray);
                console.log('===hs.4');
                console.table(paramsUrlArray);
                console.log('===hs.5');
                //IndexOf = paramsUrlArray.indexOf('screen=');
                //console.log('IndexOf screen: ' + IndexOf);
                console.log('===hs.6');
                paramsUrlArray.map((item, key) => console.log(item));
                console.log('===hs.7');

                let SearchScreen = (paramsUrl + '&').match(/screen=(\w+)&/i);
                let SearchLogin = (paramsUrl + '&').match(/login=(\w+)&/i);
                let SearchPassword = (paramsUrl + '&').match(/password=(\w+)&/i);
                console.table(SearchScreen);
                console.table(SearchLogin);
                console.table(SearchPassword);
                console.log('===hs.8');
                console.log(SearchScreen);
                console.log(SearchLogin);
                console.log(SearchPassword);
                console.log('===hs.9');
                if (SearchScreen)
                {
                    console.log('===hs.10');
                    console.log('SearchScreen[0]: ' + SearchScreen[0]);
                    console.log('SearchScreen[1]: ' + SearchScreen[1]);
                    ScreenDefault = SearchScreen[1];
                }
                console.log('screen 1: ' + ScreenDefault);
                console.log('===hs.11');
                
                //let url = new URL(url);
                //let ScreenDefault = url.searchParams.get('screen');
                //ScreenDefault = 'ChatScreen';
                //ScreenDefault = 'CalendarScreen';
            }
            console.log('===hs.12');

            if (ScreenDefault === undefined)
            {
                ScreenDefault = 'ChatScreen';
            }

            console.log('screen 2: ' + ScreenDefault);
            this.props.navigation.navigate(ScreenDefault);
        }
    });

    //console.log(props.match.path);
    console.log('===hs.13');
    //console.log(props.match.url);
    //console.log('===hs.14');
    //console.log(props.match);
    //console.log('===hs.14');
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <Text>Home! {msg} home 12 <Welcome name="Alisa" /><Welcome name="Piter" /> 13</Text>
       </View>
    );
  }

  componentDidMount() {
    console.log('HomeScreen.=========================== componentDidMount');
  }

  componentWillUnmount() {
    console.log('HomeScreen.=========================== .componentWillUnmount');
  }

}

class SettingsScreen extends React.Component {
  render() {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <Text>Settings!</Text>
      </View>
    );
  }
}

export
const TabNavigator = createMaterialBottomTabNavigator ({
  Home11: HomeScreen,
  Settings: SettingsScreen,
});

//export default createAppContainer(TabNavigator);
