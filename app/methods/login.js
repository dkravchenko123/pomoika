import storeExport from "../store";

const {store, persistor} = storeExport();
const initializedStore = store;

import {getws} from "./webSocket";
import {WS_URL} from "../constants/backend";
import {
    setChatLogin,
    setChatPassword,
    setChatToken,
    setGuestStatus,
    setLogin,
    setPassword,
    updateUserToken
} from "../actions/data";
import {backendRequestCustomSocket} from "./ws_requests";

export function authoriseUser(socket, email, password, clientID, clientSecret, device_id) {
    let request = {
        "method": "authoriseUser",
    };
    request["data"] = {
        "email":email,
        "password":password,
        "clientID":clientID,
        "clientSecret":clientSecret,
        "appid":device_id
    };
    if (socket.readyState !== 1) {
        if (socket.readyState !== 0) socket = getws();
        socket.onopen = (() => {
            let req = JSON.stringify(request);
            console.log("sending authorisation request: " + req);
            if (socket) {
                socket.send(req);
            } else {
                console.log("couldn't find socket");
            }
        });
    } else {
        let req = JSON.stringify(request);
        console.log("sending authorisation request: " + req);
        if (socket) {
            socket.send(req);
        } else {
            console.log("couldn't find socket");
        }
    }
}

export function refreshToken() {
    return (new Promise((resolve, reject) => {
        let refresh_socket = new WebSocket(WS_URL);

        refresh_socket.onmessage = (msg) => {
            let parsed_msg = JSON.parse(msg.data);
            console.log(parsed_msg);
            if (parsed_msg["statusCode"] == 200) {
                initializedStore.dispatch(updateUserToken({token:parsed_msg.data.bearerToken, refreshToken:parsed_msg.data.refreshToken}));
                refresh_socket.close();
                resolve();
            } else {
                reject(parsed_msg["statusCode"]);
                refresh_socket.close();
            }
        };

        refresh_socket.onerror = (err) => {
            reject(err);
        };

        refresh_socket.onopen = () => {
            backendRequestCustomSocket(refresh_socket, "updateToken", null, {RefreshToken:initializedStore.getState().data.refreshToken});
        };
    }));
}


