import { Notifications } from 'expo';
import * as Permissions from 'expo-permissions';
import {WS_URL} from "../constants/backend";
import {backendRequest, backendRequestCustomSocket} from "./ws_requests";

export async function fcm_auth(userToken) {
    const enabled = await firebase.messaging().hasPermission();
    if (!enabled) {
        try {
            await firebase.messaging().requestPermission();
            // User has authorised
        } catch (error) {
            return;
            // User has rejected permissions
        }
    }

    const fcmToken = await firebase.messaging().getToken();
    if (fcmToken) {
        //alert("fcm token: "+fcmToken);
        console.log("fcm token", fcmToken, "user token", userToken);
        // user has a device token
    } else {
        //alert("no token");
        console.log("no token");
        // user doesn't have a device token yet
    }

    const channel = new firebase.notifications.Android
        .Channel('default', 'Default Channel', firebase.notifications.Android.Importance.Max)
        .setDescription('The default notification channel.');

    firebase.notifications().android.createChannel(channel);

    let push_socket = new WebSocket(WS_URL);
    push_socket.onmessage = () => {
        push_socket.close();
    };
    push_socket.onopen = (() => {
        backendRequestCustomSocket(
            push_socket,
            "addNotificationToken",
            userToken,
            {
                token:             fcmToken,
                AppDisplayName:    "RK",
                DeviceDisplayName: "app",
                AppId:             "0",
                LanguageCode:      "ru"
            }
        )
    });
}

export async function registerForPushNotificationsAsync(userToken) {
    const { status: existingStatus } = await Permissions.getAsync(
        Permissions.NOTIFICATIONS
    );
    let finalStatus = existingStatus;

    // only ask if permissions have not already been determined, because
    // iOS won't necessarily prompt the user a second time.
    if (existingStatus !== 'granted') {
        // Android remote notification permissions are granted during the app
        // install, so this will only ask on iOS
        const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
        finalStatus = status;
    }

    //alert("push permission is " + JSON.stringify(finalStatus));

    // Stop here if the user did not grant permissions
    if (finalStatus !== 'granted') {
        return;
    }

    // Get the token that uniquely identifies this device
    let token = await Notifications.getExpoPushTokenAsync();

    alert("expo push token" + JSON.stringify(token));

    console.log("expo push token", token);
    // POST the token to your backend server from where you can retrieve it to send push notifications.

    let push_socket = new WebSocket(WS_URL);
    push_socket.onmessage = () => {
        push_socket.close();
    };
    push_socket.onopen = (() => {
        backendRequestCustomSocket(
            push_socket,
            "addNotificationToken",
            userToken,
            {
                ExpoToken:         token,
                AppDisplayName:    "RK",
                DeviceDisplayName: "app",
                AppId:             "0",
                LanguageCode:      "ru"
            }
        )
    });
}
