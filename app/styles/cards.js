import {StyleSheet} from "react-native";


const styles = StyleSheet.create({
    base: {
        //backgroundColor:"rgb(220,219,216)",
        backgroundColor:"#fff",
        shadowColor: 'rgb(220,219,216)',
        shadowOffset: { width: 0, height: 0 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 5,
        borderRadius:15,
        marginLeft:15,
        marginRight:15,
        marginTop:10,
        marginBottom:2
        //alignSelf:"center",
    },
    register_form:{
        width:340,
        paddingLeft:10,
        paddingRight:10,
        backgroundColor:"rgb(220,219,216)",
        borderRadius:15,
        marginLeft:15,
        marginRight:15,
        marginTop:10,
        alignSelf:"center",
    }
});

export default styles;
