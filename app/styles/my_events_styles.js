import {StyleSheet} from "react-native";


export const styles = StyleSheet.create({
   active_button:{
       borderWidth: 5,
       borderColor: "#111",
       width:140,
       height:40,
       justifyContent: "center",
       alignItems: "center"
   },
   inactive_button:{
       borderWidth: 2,
       borderColor: "#999",
       width:140,
       height:40,
       justifyContent: "center",
       alignItems: "center"
   }
});