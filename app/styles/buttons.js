import {StyleSheet} from "react-native";


const styles = StyleSheet.create({
    base: {
        width: 276,
        height: 54,
        marginTop: 10,
        backgroundColor:"#fff",
        justifyContent: "center",
        borderRadius:27,
        borderColor:"rgb(169,25,59)",
        borderWidth: 1
    },
    active: {
        backgroundColor:"rgb(169,25,59)",
    },
    disabled: {
        opacity:0.3
    },
    footer: {
        width:44,
        height:44,
        borderRadius:20,
        backgroundColor:"#00000000"
    },
    register_nav: {
        width: 120,
        height: 40,
        marginTop: 20,
        backgroundColor:"rgb(169,25,59)",
        justifyContent: "center",
        borderRadius:20,
        borderColor:"rgb(169,25,59)",
        borderWidth: 1,
        marginBottom:10
    },
    header: {
        elevation:3,
        width:110,
        height:28,
        borderRadius:5,
        paddingHorizontal:8,
        backgroundColor:"#fff",
        borderColor:"rgb(169,25,59)",
        borderWidth: 1,
        justifyContent:"center",
        alignItems:"center"
    }
});

export default styles;
